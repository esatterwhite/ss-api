/*jshint laxcomma:true, smarttabs: true */
/**
 * The main alice pacakge
 * @module alice
 * @author Eric Satterwhite
 **/

/**
 * These are the avalible configuration settings used throughout the system
 * @name settings
 * @namespace {Object} module:alice.settings
 * @borrows module:alice-log/conf.log as log
 * @borrows module:alice-conf/lib/defaults.searchly as searchly
 * @borrows module:alice-conf/lib/defaults.databases as databases
 * @borrows module:alice-conf/lib/defaults.mongoose as mongoose
 * @borrows module:alice-conf/lib/defaults.stripe as stripe
 * @borrows module:alice-conf/lib/overrides.PROJECT_ROOT as PROJECT_ROOT
 * @borrows module:alice-conf/lib/overrides.SERVICE_PATH as SERVICE_PATH
 * @borrows module:alice-conf/lib/overrides.PACKAGE_PATH as PACKAGE_PATH
 * @borrows module:alice-conf/lib/overrides.pkg as pkg
 * @borrows module:alice-core/conf.FIXTURE_PATH as FIXTURE_PATH
 * @borrows module:alice-web/conf.host as host
 * @borrows module:alice-web/conf.PORT as PORT
 * @borrows module:alice-web/conf.server as server
 * @borrows module:alice-web/conf.hapi-swagger as hapi-swagger
 * @borrows module:alice-web/conf.documentation as documentation
 * @borrows module:alice-cache/conf.caches as caches
 * @borrows module:alice-cart/conf.stripe as stripe
 * @borrows module:alice-core/conf.mail as mail
 **/
