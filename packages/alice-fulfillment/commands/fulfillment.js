/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Default command for the alice-fulfillment package
 * @module alice-fulfillment/commands/fulfillment
 * @author Jim Deakins
 * @since 0.0.1
 * @requires seeli
 * @requires util
 */
var _ = require("underscore");

var cli = require('seeli'),
  util = require('util');

//var mongoose = require('mongoose');
var OrderModel = require('alice-cart/models/order');
var ItemModel = require('alice-cart/models/item');

var CartModel = require('alice-cart/models/cart');

var daemon;

var init = function() {
  if (daemon == undefined) {
    //daemon = require("daemons/fulfillment/order.coffee");
    daemon = require("alice-fulfillment/lib/order_processor");
  }
}

var global_queue = {}

var getOrder = function() {

  var query = CartModel.findOne({
    'monitoring.lifeCycle.status': 'paid'
  });

  query.exec(function(err, cart) {
    if (cart) {
      order = processCart(cart);
      if (order) {
        daemon.ready(function() {
          //OrderProcessor.prototype.order
          var retVal = daemon.order(order);

          if(cart.monitoring.lifeCycle.status == 'fulfill_error'){
            return
          }
          if(retVal===true){
            cart.monitoring.lifeCycle.status = 'fulfill_created'
            cart.save()
          }
          if(typeof retVal == 'object'){
            cart.monitoring.lifeCycle.status = 'fulfill_received'
            cart.save()
          }
          if(retVal===false){
            cart.monitoring.lifeCycle.status = 'fulfill_error'
            cart.save()
          }
          return retVal;
        });
      } else {
        console.log('invalid order, marking as failed: ', cart._id)
        cart.monitoring.lifeCycle.status = 'fulfill_error'
        cart.save()
      }
    } else {
       // console.log('No orders in queue')
    }
  });

}

var order = function(order) {
  if (order == undefined) {
    order = getOrder();
  } else {
    daemon.ready(function() {
      return daemon.order(order);
    });
  }
}


/*
- need valid customer id.  Currently using shipping info for purchaserInfo
- product color should be string such as "black"


*/
var processCart = function(cart) {
  var shipMap = {'standard':'14', 'premium':'16', 'nextday':'6'}
  order = {
    '_id': cart._id,
    'paymentGateway': 'Stripe',
    'paymentConfirmationCode': 'OkeyDokey',
    'manufacturers': {
      'name': 'SpreadShirt'
    },
    'affiliate': 'hss'
    /*
		'orderStatus': 'queued',
		'lastOrderUpdate': 1405974294,
		'subTotal': 10000,
		'shippingTotal': 1500,
		'tax': 700,
		'discounts': 0,
		'total': 12200,
		*/
  };
  order.purchaserInfo = {
    firstName: cart.delivery.firstName,
    middleInitial: '',
    lastName: cart.delivery.lastName
  }
  order.shippingInfo = {
    'firstName': cart.delivery.firstName,
    'middleInitial': '',
    'lastName': cart.delivery.lastName,
    'address1': cart.delivery.address1,
    'address2': cart.delivery.address2,
    'city': cart.delivery.city,
    'state': cart.delivery.state,
    'postalCode': cart.delivery.postalCode,
    'shippingType':shipMap[cart.billing.shippingType]
  }
  order.orderItems = []
  for (var idx in cart.items) {
    if (cart.items[idx].product && cart.items[idx].printUrls.front && cart.items[idx].product && cart.items[idx].product.color.manufacturerId) {
      order.orderItems.push({
        manufacturer: cart.items[idx].product.manufacturer,
        product: cart.items[idx].product.name,
        size: cart.items[idx].product.size.manufacturerId,
        color: cart.items[idx].product.color.manufacturerId,
        quantity: cart.items[idx].quantity,
        graphic: cart.items[idx].printUrls.front
      })
    }
  }
  if (order.orderItems.length == 0) {
    return false
  }
  return order;
}

var loop = function(cmd, data, done) {
  init();
  setInterval(order, 1000)
  //var guid = require('alice-fulfillment/lib/helpers')
}

module.exports = new cli.Command({
  description: "Default command for alice-fulfillment package",
  usage: [
    cli.bold('Usage: ') + 'alice fulfillment --help', cli.bold('Usage: ') + 'alice fulfillment --no-color', cli.bold('Usage: ') + 'alice fulfillment --loop', cli.bold('Usage: ') + 'alice fulfillment -i'
  ]

  ,
  flags: {
    'default': {
      type: Boolean,
      description: "Enable the default",
      default: true,
      required: false
    }
  }
  /**
   * This does something
   * @param {String|null} directive a directive passed in from the cli
   * @param {Object} data the options collected from the cli input
   * @param {Function} done the callback function that must be called when this command has finished
   * @returns something
   **/

  ,
  run: function(cmd, data, done) {
    init();
    console.time("full run");
    if (cmd == 'loop') {
      loop();
    } else {
      console.log("RUNNING ONE ORDER AND EXITING")
      order();
      done( /* error */ null, /* output */ 'success')
    }
  }
});
