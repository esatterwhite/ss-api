/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Configuration options for alice-fulfillment
 * @module module:alice-fulfillment/conf
 * @author Jim Deakins
 * @since 0.1.0
 */

exports._FOO = 1

exports._THINGS = {
	BAR:{
		BAZ:2
	}
}
