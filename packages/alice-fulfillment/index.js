/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * fulfillment daemon for orders
 * @module alice-fulfillment
 * @author
 * @since 0.1.0
 * @requires alice-fulfillment/events
 * @requires alice-fulfillment/commands
 * @requires alice-fulfillment/models
 * @requires alice-fulfillment/lib
 */


module.exports = require('./events');



// models
module.exports.models = require('./models')
