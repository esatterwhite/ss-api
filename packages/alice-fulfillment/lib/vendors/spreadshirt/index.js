var BaseInterface, Interface, Transaction, http, iface, xml2js, _,
  __bind = function(fn, me) {
    return function() {
      return fn.apply(me, arguments);
    };
  },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) {
    for (var key in parent) {
      if (__hasProp.call(parent, key)) child[key] = parent[key];
    }

    function ctor() {
      this.constructor = child;
    }
    ctor.prototype = parent.prototype;
    child.prototype = new ctor();
    child.__super__ = parent.prototype;
    return child;
  },
  __indexOf = [].indexOf || function(item) {
    for (var i = 0, l = this.length; i < l; i++) {
      if (i in this && this[i] === item) return i;
    }
    return -1;
  };

xml2js = require('xml2js');

http = require('http');

_ = require("underscore");

BaseInterface = require("alice-fulfillment/lib/vendors/interface");

Transaction = require("./transaction");

Interface = (function(_super) {
  __extends(Interface, _super);

  function Interface() {
    this.setAuthHeader = __bind(this.setAuthHeader, this);
    this.withSession = __bind(this.withSession, this);
    this.checkItem = __bind(this.checkItem, this);
    this._poll = __bind(this._poll, this);
    this.__get = __bind(this.__get, this);
    this.submit = __bind(this.submit, this);
    this.order = __bind(this.order, this);
    this.setProductType = __bind(this.setProductType, this);
    this.resolveProduct = __bind(this.resolveProduct, this);
    this.tryReady = __bind(this.tryReady, this);
    this.setProduct = __bind(this.setProduct, this);
    this.fetchProduct = __bind(this.fetchProduct, this);
    this.fetchProductType = __bind(this.fetchProductType, this);
    this.setShop = __bind(this.setShop, this);
    this.getCart = __bind(this.getCart, this);
    this.ready = __bind(this.ready, this);
    this.refresh = __bind(this.refresh, this);
    this.initialize = __bind(this.initialize, this);
    return Interface.__super__.constructor.apply(this, arguments);
  }

  Interface.prototype.shopUrl = 'http://api.spreadshirt.com/api/v1/shops/509490';

  Interface.prototype.productTypes = {};

  Interface.prototype.products = {};

  Interface.prototype.polling = {};

  Interface.prototype.vendor = "spreadshirt";

  Interface.prototype.isReady = false;

  Interface.prototype.callbacks = [];

  Interface.prototype.cachetime = 0;

  Interface.prototype.cache = {
    appearance: {},
    printArea: {}
  };

  Interface.prototype.cachettl = 60000;

  Interface.prototype.orderProgress = 0;

  Interface.prototype.productMap = {
    "Women's Tank Top": "1002549239",
    "Men's T-Shirt by American Apparel": "110185539",
    "Women's Hooded Sweatshirt": "110185539",
    "Men's Hooded Sweatshirt": "1004172374"
  };

  Interface.prototype.sizes = {
    "0": "XS",
    "1": "S",
    "2": "M",
    "3": "L",
    "4": "XL"
  }

  Interface.prototype.stock = {};

  Interface.prototype.apikey = "e1bdb735-a9fa-472c-a0d2-2503b7ebcafc";

  Interface.prototype.apisecret = "20dc711d-0535-4081-b109-eb3a1f4c987f";

  Interface.prototype.apiusername = "tsullivan";

  Interface.prototype.apipassword = "sp1r1tsh0p";

  Interface.prototype.initialize = function() {
    this.broker.emitter.on("ADD_PRODUCT_TYPE", this.fetchProductType);
    this.broker.emitter.on("ADD_PRODUCT", this.fetchProduct);
  };

  Interface.prototype.refresh = function() {
    this.isReady = false;
    return this.get(this.shopUrl, this.setShop);
  };

  Interface.prototype.ready = function(callback) {
    var c, dt, _i, _len, _ref;
    if (callback) {
      this.callbacks.push(callback);
    }
    dt = new Date().getTime();
    if (this.isReady) {
      if (dt - this.cachettl > this.cachetime) {
        this.refresh();
        return;
      }
      _ref = this.callbacks;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        c = _ref[_i];
        c();
      }
      return this.callbacks = [];
    }
  };

  Interface.prototype.getCart = function() {
    return;
    this.post("http://api.spreadshirt.net/api/v1/baskets", (function(_this) {
      return function(response) {
        var raw;
        raw = '';
        response.on('data', function(c) {
          return raw += c;
        });
        return response.on('end', function() {
          return parser.parseString(raw, function(err, xml) {
            return _this.basket = xml;
          });
        });
      };
    })(this));
  };

  Interface.prototype.setShop = function(shop) {
    var productTypeUrl;
    this.isReady = false;
    this.shop = shop.shop;
    productTypeUrl = this.shop.productTypes[0].$['xlink:href'];
    this.get(productTypeUrl+'?limit=200', (function(_this) {
      return function(xml) {
        var obj, _i, _len, _ref, _results;
        _ref = xml.productTypes.productType;
        _results = [];

        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          obj = _ref[_i];
          _this.productTypes[obj.$['id']] = 0;
          _results.push(_this.broker.emitter.emit("ADD_PRODUCT_TYPE", obj));
        }
        return _results;
      };
    })(this));
    return this.get("http://api.spreadshirt.com/api/v1/shops/509490/products", (function(_this) {
      return function(data) {
        var obj, _i, _len, _ref, _results;
        // TODO: THIS IS A HUGE HACK, AND WE SHOULD GET RID OF IT ASAP!!!
        _ref = ["1004403944", "1004404105", "1004404023", "1004404004", "1004404241", "1004404244", "1004404141", "1004404359", "1004404407", "1004404146", "1004404260", "1004404264", "1004404413", "1004404461", "1004404510", "1004404558", "1004404513", "1004404224"]
        //_ref = data.products.product;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          obj = _ref[_i];
          //_this.products[obj.$['id']] = 0;
          _this.products[obj] = 0;
          _results.push(_this.broker.emitter.emit("ADD_PRODUCT", obj));
        }
        return _results;
      };
    })(this));
  };

  Interface.prototype.fetchProductType = function(shortProduct) {
    return this.get(shortProduct.$['xlink:href'], this.setProductType);
  };

  Interface.prototype.fetchProduct = function(shortProduct) {
    //HACK HACK GHACK HAIJCJSAKCHJNKICDS
    var url = "http://api.spreadshirt.com/api/v1/shops/509490/products/" + shortProduct
    //return this.get(shortProduct.$['xlink:href'], this.setProduct);
    return this.get(url, this.setProduct);
  };

  Interface.prototype.setProduct = function(product) {
    this.products[product.product.$['id']] = product;
    if (__indexOf.call(_.values(this.products), 0) < 0) {
      this.cachetime = new Date().getTime();
      return this.tryReady();
    }
  };

  Interface.prototype.tryReady = function() {
    if (__indexOf.call(_.values(this.productTypes), 0) < 0 && !_.isEmpty(this.productTypes) && __indexOf.call(_.values(this.products), 0) < 0 && !_.isEmpty(this.products)) {
      this.isReady = true;
      return this.broker.emitter.emit("READY", this.vendor);
    }
  };

  Interface.prototype.resolveProduct = function(pid) {
    return this.products[this.productMap[pid]];
  };

  Interface.prototype.setProductType = function(product) {
    var a, pa, printAreas, s, stock, _i, _j, _k, _l, _len, _len1, _len2, _len3, _ref, _ref1, _ref2, _ref3;
    this.productTypes[product.productType.$['id']] = product;
    _ref = product.productType.appearances[0].appearance;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      a = _ref[_i];
      this.cache.appearance[a.$.id] = a.name[0];
    }
    printAreas = [];
    _ref2 = product.productType.printAreas[0].printArea;
    if (_ref2){
      for (_k = 0, _len2 = _ref2.length; _k < _len2; _k++) {
        pa = _ref2[_k];
        this.cache.printArea[pa.$.id] = pa;
        printAreas.push(pa.$.id);
      }
    }
    this.stock[product.productType.$['id']] = [];
    _ref3 = product.productType.stockStates[0].stockState;
    for (_l = 0, _len3 = _ref3.length; _l < _len3; _l++) {
      stock = _ref3[_l];
      this.stock[product.productType.$['id']].push({
        color: this.cache.appearance[stock.appearance[0].$.id],
        color_id: stock.appearance[0].$.id,
        size: stock.size[0].$.id,
        available: stock.available[0] === "true",
        quantity: stock.quantity[0],
        price: product.productType.price[0].vatIncluded[0],
        printAreas: printAreas,
        _meta: {
          productType: product.productType.$['id']
        }
      });
    }
    if (__indexOf.call(_.values(this.productTypes), 0) < 0) {
      this.cachetime = new Date().getTime();
      return this.tryReady();
    }
  };

  Interface.prototype.order = function(so) {
    var order;
    order = new Transaction(so, this);
    return order;
  };

  Interface.prototype.submit = function(authHeader, xml, order) {
    var post_options, post_req, zlib;
    zlib = require('zlib');
    post_options = {
      host: "api.spreadshirt.com",
      port: "80",
      path: "/api/v1/orders",
      method: "POST",
      headers: {
        "Content-Length": xml.length,
        "Content-Type": "application/xml",
        "Authorization": authHeader
      }
    };
    post_req = http.request(post_options, (function(_this) {
      return function(res) {
        var gunzip, retVal;
        gunzip = zlib.createGunzip();
        retVal = "";
        gunzip.on('data', function(data) {
          return retVal += data.toString();
        });
        gunzip.on('end', function() {
          var parser;
          if (res.statusCode === 201) {
            parser = new xml2js.Parser();
            return parser.parseString(retVal, function(err, xml) {
              if (xml == null) {
                xml = {};
              }
              console.log("COMPLETE.  ORDER NUMBER: " + xml.reference.$.id);
              _this.polling[xml.reference.$.id] = order._id;
              _this._poll(xml.reference.$.id);
            });
          } else {
            return console.log(res.statusCode);
          }
        });
        return res.pipe(gunzip);
      };
    })(this));
    //console.log(xml)
    post_req.write(xml);
    return post_req.end();
  };

  Interface.prototype.__get = function(authHeader, oid) {
    var post_options, post_req, zlib;
    zlib = require('zlib');
    post_options = {
      host: "api.spreadshirt.com",
      port: "80",
      path: "/api/v1/orders/" + oid,
      method: "GET",
      headers: {
        "Authorization": authHeader
      }
    };
    return post_req = http.get(post_options, (function(_this) {
      return function(res) {
        var retVal;
        retVal = "";
        res.on('data', function(data) {
          return retVal += data.toString();
        });
        return res.on('end', function() {
          var parser;
          parser = new xml2js.Parser();
          return parser.parseString(retVal, function(err, xml) {
            if (xml == null) {
              xml = {};
            }

            var out = {};
            out['_id'] = _this.polling[oid];
            out['tid'] = oid;
            out['state'] = xml.order.orderItems[0].orderItem[0].fulfillmentState[0];
            out['message'] = ''

            if (xml.order.errors) {
              out['state'] = 'ERROR';
              out['message'] = xml.order.errors[0].error[0].messages[0].message[0];
            }

            _this.broker.emitter.emit("ORDER/" + _this.polling[oid] + "/TRANSACTION_POLL", out);

            if (out['state'] != "ERROR") {
              setTimeout(function() {
                _this._poll(oid)
              }, 1000);
            }
          });
        });
      };
    })(this));
  };

  Interface.prototype._poll = function(oid) {
    return this.withSession("http://api.spreadshirt.com/api/v1/orders/" + oid, "GET", (function(_this) {
      return function(authHeader) {
        _this.__get(authHeader, oid);
      };
    })(this));
  };

  Interface.prototype.checkItem = function(item) {
    var type, _i, _len, _ref;
    var pid = item.manufacturer['internalId'];
    //var pid = this.resolveProduct(item.product)

    if(this.products[pid] == undefined){
      console.error("[CRITICAL] product id ("+pid+") not in hardcoded array!");
    }
    if (pid) {

      if (this.stock[this.products[pid].product.productType[0].$.id]) {
        _ref = this.stock[this.products[pid].product.productType[0].$.id];
        //_ref = this.stock[item.manufacturer['internalId']];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          type = _ref[_i];
          //console.log(type.size === item.size, type.color_id === item.color, type.available)
          if (type.size === item.size && type.color_id === item.color && type.available) {
            return type;
          }else{
            //console.log(type.size, item.size)
            //console.log(type.color_id, item.color)
          }
        }
      } else {
        console.log('invalid stock with id:', pid)
      }
    }
    return false;
  };

  Interface.prototype.withSession = function(url, method, callback, options) {
    var post_options, post_req, xml, zlib;
    xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n<login xmlns=\"http://api.spreadshirt.net\" xmlns:ns2=\"http://www.w3.org/1999/xlink\">\n    <username>" + this.apiusername + "</username>\n    <password>" + this.apipassword + "</password>\n</login>";
    zlib = require('zlib');
    post_options = {
      host: "api.spreadshirt.com",
      port: "80",
      path: "/api/v1/sessions",
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Content-Length": xml.length
      }
    };
    post_req = http.request(post_options, (function(_this) {
      return function(res) {
        var gunzip, retVal;
        gunzip = zlib.createGunzip();
        retVal = "";
        gunzip.on('data', function(data) {
          return retVal += data.toString();
        });
        gunzip.on('end', function() {
          var parser;
          if (res.statusCode === 201) {
            parser = new xml2js.Parser();
            return parser.parseString(retVal, function(err, xml) {
              _this.apisession = xml.session.$.id;
              return callback(_this.setAuthHeader(method, url), options);
            });
          } else {
            return console.log(res.statusCode);
          }
        });
        return res.pipe(gunzip);
      };
    })(this));
    post_req.write(xml);
    return post_req.end();
  };

  Interface.prototype.setAuthHeader = function(method, url) {
    var authHeader, crypto, data, shasum, sig, time;
    time = Date.now();
    data = "" + method + " " + url + " " + time;
    crypto = require('crypto');
    shasum = crypto.createHash('sha1');
    shasum.update("" + data + " " + this.apisecret);
    sig = shasum.digest('hex');
    authHeader = "SprdAuth apiKey=\"" + this.apikey + "\", data=\"" + data + "\", sig=\"" + sig + "\", sessionId=\"" + this.apisession + "\"";
    return authHeader;
  };

  return Interface;

})(BaseInterface);

module.exports = iface = new Interface();
