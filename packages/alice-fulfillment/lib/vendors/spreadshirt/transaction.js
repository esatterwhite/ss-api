var BaseTransaction, Broker, Transaction, xml2js, _,
  __bind = function(fn, me) {
    return function() {
      return fn.apply(me, arguments);
    };
  },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) {
    for (var key in parent) {
      if (__hasProp.call(parent, key)) child[key] = parent[key];
    }

    function ctor() {
      this.constructor = child;
    }
    ctor.prototype = parent.prototype;
    child.prototype = new ctor();
    child.__super__ = parent.prototype;
    return child;
  },
  __indexOf = [].indexOf || function(item) {
    for (var i = 0, l = this.length; i < l; i++) {
      if (i in this && this[i] === item) return i;
    }
    return -1;
  };

BaseTransaction = require("../transaction");

_ = require("underscore");

Broker = require("alice-fulfillment/lib//broker");

xml2js = require('xml2js');

module.exports = Transaction = (function(_super) {
  __extends(Transaction, _super);

  function Transaction() {
    this.getProducts = __bind(this.getProducts, this);
    this._buildOrderShipping = __bind(this._buildOrderShipping, this);
    this._buildOrderItems = __bind(this._buildOrderItems, this);
    this._buildTransactionXML = __bind(this._buildTransactionXML, this);
    this._submitOrder = __bind(this._submitOrder, this);
    this.submit = __bind(this.submit, this);
    this.checkFill = __bind(this.checkFill, this);
    this.canFill = __bind(this.canFill, this);
    this._build = __bind(this._build, this);
    this.prepare = __bind(this.prepare, this);
    this.initialize = __bind(this.initialize, this);
    return Transaction.__super__.constructor.apply(this, arguments);
  }

  Transaction.prototype.initialize = function(order) {};

  Transaction.prototype.readyState = function() {
    return this.iface.isReady;
  };

  Transaction.prototype.prepare = function() {
    return this.iface.ready(this._build);
  };

  Transaction.prototype._build = function() {
    this.canFill();
    this.isReady = true;
    Broker.emitter.emit("ORDER/" + this.order._id + "/TRANSACTION_READY", this.order._id);
  };

  Transaction.prototype.canFill = function() {
    var item, r, type, _i, _len, _ref, _results;
    this.fillable = [];
    this.unfillable = [];
    if (Transaction.__super__.canFill.call(this)) {
      _ref = this.order.orderItems;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        item = _ref[_i];
        type = this.iface.checkItem(item);
        if (type.available && type.quantity >= item.quantity + this.iface.minProductStock) {
          r = _.clone(item);
          r.price = type.price;
          _results.push(this.fillable.push(r));
        }else{
          this.unfillable.push(r)
          //console.log(type)
        }
      }
      return _results;
    }
  };

  Transaction.prototype.checkFill = function(items) {
    var item, key;
    if (items == null) {
      items = {};
    }
    for (key in items) {
      item = items[key];
      if (__indexOf.call(_.keys(this.iface.productMap), key) < 0) {
        delete items[key];
      }
    }
    return items;
  };

  Transaction.prototype.submit = function() {
    this.xml = this._buildTransactionXML();
    //console.log(this.xml);
    return this.iface.withSession("http://api.spreadshirt.com/api/v1/orders", "POST", this._submitOrder, this.order);
  };

  Transaction.prototype._submitOrder = function(authHeader, order) {
    return this.iface.submit(authHeader, this.xml, order);

  };

  Transaction.prototype._buildTransactionXML = function() {
    var builder, transaction;
    transaction = {
      order: {
        orderItems: [{orderItem:[]}],
        payment: [{
          type: ['EXTERNAL_FULFILLMENT']
        }],
        shipping: [],
        $: {
          "xmlns:xlink": "http://www.w3.org/1999/xlink",
          "xmlns": "http://api.spreadshirt.net"
        }
      }
    };

    transaction.order.orderItems = this._buildOrderItems(transaction);
    transaction.order.shipping = this._buildOrderShipping(transaction);
    //console.log(JSON.stringify(transaction))
    builder = new xml2js.Builder();
    return builder.buildObject(transaction);
  };

  Transaction.prototype._buildOrderItems = function(transaction) {
    var attachments, item, orderItem, prod, product, _i, _len, _ref, _results;
    _ref = this.fillable;
    _results = [];
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      item = _ref[_i];
      //TODO: make a new resolve function that wraps all this in a callback to fetch the product if we dont have it in cache
      product = this.iface.products[item.manufacturer.internalId];
      var dims = product.product.configurations[0].configuration[0].content[0].svg[0].$.viewBox.split(" ")
      dims[2]-= dims[0]
      dims[3]-= dims[1]
      //product = this.iface.resolveProduct(item.product);
      //console.log(require('util').inspect(cart.items[0].printUrls, true, 20)); // 10 levels deep
      prod = {
        productType: [{
          '$': {
            id: product.product.productType[0].$.id
          }
        }],
        appearance: [{
          '$': {
            id: product.product.appearance[0].$.id
          }
        }],
        restrictions: [{
          example: ['false'],
          freeColorSelection: ['false']
        }],
        configurations: [{
          configuration: [{
            offset: [{
              y: [dims[0]],
              '$': {
                unit: 'mm'
              },
              x: [dims[1]]
            }],
            '$': {
              type: 'design'
            },
            printType: product.product.configurations[0].configuration[0].printType,
            printArea: product.product.configurations[0].configuration[0].printArea,
            content: [{
              svg: [{
                image: [{
                  '$': {
                    transform: '',
                    height: dims[2],
                    width: dims[3],
                    printColorIds: '',
                    designId: 'logo-reference'
                  }
                }]
              }],
              '$': {
                dpi: '25.4',
                unit: 'mm'
              }
            }],
            restrictions: [{
              changeable: ['false']
            }]
          }]
        }]
      };
      attachments = {
        attachment: []
      };
      attachments.attachment.push({
        '$': {
          type: 'sprd:design',
          'id': 'logo-reference'
        },
        'reference': [{
          '$': {
            'xlink:href': item.graphic
          }
        }]
      });
      attachments.attachment.push({
        '$': {
          type: 'sprd:product',
          'id': 'product-reference'
        },
        'product': prod
      });
      orderItem = {
          quantity: [String(item.quantity)],
          element: [{
            '$': {
              type: 'sprd:product',
              'xlink:href': 'http://api.spreadshirt.com/api/v1/shops/509490/products/product-reference'
            },
            properties: [{
              property: []
            }]
          }],
          attachments: attachments
      };
      orderItem.element[0].properties[0].property.push({
        _: item.color,
        $: {
          key: 'appearance'
        }
      });
      orderItem.element[0].properties[0].property.push({
        _: item.size,
        $: {
          key: 'size'
        }
      });
      _results.push(transaction.order.orderItems[0].orderItem.push(orderItem));
    }
    return transaction.order.orderItems;
  };

  Transaction.prototype._buildOrderShipping = function(transaction) {
    var shipping;
    //TODO: spreadshirt/transaction.coffee, ~90: Replace country hardcode when we have country in the order schema
    shipping = {
      shippingType: [{
        '$': {
          'id': this.order.shippingInfo.shippingType
        }
      }],
      address: [{
        '$': {
          'type': 'private'
        },
        company: [],
        person: [{
          salutation: {
            $: {
              id: 1
            }
          },
          firstName: [this.order.shippingInfo.firstName],
          lastName: [this.order.shippingInfo.lastName]
        }],
        street: [this.order.shippingInfo.address1],
        houseNumber: "",
        city: [this.order.shippingInfo.city],
        country: [{
          _: 'United States',
          '$': {
            code: 'US'
          }
        }],
        zipCode: [this.order.shippingInfo.postalCode],
        email: ["dummyMail@mailprovider.com"]
      }]
    };
    if (transaction.order.shipping == undefined) {
      transaction.order.shipping = [];
    }
    transaction.order.shipping.push(shipping);
    return transaction.order.shipping
  };

  Transaction.prototype.getProducts = function() {
    return this.iface.products;
  };

  return Transaction;

})(BaseTransaction);
