var Broker, Interface, http, xml2js,
  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

Broker = require("../broker");

http = require('http');

xml2js = require('xml2js');

Interface = (function() {
  Interface.prototype.broker = Broker;

  Interface.prototype.minProductStock = 10;

  function Interface() {
    this.get = __bind(this.get, this);
    this._checkReady = __bind(this._checkReady, this);
    this.broker.emitter.on("READY", this._checkReady);
    this.initialize();
    this.refresh();
  }

  Interface.prototype.initialize = function() {};

  Interface.prototype.refresh = function() {};

  Interface.prototype._checkReady = function(libname) {
    if (libname === this.vendor) {
      return this.ready();
    }
  };

  Interface.prototype.checkItem = function(item) {};

  Interface.prototype.get = function(url, callback) {
    var parser;
    parser = new xml2js.Parser();
    return http.get(url, (function(_this) {
      return function(response) {
        var raw;
        raw = '';
        response.on('data', function(c) {
          return raw += c;
        });
        return response.on('end', function() {
          return parser.parseString(raw, function(err, xml) {
            return callback(xml);
          });
        });
      };
    })(this));
  };

  Interface.prototype.post = function(data) {
    var post_data, post_options, post_req;
    post_data = querystring.stringify(data);
    post_options = {
      host: "domain",
      port: "80",
      path: "/compile",
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Content-Length": post_data.length
      }
    };
    return post_req = http.request(post_options, function(res) {
      res.setEncoding("utf8");
      return res.on("data", function(chunk) {
        return console.log("Response: " + chunk);
      });
    });
  };

  return Interface;

})();

module.exports = Interface;
