var Transaction, _,
  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

_ = require("underscore");

Transaction = (function() {
  Transaction.prototype.isReady = false;

  function Transaction(order, iface) {
    this.getCanonicalItems = __bind(this.getCanonicalItems, this);
    this.canFill = __bind(this.canFill, this);
    this.checkFill = __bind(this.checkFill, this);
    this.iface = iface;
    if (order._id == null) {
      console.log("error");
    }
    this.order = order;
    this.initialize();
  }

  Transaction.prototype.place = function() {};

  Transaction.prototype.checkFill = function() {};

  Transaction.prototype.canFill = function() {
    var canFill, items;
    items = _.clone(this.order.orderItems);
    canFill = _.omit(items, _.keys(this.checkFill(items)));
    if (_.isEmpty(canFill)) {
      return true;
    } else {
      return false;
    }
  };

  Transaction.prototype.getCanonicalItems = function() {
    return this.order.items;
  };

  Transaction.prototype.submit = function() {
    return false;
  };

  return Transaction;

})();

module.exports = Transaction;
