var OrderProcessor,
  __bind = function(fn, me) {
    return function() {
      return fn.apply(me, arguments);
    };
  },
  __indexOf = [].indexOf || function(item) {
    for (var i = 0, l = this.length; i < l; i++) {
      if (i in this && this[i] === item) return i;
    }
    return -1;
  };

var Broker = require("./broker");
var http = require('http');
var _ = require("underscore");


OrderProcessor = (function() {
  OrderProcessor.prototype.isReady = false;

  OrderProcessor.prototype.orders = {};

  OrderProcessor.prototype.callbacks = [];

  OrderProcessor.prototype._timeouts = {};

  OrderProcessor.prototype.orderPrepTimeout = 10000;

  OrderProcessor.prototype.vendors = {
    spreadshirt: require("alice-fulfillment/lib/vendors/spreadshirt")
  };

  function OrderProcessor(order) {
    this.orderReady = __bind(this.orderReady, this);
    this.transactionReady = __bind(this.transactionReady, this);
    this.order = __bind(this.order, this);
    this.ready = __bind(this.ready, this);
    this._checkReady = __bind(this._checkReady, this);
    Broker.emitter.on("READY", this._checkReady);
    this.loading = _.keys(this.vendors);
  }

  OrderProcessor.prototype._checkReady = function(libname) {
    this.loading = _.without(this.loading, libname);
    if (this.loading.length === 0) {
      this.isReady = true;
      return this.ready();
    } else {
      return this.isReady = false;
    }
  };

  OrderProcessor.prototype.ready = function(callback) {
    var c, _i, _len, _ref;
    if (callback) {
      this.callbacks.push(callback);
    }
    if (this.isReady) {
      _ref = this.callbacks;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        c = _ref[_i];
        c();
      }
      return this.callbacks = [];
    }
  };

  OrderProcessor.prototype.order = function(so) {
    var transaction, vendor;
    vendor = "spreadshirt";
    if (this.orders[so._id]) {
      return this.orders[so._id]
    }
    if (__indexOf.call(_.keys(this.vendors), vendor) >= 0) {
      transaction = this.vendors[vendor].order(so);
      if (this.orders[so._id] == null) {
        this.orders[so._id] = [];
      }
      this.orders[so._id][vendor] = transaction;
      Broker.emitter.on("ORDER/" + so._id + "/TRANSACTION_READY", this.transactionReady);
      Broker.emitter.on("ORDER/" + so._id + "/TRANSACTION_POLL", this.transactionUpdate);
      this._timeouts[so._id] = setTimeout(((function(_this) {
        return function() {
          return _this.orderReady(so._id);
        };
      })(this)), this.orderPrepTimeout);
      transaction.prepare();
      return true

      /*
      - prepare order
      - all order callback when ready
      - timeout of x to place order
      orders all deliver cost per item
      in the end, we order by vendor by stock by cost
       */
    }
  };

  OrderProcessor.prototype.transactionReady = function(oid) {
    var transaction, vendor, _ref;
    _ref = this.orders[oid];
    for (vendor in _ref) {
      transaction = _ref[vendor];
      if (!transaction.isReady) {
        return;
      }
    }
    return this.orderReady(oid);
  };


  var CartModel = require('alice-cart/models/cart');

  OrderProcessor.prototype.transactionUpdate = function(order) {
    var _this = this
    if (_this.polling == undefined) {
      _this.polling = {}
    }

    if (this.polling[order._id] == undefined) {
      var query = CartModel.findOne({
        '_id': order._id
      });
      query.exec(function(err, cart) {
        if (cart) {
          cart.monitoring.externalOrderId = order.tid
          cart.save()
          _this.polling[order._id] = cart;
        }
      });

    } else {
      if(order.state == "RECEIVED"){
        order.state = "fulfill_received"
      }
      if(order.state == "CREATED"){
        order.state = "fulfill_created"
      }
      if(order.state == "ERROR" || order.message != ""){
        order.state = "fulfill_error"
      }
      if (_this.polling[order._id].monitoring.lifeCycle.status != order.state) {
        if (order.state == 'fulfill_error') {
          _this.polling[order._id].monitoring.lifeCycle.message = order.message
        }
        _this.polling[order._id].monitoring.lifeCycle.status = order.state
        _this.polling[order._id].save()
      }
    }

  };

  OrderProcessor.prototype.orderReady = function(oid) {
    _this = this
    Broker.emitter.removeListener("ORDER/" + oid + "/TRANSACTION_READY", this.transactionReady);
    clearTimeout(this._timeouts[oid]);
    if (!_.isEmpty(this.orders[oid].spreadshirt.fillable)) {

      if(this.orders[oid].spreadshirt.fillable.length == this.orders[oid].spreadshirt.order.orderItems.length){
        this.orders[oid].spreadshirt.submit();
      }else{
        var cantFill = ""
        for (var idx in this.orders[oid].spreadshirt.unfillable){
          if (cantFill != ""){
            cantFill += ", "
          }
          cantFill += this.orders[oid].spreadshirt.unfillable[idx].product
        }
        var query = CartModel.findOne({
          '_id': this.orders[oid].spreadshirt.order._id
        });
        query.exec(function(err, cart) {
          if (cart) {
            cart.monitoring.lifeCycle.message = "COULD NOT FILL "+_this.orders[oid].spreadshirt.unfillable.length+" ITEMS IN ORDER: ["+cantFill+"]"
            cart.monitoring.lifeCycle.status = 'fulfill_error'
            cart.save()
            console.log(cart.monitoring.lifeCycle.message)
          }
        });
      }
    }

    /*
    compare fillable to order placed
    check cheapest
    place order
     */
    return console.timeEnd("full run");
  };

  return OrderProcessor;

})();

module.exports = new OrderProcessor()