var Broker, events;

events = require('events');

Broker = (function() {
  function Broker() {}

  Broker.prototype.emitter = new events.EventEmitter();

  return Broker;

})();

module.exports = new Broker();
