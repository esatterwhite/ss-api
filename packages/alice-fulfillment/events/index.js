/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Primary events module for alice-fulfillment
 * @module alice-fulfillment/events
 * @author Jim Deakins
 * @since 0.1.0
 * @requires events
 */

var events = require( 'events' )
  ;

module.exports = new events.EventEmitter();
module.exports.setMaxListeners( 50 );
