# Packages
The package directory is reserved for private and unpublished packages that are key to the platform. Each package should have a small, narrow focus. Much like the UNIX mindset, it should do one thing and do that one thing very well. A package should be a valid [NPM Package](https://www.npmjs.org/doc/#package-json-5-) and follow a few simple conventions

### Package Structure

A package should follow the following conventions

#### Directories

These directories are reserved for a special purpose within the application and are expected follow these guidelines. If only a single file is expected, the directory should contain a single module named **index**, or the directory can be omitted in exchange for a module of the same name. Any of the folders can be omitted if they are not needed

Directory | Purpose 
---------:|--------
**conf**  | Application / package specific configuration defaults
**lib**   | contains modules for package specific functionality
**test**  | location for package specific tests and test data
**commands** | houses management commands for the command line interface

#### Modules ( Files )

Modules | Purpose 
-------:|--------
**index.js**  | The entry point for the package. This will be loaded when someone executes `require('your-package')`
**README.md**  | Package specific documentation and examples of usage
**package.json**  | The npm package definition. You should define a **name**, **version**, and **dependencies** at the least
**resources.js** | API Resources that define endpoints for a given content type with in the system
**signals.js** | predefined event emitters used to provide hooks into application behavior
**.gitkeep**  | will force git to keep the folder when changing branches
**.gitignore**  | Will force git to ignore specific files or patterns of files
**.npmignore**  | similar to gitignore, but tells npm which files and directories to ignore when publish
