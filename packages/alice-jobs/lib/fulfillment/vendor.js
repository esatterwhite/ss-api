/*jshint node:true, laxcomma: true, smarttabs: true*/
// daemons/fulfillment/vendors/interface.coffee
'use strict';
/**
 * <DESCRIPTION>
 * @module vendor.js
 * @author Eric Satterwhite
 * @author Jim Deakins
 * @since 0.1.0
 * @requires events
 * @requires xml2js
 * @requires http
 * @requires module:alice-stdlib/class
 * @requires module:alice-stdlib/class/options
 * @requires module:alice-stdlib/class/parent
 * @requires module:alice-stdlib/function
 * @require module:alice-jobs/lib/fulfillment/events
 */

 var EventEmitter = require('events').EventEmitter           // Node Event Emitter
	, http         = require( 'http' )                       // Node http module
	, qs           = require( 'querystring' )                // Node querystring moduile
	, xml2js       = require( 'xml2js' )                     // npm xml2js module to convert xml data to json
	, logger       = require( 'alice-log' )                  // The standard logging interface
	, Class        = require( 'alice-stdlib/class' )         // Base Class implementation
	, Options      = require( 'alice-stdlib/class/options' ) // Options mixin
	, Parent       = require( 'alice-stdlib/class/parent' )  // Parent Class mixin
	, NOOP         = require( 'alice-stdlib/function' ).noop // empty function stub
	, broker       = require( '../../events' )               // Singleton event emitter
	, Vendor                                                 // The Vendor Base Class
	, checkReady                                             // private function
	;


/**
 * check if the vendor instance is `ready`
 * @private
 * @method checkReady
 */
checkReady = function (libname){
	if ( libname === this.vendor ){
		this.ready();
	}
};

/**
 * Description
 * @class module:alice-jobs/lib/fulfilment/vendor.Vendor
 * @extends events.EventEmitter
 * @mixes module:alice-stdlib/parent
 * @mixes module:alice-stdlib/options
 */
Vendor = new Class(/* @lends module:alice-jobs/lib/fulfilment/vendor.Vendor.prototype */{
	inherits:EventEmitter
	, mixin:[ Parent, Options ]
	, options:{
		minStock: 10
	}
	, constructor: function( options ){
		broker.once( 'ready', checkReady.bind( this ) );
		this.setOptions( options );

		// thats what a constructor is for
		// this.initialize()

		this.refresh();
		this.emit('ready')
	}

	/**
	 * Description
	 * @method module:alice-jobs/lib/fulfilment/vendor
	 * @param {TYPE} <NAME> <Description>
	 * @param {TYPE} <NAME> <Description>
	 * @return
	 */
	, checkItem: function checkItem( ){
		return;
	}

	/**
	 * Description
	 * @method module:alice-jobs/lib/fulfilment/vendor
	 * @param {TYPE} <NAME> <Description>
	 * @param {TYPE} <NAME> <Description>
	 * @return
	 */
	, refresh: function refresh( ){
		this.isRead = false;
		return this;
	}

	/**
	 * Description
	 * @method module:alice-jobs/lib/fulfilment/vendor
	 * @param {TYPE} <NAME> <Description>
	 * @param {TYPE} <NAME> <Description>
	 * @return
	 */
	, get: function get( url, callback ){
		var parser = new xml2js.Parser();
		callback = callback || NOOP;

		http.get(url, function(response){
			var raw = '';

			response.on('data', function onData( chunk ){
				raw += chunk;
			});

			response.once('end', function onEnd(){
				parser.parseString(raw, function(err, xml){
					callback(xml);
				});
			});
		});
	}

	/**
	 * This method looks to be unused
	 * @method module:alice-jobs/lib/fulfilment/vendor#post
	 * @return
	 */
	, post: function post( data ){
		 
		 var post_options ={
			host: 'domain'
			,port: '80'
			,path: '/compile'
			,method: 'POST'
			,headers:{
			  'Content-Type': 'application/x-www-form-urlencoded'
			  ,'Content-Length': qs.stringify( data || {} ).length
			}
		 	
		 };
		 
		  http.request( post_options, function(res){
			res.setEncoding('utf8')
			res.on('data', function(chunk) {
			  logger.info( 'Response: %s', chunk )
			});
		 });
	}
	
});

module.exports = Vendor;
