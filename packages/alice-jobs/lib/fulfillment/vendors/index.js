/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * This module exports an instance of the spread shirt vendor class.
 * which is should not do.
 * @module module:alice-jobs/lib/fulfillment/vendors
 * @author Eric satterwhite
 * @author Jim Deakins
 * @since 0.0.1
 * @requires module:alice-log
 * @requires module:alice-jobs/lib/fulfillment/vendors/spreadshirt
 */
var spreadshirt = require("./spreadshirt")
  , logger      = require("alice-log")

exports.spreadshirt = spreadshirt; 
