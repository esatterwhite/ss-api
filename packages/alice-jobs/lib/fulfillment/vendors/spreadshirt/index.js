// daemons/fulfilment/vendors/spreadshirt/index.coffee
/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * Spreadshirt vendor implementation 
 * @module module:alice-jobs/fulfillment/vendors/spreadshirt
 * @author Eric Satterwhite
 * @author Jim Deakins
 * @since 0.1.0
 * @requires util
 * @requires zlib
 * @requires crypto
 * @requires xml2js
 * @requires module:alice-log
 * @requires module:alice-conf
 * @requires module:alice-stdlib/object
 * @requires module:alice-stdlib/lang
 * @requires module:alice-stdlib/string
 * @requires module:alice-stdlib/class
 * @requires module:alice-jobs/lib/fulfillment/spreadshirt/transaction
 * @requires module:alice-jobs/events
 * @requires module:alice-jobs/lib/vendor
 * @requires module:alice-jobs/lib/fulfillment/spreadshirt/transaction
 */

 var util        = require( 'util' )                            // Node util module
   , zlib        = require( 'zlib' )                            // node zlib moudle to decompress xml data from Spreadshirt
   , crypto      = require( 'crypto' )                          // Node crypto module used for Spreadsirt API auth 
   , http        = require( 'http' )                            // node http module
   , xml2js      = require( 'xml2js' )                          // node xml2js to deal with spreadshirt's API
   , logger      = require( 'alice-log' )                       // Alice loging module
   , conf        = require( 'alice-conf' )                      // Alice config loader
   , Class       = require( 'alice-stdlib/class' )              // Alice Standard Class interface
   , object      = require( 'alice-stdlib/object' )             // Alice standard object module
   , isEmpty     = require( 'alice-stdlib/lang' ).isEmpty       // isEmpty method for checking strings arrays and object
   , interpolate = require( 'alice-stdlib/string' ).interpolate // String interpolate function 
   , Transaction = require( './transaction' )                   // Spreadshirt specific Transaction Class
   , emitter     = require( '../../../../events' )              // The Base Jobs Event emitter
   , Vendor      = require( '../../vendor' )                    // Base Vendor Class
   , SpreadShirt
   ;

// Hard coded data for testing
var productMap  =  {
	'Women\'s Tank Top':'1002549239'
	,'Men\'s T-Shirt':'110185539'
	,'Women\'s Hooded Sweatshirt':'110185539'
};


/**
 * Spreadshirt Specific Vendor implementation to fulfill incoming SpiritShip orders
 * @class module:alice-jobs/fulfillment/vendors/spreadshirt.SpreadShirt
 * @extends module:alice-jobs/fulfillment/vendor.Vendor
 * @mixes module:alice-stdlib/class/options
 * @mixes module:alice-stdlib/class/parent
 * @param {Object} options Instance configuration options
 * @param {String} options.secret Account secret for Spreadshirt API
 * @param {String} options.apikey Account API key for Spreadshirt API
 * @param {String} options.username Spreadshirt Account username
 * @param {String} options.password Spreadshirt Account password
 * @param {String} [options.vendor=spreadshirt] The vendor specific name
 * @param {Number} [options.ttl=60000] The amount of time in ms cached data should persist
 * @example var vendors = require("alice-jobs/lib/vendors")
var spreadshirt = new vendors.SpreadShirt({
	
});
 */
SpreadShirt = new Class(/* @lends module:alice-jobs/fulfillment/vendors/spreadshirt.Spreadshirt.prototype */{
	inherits: Vendor
	, productMap : productMap // fake hard coded data
	/**
	 * Instance cache of known products
	 * @readonly
	 * @property products
	 * @type Object
	 **/
	,products:{}

	/**
	 * instance cache of known product types
	 * @readonly
	 * @property productTypes
	 * @type Object
	 **/
	,productTypes:{}

	/**
	 * inventory information about specific products
	 * @readonly
	 * @property stock
	 * @type Object 
	 **/
	,stock:{}

	/**
	 * Instance cache infomration about the related spreadshirt shop instance 
	 * @readonly
	 * @property shop
	 * @type Object
	 **/
	,shop: null
	// AUTH options need to be pushed down into conf defaults
	// CACHING junk should use the real cache backend if posible
	,cache:{
		size:{}
		,appearance:{}
	    ,printArea:{}
	}
	, options:{

		/**
		 * Sets the internal vendor type
		 * @cfg vendor
		 * @type String
		 * @default spreadshirt
		 **/
		 vendor:'spreadshirt'
		/**
		  * Not used ??
		  * @cfg orderProgress
		  * @type Number
		  * @default 0
		  **/ 
		,orderProgress:0
		/**
		 * Time out in millisconds for cachedinformation
		 * @cfg ttl
		 * @type Number
		 * @default 60000
		 **/
		,ttl:60000
		/**
		 * Vendor specific API secret
		 * @cfg secret
		 * @type String
		 **/
		,secret:'20dc711d-0535-4081-b109-eb3a1f4c987f' // should default to config
		/**
		 * Vendor Specific account username
		 * @cfg username
		 * @type String
		 **/
		,username:'tsullivan' // should default to config
		/**
		 * Vendor specific account password
		 * @cfg password
		 * @type String
		 **/
		,password:'sp1r1tsh0p' // should default to config
		/**
		 * Vendor Specific apikey
		 * @cfg apikey
		 * @type String
		 **/
		,apikey:'e1bdb735-a9fa-472c-a0d2-2503b7ebcafc' // should default to config
		/**
		 * Url to Valid SpreadShirt Shop
		 * @cfg shopUrl
		 * @type String
		 * @default http://api.spreadshirt.com/api/v1/shops/509490
		 **/
		,shopUrl: 'http://api.spreadshirt.com/api/v1/shops/509490' 
		/**
		 * the last time the cache was updated
		 * @cfg cachetime
		 * @type Number
		 * @default 0
		 **/
		,cachetime:0
	}

	, constructor: function( options ){
		this.parent('constructor', options );
		emitter.on('add_product_type', this.fetchProductType.bind( this ) );
		emitter.on('add_product', this.fetchProduct.bind( this ) );
	}

	/**
	 * Re fetches the spreadshirt shop information and resets the ready state
	 * @method module:alice-jobs/fulfillment/vendors/spreadshirt.Spreadshirt#refresh
	 */
	, refresh: function refresh( ){
		logger.warning('flufillment:spreadshirt refreshing vendor state')
		this.isRead = false;
		this.get(this.options.shopUrl, this.setShop.bind( this ) );
	}

	/**
	 * Shortcut to .once('ready') and auto refresh
	 * @method module:alice-jobs/fulfillment/vendors/spreadshirt.Spreadshirt#ready
	 * @param {Function} callback Call back handler for the ready event
	 */
	, ready: function ready( cb ){
		var now = ( + new Date() );

		if( cb ){
			this.once('ready', cb );
		}

		if( this.isReady ){
			if( now - this.options.ttl > this.options.cachetime ){
				this.refresh();
				return;
			}

			this.emit('ready');
		}
	}

	/**
	 * Not implemented. Should emit a not implemented error
	 * @method module:alice-jobs/fulfillment/vendors/spreadshirt.Spreadshirt#getCart
	 */
	, getCart: function getCart( ){

	} 

	/**
	 * set the internal shop, as well as caches product information from that shop
	 * @private
	 * @method module:alice-jobs/fulfillment/vendors/spreadshirt.Spreadshirt#etShop
	 * @param {Shop} shop A spreadshirt Shop entity object
	 */
	,setShop: function setShop( shop ){
		this.isReady = false;
		this.shop = shop.shop;

		var url = this.shop.productTypes[0].$['xlink:href'];

		this.get( url, function( data ){
			data.productTypes.productType.forEach(function( obj ){
				this.productTypes[ obj.$.id] = 0;
				emitter.emit('add_product_type', obj );
			}.bind( this ));
		}.bind(this));

		this.get( 'http://api.spreadshirt.com/api/v1/shops/509490/products', function( data ){
			data.products.product.forEach(function( obj ){
				this.products[ obj.$.id ] = 0;
				emitter.emit('add_product', obj );
			}.bind( this ));
		}.bind(this));
	}

	/**
	 * For a particular product, finds related types
	 * @private
	 * @method module:alice-jobs/fulfillment/vendors/spreadshirt.Spreadshirt#etchProductType
	 * @param {Procut} product A spreadshirt product entity object ( ? )
	 */
	,fetchProductType: function fetchProductType( shortProduct ){
		return this.get( shortProduct.$['xlink:href'], this.setProductType.bind( this ));
	}

	/**
	 * For a sparse product, will download and cache full product information
	 * @private
	 * @method module:alice-jobs/fulfillment/vendors/spreadshirt.Spreadshirt#etchProduct
	 * @param {Product} product A sparse spreadshirt product entity object
	 */
	,fetchProduct: function fetchProduct( shortProduct ){
		return this.get( shortProduct.$['xlink:href'], this.setProduct.bind( this ));
	}

	/**
	 * Stores the product in memeory and tries to validate the ready state
	 * @private
	 * @method module:alice-jobs/fulfillment/vendors/spreadshirt.Spreadshirt#etProduct
	 * @param {Product} product A full spreadshirt product entity object
	 */
	,setProduct: function setProduct( product ){
		

		this.products[product.product.$.id] = product;
		if (object.values( this.products ).indexOf( 0 ) == -1 ) {
			this.options.cachetime = ( + new Date() );
			return this.tryReady();
		}

	}



	/**
	 * Tries to determine of this vendor instance is ready - has populated products & related types
	 * @method module:alice-jobs/fulfillment/vendors/spreadshirt.Spreadshirt#tryReady
	 * @fires module:alice-jobs/fulfillment/vendors/spreadshirt.Spreadshirt#ready
	 * @param {String} vendor, the name of the vendor instance that is ready
	 */
	, tryReady: function tryReady( ){
		if ( 
				!isEmpty(this.products) && 
				!isEmpty(this.productTypes) && 
				object.values(this.productTypes).indexOf( 0 ) == -1  &&  
				object.values(this.products).indexOf( 0 ) == -1 
			){
			  this.isReady = true;
				logger.info("fulfillment:spreadshirt - vendor instance is ready")
			/**
			 * @name module:alice-jobs/fulfillment/vendors/spreadshirt.Spreadshirt#ready
			 * @event
			 * @param {Event} e
			 * @param {Boolean} [e.withIce=false]
			 */
			emitter.emit( 'ready', this.options.vendor );
		}

	}

	/**
	 * Looks up a product by ID
	 * @method module:alice-jobs/fulfillment/vendors/spreadshirt.Spreadshirt#resolveProduct
	 * @param {String} id ID of the product to find
	 * @return {Product} A full product object
	 */
  , resolveProduct: function resolveProduct( pid ){
		return this.products[ productMap[ pid ] ];
	}


	/**
	 * Given a product object, attempts to populate inventory information about that product
	 * @method module:alice-jobs/fulfillment/vendors/spreadshirt.Spreadshirt#setProductType
	 * @param {Product} product Full spreadshirt product entity object
	 */
  , setProductType:function setProductType(product){
  		var appearance //  product type appearance
  		  , sizes      //  product type sizes
  		  , printAreas //  product type printAreas
  		  , stocks     //  product type stocks
  		  ;

  		logger.info('loading product %s ( %s )', product.productType.name[0], product.productType.$.id )

		appearance = product.productType.appearances[0].appearance;
		sizes      = product.productType.sizes[0].size;
		printAreas = product.productType.printAreas[0].printArea;
		stocks     = product.productType.stockStates[0].stockState;

		this.cache.size[product.productType.$.id] = {};
		this.stock[product.productType.$.id] = [];

		this.productTypes[product.productType.$.id] = product;

		// FIXME: condence into a single loop;
		// should be a single loop whose limit is the largest value between appaearance, sizes, and printAreas
		// and perform 3 operations per iteration

		appearance.forEach(function( a ){
		  this.cache.appearance[ a.$.id] = a.name[0];
		}.bind( this ));

		sizes.forEach( function( size ){
		  this.cache.size[product.productType.$.id][size.$.id] = size.name[0];
		}.bind( this ));

		printAreas.forEach(function(area){
		  this.cache.printArea[area.$.id] = area;
		  printAreas.push( area.$.id );
		}.bind( this ));


		stocks.forEach( function( stock ){
		  this.stock[product.productType.$.id].push({
			color: this.cache.appearance[stock.appearance[0].$.id]
			,size: this.cache.size[product.productType.$.id][stock.size[0].$.id]
			,available: stock.available[0] == 'true'
			,quantity: stock.quantity[0]
			,price: product.productType.price[0].vatIncluded[0]
			,printAreas: printAreas
			,_meta:{
			  productType: product.productType.$.id
			}
		  });
		}.bind( this ));

		if( object.values(this.productTypes).indexOf(0) == -1 ){
		  this.options.cachetime = ( + new Date() );
		  this.tryReady();
		}
	
  }



	/**
	 * Generates a new Transaction for Spreadshirt API
	 * @method module:alice-jobs/fulfillment/vendors/spreadshirt.Spreadshirt#order
	 * @param {TYPE} <NAME> <Description>
	 * @param {TYPE} <NAME> <Description>
	 * @return
	 */
  , order:function order (so){
		return new Transaction(so, this);
  }

	/**
	 * Submits a valid order to Spreadshirt
	 * @method module:alice-jobs/fulfillment/vendors/spreadshirt.Spreadshirt#submit
	 * @param {Sting} header A valid authentication header for Spreadshirt's API
	 * @param {String} order A valid Order object( xml ) to place with Spreadshirt
	 */
  , submit:function submit(authHeader, xml){
		var that = this;
		var post_options ={
		  host: 'api.spreadshirt.com'
		  ,port: '80'
		  ,path: '/api/v1/orders'
		  ,method: 'POST'
		  ,headers:{
			'Content-Length': xml.length
			,'Content-Type': 'application/xml'
			,'Authorization': authHeader
		  }
		};
				
		var post_req = http.request( post_options, function(res){
		  var gunzip = zlib.createGunzip();
		  var retVal = '';
		  
		  gunzip.on( 'data', function (data){
			retVal += data.toString();
		  });

		  gunzip.on('end', function(){
		  	debugger;
			if (res.statusCode === 201){
			 var  parser = new xml2js.Parser();
			  parser.parseString( retVal, function(err, xml){
				xml = xml || {};
				logger.info('COMPLETE.  ORDER NUMBER: %s', xml.reference.$.id );
				that._poll(xml.reference.$.id);
			  });
			} else {
				logger.error(
					"fullfilment:spreadshirt - problem submitting order: %s. Status: %s"
					, retVal
					, res.statusCode
				);

				// needs a real exception type
				var e =  new Error()
				e.message = retVal
				return this.emit('error', e)
			}
		  });
		  res.pipe(gunzip);
		});
		post_req.write(xml);
		post_req.end();
  }


	/**
	 * Checks the status of a particula order by ID
	 * @private
	 * @method module:alice-jobs/fulfillment/vendors/spreadshirt.Spreadshirt#_get
	 * @param {String} header a valid auth header for SpreadShirt's API
	 * @param {String|Number} id The ID of the order to lookup
	 * @return
	 */
  ,__get:function __get(authHeader, oid){
		var post_options ={
		  host: 'api.spreadshirt.com'
		  ,port: '80'
		  ,path: '/api/v1/orders/' + oid
		  ,method: 'GET'
		  ,headers:{
			'Authorization': authHeader
		  }
		};

		http.get(post_options, function(res){

		  var retVal = '';

		  res.on('data', function(data){
			retVal += data.toString();
		  });

		  res.on( 'end', function(){
			var parser = new xml2js.Parser();
			parser.parseString(retVal, function(err, xml){
			  var state = xml.order.orderItems[0].orderItem[0].fulfillmentState[0] ;
			  logger.debug('fulfillment:spreadshirt - Order number %s is currently: %s', oid, state);
			  if (state == 'ERROR'){
			  	logger.error('fulfillment:spreadshirt - there was a problem submmitting order %s', oid)
				logger.error(retVal);
			  }
			});
		  });
		});
	
  }
	/**
	 * Polls Spreadshirt order API every second to check it's status
	 * @private
	 * @method module:alice-jobs/fulfillment/vendors/spreadshirt.Spreadshirt#_poll
	 * @param {String|Number} id Order id to poll
	 */
  , _poll: function _poll(oid){
  		logger.warning("polling %s", oid)
		this.withSession('http://api.spreadshirt.com/api/v1/orders/' + oid, 'GET', function(authHeader){
			setInterval(this.__get.bind( this, authHeader, oid), 1000);
		}.bind( this ));
  }

	/**
	 * check to see if an item is currently in stock 
	 * @method module:alice-jobs/fulfillment/vendors/spreadshirt.Spreadshirt#checkItem
	 * @param {Object} item A item of a order 
	 * @return {Object|Boolean}
	 */
  , checkItem: function checkItem(item){
		logger.info('----> this can be a productType id');
		var stocks = this.stock[this.products[productMap[item.product]].product.productType[0].$.id];
		for (var x = 0, _len = stocks.length; x < _len; x++) {
		  var type = stocks[x];
		  if (type.size === item.size && type.color === item.color && type.available) {
			return type;
		  }
		}
		return false;
  }

	/**
	 * Authenticates and places a Spreadshirt API request to a specified url 
	 * @method module:alice-jobs/fulfillment/vendors/spreadshirt.Spreadshirt#withSession
	 * @param {String} uri The full url to issue a request to
	 * @param {String} method HTTP method to use when placing the request
	 */
  , withSession: function withSession(url, method, callback){
		var that = this;
		var xml = [
			'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'
			,'<login xmlns="http://api.spreadshirt.net" xmlns:ns2="http://www.w3.org/1999/xlink">'
				,util.format( '<username>%s</username>', this.options.username )
				,util.format('<password>%s</password>', this.options.password )
			,'</login>'
			].join('\n');

		var post_options = {
		  host: 'api.spreadshirt.com'
		  ,port: '80'
		  ,path: '/api/v1/sessions'
		  ,method: 'POST'
		  ,headers:{
			'Content-Type': 'application/x-www-form-urlencoded'
		   ,'Content-Length': xml.length
		  }
		};

		var post_req = http.request( post_options, function(res){
			var gunzip = zlib.createGunzip();
			var retVal = '';
			gunzip.on('data', function(data){
				retVal += data.toString();
			});

			gunzip.on('end', function(){

				if ( res.statusCode == 201 ){
				  var parser = new xml2js.Parser();
				  parser.parseString(retVal, function(err, xml){
					that.apisession = xml.session.$.id;
					return callback(that.setAuthHeader(method, url));
				  });
				} else {
					return logger.error("API authentication error: %s", res.statusCode, res );
				}
			});
			res.pipe( gunzip );
		});

		post_req.write( xml );
		post_req.end( );
  }

  /**
   * Generates an authentication header for Spreadshirt API requests
   * @private
   * @method module:alice-jobs/fulfillment/vendors/spreadshirt.Spreadshirt#setAuthHeader
   * @param {String} method HTTP Method for the request
   * @param {String} url The url being requested
   * @return
   */
  , setAuthHeader: function setAuthHeader(method, url){
  		logger.info("generating new authentcation header")
  		var time   = Date.now()
  		  , data   = [method, url, time].join(' ')
  		  , shasum = crypto.createHash('sha1')
  		  , sig
  		  ;

  		shasum.update(util.format('%s %s', data, this.options.secret ));
  		sig = shasum.digest('hex');

  		var authHeader = interpolate(
  		'SprdAuth apiKey="{{apikey}}", data="{{data}}", sig="{{sig}}", sessionId="{{apisession}}"'
  		,{
  			apikey:this.options.apikey
  			,apisession: this.apisession
  			, data:data
  			,sig:sig
  		});
		logger.warning("auth header %s", authHeader)
		return authHeader;
  }
});

module.exports = SpreadShirt;
