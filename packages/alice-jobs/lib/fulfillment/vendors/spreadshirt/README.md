Spreadshirt Vendor Integration
==============================

The spread shirt api has a fiew quiks that are worth mentioning

## General

* The API doesn't respect the `Content-Type` or `Accepts` headers. Instead you must pass the query param `mediaType`. For example, to deal with json, you must append `?mediaType=json` on to ever url.

## Authentication

* The JSON data format for the `/session` endpoint to log in differs from that of the XML data format, in that the JSON structure omits the Login block

###### XML
```xml
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<login xmlns="http://api.spreadshirt.net" xmlns:ns2="http://www.w3.org/1999/xlink">
	<username>USERNAME</username>
	<password>PASSWORD</password>
</login>
```

###### JSON
```js
{
	"username":"USERNAME",
	"password":"PASSWORD"
}
```
