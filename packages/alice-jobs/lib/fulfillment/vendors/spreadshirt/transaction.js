/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * profides the base implementation for an order transaction
 * @module module:alice-jobs/lib/fulfillment/vendor/spreadshirt/transaction
 * @author Eric Satterwhite
 * @author Jim Deakins
 * @since 0.1.0
 * @requires module:alice-log
 * @requires module:alice-stdlib/class
 * @requires module:alice-jobs/lib/fulfillment/spreadshirt/transaction
 */

 var util        = require( 'util' ) // node util module for string formatting
   , xml2js      = require('xml2js') // npm xml2js for converting xml <> js
   , logger      = require( 'alice-log' ) // standard logger module
   , Class       = require( 'alice-stdlib/class') // Base Class implementation
   , emitter     = require( '../../../../events' ) // emitter
   , Transaction = require( '../../transaction' ) // Transaction
   , clone       = require( 'mout/lang/clone') // clone
   , invert      = require('alice-stdlib/object').invert // invert
   , SpreadShirtTransaction
   ;

/**
 * Spreadshirt specific implemention of a fulfillment transaction
 * @class module:alice-jobs/lib/fulfillment/vendor/spreadshirt/transaction.Transaction
 * @extends module:alice-jobs/lib/fulfillment/vendors/transaction.Transaction
 */
SpreadShirtTransaction = new Class(/* @lends module:alice-jobs/lib/fulfillment/vendor/spreadshirt/transaction.Transaction.prototype */{
   inherits: Transaction

	/**
	 * Builds the full order xml and dispatches a ready event for the order
	 * @module:alice-jobs/lib/fulfillment/vendor/spreadshirt/transaction.Transaction#prepare
	 **/
  , prepare: function prepare(  ){
  		logger.info('spiritshop:transaction - preparing new transaction')
		return this.iface.ready( this._build.bind( this ) );
	}

	/**
	 * Determines items that can be filled and dispatches a ready event for the order
	 * @private
	 * @module:alice-jobs/lib/fulfillment/vendor/spreadshirt/transaction.Transaction#_build
	 */
  , _build: function _build( ){
		this.canFill();
		this.isReady = true;
		emitter.emit( util.format( 'order/%s/transaction_ready', this.order._id), this.order._id );
	}

	/**
	 * Description determines which items can be filled at the current time
	 * @module:alice-jobs/lib/fulfillment/vendor/spreadshirt/transaction.Transaction#canFill
	 */
  , canFill: function canFill( ){
		var key
		  , type
		  , that 
		  ;

		this.fillable = [];
		that = this;

		if( this.parent('canFill') ){
			this.order.orderItems.forEach(function( item ){
				var _item = clone( item )
				type = that.iface.checkItem( item );
				if( type.available && type.quantity >= item.quantity + that.iface.options.minStock ){
					item.price = type.price;
					that.fillable.push( item );
				}
			});
		}
	}

	/**
	 * determines which items can be filled at the current time
	 * @module:alice-jobs/lib/fulfillment/vendor/spreadshirt/transaction.Transaction#checkFill
	 * @param {Object} items an object container items to be ordered
	 * @return {Object} An object containing the items that can be filled
	 */
  , checkFill: function checkFIll( items ){
		items = items || {};
		for(var key in items ){
			if( this.iface.productMap.hasOwnProperty( key ) ){
				items[ key ] = undefined;
			}
		}

		return items;
	}

	/**
	 * Dispatches the current order to Spreadshirt
	 * @module:alice-jobs/lib/fulfillment/vendor/spreadshirt/transaction.Transaction#submit
	 */
  , submit: function submit( ){
		// FIXME: this is dangrous. 
		// submit order depends on `this.xml`
		// but this.xml is created before 2 aysnc / out of process operations.
		// there is no garuntee it is still there or correct.
		// should just pass the xml into submitOrder

		this.xml = this._buildTransactionXML();
		this.iface.withSession('http://api.spreadshirt.com/api/v1/orders', 'POST', this._submitOrder.bind( this ) );
	}

	/**
	 * wrapper around the vendor backend submit
	 * @module:alice-jobs/lib/fulfillment/vendor/spreadshirt/transaction.Transaction#_submitOrder
	 * @param {String} header a valid authentication header for a Spreadshirt API request
	 */
  , _submitOrder: function _submitOrder( header ){
		this.iface.submit( header, this.xml);
	}

	/**
	 * Generates xml for a spreadshirt order
	 * @module:alice-jobs/lib/fulfillment/vendor/spreadshirt/transaction.Transaction#_buildTransactionXML
	 * @return XML object that represents a new order tansaction for spreadshirt
	 */
  , _buildTransactionXML: function _buildTransactionXML( ){
		var transaction
		  , builder;

		transaction = { 
			order:{
				orderItems:[]
				,payment:[{type:['EXTERNAL_FULFILLMENT']}]
				,shipping:[]
				,$:{
					'xmlns:xlink':'http://www.w3.org/1999/xlink'
				  , 'xmlns':'http://api.spreadshirt.net'
				} 
			}
		};

		builder = new xml2js.Builder();

		this._buildOrderItems(transaction);
		this._buildOrderShipping(transaction);

		return builder.buildObject(transaction);
  }

  , _buildOrderItems: function _buildOrderItems( transaction ){
		var product     // the product matching the fillable item
		  , fillable    // refernce to this.fillable, and array of items
		  , prod        // fake product information stub
		  , attachments // place holder for the xml object attachements
		  , orderItem   // place order for the order item to be appended
		  , item
		  ;

		fillable = this.fillable;
		for( var idx=0, len=fillable.length; idx < len; idx++ ){
			item = fillable[ idx ];
			product = this.iface.resolveProduct( item.product );
			logger.warning( 'spreadshirt:transaction - get correct productType and appearance id');
			logger.debug( product.product.configurations );

			prod = {
				productType: [{
					'$': {id: product.product.productType[0].$.id }
				}],
				appearance: [{
					'$': { id: product.product.appearance[0].$.id }
				}],
				restrictions: [{
					example: ['false'],
					freeColorSelection: ['false']
				}],
				configurations: [{
					configuration: [{
						offset: [{
							y: ['0'],
							'$': { unit: 'mm' },
							x: ['0']
						}],
						'$': { type: 'design' },
						printType: product.product.configurations[0].configuration[0].printType,
						printArea: product.product.configurations[0].configuration[0].printArea,
						content: [{
							svg: [{
								image: [{
									'$': {
										transform: '',
										height: '300.0',
										width: '300.0',
										printColorIds: '',
										designId: 'logo-reference'
									}
								}]
							}],
							'$': {
								dpi: '25.4',
								unit: 'mm'
							}
						}],
						restrictions: [{
							changeable: ['false']
						}]
					}]
				}]
			};

			// create attachemnt XML Object
			attachments = { attachment:[] };
			attachments.attachment.push( {'$': {type: 'sprd:design','id':'logo-reference'}, 'reference':[{'$': {'xlink:href': 'http://s7d7.scene7.com/is/agm/spiritshop/ss0004?$txttl=Hornets&$txttnm=Saint%20Elizabeth&setAttr.clrp={baseColorValue=%23FFD700}&setAttr.clrs={baseColorValue=%23000000}&setAttr.clrt={baseColorValue=%23ffffff}&setAttr.txt_t_nm_MED={visible=false}&setAttr.txt_t_nm_LONG={visible=true}&setAttr.txt_t_nm_SHRT={visible=false}&setAttr.txt_t_l_MED={visible=true}&setAttr.txt_t_l_SHRT={visible=false}&setAttr.txt_t_l_LONG={visible=false}&$txttl=Hornets%20&$txttnm=Saint%20Elizabeth&$arttm=art-logo-yellowjacket-0001&fmt=png&hei=2000'}}] } );
			attachments.attachment.push({'$': {type: 'sprd:product','id':'product-reference'}, 'product':prod} );

			// create an order item XML object
			orderItem = { orderItem: [{quantity: [String(item.quantity)],element: [{ '$': {type: 'sprd:product','xlink:href':'http://api.spreadshirt.com/api/v1/shops/509490/products/product-reference'}, properties: [{property:[]}] }], attachments: attachments }]  };
			orderItem.orderItem[0].element[0].properties[0].property.push( {'_':invert(this.iface.cache.appearance)[item.color],$:{key:'appearance'}} );
			orderItem.orderItem[0].element[0].properties[0].property.push( {'_':invert(this.iface.cache.size[product.product.productType[0].$.id])[item.size] ,$:{key:'size'} } );

			// push the order item on to the order items array of the transaction
			transaction.order.orderItems.push(orderItem);
		} // end loop
  }
  /**
   * generates a shipping object to generate XML for spreadshirt orders
   * @private
   * @module:alice-jobs/lib/fulfillment/vendor/spreadshirt/transaction.Transaction#_buildOrderShipping
   * @param {Transaction} transaction A transaction object to add shipping information onto
   */
  , _buildOrderShipping: function _buildOrderShipping( transaction ){
		var shipping
		;
		logger.debug('TODO: spreadshirt:transaction - ~90: Replace country hardcode when we have country in the order schema' );

		shipping = {
		  shippingType: [ {'$': {'id': '14'}} ]
		  , address: [
			{
			  '$': {'type': 'private'}
			  , company: []
			  , person: [{
				  salutation: { $: { id: 1 } }
				, firstName: [this.order.shippingInfo.firstName]
				, lastName: [this.order.shippingInfo.lastName]
			  }]
			  , street: [this.order.shippingInfo.address1]
			  , houseNumber: ''
			  , city: [this.order.shippingInfo.city]
			  , country: [{
				  '_': 'United States'
				  ,'$': { code: 'US' }
			  }]
			  , zipCode: [this.order.shippingInfo.postalCode]
			  , email: ['dummyMail@mailprovider.com']
			}
		  ]
		};
		return transaction.order.shipping.push( shipping );
  }

  /**
   * returns the current set of available products
   * @module:alice-jobs/lib/fulfillment/vendor/spreadshirt/transaction.Transaction#getProducts
   * @return {Object} Object containing product information
   */
  , getProducts: function getProducts( ){
		return this.iface.products;
  }

});

module.exports = SpreadShirtTransaction
