/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Collects all realted order handling logic
 * @module index.js
 * @author Eric Satterhite
 * @since 0.0.1
 * @requires module:alice-jobs/lib/fulfillment/order/processor
 */

exports.processor = require('./processor')
