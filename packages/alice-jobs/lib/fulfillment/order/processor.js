/*jshint laxcomma: true, smarttabs: true, node: true*/
// daemons/fulfillment/index.coffee
'use strict';
/**
 * Provides a base implementation for processing orders.
 * @module module:alice-jobs/fulfillment/order/processor
 * @author Eric Satterwhite
 * @author Jim Deakins
 * @since 0.1.0
 * @requires events 
 * @requires util
 * @requires module:alice-stdlib/class 
 * @requires module:alice-stdlib/string 
 * @requires module:alice-stdlib/class/options 
 * @requires module:alice-stdlib/class/timers
 * @requires module:alice-stdlib/class/parent
 * @requires module:alice-log 
 * @requires module:alice-jobs/events 
 */
 
// https://developer.spreadshirt.net/display/API/Basket+Resources#BasketResources-AddArticletoBasketandCheckout

var util         = require('util')
  , logger       = require('alice-log')
  , EventEmitter = require('events').EventEmitter
  , Class        = require('alice-stdlib/class')
  , Timers       = require('alice-stdlib/class/timers')
  , Options      = require('alice-stdlib/class/options')
  , Parent       = require('alice-stdlib/class/parent')
  , interpolate  = require('alice-stdlib/string').interpolate
  , isEmpty      = require('alice-stdlib/lang').isEmpty
  , emitter      = require('../../../events')
  , vendors      = require('../vendors')
  , checkReady
  ;

checkReady = function (libname){
	 this.loading = this.loading.filter(function( x ){
	 	return x != libname
	 });

	 if( !this.loading.length ){
		this.isReady = true;
		return this.ready();
	 } 

	 this.isReady = false;
  };

// ### canonical order in system
// * create new order, feed in canonical order
// * determine vendors for order
// * each vendor should supply a list of items?
// * try to match whole order to one vendor first 
// * create order through third part API

// build order
// ===========
// shirt, picture, metadata
//
// ### Order Model
//
// vendor order model
// products map:
//
// * product id to vendor order


// list of vendor orders


console.time( 'full run' );

var OrderProcessor = new Class(/* @lends module:alice-jobs/fulfillment/order/processor.Processor.prototype*/{
	inherits:EventEmitter
  , mixin:[ Parent, Timers, Options ]
  , options:{
		onReady: new Function()
  }

  , isReady: false
  , orders: {
	  timeout: 10000 
	
  }
  , vendors: vendors
  , constructor: function( options ){
		this.setOptions( options )
		this.loading = Object.keys( this.vendors )
		
		this.vendors = {};

		Object.keys( vendors )
			  .forEach(function( vendor ){
			  	this.vendors[ vendor ] = new vendors[vendor]();
			  }.bind( this ) )
		emitter.once('ready', checkReady.bind( this ) )

  }

  /**
	* performs the `ready` check which in turn fires the ready event
	* @chainable
	* @method module:alice-jobs/fulfillment/order/processor.Processor#ready
	* @param {Function} 
	* @return {OrderProcess} the current class instance
	**/
  , ready: function ready(callback){
		if( callback ){
		  this.once('ready', callback )
		}
		if( this.isReady ){
			logger.info("fulfillment:processor - Base processor is ready.")
			this.emit('ready')
		}

		return this;
	}

  /**
	* Given an order, generates a new {Transaction|@link module:alice-jobs/fulfillment/vendor/transaction}
	* @method module:alice-jobs/fulfillment/order/processor.Processor#order
	* @param {Object} order A mongoose Document representing an order in the `paid` state to be processed
	**/
  , order:function order(so){
		var vendor = 'spreadshirt'
		  , transaction;

		if ( Object.keys( this.vendors ).indexOf( vendor ) !== -1 ){
			transaction = this.vendors[ vendor ].order( so );
			if ( !this.orders[so._id] ){
				this.orders[so._id] = {};
			}

		  this.orders[so._id][vendor] = transaction;
		  
		  emitter.once( 
		  		interpolate( 'order/{{_id}}/transaction_ready', so )
		  	 , this.transactionReady.bind( this ) 
		  );

		  this.setTimer( 
			so._id
			, this.options.timeout 
			, function(){
				this.orderReady.bind(this, so._id)
			}
		  );
		  return transaction.prepare();

		  /*
		  - prepare order
		  - all order callback when ready
		  - timeout of x to place order
		  orders all deliver cost per item
		  in the end, we order by vendor by stock by cost
		  */
		}

  }

  /**
	* This checks to see if a transaction for a vendor is ready. if it is, it calls order ready.
	* @chainable
	* @method module:alice-jobs/fulfillment/order/processor.Processor#transactionReady
	* @param {TYPE} NAME ...
	**/
  , transactionReady: function transactionReady (oid){
		for(var vendor in this.orders[oid]){
			var transaction = this.orders[oid][vendor];
		
			if ( !transaction.isReady ){
				return this;
			}
		}

		return this.orderReady(oid);
  }

  /**
	* Effectively an event handler for the transaction_ready event. 
	* @method module:alice-jobs/fulfillment/order/processor.Processor#orderReady
	* @param {Object} id The ID of the order to process
	* @return {OrderProcesser} Returns the class instance
	**/
  , orderReady: function orderReady(oid){

		this.clearTimer( oid );

		if ( !isEmpty( this.orders[oid].spreadshirt.fillable ) ){
			// FIXME: Bad code smell 
			// shouldn't the submit function do this check so the processor
			// doesn't have to know so much of the internals ?
			// it also seems bad to have the primary processor haveto access the 
			// spread shirt property directly
		  this.orders[oid].spreadshirt.submit();
		}

		/*
			compare fillable to order placed
			check cheapest
			place order
		*/
		logger.info( 'fulfilment:processor placing order %s', oid );
		console.timeEnd( 'full run' );

		return this;
  }
});

module.exports = OrderProcessor;
