/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * This is the base transaction Class for vendor specific transaction types
 * @module module:alice-jobs/lib/fulfillment/transaction
 * @author Eric Satterwhite
 * @author Jim Deakins
 * @since 0.1.0
 * @requires util
 * @requires events
 * @requires module:alice-stdlib/class
 * @requires module:alice-stdlib/class/options
 * @requires module:alice-stdlib/class/parent
 * @requires module:alice-stdlib/lang
 */

 var util    = require('util')                         // Node util module
   , events  = require( 'events' )                     // Node events Module
   , logger  = require( 'alice-log' )                  // standard logging module
   , Class   = require( 'alice-stdlib/class' )         // Base Class implementation
   , Options = require( 'alice-stdlib/class/options' ) // Class Options mixin
   , Parent  = require( 'alice-stdlib/class/parent' )  // Class Parent mixin 
   , clone   = require("alice-stdlib/lang").clone      // Standard function to clone objects and arrays
   , isEmpty = require('alice-stdlib/lang').isEmpty    // standard isEmpty function to check empt strings arrays and object
   , omit    = require("alice-stdlib/object").omit     // standart object omit function to filter properties   
   , Transaction
   ;


/**
 * Base implementation of a vendor Transaction
 * @class module:alice-jobs/lib/fulfillment/transaction.Transaction
 * @extends EventEmitter
 * @mixes module:alice-stdlib/class/options
 * @mixes module:alice-stdlib/class/parent
 * @param {Object} [options={}] Insance configuration options
 */
Transaction = new Class(/* @lends module:alice-jobs/lib/fulfillment/transaction.Transaction.prototype */{
	isReady: false
	, inherits: events.EventEmitter
	, mixin:[Parent, Options]
	, constructor: function( order, iface ){
		this.iface = iface
		if ( !( order && order._id ) ){
		  logger.error( "fulfillment:transaction no order id found" );
		  this.emit('error', new Error() )
		}
		this.order = order;
	}

	/**
	 * This does nothing. Should probably throw a not implemented exception
	 * @method module:alice-jobs/lib/fulfillment/transaction.Transaction#place
	 * @return
	 */
	, place: function place( ){

		return;
	}

	/**
	 * This method does nothing. Should throw a not implemented excpetion.
	 * @method module:alice-jobs/lib/fulfillment/transaction.Transaction#checkFill
	 * @param {Array} itmes an array of order items to determine if an order can be filled.
	 * @return
	 */
	, checkFill: function checkFill( ){}

	/**
	 * This method calls the checkFill method.
	 * @method module:alice-jobs/lib/fulfillment/transaction.Transaction#canFill
	 * @return {Boolean} Checks to see if the items can all be fileed
	 */
	, canFill: function canFill( ){
		var items = clone( this.order.orderItems )
		var canFill = omit( items, Object.keys( this.checkFill( items ) ) )
		return isEmpty( canFill );
	}

	/**
	 * Not implemented
	 * @method module:alice-jobs/lib/fulfillment/transaction.Transaction#submit
	 */
	, submit: function submit(){

	}
});

Object.defineProperties(Transaction.prototype,{
	items:{
		get: function(){
			return this.order && this.order.items 
		}
	}
})

module.exports = Transaction
