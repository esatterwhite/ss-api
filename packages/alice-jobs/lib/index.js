/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * Entry point for the jobs package. Exports all primary peices
 * @module module:alice-job/lib
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires module:alice-jobs/lib/fulfillment/vendors
 * @requires module:alice-jobs/lib/fulfillment/order
 * @requires module:alice-jobs/lib/fulfillment/transaction
 * @requires module:alice-jobs/lib/fulfillment/vendor
 */


exports.vendors     = require( './vendors' )
exports.order       = require( './order' )
exports.Transaction = require( './transaction' )
exports.Vendor      = require( './vendor' )

exports.run = function( options ){
	// run a specific job w/ options 
}
