## Configuration for the Alice jobs packages

These a the available configuration options and their default values for the jobs packages

spreadshirt `<Object>`
======================
An object containing spreadshirt specific configuration

#### account `<Object>`

Spread shirt specific account credientials and information

###### username `<String>`
spreadshirt API account user name

###### password `<String>`
spreadshirt API account password

###### apikey `<String>`
spreadshirt API ket for the associated account username / password

###### secret `<String>`
spreadshirt API Secret for the matching API Key.

All settings can be changed using the means exposed by the `alice-conf` packages - environment variables, json files, command line arguments or any combination of them.

To change the account username and password you might use the following invocation from the management command:

- command line arguments
```bash
alice jobs --job=fulfillment --spreadshirt:account:username=foo --spreadshirt:account:password=abc123
```  

- environment variables
```bash
spreadshirt__account__username=foo spreadshirt__account__password=abc123 alice jobs --job=fulfillment
```
