/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Package for storing background tasks and jobs
 * @module module:alice-jobs
 * @author 
 * @since 0.1.0
 * @requires module:alice-jobs/events 
 * @requires module:alice-jobs/commands 
 
 * @requires module:alice-jobs/lib 
 */


module.exports = require('./events');


