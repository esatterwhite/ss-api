/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Default command for the alice-jobs package
 * @module module:alice-jobs/commands/jobs
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires seeli
 * @requires util
 * @requires path
 * @requires child_process
 */

var cli = require( 'seeli' )
  , util = require( 'util' )
  , path = require( 'path' )
  , child_process = require( 'child_process' )
  ;

module.exports = new cli.Command({
	description:"Default command for alice-jobs package"
	,usage:[
		cli.bold('Usage: ') + 'alice job --help'
	  , cli.bold('Usage: ') + 'alice job --no-color'
	  , cli.bold('Usage: ') + 'alice job -i'
	  , cli.bold('Usage: ') + 'alice job --job=fulfilment'
	]

	,flags:{
		'job':{
			type: String
			,description:"The Job to execute"
			,required:true
			,shorthand:'j'
			,choices:['fulfilment']
		}
	}
	/**
	 * This does something
	 * @param {String|null} directive a directive passed in from the cli
	 * @param {Object} data the options collected from the cli input
	 * @param {Function} done the callback function that must be called when this command has finished
	 * @returns something
	 **/
	,run: function( cmd, data, done ){
		try{
			var proc = child_process.fork( path.resolve(__dirname,'..', 'scripts', data.job ), {
				env:{
					NODE_PATH:path.resolve(__dirname, '..', '..' )
					,logger:'stdout'
				}
			}, function(err, stdout, stderr ){
				if( err ){
					console.log("ERROR! ", err )
					process.exit(1)
				}
			});
			
		} catch( e ){
			console.log('error', e )
		}

		// done(/* error */ null, /* output */ 'success')
	}
});
