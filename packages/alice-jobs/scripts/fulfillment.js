var SpreadShirt = require("../lib/fulfillment/vendors/spreadshirt")
  , Processor = require("../lib/fulfillment/order/processor")
  , p = new Processor()
  , fakeOrder 

fakeOrder = {
_id: '93D503A05BF25A9C73A6CE62',
'purchaserInfo': {
  'customerId': '93D503A05BF25A9C73A6CE6D',
  'firstName': 'John',
  'middleInitial': 'M',
  'lastName': 'Doe'
  },
'shippingInfo': {
  'firstName': 'John',
  'middleInitial': 'M',
  'lastName': 'Doe',
  'address1': '12345 Somewhere Road',
  'city': 'ThatPlace',
  'state': 'IL',
  'postalCode': '12345'
  },
'paymentGateway': 'Stripe',
'paymentConfirmationCode': 'OkeyDokey',
'manufacturers': {
  'name': 'SpreadShirt'
  },
'orderStatus': 'queued',
'lastOrderUpdate': 1405974294,
'subTotal': 10000,
'shippingTotal': 1500,
'tax': 700,
'discounts': 0,
'total': 12200,
'orderItems': [
{
  product: "Men's T-Shirt",
  size: "S",
  color: "black",
  quantity: 1
  }
],
'affiliate': 'hss'
};


p.ready( p.order.bind( p, fakeOrder ) )
