/*jshint laxcomma: true, smarttabs: true, node: true*/
'use strict';
/**
 * A redis backend used for easy integration with the Redis Cloud Heroku add-on
 * This backend looks for a fully quified URI under the `REDISCLOUD_URL` using the {@link module:alice-conf|conf} module.
 * The Url should take the form of `redis://USER:PASSWORD@uri:PORT`
 * @module alice-cache/lib/backends/rediscloud
 * @author Eric Satterwhite
 * @since 0.3.0
 * @requires alice-cache/lib/backends/redis
 * @requires alice-conf
 * @requires alice-stdlib/class
 * @requires alice-cache/lib/backends/redis
 * @example
alice web --caches:default:backend=rediscloud --REDISCLOUD_URL=redis://admin:abc123@db1.rediscloud.com:19469
 * @example
caches__default__backend=rediscloud REDISCLOUD_URL=redis://admin:abc123@db1.rediscloud.com:19469 alice web -l stdout
 */

var url           = require( 'url' )
  , conf          = require( 'alice-conf' )
  , logger        = require( 'alice-log' )
  , Class         = require( 'alice-stdlib/class' )
  , exceptions    = require( 'alice-core/exceptions' )
  , Redis         = require( './redis' )

/**
 * @alias module:alice-cache/lib/backends/rediscloud
 * @constructor
 * @extends module:alice-cache/lib/backends/redis
 */
module.exports = new Class({
	inherits:Redis
	,constructor: function( options ){
		var REDIS_URL = conf.get('REDISCLOUD_URL');

		// bail if we aren't configured correctly
		if( !REDIS_URL ){
			throw new exceptions.ImproperlyConfigured({
				message:'rediscloud cache backend requires REDISCLOUD_URL to be configured'
			});
		}

		logger.info('Setting Cache location to %s', REDIS_URL );
		this.setOptions({ location: REDIS_URL });
		this.parent('constructor', options );
		REDIS_URL = null;
	}
});
