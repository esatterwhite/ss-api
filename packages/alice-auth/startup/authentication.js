/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * authentication.js
 * @module alice-auth/startup/authentication
 * @author Eric satterwhite
 * @since 0.1.0
 * @requires passport
 * @requires alice-log
 */

var passport  = require( 'passport' )
  , logger    = require('alice-log')
  , _passport = new passport.Passport()
  ;

_passport.framework( require('../lib/passport/framework/hapi')() )
_passport.use(require('../lib/middleware/authentication/apikey'));
_passport.use(require('../lib/middleware/authentication/jwtbearer'));
// user is tokenPayload from above
// because their are no session cookies here.
_passport.serializeUser(function (user, done) {
    logger.info('serializing user', user)
    done(null, user);
});

_passport.deserializeUser(function (user, done) {
    console.log("deserializing user")
    done(null, user);
});

/**
 * @param {Server} plugin hapi server instance
 * @param {Object} options runtime configuration options
 * @param {Function} next Callback to be called when registration is complete
 **/
exports.register = function(plugin, options, next ){
	plugin.auth.scheme('alice', function(server,options){

		return {
			authenticate: function(){
				return _passport.authenticate(['localapikey', 'jwtbearer']).apply( _passport, arguments )
			}
		}
	})
	plugin.auth.strategy(
		'alice'
		, 'alice'
		, true
		, {
			validateFunc: function(){
			}
		}
	)
	next();
}

exports.register.attributes = {
	name:'authentication'
	,version:'0.1.0'
};
