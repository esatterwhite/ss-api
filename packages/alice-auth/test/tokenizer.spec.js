var assert      = require('assert')
  , path        = require('path')
  , os          = require('os')
  , util        = require('util')
  , token = require('alice-auth/lib/token');


describe('auth', function(){

	before(function( done ){

		done();
	});

	after(function( done ){
		done();
	});

	var KEY = 'abacadaba';
	var SECRET = "fuzzykitten";

	describe('lib/token', function(){

		it( 'should generate an auth token ', function(){
			var auth = token.tokenize(KEY);

			assert.ok( auth.hasOwnProperty('apiKey') );
			assert.ok( auth.hasOwnProperty('expiry') );
			assert.ok( auth.hasOwnProperty('issued') );
			assert.ok( auth.hasOwnProperty('role') );

			assert.equal( auth.apiKey, KEY )
		});

		it('should encode/decode auth tokens', function(){
			var auth = token.tokenize( KEY );

			var signature = token.encode( auth, SECRET );

			var decoded = token.decode( signature, SECRET )

			assert.equal( auth.apiKey, decoded.apiKey )
			assert.equal( auth.expiry, decoded.expiry )
			assert.equal( auth.issued, decoded.issued )
			assert.equal( auth.role, decoded.role )

		});
		
	});

})
