/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * module for dealing with API Bearer tokens
 * @module alice-auth/lib/token
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires jwt-simple
 * @requires alice-conf
 */

var jwt            = require("jwt-simple") // generates JSON Web Token
  , config         = require('alice-conf')
  , jwtTokenConfig = config.get('jwtToken')

/**
 * An object describing a point in time broken into logical units
 * @typedef {Object} module:alice-auth/lib/token~AuthToken
 * @property {String} apikey apikey the original request was made with 
 * @property {Number} expiry The time in ms when the token is no longer valid
 * @property {Number} issued The time the token as generated
 * @property {String} role The authorization role associated with the requesting account
 */


/**
 * Generates a Token object suitable for encoding for api request
 * @param {String} key API Key for signature encoding
 * @param {String} [role=read] Account role for permission handling
 * @return {module:alice-auth/lib/token~AuthToken} Token payload to be encoded
 * @example
var token = require('alice-auth/lib/token')
token.tokenize( 'abacadaba', 'write' )
 **/
exports.tokenize = function( key, role ){
	var currentDateTime = new Date()
	  , issued        = currentDateTime.getTime()
	  , expiry          = currentDateTime.setTime(currentDateTime.getTime() + (jwtTokenConfig.hours * 60 * 60 * 1000))

	return {
		apiKey: key
	  , expiry: expiry
	  , issued: issued
	  , role: role || 'read'
	};
}
/**
 * encodes an object into a JWT string
 * @param {Object} object used to generate a auth token signature
 * @param {String} secret secret shared between parties for signing tokens
 * @return {String} An encoded JWT string
 * @example
var token = require('alice-auth/lib/token')
var payload = token.tokenize( 'abacadaba', 'write' )
token.encode( payload, 'secret' )
 **/ 
exports.encode = function( data, secret ){
	return jwt.encode( data, secret )
};
/**
 * decodes a generated authentication token to determine if access is to be granted
 * @param {String} token The token to decode
 * @param {String} secret The shared secret the token was originally signed with
 * @return {Object} The decoded auth token object
 * @example
var token = require('alice-auth/lib/token')
var payload = token.tokenize( 'abacadaba', 'write' )
var signature = token.encode( payload, 'secret' )
token.decode( signature )
 **/
exports.decode = function( token, secret ){
	return jwt.decode( token, secret )
};
