/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * Entry point for auth module
 * @module alice-auth/lib
 * @author Eric satterwhite
 * @since 0.1.0
 * @requires alice-auth/lib/middleware
 * @requires alice-auth/lib/token
 */


exports.middleware = require("./middleware")
exports.token = require('./token')
