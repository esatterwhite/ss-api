/*jshint node: true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * index.js
 * @module alice-auth/lib/api/v1
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires alice/auth/lib/api/v1/resources
 */


/**
 * @property {Object} resources An object container all defined api resources for v1
 **/
exports.resources = require('./resources')
