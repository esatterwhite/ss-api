/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * index.js
 * @module alice-auth/lib/api/v1/resources
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires alice-auth/lib/api/v1/resources/token
 */


/**
 * @property {Function} token Token resource function for express
 **/
exports.token = require('./token')
