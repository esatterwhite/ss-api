/*jshint node: true, laxcomma: true, smarttabs: true*/
'use strict';
/**
   Based on [OAuth2 Blog Post](https://ahmetalpbalkan.com/blog/designing-a-secure-rest-api-with-oauth2-you-can-be-proud-of/)
   and [Gist](https://gist.github.com/joshbirk/1732068)
   We will use [JWT](https://github.com/hokaccha/node-jwt-simple) for token generation
 * @module alice-auth/lib/api/v1/resources/token
 * @author Eric Satterwhite
 * @author shekar
 * @since 0.1.1
 * @requires util
 * @requires jwt-simple
 * @requires alice-conf
 * @requires alice-log
 * @requires alice-auth/lib/token
 */
 
var jwt            = require('jwt-simple') // used to generate tokens. should be moved to alice-auth
  , config         = require('alice-conf')
  , logger         = require('alice-log')
  , token          = require('alice-auth/lib/token')
  , jwtTokenConfig = config.get('jwtToken')
  , file
  , methods
  , get
  , affiliateApiKeys
  ;

file = {file:__filename, dir:__dirname};

affiliateApiKeys = config.get( 'affiliateApiKeys' );

//Helper method to interogate params, body, query and headers in order
function findParam(req, find) {
    return req.param(find) || req.headers[find];
}

get = function getToken( req, res, next ){
    var apikey          = findParam(req, 'apikey');
    
    // found in alice-conf/lib/defaults.js
    if (!(affiliateApiKeys[apikey] ) ) {
        logger.error('unknown api key: %s', apikey, file );
        return res.json({
            error: 'unknown API Key'
        }, 401);
    }
    var payload = token.tokenize( apikey, 'read');
    logger.debug('token payload', payload );
    res.json({
        access_token: token.encode(payload, jwtTokenConfig.secret)
    });
};

// quick lookup for the middle ware function
methods = {
    /**
     * @alias module:alice-auth/lib/api/v1/resources/token
     * @name get
     * @function get
     **/
    get:get
};

module.exports.get = get;

// This is an odd place for this should be come its
// own resource for account access - ERS

/**
 * Serves a middleware function for express. It looks up a method the same as the http verb
 * and passes the arguments through. It is a convience for generating a valid JWT for an OAUTH2 implementation
 * @exports token
 * @function
 * @alias module:alice-auth/lib/api/v1/resources/token
 * @param {Object} request Express request object
 * @param {Object} response Express response object
 * @param {Function} next express next layer function
 **/
module.exports = function token( req, res, next ){
    var method = req.method.toLowerCase();

    if( methods.hasOwnProperty( method ) ){
        return methods[ method ]( req, res, next );
    }

    next();
};

