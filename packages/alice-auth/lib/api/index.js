/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Collection v1 resources
 * @module alice-auth/lib/api
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires alice-auth/lib/api/v1
 */

/**
 * @property {Object} v1 All v1 resources by name
 **/
exports.v1 = require('./v1')
