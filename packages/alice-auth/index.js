/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Authentication and Authorization handleing for the alice platform
 * @module alice-auth
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires alice-auth/events 
 * @requires alice-auth/commands 
 * @requires alice-auth/models 
 * @requires alice-auth/lib 
 */


module.exports = require('./events');



// models
module.exports.models = require('./models')
module.exports.api = require('./lib/api')
module.exports.middleware = require('./lib/middleware')
