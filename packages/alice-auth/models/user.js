/**
 * Created by Ryan Fisch on 4/21/2014.
 */
'use strict';
var connection = require('alice-core/lib/db').connection;

var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , AuditableSchema = require('alice-core/lib/db/models').auditable
    // , userAddress = require('../../../models/address')

var userSchema = new AuditableSchema({
    userName: {type: String, required: true},
    firstName: {type: String, required: true},
    lastName: {type: String},
    email: {type: String, required: true},
    lastLoginDate: {type: Date},
    role: {type: String, required: true, enum: ['super', 'admin', 'edit', 'read']},
    // address: [userAddress],
    userStatus:{type:String, enum:['active', 'suspended', 'inactive']},
    appliedPromotionalCodes:[{type:String}]
},{collection:'auth_user'});

module.exports =  connection.commerce.model('User', userSchema);
