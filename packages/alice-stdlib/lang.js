/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * general javascript helpers and short cuts
 * @module alice-stdlib/lang
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires mout/lang
 */
module.exports = require('mout/lang')