/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * Provides abstraction around dealing with object and array based collections
 * @module alice-stdlib/collection
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires mout/colection
 */

module.exports = require('mout/collection');
