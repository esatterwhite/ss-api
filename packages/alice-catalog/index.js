/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Handles Product management and generates store catalog items
 * @module alice-catalog
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires alice-catalog/events 
 * @requires alice-catalog/commands 
 * @requires alice-catalog/models 
 * @requires alice-catalog/lib 
 * @tutorial alice-catalog-decal-inclusion
 */


module.exports = require('./events');

// models
module.exports.models  = {
	activity  : require('./models/activity')
	,category : require('./models/category')
	,decal    : require('./models/decal')
	,mascot   : require('./models/mascot')
	,product  : require('./models/product')
	,vignette : require('./models/vignette')
}

