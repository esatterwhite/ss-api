/*
 * Created by Ryan Fisch on 4/12/2014.
 */
var mongoose = require('mongoose')
    , SpiritShopSchema = require('alice-core/lib/db/models').auditable
    , connection = require('alice-core/lib/db').connection;

var categorySchema = new SpiritShopSchema({
    name: String,
    parent: {type: mongoose.Schema.ObjectId, ref: 'category'},
    children: [
        {type: mongoose.Schema.ObjectId, ref: 'category'}
    ]
},{collection:'catalog_category'});

module.exports = connection.commerce.model('Category', categorySchema);

