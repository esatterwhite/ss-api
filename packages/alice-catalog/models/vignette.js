'use strict';

var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectId
    , SpiritShopSchema = require('alice-core/lib/db/models').auditable
    , Product = require('../models/product')
    , Decal = require('./decal')
    , connection = require('alice-core/lib/db').connection;

var vignetteSchema =  new SpiritShopSchema({
    name: {type:String, required:true},
    urlTemplate: {type:String, required:true},
    products: [
        {type: ObjectId, ref: 'Product'}
    ],
    decals:[{type:ObjectId, ref: 'Decal'}]
},{collection:'catalog_vignette'});

module.exports = connection.commerce.model('Vignette', vignetteSchema);
