/*jshint laxcomma: true, smarttabs: true, node: true*/
'use strict';
/**
 * Data model Dynamically Generating Artwork for mascots Logos
 * @module alice-catalog/models/mascot
 * @author Eric satterwhite
 * @author Ryan Fisch
 * @since 0.1.0
 * @requires mongoose
 * @requires alice-core/lib/db
 * @requires alice-stdlib/string
 * @requires alice-stdlib/array
 */
var mongoose   = require('mongoose')
	, connection = require('alice-core/lib/db').connection
	, slugify    = require('alice-stdlib/string').slugify
	, getRandom  = require('alice-stdlib/array').getRandom
	, Schema     = mongoose.Schema
	, ObjectId   = Schema.ObjectId
	, Mascot
	;


/**
 * Represents a single peice of artwork that Live in Scene7.
 * A mascot is the image a specific school identifies with ( Bears, Tigers, etc)
 * @alias module:alice-catalog/models/mascot
 * @constructor
 * @param {Object} fields an object of field values for the mascot instance
 * @param {String} fields.name Internal name of the mascot instance
 * @param {String} fields.s7id Associated id in Adobe Scene 7
 * @param {String} fields.slug url friendly id slug
 * @param {Boolean} fields.complete true denotes that all of the fields needed to make a mascot usable have been filled in. This is auto calcuated on save
 * @param {Boolean} fields.isLicensed True if the mascot is an LRG licensed mascot
 * @param {String[]} fields.mascots mascots List of related mascot logos that can be used to genenrate artwork
 * @param {Objects} fields.orientation represents the image orientation according to dimensions
 * @param {Boolean} [fields.orientation.square=false] True if the image is square
 * @param {Boolean} [fields.orientation.horizontal=false] True if the image is wider than is is tall
 * @param {Boolean} [fields.orientation.vertical=false] True if the image is taller than it is wide
 */
Mascot = new Schema({
   /**
	* @name name
	* @instance
	* @memberof module:alice-catalog/models/mascot
	* @property {String} name Internal name of the mascot instance
	**/
	name       : {type:String}
	/**
	 * @name s7id
	 * @instance
	 * @memberof module:alice-catalog/models/mascot
	 * @property {String} s7id Associated id in Adobe Scene 7
	 **/
	, s7id       : {type:String}
	/**
	 * @name slug
	 * @instance
	 * @memberof module:alice-catalog/models/mascot
	 * @property {String} slug url friendly id slug
	 **/
	, slug       : {type:String}
	/**
	 * @name complete
	 * @instance
	 * @memberof module:alice-catalog/models/mascot
	 * @property {Boolean} complete flag to denot if the mascot is complete
	 **/
	, complete   : {type:Boolean, default:false, index:true}
	 /**
	  * @name isLicensed
	  * @instance
	  * @memberof module:alice-catalog/models/mascot
	  * @property {Boolean} isLicensed Flag to denote if the artwork has been licensed
	  **/
	, isLicensed : {type:Boolean, default:false}
	 /**
	  * @name mascots
	  * @instance
	  * @memberof module:alice-catalog/models/mascot
	  * @property {String} mascots List of related mascot logos that can be used to genenrate artwork
	  **/
	, mascots    : [{type:String, required:true}]

	 /**
	  * @name sequence
	  * @instance
	  * @memberof module:alice-catalog/models/mascot
	  * @property {Number} A randomly generated number for randomized selections
	  **/
	,sequence    : {type:Number, index:true}

	/**
	 * @name orientation
	 * @instance
	 * @memberof module:alice-catalog/models/mascot
	 * @property {Object} orientation hold information about the orientation of the related image
	 **/
	,orientation : {
		 square:{type:Boolean, default:false}
		,horizontal: {type:Boolean, default:false}
		,vertical: {type:Boolean, default: false}
	}
},{collection:'catalog_mascot'});


Mascot.pre('save', function( next ){
	if(!this.slug){
		this.slug = slugify( this.name )
	}
	this.complete = !!( this.mascots.length && this.name && this.slug && this.s7id )
	this.sequence = this.sequence ? this.sequence : ( Math.random() * Math.random() / Math.random() )
	next();
});

/**
 * finds a mascot based on a random number
 * @private
 * @method randomMascot
 * @param {Function} callback callback passed error if their is one, and mascot if their is one
 **/
function randomMascot( callback ){
	var rand = ( Math.random() * Math.random() / Math.random() )
		, that = this;

	this.find({complete:true, sequence:{$gte:rand} }, function(e, m){
		if( m.length ){
			callback(e, getRandom( m ).s7id );
		} else{
			that.find({complete:true, sequence:{$lt:rand}}, function(e, m2){
				callback( e, getRandom( m2 ).s7id )
			})
		}
	});
}

/**
 * Fetches all decals the is allowed to be used as a back decal.
 * *NOTE* If The Tenant is lrg licensed, return the licenseName, which is actually the scene7 id
 * @static
 * @function idForTenant
 * @memberof module:alice-catalog/models/mascot
 * @param {function} callback A callback to execute when the data is returned with a signature of ( err, data )
 * @example
Tenant.findOne({}, function(e, t){
	Mascot.idForTenant( t, console.log)
})
 * @example Mascot.idForTeant( null, console.log )
 **/
Mascot.statics.idForTenant = function( tenant, callback ){
	var that = this
		, mascotname
		;

	tenant = tenant || {};

	if (tenant.lrg.isLicensed) {
		return callback( null, tenant.lrg.licenseName );
	}

	if ( !tenant.mascot ) {
		return callback(null, false);
	}

	mascotname = tenant.mascot.name.toLowerCase().trim();

	this.find({complete:true, mascots:{$elemMatch:{$eq:mascotname} }},function(e, m ){
		if(e){
			return callback(e,null)
		}
		return m.length ? callback( e, getRandom(m).s7id ) : callback(null, false)
	});
};

module.exports =  connection.commerce.model('Mascot', Mascot);


