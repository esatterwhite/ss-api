/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Data model for the Primary tenant
 * @module alice-catalog/models/tenant
 * @author Eric satterwhite
 * @since 0.0.1
 * @requires mongoose
 * @requires util
 * @requires alice-core/lib/db/models/auditable
 * @requires alice-core/lib/db/connection
 * @requires alice-core/constants
 * @requires alice-core/models/color
 * @requires alice-catalog/models/category
 * @requires alice-catalog/models/product
 * @requires alice-catalog/models/activity
 * @requires models/promotion
 */

var mongoose         = require('mongoose')
  , util             = require('util')
  , SpiritShopSchema = require('alice-core/lib/db/models').auditable
  , connection       = require('alice-core/lib/db').connection
  , constants        = require('alice-core/lib/constants')
  , Category         = require('alice-catalog/models/category')
  , Product          = require('alice-catalog/models/product')
  , Activity         = require('alice-catalog/models/activity')
  , Mascot           = require('alice-catalog/models/mascot')
  , properCase       = require('alice-stdlib/string').properCase
  , url              = require('alice-stdlib/url')
  , Color            = require('./color')
  , Promotion        = require('./promotion')
  , ACROMYN_EXP      = /[a-z]{1}/i  // Expression to find the character of a tenant name. It may be a number, or a space...
  , SCHOOL_EXP       = /community college|learning center|homeschool|(high|elementary|middle|christian|lutheran|preparatory|prep|public) school|junior high(\s)?(school)?|christian academy|academ(y|ies)|christian prep(\.)?(atory)?|school|elementary|prep|school|christian|college|co-op|recreation(\s)?([\w]+)?|([\w\']+) \& ([\w\']+) club|([\w\']+) club|([\w\']+) assoc(\.)?(iation)?/i
  , Schema           = mongoose.Schema
  , States           = constants.states
  , Countries        = constants.countries
  , Affiliates       = constants.affiliateCodes
  , Gender           = constants.gender
  , Tenant
  , slug
  ;



slug = function slug( str ){
    return ( str || '' ).toLowerCase().replace(/ /g, '_')
};
/**
 * Represents a physical schoool offering products for sale
 * @constructor
 * @alias module:alice-catalog/models/tenant
 * @param {Object} properties instance data
 * @param {String} properties.name Default name of the tenant
 * @param {String} properties.longName A longer version of the name
 * @param {String} [properties.slug] A url friendly version of the name. WIll be generated if not provided
 * @param {String} [properties.abbr] A short abbreviation for the tenant for easier lookup
 * @param {String} [properties.tenantType] The type of the tenant
 * @param {String} [properties.address1] Line one of the tenant address
 * @param {String} [properties.address2] Line two of the tenant address
 * @param {String} [properties.city] Resident city of the tenant
 * @param {String} [properties.state] Resident state of the tenant
 * @param {String} [properties.zip] Resident zip of the tenant
 * @param {String} [properties.longitude] Resident longitude of the tenant
 * @param {String} [properties.latitude] Resident latitude of the tenant
 * @param {Object} [properties.mascot]
 * @param {String} [properties.externalUrl]
 * @param {Objec} [properties.primaryColor]
 * @param {Object} [properties.secondaryColor]
 * @param {Object} [properties.tertiaryColor]
 * @param {String} [properties.legacySystemId]
 * @param {Array} [properties.categoryExclusions]
 * @param {Array} [properties.productExclusions]
 * @param {Array} [properties.activityExclusions]
 * @param {Array} [properties.affiliateMaps]
 * @param {Object} [properties.lrg]
 * @param {Array} [properties.exclusivePromotions]
 */
Tenant = new Schema({

    'name'       : { type: String, required: false, trim: true,index:true },
    'acronym'    : { type: String, required: false, trim: true },
    'longName'   : { type: String, required: true, trim: true, index: true },
    'slug'       : { type: String, index:true, required:false },
    'url'        : { type: String,  required:false },
    'abbr'       : { type: String, trim: true, index: true },
    'tenantType' : { type: String  },
    'displayType': { type: String  },
    'longitude'  : { type: Number, min: -180.0, max: 180.0 },
    'latitude'   : { type: Number, min: -90.0, max: 90.0 },
    'address1'   : { type: String, trim: true },
    'address2'   : { type: String, trim: true },
    'city'       : { type: String, trim: true, default: '' },
    'zip'        : { type: String, trim: true },
    'state'      : { type: String, enum: States, default: 'ZZ', set: function( v ){ return v.toUpperCase(); } },
    'country'    : { type: String, enum: Countries, default: 'US' },
    'mascot'     : {
        'mascotRef': { type: mongoose.Schema.ObjectId, ref: 'Mascot' },
        'name'     : { type: String, default: 'default', index: true },
        'url'      : { type: String, default: 'http://s7.spiritshop.com/somes7referenceforspiritshopgenericmascot' }
    },
    'externalUrl': { type: String },

    'primaryColor': {
        'colorRef': { type: mongoose.Schema.ObjectId, ref: 'Color' },
        'hex'     : { type: String, validate: /^[0-9a-f]{6}$/i }
    },
    'secondaryColor': {
        'colorRef': { type: mongoose.Schema.ObjectId, ref: 'Color' },
        'hex'     : { type: String, validate: /^[0-9a-f]{6}$/i }
    },
    'tertiaryColor': {
        'colorRef': { type: mongoose.Schema.ObjectId, ref: 'Color' },
        'hex'     : { type: String, validate: /^[0-9a-f]{6}$/i }
    },

    'legacySystemId': { type: Number, required: false },
    'categoryExclusions': [{
        ref: 'Category'
     , type: mongoose.Schema.ObjectId
    }],
    'productExclusions': [
        { type: mongoose.Schema.ObjectId, ref: 'Product' }
    ],
    'activityExclusions': [
        { type: mongoose.Schema.ObjectId, ref: 'Activity' }
    ],
    'affiliateMaps': [{
        'affiliateId'          : { type: String, enum: Affiliates, required:true },
        'commissionPercentage' : { type: Number },
        'externalId'           : { type: String, required: true },
        'externalName'         : { type: String, required: false },
        'externalMascot'       : { type: String}
    }],
    // if is lrg licsenced has own mascot
    // license name is the s7 id
    'lrg':{
        'isLicensed' : { type:Boolean, required: true, default:false }
      , 'doNotSell'  : { type:Boolean, required: true, default:false }
      , 'licenseName': { type:String,  required: false }
    },
    'exclusivePromotions': [{
        ref: 'Promotion'
      , type: mongoose.Schema.ObjectId
    }]
    }, {collection: 'catalog_tenant'}
);

Tenant.index({ name     :1, type :1 });
Tenant.index({ longName :1, type :1 });
Tenant.index({ abbr     :1, type :1 });
Tenant.index({ state    :1, type :1 });
Tenant.index({ state    :1, city :1, type: 1 });
Tenant.index({ 'mascot.name': 1 });
Tenant.index({
    name:1
  , longName:1
  , abbr:1
  , city:1
  , state:1
  , 'primaryColor.hex':1
  , 'secondaryColor.hex':1
  , 'tertiaryColor.hex':1
});

Tenant.set('toJSON', { getters: true, virtuals: true });

Tenant.statics.index = function( ){
  return util.format('idx-%ss',  this.modelName.toLowerCase());
}

Tenant.statics.type = function( ){
  return this.modelName.toLowerCase();
}


/**
 * @name stateName
 * @instance
 * @memberof module:alice-catalog/models/tenant
 * @property {String} The full name of the resident state if their is a mapping to it. If not,  the state property will be returned
 **/
Tenant.virtual('stateName').get( function(){
  return States.names[ this.state ] || this.state
})

Tenant.pre('save', function( next ){
    var acromatch = ACROMYN_EXP.exec( this.name );
    var typematch = SCHOOL_EXP.exec( this.longName )

    this.slug = this.longName.toLowerCase().replace(/\s+/g, '_')
    this.acronym = acromatch ? acromatch[0].toUpperCase() : acromatch;
    // try to find a school type identifier, or use the name if it is the same as the long name.
    // otherwise take the different of longName & name
    this.displayType = typematch ? properCase( typematch.input.substr(typematch.index, typematch[0].length) ) : this.name == this.longName ? this.name : this.longName.replace( this.name, '' ).trim() 


    // if we can break the name from school type cleanly, use that as the name.
    // if the result is empty, use the longname
    var name_bits = this.longName.split( this.displayType )
    this.name = (!name_bits[1] ? name_bits[0] : this.name).trim() || this.longName;

    // FIXME: This is the way the UI wants the url, but its dumb
    // http://spiritshop-ui-dev.herokuapp.com/collegiate/US/CA/morga/53e8d7946ed5488d555b60ac/st._mary''s_college_of_california-gaels/products
    // This should be a valid slug: st-marys-college-of-california/gaels/products
    this.url = url.join(
        '/'
        ,this.tenantType
        ,this.country
        ,this.state
        ,slug( this.city )
        ,this._doc._id.toString()
        ,slug( this.longName ) + '-' + slug( this.mascot.name || 'default' )
    ).trim();
    next()
})
module.exports = connection.commerce.model('Tenant', Tenant);
