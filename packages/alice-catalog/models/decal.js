
/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * Provides the Decal Schema & Data Model
 * @module alice-catalog/models/decal
 * @author Ryan Fisch
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires mongoose
 * @requires module:alice-catalog/models/vignette
 * @tutorial alice-catalog-decal-inclusion
 */
var mongoose   = require('mongoose')
  , cache      = require('alice-cache')
  , connection = require('alice-core/lib/db').connection
  , Activity   = require('./activity')
  , constants  = require('../lib/constants')
  , Schema     = mongoose.Schema
  , joi        = require('joi')
  , ObjectId   = Schema.ObjectId
  , debug      = require('debug')('alice:catalog:decal')
  , typeschema = joi.string().valid('tenant','mascot')
  , nameschema = joi.alternatives(joi.string().allow(''), joi.number().integer() )
  , sizing     = require('../lib/sizing')
  ;


/**
 * The data model for the catalog and product decals
 * @constructor
 * @alias module:alice-catalog/models/decal
 * @tutorial alice-catalog-decal-inclusion
 * @extends mongoose.Schema
 * @param {Object} fields Field definitions for Mongoose
 * @param {String} fields.s7Id Field containg a related scene7 id
 * @param {String} fields.pk Alias to the instance primary key. what ever it might be
 * @param {String} fields.decal_id explicit ID as a string for easy aggregation and to avoid field clobbering
 * @param {String} fields.name Human readible name of the decal
 * @param {String} fields.id aliase of scene 7 id ( for the front end ( ? ) )
 * @param {Boolean} fields.lrgLicensed Flag to denot licensure
 * @param {Vingette} fields.vingettes Array of vingette overlay definitions
 * @param {String} fields.personalization Arrray of idetnifiers for scheme 7
 * @param {Boolean} [fields.front=true] flag to denote usage on the front of products
 * @param {Boolean} [fields.back=false] flat to denote usage of the back of production
 * @param {Object} fields.personalization
 * @param {Boolean} [fields.personalization.arttm=false]
 * @param {Boolean} [fields.personalization.artad=false]
 * @param {Boolean} [fields.personalization.txttnm=false]
 * @param {Boolean} [fields.personalization.txttacro=false]
 * @param {Boolean} [fields.personalization.txtttyp=false]
 * @param {Boolean} [fields.personalization.txttl=false]
 * @param {Boolean} [fields.personalization.txttcty=false]
 * @param {Boolean} [fields.personalization.txttst=false]
 * @param {Boolean} [fields.personalization.txttctry=false]
 * @param {Boolean} [fields.personalization.txtanm=false] Specifies if the name of the activity should be included in the decal template. A *null* value denotes that all values are possible. False means no values are possible
 * @param {?String} [fields.personalization.date1=null]
 * @param {?String} [fields.personalization.date2=null]
 * @param {Boolean} [fields.personalization.txtpnm=false]
 * @param {Boolean} [fields.personalization.txtpnbr=false]
 * @param {Object} fields.inclusions
 * @param {Object} fields.inclusions.activities
 * @param {Object} fields.inclusions.decals
 * @param {String} fields.colors
 * @param {Bumber} fields.colors.clrp primary color
 * @param {Bumber} fields.colors.clrs secondary color
 * @param {Bumber} fields.colors.clrt tertiary color
 * @example
 var decal = new Decal({
    "name":"ss0001"
    ,"s7id":"ss0001"
    ,"colors":{
        "clrp":1
        ,"clrs":0.5
        ,"clrt":0
    }
    ,"front":true
    ,"back":false
    ,"personalization":["txtnm","txttl"]
    ,"activities":[]
    ,"vignettes":[]
    ,"lrgLicensed":false
})
 decal.save( function( e ){
    Decal.findByScene7Id('ss0001', console.log )
 })
 */

var Decal = new Schema({

    /**
     * @instance
     * @name s7id
     * @memberof module:alice-catalog/models/decal
     * @property {String} s7Id Description
     */
    s7id:{
        type:String ,index:true
    }

    /**
     * @instance
     * @name pk
     * @memberof module:alice-catalog/models/decal
     * @property {ObjectId} pk Virtual Alias to The primay key in the document.
     */
    ,pk:{
        type: ObjectId
        , select:true
        , get: function(){
            return this._id;
        }
    }

    ,decal_id:{type:String, index:true}
    ,display_order:{ type: Number, default:0, min:0, max:10, required:false }
    /**
     * @instance
     * @name name
     * @memberof module:alice-catalog/models/decal
     * @property {String} name Human readible name of the decal
     */
    ,name:{
        type:String ,index:true
    }

    /**
     * @instance
     * @name id
     * @memberof module:alice-catalog/models/decal
     * @property {String} id Seems as if the Front end is looking up by id... So its here for now -ERS
     */
    ,id:{
        type:String ,index:true
    }
    /**
     * @instance
     * @name lrgLicensed
     * @memberof module:alice-catalog/models/decal
     * @property {Boolean} lrgLicensed True if the decals is LRG licensed
     */
    , lrgLicensed:{
        type:Boolean, default:false
    }
    /**
     * @instance
     * @memberof module:alice-catalog/models/decal
     * @name vignettes
     * @property {Array} vignettes Collection of vignette overlays
     */
    , vignettes:[{
        type:ObjectId, ref:'Vignette'
    }]

    /**
     * FK to the activity collection.
     * @instance
     * @memberof module:alice-catalog/models/decal
     * @name activities
     * @property {Array} activities This wasn't anywhere in the hardcoded data, but on the schema... I'm leaving it -ERS
     */
    , activities: [{
        type:ObjectId, ref:'Activity'
    }]


    /**
     * @instance
     * @memberof module:alice-catalog/models/decal
     * @name personalizeLimits
     * @property {String[]} personalizeLimits Some personalizations have text limits
     */
    , personalizeLimits: [{
        name: {type: String, enum: ['date1', 'date2']}
       , limit: {type: Number}

    }]
    /**
     * @instance
     * @memberof module:alice-catalog/models/decal
     * @name categories
     * @property {String[]} categories Data cache of all associated activity categories. **NOTE** This field is automatically generated by the categorizedecals npm script. Do *NOT* try to manage this by hand
     */
    ,categories:[{type:String, required:false, notEmpty:false, default: null}]
    /**
     * @memberof module:alice-catalog/models/decal
     * @name personalization
     * @instance
     * @property {Object} personalization An object containing personalization information for the Decal template
     * @property {Boolean} personalization.arttm
     * @property {Boolean} personalization.artad
     * @property {Boolean} personalization.txttnm
     * @property {Boolean} personalization.txttacro
     * @property {Boolean} personalization.txtttyp
     * @property {Boolean} personalization.txttl
     * @property {Boolean} personalization.txttcty
     * @property {Boolean} personalization.txttst
     * @property {Boolean} personalization.txttctry
     * @property {?Boolean} personalization.txtanm
     * @property {?String} personalization.date1
     * @property {?String} personalization.date2
     * @property {Boolean} personalization.txtpnm
     * @property {Boolean} personalization.txtpnbr
     */
    ,personalization:{
          'arttm'    :{type:Boolean, default: false, required:false }
        , 'artad'    :{type:Boolean, default: false, required:false }
        , 'txttnm'   :{type:Boolean, default: false, required:false }
        , 'txttacro' :{type:Boolean, default: false, required:false }
        , 'txtttyp'   :{type:Boolean, default: false, required:false }
        , 'txttl'    :{type:Boolean, default: false, required:false }
        , 'txttcty'  :{type:Boolean, default: false, required:false }
        , 'txttst'   :{type:Boolean, default: false, required:false }
        , 'txttctry' :{type:Boolean, default: false, required:false }
        , 'txtanm'   :{type:Boolean, default: false, required:false }
        , 'date1'    :{type:String,  default: null,  required:false }
        , 'date2'    :{type:String,  default: null,  required:false }
        , 'txtpnm'   :{type:Boolean, default: false, required:false }
        , 'txtpnbr'  :{type:Boolean, default: false, required:false }
        , 'txto': {type: String, default: null, required: false }
    }

    /**
     * @instance
     * @name back
     * @property {Boolean} back
     * @memberof module:alice-catalog/models/decal
     **/
    ,back:{
        type:Boolean, default:false, index:true
    }
    /**
     * @instance
     * @name custom
     * @property {Boolean} custom Indicates this is a customizable decal
     **/
    ,custom:{
      type:Boolean, default: false, index: true, required: false
    }
    /**
     * @instance
     * @name back
     * @property {Boolean} front
     * @memberof module:alice-catalog/models/decal
     **/
    ,front:{
        type:Boolean, default:true, index: true
    }

    /**
     * @instance
     * @name inclusions
     * @property {Object} inclusions lists activity categories and other decals... Looks not used -ERS
     */
    ,inclusions:{
        activities:[{ type:String }]
      , decals:[{ type:String }]
      , tags: [{type:String}]
    }
    /**
     * @instance
     * @name wbgOnly
     * @property {Boolean} wbgOnly Only use in wbg ?
     */
    ,wbgOnly: {type: Boolean, default:false}

    /**
     * @instance
     * @name mascotFilter
     * @property {Object} [mascotFilter=default] Filter mascots?
     */
    ,mascotFilter: {type: Boolean, default:false}
    /**
     * @name lengthLimits
     * @instance
     * @property {Object} lengthLimits Specifieds the size representation of the deal on a mascot and tenant
     * @property {String[]} lengthLimits.mascot allowable size of decal on mascot
      * @property {String[]} lengthLimits.tenant allowable size for tenant decal
     */
    ,lengthLimits: {
         mascot: [{type: String, enum: ['SHRT', 'MED', 'LONG']}]
        ,tenant: [{type: String, enum: ['SHRT', 'MED', 'LONG']}]
    }
    /**
     * @name colors
     * @instance
     * @memberof module:alice-catalog/models/decal
     * @property {Object} colors Object containing the primay, secondary and tertiary colors for this decal
     * @property {Number} colors.clrp
     * @property {Number} colors.clrs
     * @property {Number} colors.clrt
     * @property {Number} colors.clrb
     * @property {Number} colors.clrw
     * @property {Number} colors.clrg
     */
    ,colors:{
        clrp  : { type:Number, min: 0, max:1, default:0 }
      , clrs  : { type:Number, min: 0, max:1, default:0 }
      , clrt  : { type:Number, min: 0, max:1, default:0 }
      , clrb  : { type:Number, min: 0, max:1, default:0 }
      , clrw  : { type:Number, min: 0, max:1, default:0 }
      , clrg  : { type:Number, min: 0, max:1, default:0 }
      , whiteContrast: { type: String, required: false}
    }
      /**
     * @name active
     * @instance
     * @memberof module:alice-catalog/models/product
     * @property {String} active Whether the decal should be shown or not
     **/
    ,active: {type: Boolean, default: true}
},{collection:'catalog_decal'});

/**
 * determins the length category a specific value falls under
 * @method module:alice-catalog/models/decal#calculateSize
 * @param {Number} length
 * @return {String} category
 * @example
Decal.findOne(function(e, decal){
  decal.calculateSize( 4 ) // SHRT
  decal.calculateSize( 10 ) // MED
  decal.calculateSize( 30 ) // LONG
})
 **/
Decal.methods.calculateSize = sizing.calculateSize;

/**
 * Determines if a given bit of text is allowed on this decal instance based on it's inclusion rules
 * @method NAME
 * @param {String} type The type of size restriction to look up - `tenant` or `mascot`
 * @param {String|Number} name The name of expressed type to look up or an explict length as a number
 * @return {Boolean} allowed
 * @example
Decal.findOne(function( e, decal ){
  decal.sizeAllowed( 'mascot', "Hello world" ) // true
  decal.sizeAllowed('tenant', 40 ) // false
})
 **/
Decal.methods.sizeAllowed = function( type, name ){
    joi.assert( type, typeschema );
    joi.assert( name, nameschema );
    var allowed = !!sizing[ type ].call( this, name, this.lengthLimits[ type ] );
    debug('%s allows %s %s - %s',this.s7id, type, name, allowed);
    return allowed;
};

Decal.methods.mascotAllowed = function (mascotName, mascot_id) {
   var canmascot // can this decal display mascot images
      , allowed  // can this decal be displayed with the mascot_id
      ;

   // basically their are mascots that we don't have art for.
   // so we need to exclude decals where that have placeholders
   // for mascot artwork, and we can't find a mascot for it.
   canmascot = this.personalization[constants.ART_MASCOT];
   allowed =  canmascot ?  (canmascot && mascot_id) : true;
   return !(mascotName == '' && canmascot) && allowed;
};

/**
 * Returns the activity information that is associated with this decal.
 * If no activities have been specified, then all activities are returned
 * If the decal instance does not have any allow activity inclusions, then only a single activity is returned as to allow for a single image render
 * @method module:alice-catalog/models/decal#allActivities
 * @param {Function} callback Callback function which will passed an `error` if their is one and the results
 **/
Decal.methods.allActivities = function( callback ){
    var hasActivities   = this.activities.length
      , personalization = this.personalization
      , allowtxt        = personalization[ constants.TEXT_ACTIVITY_NAME ]
      , includeArt      = personalization[ constants.ART_ACTIVITY]
      , includeText     = ( !!allowtxt || allowtxt === null )
      , key = ['decal',this.s7id, 'activities'].join(':')
      , that = this;

    cache.get(key, function( err, cached ){
      if( cached ){
        return callback( err, JSON.parse( cached ) );
      }
      if( !hasActivities ){
          return Activity.aggregate([
              {
                $match:{
                  general:true
                }
              },{
                  $group:{
                      _id:'$category'
                      ,s7Id:{$first:'$s7Id'}
                      ,artad:{$first:'$s7Id'}
                      ,category:{$first:'$category'}
                      ,txtanm:{$first:'$category'}
                  }
              }
          ], function( err, results ){

              // if their is no activity text inclusion
              // and no activity are inclusion.
              // there is only 1 possible permutation for this decal
              if( !includeArt && !includeText ){
                  results = results.slice(0,1);
              }
              cache.set( key, JSON.stringify(results) );
              callback( err, results );
          });
      }

      // if the activities array hasn't been
      // pre-populated by mongoose
      // we have to fetch those records.
      if( typeof that.activities[0] === 'string' ){
          return Activity.aggregate([
            {
                $match:{s7Id:{$in:that.activities}}
            },{
                $group:{
                    _id:'$_id'
                    ,s7Id:{$first:'$s7Id'}
                    ,artad:{$first:'$s7Id'}
                    ,category:{$first:'$category'}
                    ,txtanm:{$first:'$category'}
                }
            }
          ], function( e, act ){
              if( !e ){
                cache.set(key, JSON.stringify( act ) );
              }
              callback( e, act );
          });
      }

      // if The activities array is populated
      // just send those back
      var results = that.activities.map(function( a ){
        var o = a.toObject();
        o.txtanm = o.category;
        o.artad = o.s7Id;
        return o;
      });

      cache.set( key, JSON.stringify( results ) );
      return callback(null, results );
    });
};

/**
 * fetches decals matching a scene seven id
 * @static
 * @function findBySceneId
 * @memberof module:alice-catalog/models/decal
 * @param {String} ss_id A valid scene seven ID
 * @param {function} callback A callback to execute when the data is returned with a signature of ( err, data )
 * @example
Decal.findBySceneId('ss00001',console.log)
 **/
Decal.statics.findBySceneId = function( ss_id, callback ){
    this.findOne({ s7id:ss_id }).lean().exec( callback );
};

/**
 * Fetches all decals the is allowed to be used as a front decal
 * @static
 * @function front
 * @memberof module:alice-catalog/models/decal
 * @param {String} [category] A specific activity category to restrict results to
 * @param {function} callback A callback to execute when the data is returned with a signature of ( err, data )
 * @example
Decal.front(console.log)
 * @example
Decal.front('Golf', console.log)
 **/
Decal.statics.front = function( callback ){
    var query = this.find({ active:true, front:true, colors:{ $exists:true } }).sort({display_order:-1});
    return callback ? query.lean().exec( callback ) : query;
};
/**
 * Fetches all decals the is allowed to be used as a front decal
 * @static
 * @function front
 * @memberof module:alice-catalog/models/decal
 * @param {String} [category] A specific activity category to restrict results to
 * @param {function} callback A callback to execute when the data is returned with a signature of ( err, data )
 * @example
Decal.front(console.log)
 * @example
Decal.front('Golf', console.log)
 **/
Decal.statics.bySceneIdArray = function (ids, callback) {
    var dids = ids.split(','),
        didArray = [],
        findObj = {};
    for (var i = 0; i < dids.length; i++) {
      didArray.push(dids[i]);
    }
    findObj = {'s7id': {'$in': didArray}};
  var query = this.find(findObj).sort({display_order:-1});
  return callback ? query.lean().exec(callback) : query;
};

/**
 * Fetches all decals the is allowed to be used as a back decal
 * @static
 * @function back
 * @memberof module:alice-catalog/models/decal
 * @param {function} callback A callback to execute when the data is returned with a signature of ( err, data )
 * @example
Decal.back(console.log)
 **/
Decal.statics.back = function( callback ){
    var query = this.find({ back:true, colors:{ $exists:true } }).sort({display_order:-1});
    return callback ? query.lean().exec( callback ) : query;
};

Decal.pre('save', function( next ){

    if( !this.decal_id ){
        this.decal_id = this._id.toString();
    }
    next();
});
Decal.index({categories:1});
Decal.index({
  'personalization.artad':1
, 'personalization.txtanm':1
, active:1
, front:1
, colors:1
});
module.exports = connection.commerce.model('Decal', Decal);
