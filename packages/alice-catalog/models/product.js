/*jshint laxcomma: true, smarttabs: true, node: true*/
'use strict';
/**
 * @module alice-catalog/models/product
 * @author Ryan Fisch
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires mongoose
 * @requires alice-stdlib/number
 * @requires alice-core/lib/db
 * @requires alice-core/lib/db/models
 */
var mongoose         = require('mongoose')
  , connection       = require('alice-core/lib/db').connection
  , SpiritShopSchema = require('alice-core/lib/db/models').auditable
  , number           = require('alice-stdlib/number')
  , Schema           = mongoose.Schema
  , ObjectId         = Schema.ObjectId
  , Product
  ;

/**
 * @typedef {Object} Color
 * @property {String} hexColor Valid hex code of the color
 * @property {String} name Human readible name of the color
 * @property {String} manufacturerId ID of the manufacture this color is available for
 **/

/**
 * Data represention of a base clothing item for sale.
 * https://docs.google.com/a/spiritshop.com/spreadsheets/d/119nx0ki8MzQKa29B0-hg7TTocuzK6J1tJ3HbDerSNpg/edit#gid=1641259996
 * @constructor
 * @alias module:alice-catalog/models/product
 * @param {Object} fields An object of field values for the Product instance
 * @param {String} fields.name Name of the product
 * @param {String} [fields.brand] Brand of the product manufacturer
 * @param {Color[]} [fields.colors] An array of available colors for the product
 * @param {Object} [fields.frontImage] A reference to the image to display on the front of the image
 * @param {Object} [fields.backImage] A reference to the image to display on the back of the image
 * @param {String} fields.shortDescription A short description of the Product
 * @param {String} fields.fullDescription The full descriptiong for the product
 * @param {Object} fields.manufacturer
 * @param {String} fields.manufacturer.name The full name of the manufacture
 * @param {String} fields.manufacturer.internalId An internal unique identifier for the namufacturer
 * @param {Object} [fields.dimensions] An object containing the full dimensions of the product
 * @param {Number} [fields.dimensions.weight] The product weight
 * @param {Number} [fields.dimensions.length] The product length
 * @param {Number} [fields.dimensions.width] The product width
 * @param {Number} [fields.dimensions.height] The product height
 * @param {Object} fields.lifecycle An object containing information about the state of the product
 * @param {Date} fields.lifecycle.availableOn A date used to determine when the product is available
 * @param {Date} [fields.lifecycle.expiresOn=null] A date used to restrict the product from sale
 * @param {String[]} [fields.categories] A list of categories the product should be listed under
 * @param {Object} [fields.economics]
 * @param {Number} [fields.economics.productCost=0] The full product cost incurred by the company
 * @param {Number} [fields.economics.productMSRP=0] The original mnufacturer's suggested retail price
 * @param {Number} [fields.economics.price=0] The sale price of the product
 * @param {Number} [fields.economics.spclPrice] The price used during specials
 * @param {Date} [fields.economics.spclPriceStart] The date at which the sale price goes into effect
 * @param {Date} [fields.economics.spclPriceEnd] The date at which the sales price expires
 * @param {Boolean} [fields.economics.callForPrice=false] If the users should be prompted to call for a price quote
 * @param {String} [fields.approvedForPublish=false] If the product has been given final approval to be displayed
 * @param {String} [fields.swatch]
 * @param {Object} [fields.search]
 * @param {String[]} [fields.search.tags] a list of keyword tags for search engines
 * @param {String[]} [fields.printTypes] The types of printing that are applicable to this product - `dtg`, `scr`, `fl`
 * @param {String} [fields.gender] The gender this product should be listed for - `m`, `f`, `u`, `tb`, `tg`, `b`, `g`
 */
Product = new SpiritShopSchema({
    /**
     * @name name
     * @instance
     * @memberof module:alice-catalog/models/product
     * @property {String} name The name of the the product
     **/
    name: {type: String, required: true},
    /**
     * @name brand
     * @instance
     * @memberof module:alice-catalog/models/product
     * @property {String} brand The name of the comany brand that produces the product
     **/
    brand:{type:String},
    /**
     * @name colors
     * @instance
     * @memberof module:alice-catalog/models/product
     * @property {Color[]} colors An array of available {@link Color|Colors}
     **/
    colors: [
        {
            hexColor:{type:String},
            name: {type: String},
            manufacturerId: {type: String}
        }
    ],
    /**
    * @name sizes
    * @instance
    * @memberof module:alice-catalog/models/product
    * @property {Size[]} sizes an Array of available {@link Size|Sizes}
    **/
    sizes: [
      {
        name: {type: String},
        manufacturerId: {type: String},
        measurements: [
          {
            name: {type: String},
            value: {type: Number},
            unit: {type: String}
          }
        ]
      }

    ],
    /**
     * @name frontImage
     * @instance
     * @memberof module:alice-catalog/models/product
     * @property {Object} frontImage A reference to the image to display on the front of the image
     **/
    frontImage:{
        imageRef:{type:String},
    },
    /**
     * @name backImage
     * @instance
     * @memberof module:alice-catalog/models/product
     * @property {String} backImage A reference to the image to display on the back of the image
     **/
    backImage:{
        imageRef:{type:String},
    },
    /**
     * @name shortDescription
     * @instance
     * @memberof module:alice-catalog/models/product
     * @property {String} shortDescription A short description of the Product
     **/
    shortDescription: {type: String, required: true},
    /**
     * @name fullDescription
     * @instance
     * @memberof module:alice-catalog/models/product
     * @property {String} fullDescription The full description of the product
     **/
    fullDescription: {type: String, required: true},
    /**
     * @name manufacturer
     * @instance
     * @memberof module:alice-catalog/models/product
     * @property {Object} manufacturer manufacturer specific information
     * @property {String} manufacturer.name name of the manufacturer
     * @property {String} manufacturer.internalId an unique id used internally
     **/
    manufacturer: {
        name: {type: String, required: true},
        internalId: {type: String, required: true},
        internalType: {type: String}
    },
    /**
     * @name dimensions
     * @instance
     * @memberof module:alice-catalog/models/product
     * @property {Object} dimensions An object containing the products dimensions
     * @property {Object} dimensions.weight the product's weight
     * @property {Object} dimensions.length the product's length in inches
     * @property {Object} dimensions.width the product's width in inches
     * @property {Object} dimensions.height the product's height in inches
     **/
    dimensions: {
        weight: Number,
        length: Number,
        width: Number,
        height: Number
    },
    /**
     * @name lifecycle
     * @instance
     * @memberof module:alice-catalog/models/product
     * @property {Object} lifecycle Monitoring inforation
     * @property {Object} lifecycle.availableOn Monitoring inforation
     * @property {Object} lifecycle.expiresOn Monitoring inforation
     **/
    lifecycle: {
        availableOn: {type: Date, required: true},
        expiresOn: Date
    },
    /**
     * @name categories
     * @instance
     * @memberof module:alice-catalog/models/product
     * @property {Array} categories This is probably not used... ?
     **/
    categories: [
        {type: ObjectId, ref: 'category'}
    ],
    /**
     * @name economics
     * @instance
     * @memberof module:alice-catalog/models/product
     * @property {Object} economics product pricing information
     * @property {Number} economics.productCost The full product cost incurred by the company
     * @property {Number} economics.productMSRP The original mnufacturer's suggested retail price
     * @property {Number} economics.price The sale price of the product
     * @property {Number} economics.spclPrice The price used during specials
     * @property {Date} economics.spclPriceStart The date at which the sale price goes into effect
     * @property {Date} economics.spclPriceEnd The date at which the sales price expires
     * @property {Boolean} economics.callForPrice If the users should be prompted to call for a price quote
     **/
    economics: {
        productCost: {type: Number, default:0},
        productMSRP: {type: Number, default:0},
        price: {type: Number, default:0},
        spclPrice: {type: Number},
        spclPriceStart: {type: Date},
        spclPriceEnd: {type: Date},
        callForPrice: {type: Boolean, default: false},
        personalization:{type:Number, default:699}
    },
    /**
     * @name approvedForPublish
     * @instance
     * @memberof module:alice-catalog/models/product
     * @property {Boolean} approvedForPublish Whether or not a product has been approved for publishing
     **/
    approvedForPublish: {type: Boolean, default: false},

    /**
     * @name swatch
     * @instance
     * @memberof module:alice-catalog/models/product
     * @property {String} approvedForPublish a swatch...
     **/
    swatch: {type: String},
    /**
     * @name search
     * @instance
     * @memberof module:alice-catalog/models/product
     * @property {Object} search
     * @property {String[]} search.tags SEO keword tags
     **/
    search: {
        tags: [{type:String}]
    },
    /**
     * @name printTypes
     * @instance
     * @memberof module:alice-catalog/models/product
     * @property {String[]} printTypes The available print types for the product
     **/
    printTypes: [
        {type: String, enum: ['dtg', 'scr', 'fl']}
    ],
    /**
     * @name gender
     * @instance
     * @memberof module:alice-catalog/models/product
     * @property {String} gender The gender the product should be displayed for
     **/
    gender: {type: String, enum: ['m', 'f', 'u', 'tb', 'tg', 'b', 'g']}
},{collection:'catalog_product'});

module.exports = connection.commerce.model('Product', Product);
