/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * Primary Activity Model for The alice platfrom
 * This calls is poorly named. It is not an activity, it is an image in scene 7
 * That we categorize by the activity it relates to. The category of the image is actually
 * An activity.
 * @module alice-catalog/models/activity
 * @author Eric Satterwhite
 * @author John Hart
 * @author Ryan Fisch
 * @since 1.9.0
 * @requires mongoose
 * @requires alice-conf
 * @requires alice-core/lib/db
 * @requires common/AuditableSchema
 */

var connection       = require('alice-core/lib/db').connection          // The connection cache...
  , Case             = require('case')
  , mongoose         = require('mongoose')                              // mongoose ODM
  , logger           = require("alice-log")
  , SpiritShopSchema = require('alice-core/lib/db/models').auditable    // The SS Auditable schema
  , conf             = require('alice-conf')                            // alice configuration loader module
  , toArray          = require( 'alice-stdlib/lang').toArray
  , _                = require( 'underscore' )                          // replace w/ stdlib

/**
 * @constructor
 * @alias module:alice-catalog/models/activity
 * @extends module:alice-core/lib/db/models/AuditableSchema
 * @mixes mongoose.Schema
 * @param {Object} fields
 * @param {String} fields.category The type of activity the image belongs too
 * @param {String} fields.s7Id The interan scene 7 id of the image related to the activity
 * @param {Boolean} [fields.general=true] true indicates this template is ok for usage in any decal template. Decals with no activities defined use this field
 * @param {String[]} fields.tags And array of defining tags for the activity image
 */
var Activity = new SpiritShopSchema(/* @lends models/activity.prototype */{
  /**
   * @instance
   * @property {String} category the activity type the image belongs to ( `Football`, `Soccer`, etc.)
   */
    category:{type:String, index:true},
    /**
     * @instance
     * @property {String} s7Id scene 7 ID 
     */
    s7Id:{type:String, index:true},
    /**
     * @instance
     * @property {Boolean} general denotes that the activity insance is ok for general use in templates
     */
    general:{type:Boolean, default:true, index:true},
    /**
     * @instance
     * @property {String[]} tags additional meta data 
     */
    tags: [
        { type: String }
    ]
},{collection:'catalog_activity'});

Activity.index({ category     :1, s7Id :1, general: 1 });

/**
 * locates valid activity images for a specific decal
 * @name byDecal
 * @static
 * @function
 * @deprecated uses decal inclusions fields, which is no longer in use
 * @memberof module:alice-catalog/models/activity
 * @param {module:alice-catalog/models/decal} decal A decal instance to find activities for
 * @param {Function} callback A callback that will be passed `error` if there is one, and an array of `activities`
 **/
Activity.statics.byDecal = function byDecal( decal, callback){
  logger.debug("getting activities for specific decal", decal )
  var returnData = [];
  var inclusions =  toArray( decal.inclusions && decal.inclusions.decals )
  var query = this.find({});

  if( inclusions.length ){
    
    logger.info("models:activity - filtering activity art",{
      inclusions: inclusions 
    });

    query = query
              .where('s7Id').in( inclusions )
  }
  query.exec(function (err, activityArt) {
      if( err ){
        return callback( err, null)
      }

      if (decal.personalization.indexOf('artad') != -1) {
          for (var i = 0; i < activityArt.length; i++) {
              returnData.push({ 'artad': activityArt[i].s7Id, 'txtanm': Case.titlecase(activityArt[i].category) });
          }
          callback(false, returnData);
          return;

      } else if (decal.personalization.indexOf('txtanm') != -1) {
          var activityText = _.uniq(_.pluck(activityArt, "category"));
          for (var i = 0; i < activityText.length; i++) {
              returnData.push({ 'txtanm': activityText[i] });
          }
          callback(false, returnData);
          return;
      }
  });
};


/**
 * Returns Acitvity information ready for use in a product catalog
 * @function
 * @static
 * @name forCatalog
 * @memberof module:alice-catalog/models/activity
 * @param {Function} callback Callback function that will be passed `error` and `activities`
 **/
Activity.statics.forCatalog = function forCatalog( callback ){
  logger.info('aggregating activities')
  this.aggregate([
     {
       $project:{'category':"$category",artad: { txtanm: '$category', artad:'$s7Id' }, txtanm:{txtanm: '$category'} }
     },{
         $group:{
             _id:"category",
             artad:{
               $push:'$artad'
             },
             txtanm:{
                $addToSet:'$txtanm'    
             }
          
         }
     },{
      $project:{
          _id:false, artad:'$artad', txtanm:'$txtanm'    
          
      }    
     }
     
  ], function( e, result ){
    logger.info('activity aggregation complete')
    callback(e, result && result[0])
  } );
}
module.exports = connection.commerce.model('Activity', Activity);
