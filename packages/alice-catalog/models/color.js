/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Defines a standard color model for product values
 * @module alice-catalog/models/color
 * @author Ryan Fisch
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires mongoose
 * @requires alice-core/lib/db/models/AuditableSChema
 * @requires alice-core/lib/db
 */
var mongoose         = require('mongoose')
  , color            = require("alice-stdlib/color")
  , SpiritShopSchema = require('alice-core/lib/db/models').auditable
  , connection       = require('alice-core/lib/db').connection
  , Color
  ;


/**
 * Base Color data model
 * @constructor
 * @alias module:alice-catalog/models/color
 * @param {Object} fields Model fields / values
 * @param {String} fields.name Name of the color
 */
Color = new SpiritShopSchema( {
    /**
     * @memberof module:alice-catalog/models/color
     * @property {String} name The name of the color
     */
    'name': {type: String}, //use validator to ensure conversion between color encodings are correct
    'rgb': {
        'r': {type: Number, min: 0, max: 255},
        'g': {type: Number, min: 0, max: 255},
        'b': {type: Number, min: 0, max: 255}
    },
    'cmyk': {
        'c': {type: Number, min: 0.0, max: 1.0},
        'm': {type: Number, min: 0.0, max: 1.0},
        'y': {type: Number, min: 0.0, max: 1.0},
        'k': {type: Number, min: 0.0, max: 1.0}
    },
    'hex': {type: String, validate: /^[0-9a-f]{6}$/}
});


Color.pre('save', function () {
    var tmpclr;
    if (this.rgb) {
        tmpclr    = new color.Color([this.rgb.r, this.rgb.g, this.rgb.b],'rgb' );
        this.cmyk = tmpclr.toCMYK( true )
        this.hex  = tmpclr.toHEX();

    } else if (this.cmyk) {
        var c    = this.cmyk;
        tmpclr   = new color.Color([c.c, c.m, c.y, c.k],'cmyk' );
        this.rgb = tmpclr.toRGB(true);
        this.hex = tmpclr.toHEX();
    } else if (this.hex) {
        tmpclr    = new color.Color( this.hex,'hex' );
        this.rgb  = tmpclr.toRGB(true);
        this.cmyk = tmpclr.toCMYK(true);
    }
},{collection:'catalog_color'});

module.exports = connection.commerce.model('Color', Color);
