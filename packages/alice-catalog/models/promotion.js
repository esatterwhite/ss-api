/*jshint laxcomma: true, smarttabs: true, node: true*/
'use strict';
/**
 * promotion.js
 * @module alice-catalog/models/promotion
 * @author Eric Satterwhite
 * @since 2.0.0
 * @requires mongoose
 * @requires alice-core/lib/db
 * @requires alice-stdlib/date
 */
var mongoose   = require('mongoose')
  , connection = require('alice-core/lib/db').connection
  , date       = require('alice-stdlib/date')
  , util       = require('util')
  , logger     = require('alice-log')
  , noop       = require('alice-stdlib/function').noop
  , isFunction = require('alice-stdlib/lang').isFunction
  , exceptions = require('../lib/exceptions')
  , OIDTest    = /^[0-9a-fA-F]{24}$/
  , Promotion
  ;

/**
 * Contains the resulting discount cost and final cost of applied promotion
 * @typedef {Object} DiscountPrice
 * @property {Number} cost
 * @property {Number} discount
 **/

/**
 * @constructor
 * @alias module:alice-catalog/models/promotion
 * @param {Object} fields
 * @param {String} fields.description A short description of the Promotion
 * @param {String} fields.type Denotes if the promotion is a flat rate discount or percentage. Can be one of `flat` or `percentage`
 * @param {String} fields.code The Promotion's unique code. Duplicates are not allowed
 * @param {Date} fields.start The start date of the promotion. If null, will be immediately active
 * @param {Date} fields.end The date in which the promotion will end. If null, The promtion will be active indefvinately.
 * @param {Date} fields.used If volatile, will be populated with the date in which the promotion was applied
 * @param {Boolean} fields.volatile If true, the promotion can only be used once
 * @param {Number} fields.rate The discount rate for the promotion. If the promotion is a percentace, rate should be an integer, e.g. 20 would mean 20% or .20
 * @example
var promo = new Promotion({
	code:'SS101'
	,rate:20
	,type:'percentage'
	,volatile:fale
});

promo.discount( 1995 ) // 399
promo.cost( 1995 ) // 1596
promo.calculated( 1995 )  // { discount: 399, cost:1596 }
promo.apply( 1995 )  // { discount: 399, cost:1596 }
 */
Promotion = new mongoose.Schema({
	/**
	 * @instance
	 * @name description
	 * @property {String} description A short description of the Promotion
	 **/
    description  : { type: String },
    /**
     * @instance
     * @name promotion_id
     * @property {String} promotion_id Promotion primary key as a string
     **/
    promotion_id : { type: String },
    /**
     * @instance
     * @name start
     * @property {Date} start The start date of the promotion. If null, will be immediately active
     **/
    start        : { type: Date    , default:null },
    /**
     * @instance
     * @name end
     * @property {Date} end The date in which the promotion will end. If null, The promtion will be active indefvinately.
     **/
    end          : { type: Date    , default:null },
    /**
     * @instance
     * @name used
     * @property {Date} used If volatile, will be populated with the date in which the promotion was applied
     **/
    used         : { type: Date    , default:null },
    /**
     * @instance
     * @name volatile
     * @property {Boolean} volatile If true, the promotion can only be used once
     **/
    volatile     : { type: Boolean , default:false },
    /**
     * @instance
     * @name rate
     * @property {Number} rate The discount rate for the promotion. If the promotion is a percentace, rate should be an integer, e.g. 20 would mean 20% or .20
     **/
    rate         : { type: Number  , required:true },
    /**
     * @instance
     * @name type
     * @property {String} type Denotes if the promotion is a flat rate discount or percentage. Can be one of `flat` or `percentage`
     **/
    type         : { type: String  , enum:['flat','percentage'], default:'percentage'},
    /**
     * @instance
     * @name code
     * @property {String} code The Promotion's unique code. Duplicates are not allowed
     **/
    code         : { type: String  , required:true, index:true, unique:true , dropDups: true }
},{collection:'catalog_promotion'});


/**
 * @instance
 * @name active
 * @property {Boolean} acitve A virtual property which calculates if the promotion is still active and still usaable
 **/
Promotion.virtual('active').get( function(){

	var now = date.utc()
		, start
		, end
		;

	if( this.volatile && this.used ){
		return false;
	}

	if(!this.start){
		return this.end ? date.utc( this.end ).isAfter( now ) : true;
	}
	return now.isAfter( date.utc( this.start ) ) && ( this.end ? now.isBefore( date.utc( end ) ) : true )
})


/**
 * Attempts to retrieve a promotion by id or promo code.
 * If the provided id looks like an Object Id it will attempt a lookup by id. Other wise, will look for the first promotion
 * With a matching code.
 * @static
 * @function
 * @name get
 * @memberof module:alice-catalog/models/promotion
 * @param {String} id The promotion primary key or {@link module:alice-catalog/models/promotion#code|Code} to look up
 * @param {Function} callback
 **/
Promotion.statics.get = function( id, callback ){
	var query  // instance of Promotion query
	  , isOID  // boolean value denoting if parameter looks like a mongo oid
	  , id     // alias to promotion_id request parameter
	  ;

	isOID = !!OIDTest.exec( id );
	query = isOID ? this.findById(id) : this.findOne({code:id});
	query.exec( callback || noop )
}

/**
 * retrievs all active promotions
 * @static
 * @function
 * @name live
 * @memberof module:alice-catalog/models/promotion
 * @param {Boolean} [all=false] if false, returns only active promotions. If true returns all promotions
 * @param {Function} callback
 **/
Promotion.statics.live = function( all, cb ){
	var query = this.find()
	   , now
	   ;

	// allow all to be optional and a
	// callback function in its place
	if( arguments.length === 1){
		if( isFunction( arguments[0] ) ){
			cb  = all;
			all = null;
		}
	}

	// UTC Now
	now = date.utc(new Date()).toDate();

	if( !all ){
		query
			.and([
				// find general & un-used single promos
				{$or: [{ volatile:false },{ volatile:true, used:null } ]}
				// And have no end date or the end date is in the future
			  , {$or: [{ end:null }, { end:{ $gt:now } } ]}
			   // And have no start date, or the start date is in the past
			  , {$or: [{ start:null }, { start:{ $lt:now } } ]}
			])
	}

	return cb ? query.exec( cb ) : query;
}

/**
 * Calculates the discount amount for a given cost, in cents
 * @method module:alice-catalog/models/promotion#discount
 * @param {Number} cost The numeric value ( cents ) to discount a discount amount for
 * @return {Number} discount the calcuated discount amount to be applied to the original cost
 **/
Promotion.methods.discount = function( cost ){
	return this.type == 'flat' ? ( this.rate ) : Math.round( ( cost * ( this.rate/100 ) ) )
}

/**
 * Calculates what the adjusted cost will be when the promotion is applied
 * @method module:alice-catalog/models/promotion#cost
 * @param {Number} cost The numeric value ( cents ) to discount a discount amount for
 * @return {Number} discount the calcuated discount amount to be applied to the original cost
 **/
Promotion.methods.cost = function( cost ){
	return this.type == 'flat' ? ( cost - this.rate ) : Math.round( Math.max( ( cost - this.discount( cost ) ), 0 ) );
}

/**
 * Applies the current promotion to the given price.
 * If the promotion is volitile, it will be marked as used as used
 * @method module:alice-catalog/models/promotion#apply
 * @param {Number} cost The cost, in cents, that the promotion is to be applied to
 * @param {Function} callback
 * @returns {DiscountPrice} prices
 **/
Promotion.methods.apply = function( cost, cb ){
	var prices = this.calculate( cost, cb )
	this.used = this.volatile ? new Date() : null;
	this.save(cb);
	return prices;
};

Promotion.methods.reset = function( cb ){
	logger.notice("resetting promotion %s", this.code )
	this.used = this.volatile ? null : this.used
	this.save( cb )
};


/**
 * Calculates a Discount price for a given cost without applying the promotion
 * @method module:alice-catalog/models/promotion#calculate
 * @param {Number} cost The cost, in cents, that the promotion is to be applied to
 * @param {Function} callback
 * @returns {DiscountPrice} prices
 * @throws {module:alice-catalog/exceptions.InactivePromotion}
 **/
Promotion.methods.calculate = function( cost, cb ){
	if( !this.active ){
		throw new exceptions.InactivePromotion({
			message:util.format("Promotion %s no longer active", this.code )
		})
		return null
	}

	return {
		discount: this.discount( cost )
		,cost: this.cost( cost )
	};
};

Promotion.pre('save', function( next ){
	this.promotion_id = this._id.toString();
	next();
});

module.exports = connection.commerce.model('Promotion', Promotion);
