/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Default command for the alice-catalog package
 * @module alice-catalog/commands/catalog
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires seeli
 * @requires util
 */

var cli = require( 'seeli' )
  , util = require( 'util' )
  ;

module.exports = new cli.Command({
	description:"Default command for alice-catalog package"
	,usage:[
		cli.bold('Usage: ') + 'alice catalog --help'
	  , cli.bold('Usage: ') + 'alice catalog --no-color'
	  , cli.bold('Usage: ') + 'alice catalog -i'
	]

	,flags:{
		'default':{
			type: Boolean
			,description:"Enable the default"
			,default:true
			,required:false
		}
	}
	/**
	 * This does something
	 * @param {String|null} directive a directive passed in from the cli
	 * @param {Object} data the options collected from the cli input
	 * @param {Function} done the callback function that must be called when this command has finished
	 * @returns something
	 **/
	,run: function( cmd, data, done){

		done(/* error */ null, /* output */ 'success')
	}
});
