/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Constant immutable values used through out the statem
 * @module alice-catalog/lib/constants
 * @author Eric Satterwhite
 * @since 0.1.0
 */

/**
 * Value used to reference Mascot images values for decal personalization
 * @summary Art inclusion of tenant mascot
 * @static
 * @constant {STRING} ART_MASCOT
 **/
exports.ART_MASCOT          = 'arttm';
/**
 * Value used to reference activity images for decal personalization
 * @summary Art inclusion of activity image
 * @static
 * @constant {STRING} ART_ACTIVITY
 **/
exports.ART_ACTIVITY        = 'artad';

/**
 * Value used to reference a text value for tenant_name
 * @static
 * @constant {STRING} TEXT_TENANT_NAME
 **/
exports.TEXT_TENANT_NAME    = 'txttnm';
/**
 * Value used to reference a text value for tenant_acronym
 * @static
 * @constant {STRING} TEXT_TENANT_ACRONYM
 **/
exports.TEXT_TENANT_ACRONYM = 'txttacro';
/**
 * Value used to reference a text value for the type of tenant for a decal personalization
 * @static
 * @constant {STRING} TEXT_TENANT_TYPE
 **/
exports.TEXT_TENANT_TYPE    = 'txtttyp';
/**
 * Value used to reference a text value for a tenant's logo name personalization
 * @static
 * @constant {STRING} TEXT_LOGO_NAME
 **/
exports.TEXT_LOGO_NAME      = 'txttl';
/**
 * Value used to reference a text value for a tenant's city in decal personalization
 * @static
 * @constant {STRING} TEXT_TENANT_CITY
 **/
exports.TEXT_TENANT_CITY    = 'txttcty';
/**
 * Value used to reference a text value for a tenant's state in decal personalization
 * @static
 * @constant {STRING} TEXT_TENANT_STATE
 **/
exports.TEXT_TENANT_STATE   = 'txttst';
/**
 * Value used to reference a text value for a tenant's country in decal personalization
 * @static
 * @constant {STRING} TEXT_TENANT_COUNTRY
 **/
exports.TEXT_TENANT_COUNTRY = 'txtctry';
/**
 * Value used to reference a text value for the name of an activityfor decal personalization
 * @static
 * @constant {STRING} TEXT_ACTIVITY_NAME
 **/
exports.TEXT_ACTIVITY_NAME  = 'txtanm';
/**
 * Value used to reference a text value for date_one
 * @static
 * @constant {STRING} TEXT_DATE_ONE
 **/
exports.TEXT_DATE_ONE       = 'date1';
/**
 * Value used to reference a text value for date_two
 * @static
 * @constant {STRING} TEXT_DATE_TWO
 **/
exports.TEXT_DATE_TWO       = 'date2';

/**
 * Value used to reference an items primary color for customization
 * @static
 * @constant {STRING} COLOR_PRIMARY
 **/
exports.COLOR_PRIMARY       = 'clrp';
/**
 * Value used to reference an items seconday color for customization
 * @static
 * @constant {STRING} COLOR_SECONDARY
 **/
exports.COLOR_SECONDARY     = 'clrs';
/**
 * Value used to reference an items tertiary color for customization
 * @static
 * @constant {STRING} COLOR_TERTIARY
 **/
exports.COLOR_TERTIARY      = 'clrt';
/**
 * Value used to denot the color black for item customization
 * @static
 * @constant {STRING} COLOR_BLACK
 **/
exports.COLOR_BLACK         = 'clrb';
/**
 * Value used to denot the color white for item customization
 * @static
 * @constant {STRING} COLOR_WHITE
 **/
exports.COLOR_WHITE         = 'clrw';
/**
 * Value used to denot the color grey for item customization
 * @static
 * @constant {STRING} COLOR_GREY
 **/
exports.COLOR_GREY          = 'clrg';

/**
 * to denote size of something Short in size
 * @static
 * @constant {STRING} Used
 **/
exports.SIZE_SHORT          = 'SHRT'

/**
 * Used to denote size of something Medium in size
 * @static
 * @constant {STRING} SIZE_MEDIUM
 **/
exports.SIZE_MEDIUM         = 'MED'

/**
 * Used to denote size of something Long in size
 * @static
 * @constant {STRING} SIZE_LONG
 **/
exports.SIZE_LONG           = 'LONG'

/**
 * The Mimum value used for text based decal inclusion rules for tenants and mascots
 * @static
 * @constant {Number} LENGTH_MIN
 **/
exports.LENGTH_MIN =3
/**
 * The  value used as the cutoff point for text that qualifies as SHORT in text based decal inclusion rules for tenants and mascots
 * @summary Cutoff point for short length text
 * @static
 * @constant {Number} LENGTH_SHORT
 **/
exports.LENGTH_SHORT = 8
/**
 * The value used as the cutoff point for text that qualifies as MEDIUM in text based decal inclusion rules for tenants and mascots
 * @summary Cutoff point for medium length text
 * @static
 * @constant {Number} LENGTH_MEDIUM
 **/
exports.LENGTH_MEDIUM = 15
/**
 * The value used as the cutoff point for text that qualifies as LONG in text based decal inclusion rules for tenants and mascots
 * @summary Cutoff value for long text
 * @static
 * @constant {Number} LENGTH_LONG
 **/
exports.LENGTH_LONG = 36

Object.freeze( exports )
