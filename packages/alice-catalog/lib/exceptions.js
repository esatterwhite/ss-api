/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Exception types for the catalog package
 * @module alice-catalog/lib/exceptions
 * @author Eric Satterwhite
 * @since 2.0.0
 * @requires alice-stdlib/exception
 * @requires alice-stdlib/class
 */

var Exception = require( 'alice-stdlib/exception' )
  , Class = require( 'alice-stdlib/class' )
  ;


/**
 * General catalog exception type
 * @class module:alice-catalog/exceptions.CatalogException
 */
exports.CatalogException = new Class(/* @lends module .THING.prototype */{
	inherits:Exception
	,options:{
		type:'catalog_exception'
		,code:6000
	}
});


/**
 * Use of an inactive promotion was attempted
 * @class module:alice-catalog/exceptions.InactivePromotion
 */
exports.InactivePromotion = new Class(/* @lends module .THING.prototype */{
	inherits:Exception
	,options:{
		type:'inactive_promotion'
		,code:6001
	}
});
