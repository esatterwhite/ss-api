/*jshint node:true, laxcomma: true, smarttabs: true, camelcase:false, node: true */
'use strict';
/**
 * Helper methods used to interpolate catalog item permutations based on a product, decal, tenant and mascots
 * @module alice-catalog/lib/interpolate
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires util
 * @requires async
 * @requires boom
 * @requires alice-log
 * @requires alice-stdlib/url
 * @requires alice-catalog/lib/colors
 * @requires alice-catalog/lib/item/base
 * @requires alice-catalog/lib/constants
 * @requires alice-catalog/lib/item/product
 * @requires alice-catalog/lib/item/activity
 * @requires alice-catalog/models/tenant
 * @requires alice-catalog/models/product
 * @requires alice-catalog/models/decal
 * @requires alice-catalog/models/mascot
 * @requires alice-catalog/models/activity
 */

var util                 = require( 'util' )
	, async                = require( 'async' )
	, Boom                 = require( 'boom' )
	, logger               = require( 'alice-log' )
	, url                  = require( 'alice-stdlib/url' )
	, colours              = require( './colors' )                // internal colours lib for interpolation of colors
	, catalogItem          = require( './item/base' )             // interal catalogItem for generating image layer definitions for S7
	, constants            = require( './constants' )             // internal constant values
	, ProductItem          = require( './item/product' )
	, ActivityItem         = require( './item/activity' )
	, WidgetItem           = require( './item/widget' )
	, Tenant               = require( '../models/tenant' )        // Tenant Model
	, Product              = require( '../models/product' )       // Product model
	, Decal                = require( '../models/decal' )         // Decal model
	, Mascot               = require( '../models/mascot' )        // Mascot Model
	, Activity             = require( '../models/activity' )      // Activity Model
	, ART_MASCOT           = constants.ART_MASCOT                 // shortcut constant value for ART_MASCOT
	, TEXT_ACTIVITY_NAME   = constants.TEXT_ACTIVITY_NAME         // shortcut constant value for TEXT_ACTIVITY_NAME
	, ART_ACTIVITY         = constants.ART_ACTIVITY               // shortcut constant value for ART_ACTIVITY
	, EMPTY_ARRAY          = []                                   // static empty array for fallback value
	, NULL_QUERY           = {'personalization.artad':false, 'personalization.txtanm':false}
	;

function interpolateProducts( tenant, products, decal, additions ) {

	var catalogItems = []
		, decalColors
		, colors
		;

	logger.debug('interpolating products for tenant %s', tenant.name )
	for(var p = 0, len=products.length; p<len; p++ ){
		colors = colours.colorMatch(products[p], tenant);
		for (var j = 0; j < colors.length; j++) {
			if (decal){
				decalColors = colours.generateDecalColors(colors[j], tenant, decal);
			}

			catalogItems.push(
				new ProductItem({
					product: products[p]
					, color: colors[j]
					, decal: decal
					, decalAdditions: additions
					, tenant: tenant
					, decalColors: decalColors
				})
			);
		}
	}

	return catalogItems;
};
function interpolateDoubles(tenant, mascot, decals, products, category, cb) {

	var catalogItems = [];
	var fluffArray = function (big, small) {
		for (var i = 0, origLen = small.length, len = big.length - small.length; i < len; i++) {
			 small.push(small[i%origLen]);
		}
	}

	if (decals.length > products.length) {
		fluffArray(decals, products);
	}
	else if (products.length > decals.length) {
		fluffArray(products, decals);
	}
	async.eachSeries(decals, function ( decal, decal_cb ) {

		var decalAdditions
		, personalization
		, inclusions
		, decalInclusions
		, textInclusions
		;

		decalAdditions      = [{}];
		personalization     = decal.personalization || {};
		inclusions          = decal.inclusions || {};
		decalInclusions     = inclusions.decals || EMPTY_ARRAY;
		textInclusions      = inclusions.activities || EMPTY_ARRAY;

		if( decal.mascotFilter && tenant.lrg.isLicensed ){
			logger.warning('tenant %s is lrg license, and decal %s has a filter. Excluding fron catalog', tenant.longName, decal.s7id)
			return decal_cb();
		}
		var product = products.pop();
		var color = colours.colorMatch(product, tenant)[0];
		var decalColors = product ? colours.generateDecalColors(color, tenant, decal) : false;
		decal.allActivities(function(e, activities ){
			activities.forEach(function ( addition, idx ) {
				var  adtn = addition.toObject ? addition.toObject() : addition
				, includeArt = personalization[ART_ACTIVITY]  // boolean denoting if activity art can be included with the decal
				, includeText = personalization[TEXT_ACTIVITY_NAME] // boolean denoting if the activity name should be included
				, activityArt  // The s7id of the art file associated for the given activity
				, activityName // The category name of the specified activity
				;
				if (!includeArt && !includeText) adtn.category = "none";
				if (category && adtn.category != category) {
					 return;
				}
				adtn.arttm = mascot;
				// The catalog iten is all or nothing.
				// either all conditions pass, or we don't include it
				var dcl = decal.toObject()
				dcl.activities = dcl.activities.length ? dcl.activities : activities

				catalogItems.push(
					new WidgetItem({
						product: product
						, color: color
						, decalAdditions: adtn
						, decal: dcl
						, tenant: tenant
						, decalColors: decalColors
						, fetching: 'widget'
					})
				);
			});
			decal_cb()
		})
	}, function( err ){
		logger.debug("interpolate decals done")
		cb && cb( err, catalogItems )
	});
};
function interpolateDecals( tenant, mascot, decals, color, product, cb ) {
	var catalogItems = [];
	color = product ? color : 'FFFFFF';
	async.eachSeries(decals, function ( decal, decal_cb ) {

		var decalAdditions  // decal personalaization add on params for scene 7
			, personalization // local reference to decal's personalization data
			, x               // loop counter
			, len             // loop limit
			;

		decalAdditions      = [{}];
		personalization     = decal.personalization || {};

		if( decal.mascotFilter && tenant.lrg.isLicensed ){
			logger.warning('tenant %s is lrg license, and decal %s has a filter. Excluding fron catalog', tenant.longName, decal.s7id)
			return decal_cb();
		}
		decal.allActivities(function(e, activities ){

			for( x=0, len=activities.length; x< len; x++ ){
				var includeArt   = personalization[ART_ACTIVITY]                      // boolean denoting if activity art can be included with the decal
					, includeText = personalization[TEXT_ACTIVITY_NAME]                // boolean denoting if the activity name should be included
					, addition    = activities[x]
					, adtn        = addition.toObject ? addition.toObject() : addition // object version of the activity. It may have come from cache
					, activityArt                                                      // The s7id of the art file associated for the given activity
					, activityName                                                     // The category name of the specified activity
					, decalColors                                                      // interpolated colors from apecified color for a decal
					, dcl                                                              // plain object version of decal model instance
					;

				adtn.arttm  = mascot;
				decalColors = colours.generateDecalColors(color, tenant, decal);
				dcl         = decal.toObject()
				dcl.activities = dcl.activities.length ? dcl.activities : activities

				catalogItems.push(
					new ActivityItem({
						product        : product
					 , color          : color
					 , decalAdditions : adtn
					 , decal          : dcl
					 , tenant         : tenant
					 , decalColors    : decalColors
					})
				);
			}
			decal_cb()
		})
	}, function( err ){
		logger.debug("interpolate decals done")
		cb && cb( err, catalogItems )
	});
};



/**
 * Generates a catalog of items for each product with the specified decal
 * @param {String} tenant_id Id of the tenant to generate a product catalog for
 * @param {String} decal_id
 * @param {Object} options
 * @param {Function} [callback]
 */
exports.products = function(tenant_id, decalId, additions, callback){
	async.parallel([
		function findtenant( cb ){
			Tenant.findById(tenant_id, function(err, tenant){
				cb(
					!tenant ? new Boom.notFound( util.format( 'No Tenant with ID %s found', tenant_id ) ) : err ? Boom.wrap( err ) : err
					, tenant
				);
			});
		}
		, function( cb ){
			Product
				.find()
				.exec( cb )
		}
		, function finddecals(cb ){
			return decalId ? Decal.findBySceneId( decalId, cb )	: cb( null, null );
		}
	], function( err, results ){
		var items;
		if( err ){
			return callback && callback( err, null );
		}

		additions = additions || {};
		additions = typeof additions === 'string' ? JSON.parse( additions ) : additions;
		items     = interpolateProducts(results[0], results[1], results[2], additions );
		return callback && callback( err, {items: items, tenantInfo: results[0] } );
	});
};

/**
 * Generates a catalog of item permutations based on tenant, product and activity
 * @param {String} tenantId Valid id for a tenant record
 * @param {String} productId valid id for a product record
 * @param {Object} options options used to alter the activity catalog generation
 * @param {String} options.activity The name of the activity to generate a catalog for
 * @param {String} options.color The hex string of base to use color to generate colors for the catalog
 * @param {Function} callback A call back function to be used
 */
exports.activities = function( tenant_id, product_id, options, callback ){
	var key = [tenant_id, product_id || "", url.qs.stringify( options ||{} ) ].join(':')

	async.waterfall([
		function findtenant(cb){
			Tenant.findById(tenant_id, function( err, tenant ){
				cb(
					err ? new Boom.notFound( util.format( 'No Tenant with ID %s found', tenant_id ) ) : err
					, tenant
				);
			});
		}

		,function findmascot( tenant, cb ){
			Mascot.idForTenant(tenant, function(err, mascot ){
				cb( err, tenant, mascot );
			});
		}
	], function( err, tenant, mascot_id ){
		async.parallel({
			product: function findproduct( cb ){
				if( product_id ){
					Product.findById( product_id, function( err, product ){
						if ( !product ) {
							return cb && cb(
								new Boom.notFound(
									util.format( 'No Product with ID %s found', product_id )
								)
							);
						}
						cb( err, product );
					});
					return;
				}
				return cb( null, null );
			}
			, decals: function finddecals( cb ){
				var query = Decal.front().populate({
					path:'activities'
				});

				// if no category is specified, we look for decals
				// that have no activity art & no activity text options
				query = !options.category
							 ?	query.where( NULL_QUERY )
							 : query.where({ categories:{$elemMatch:{$eq:options.category}} })
        var custom = (options.getCustom) ? Boolean(options.getCustom) : false;
				query.exec(function( err, decals ){
					decals = decals.filter( function( decal ){
						return decal.mascotAllowed(tenant.mascot.name, mascot_id) && decal.sizeAllowed('tenant', tenant.name) && decal.sizeAllowed('mascot', tenant.mascot.name ) && decal.custom == custom
					})
					cb( err, decals )
				});
			}

		}, function( err, results ){

			if( err){
				return callback && callback( err, null );
			}
			interpolateDecals(tenant, mascot_id, results.decals, options.color, results.product, function( err, items  ){
				var payload = {
						items: items
					, tenantInfo: tenant
				};

				callback && callback( err, payload );

			})
		});
	});
};

exports.widgets = function (tenant_id, options, callback) {
	async.waterfall([
		function findtenant(cb){
			Tenant.findById(tenant_id, function( err, tenant ){
				cb(
					err ? new Boom.notFound( util.format( 'No Tenant with ID %s found', tenant_id ) ) : err
					, tenant
				);
			});
		}

		,function findmascot( tenant, cb ){
			Mascot.idForTenant(tenant, function(err, mascot ){
				cb( err, tenant, mascot );
			});
		}
	], function( err, tenant, mascot_id ){
		async.parallel({
			product:  function( cb ){
				var findObj = {};
				if (options.productIds) {
						var pids = options.productIds.split(","),
								pidArray = [];
					for (var i = 0; i < pids.length; i++) {
						pidArray.push(pids[i]);
					}
          findObj = {'_id': {'$in': pidArray}};
				}
			Product
			.find(findObj)
			.exec( cb )
		}
			, decals: function finddecals( cb ){
				if (options.decalIds) {
					var query = Decal.bySceneIdArray( options.decalIds, options.activity ).populate({
						path:'activities'
					})
				} else {
					var query = Decal.front( options.activity ).populate({
						path:'activities'
					})
				}
				query.exec(function( err, decals ){
					decals = decals.filter( function( decal ){
						return decal.mascotAllowed(tenant.mascot.name, mascot_id) && decal.sizeAllowed('tenant', tenant.name) && decal.sizeAllowed('mascot', tenant.mascot.name ) && !decal.custom
					})
					cb( err, decals )
				});
			}

		}, function( err, results ){
			if( err){
				return callback && callback( err, null );
			}
			interpolateDoubles(tenant, mascot_id, results.decals, results.product, options.category, function( err, items  ){
				callback && callback( err, {
					items: items
					, tenantInfo: tenant
				});
			})
		});
	});
};

// FIXME: this api accepts a full decal instance for the sake of lookup up its personalization bits.
// could just take an ID
/**
 * generates a hash of viable additions for a specific decal
 * @param {module:alice-catalog/models/decal} decal a decal instance to generate additions for
 * @param {module:alice-catalog/models/tenant}
 * @param {Object} options inclusion options for the decal
 * @param {String} options.txtanm The name of the tenant to include
 * @param {String} options.artad the id of the mascot art to use
 * @param {Function} callback A callback function to be executed upon completion
 */
exports.additions = function( decal, tenant, params, callback ){
	async.waterfall([
		function checkMascots( cb ){
			var additions = {} // return data for additions values
			, mascots   = [] // default return data for mascots
			;

			if (decal.personalization.hasOwnProperty( ART_MASCOT ) ) {
				return Mascot.idForTenant(tenant, function( e, mascot ){
					additions[ART_MASCOT] = mascot;
					cb( null, additions, mascots );
				});
			}
				cb( null, additions, mascots );
		}
		], function( err, additions ){
			// FIXME: This is duplicated logic from catalogItem.
			// this needs to be moved into a separate module
				additions[constants.TEXT_LOGO_NAME] = tenant.mascot.name;
				additions[constants.TEXT_TENANT_NAME] = tenant.name || tenant.longName.trim();
				additions[constants.TEXT_TENANT_CITY] = tenant.city;
				additions[constants.TEXT_TENANT_STATE] = tenant.stateName;
				additions[constants.TEXT_TENANT_ACRONYM] = tenant.acronym;
				additions[constants.TEXT_TENANT_TYPE] = !tenant.name ? " " : tenant.longName.replace( tenant.name, '' ).trim()
				if (params[TEXT_ACTIVITY_NAME]) {
						additions[TEXT_ACTIVITY_NAME] = params[TEXT_ACTIVITY_NAME];
				}
				if (params[ART_ACTIVITY]) {
					additions[ART_ACTIVITY] = params[ART_ACTIVITY];
					if (!additions[TEXT_ACTIVITY_NAME]) {
						// grab a single art
						Activity.findOne({s7Id:additions[ ART_ACTIVITY ] }, function (err, activity) {
								additions[TEXT_ACTIVITY_NAME] = activity.category;
								callback(err, additions);
						});
					}
					else {
						callback(null, additions);
					}
				} else {
					callback(null, additions);
				}
		});
}
