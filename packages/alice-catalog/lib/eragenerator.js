var Tenant = require('alice-catalog/models/tenant'),
    staticData = require('../fixtures/sortedMascots'),
    catalogItem = require('alice-catalog/lib/catalogItem'),
    fs = require('fs');



var results;
var templates = {
    "all": [{"name":"ss0053","s7id":"ss_0053","personalization":["date1", "arttm"],"colors":{"clrp":"1","clrs":"1","clrt":"0"},"inclusions":false},
            {"name":"ss0052","s7id":"ss_0052","personalization":["arttm"],"colors":{"clrp":"1","clrs":"0.5","clrt":"0"},"inclusions":false},
            {"name":"ss0056","s7id":"ss_0056","personalization":["arttm"],"colors":{"clrp":"1","clrs":"1","clrt":"0"},"inclusions":false},
            {"name":"ss0057","s7id":"ss_0057","personalization":["arttm"],"colors":{"clrp":"1","clrs":"1","clrt":"0"},"inclusions":false}
           ],
    "mascotName": [{"name":"ss0017","s7id":"ss_0017","personalization":["txttnm", "arttm"],"colors":{"clrp":"1","clrs":"1","clrt":"0"},"inclusions":false}],
    "mascotAndSchoolName": [{"name":"ss0055","s7id":"ss_0055","personalization":["txttnm", "arttm", "txttl", "txtttyp"],"colors":{"clrp":"1","clrs":"1","clrt":"0"},"inclusions":false}]
};
(function () {
    for (var k in templates) {
        var templateArr = templates[k];
        for (var i = 0; i < templateArr.length; i++) {
            var ph = {};
            for (var j = 0; j < templateArr[i].personalization.length; j++) {
                ph[templateArr[i].personalization[j]] = true;
            }
            templates[k][i].personalizationHash = ph;
        }
    }
})();
function iterateThroughData(callback) {
    var l = 0, keyArray = [];
    results = [];
    for (var j in staticData) {
        keyArray.push(j);
    }
    console.log("Going to generate: " + keyArray.length);
    var iterateCallback = function () {
        l++;
        console.log("Generated: " + l + "/" + keyArray.length);
        if (l <  keyArray.length) generateEra(staticData[keyArray[l]], keyArray[l], iterateCallback);
        else {
            var msg =  results.join("\r\n");
            fs.writeFileSync("data/era.tsv", msg);
            callback(false, msg);
        }
    }
    generateEra(staticData[keyArray[l]], keyArray[l], iterateCallback);
}
function generateEra (mascotData, mascotName, callback) {
    if (mascotData.trash) {
        callback();
        return;
    }
    var searchParams = {"lrg.licenseName": mascotName};
    var t = Tenant.find(searchParams).exec(function (err, foundTenants) {
        if (foundTenants.length > 1){
            console.log("Too many tenants: " + JSON.stringify(searchParams));
            callback();
        }
        else if (foundTenants.length < 1 || !foundTenants)
        {
            console.log("Not matched: " + mascotName);
            callback();
        } else {
            var tenant = foundTenants[0];
            var identifyingData = [tenant.longName, tenant.legacySystemId];
            var templatesToGenerate = [], returnTemplates = [];
            templatesToGenerate = templatesToGenerate.concat(templates.all);
            if (!mascotData.hasMascot && tenant.mascot) {
                templatesToGenerate = templatesToGenerate.concat(templates.mascotName);
                identifyingData.push(" ");
            } else {
                identifyingData.push("X");
            }
            if (!mascotData.hasMascot && !mascotData.hasName && tenant.mascot) {
                templatesToGenerate = templatesToGenerate.concat(templates.mascotAndSchoolName);
                identifyingData.push(" ");
            } else {
                identifyingData.push("X");
            }
            var additions = {"arttm": mascotName, "date1": "2015"};
            var colors = {clrp: tenant.primaryColor.hex, clrs: tenant.secondaryColor.hex, clrt: '000000', clrb: '000000', clrw: 'ffffff'};
            var colorsFor55 = {clrp: tenant.primaryColor.hex, clrs: tenant.primaryColor.hex, clrt: '000000', clrb: '000000', clrw: 'ffffff'};
            for (var i = 0; i < templatesToGenerate.length; i++) {
                var c = colors;
                if (templatesToGenerate[i].name == "ss0055") c = colorsFor55;
                var ci = new catalogItem({decalAdditions: additions,  decal: templatesToGenerate[i], tenant: tenant, decalColors: c}, "activities");
                returnTemplates.push(ci.printView);
            }
            while (returnTemplates.length < 6) {
                   returnTemplates.push("X");
            }
            results.push(identifyingData.concat(returnTemplates).join("\t"));
            callback();
        }
        });
    }

module.exports = function Get(params, callback) {
    if (params.secret != "debug") return callback("No Secret", false);
    else {
        iterateThroughData(callback);
        //callback(false, "Society is fundamentally flawed");
    }

}
