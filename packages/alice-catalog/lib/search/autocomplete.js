/*jshint laxcomma: true, smarttabs: true, node: true*/
'use strict';
/**
 * Proxy to Tenant index in elastic search
 * @module alice-catalog/lib/search/autocomplete
 * @author Ryan Fisch
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires events
 * @requires alice-stdlib/class
 */

var events              = require( 'events' )                       // Node events module
  , conf                = require( 'alice-conf' )                   // standard config module
  , typecast            = require('alice-stdlib/string').typecast
  , Class               = require( 'alice-stdlib/class' )           // standard Class module
  , Options             = require( 'alice-stdlib/class/options' )   // standard Class options module
  , Parent              = require( 'alice-stdlib/class/parent' )    // standard Class Parent Module
  , ElasticSearchClient = require( 'elasticsearchclient' )          // Elastic search client lib
  , url                 = require( 'alice-stdlib/url' )             // standard url lib
  , search            = conf.get( 'search' )                    // search options
  , client
  , AutoComplete
  , slug
  ;

slug = function slug( str ){
    return ( str || '' ).toLowerCase().replace(/ /g, '_')
};

/**
 * @constructor
 * @alias module:alice-catalog/lib/search/autocomplete
 * @fires module:alice-catalog/lib/search/autocomplete#error
 * @fires module:alice-catalog/lib/search/autocomplete#done
 * @param {Object} options
 * @param {Object} [options.weights]
 * @param {String[]} [options.weights.state=["name^3", "mascot_name^1", "city^7", "state^10"]] sorting weights to use when searching for states
 * @param {String[]} [options.weights.city=["name^3", "mascot_name^1", "city^10", "state^7"]] sorting weights to use when searching for city
 * @param {String[]} [options.weights.tenants=["longName^20", "mascot_name", "city^3", "state^10", "zip^10"]] sorting weights to use when searching for tenanats
 * @param {String} options.host elastic search host address
 * @param {Number} options.port elastic search host port to connect to
 * @param {Boolean} options.secure Set to true to make a connection using https
 * @param {Object} auth Authentication credentials
 * @param {String} auth.username username to authenticate connection with
 * @param {String} auth.password Password to validate account with
 */
AutoComplete = new Class({
    inherits: events.EventEmitter
  , mixin:[ Options, Parent ]
  , options:{
        weights:{
             state : ["name^3", "mascot.name^1", "city^7", "state^10"]
            ,city  : ["name^3", "mascot.name^1", "city^10", "state^7"]
            ,tenant: ["longName^20", "mascot.name^1", "city^3", "state^10", "zip^10"]
        }
        ,host: search.host
        ,port: parseInt( search.port, 10 )
        ,secure:!!typecast(  search.secure )
        ,auth: search.auth
    }
    , constructor: function( options ){
        this.setOptions( options );
        this.client = new ElasticSearchClient( this.options )
    }

    /**
     * Creates and executes a search query through elastic search
     * @param {String} term The term to search for
     * @param {String} type The tenant type to search for
     * @param {Number} [take=100] The number of results to return at per request
     * @param {Number} [page=1] The starting number of the page of results to return
     **/
    , search: function search(term, type, scope, take, page){
        var weights = this.options.weights
          , qryObj
          ;

        // Make sure we have paging vars
        page = page || 1;
        take = take || 100;

        //TODO: RF 20140430 - Refine Query for more relevant results
        qryObj = {
            "from" : (page - 1) * take, "size" : take,
            "query": {
                "filtered": {
                    "query": {
                        "multi_match": {
                            "query": term,
                            "analyzer": "standard",
                            "type": "cross_fields",
                            "fields": weights[scope] ? weights[scope] : weights['tenant'],
                            "tie_breaker": 0.3,
                            "operator": "and"
                        }
                    }
                }
            }
        };

        // FIXME: bandaid until we reload tenant data: don't show tenants with no tenant type.
        qryObj.query.filtered.filter = { "or": [{"term": {"tenantType": "k12"}},{"term": {"tenantType": "collegiate"}}]};

        this.client.search('idx-tenants', 'tenant', qryObj)
            .on('data', function (data) {
                var response = { "group": type, "scope": scope, "totalResults": 0, "results": [] };
                if (data.length > 0) {
                    var objData = JSON.parse(data);
                    response.totalResults = objData.hits.total;

                    if (objData.hits.hits.length > 0) {
                        for(var i = 0; i < objData.hits.hits.length; i++){
                            var t = objData.hits.hits[i]._source;
                            var mascotName = t.mascot.name;
                            if (!mascotName) mascotName = "default"; // Empty mascot names throw an error
                            response.results.push({
                                "display": t.longName
                              , "url":  url.join( // TODO: t.url should be used, but is not currently correct
                                    "/"
                                    , t.tenantType
                                    , t.country
                                    , t.state
                                    , slug( t.city )
                                    , t._id
                                    , slug( t.longName) + "-" + slug( mascotName )
                                )
                            });
                        }
                    }
                }

                /**
                 * @name module:alice-catalog/lib/search/autocomplete#done
                 * @event
                 * @param {Object} esponse
                 **/
                this.emit('done', response);
            }.bind( this ))
            .on('error', function (error) {
                /**
                 * @name error
                 * @event
                 * @param {Error} error
                 **/
                this.emit('error', error);
            }.bind( this ))
            .exec();
    }
});

module.exports = AutoComplete;
