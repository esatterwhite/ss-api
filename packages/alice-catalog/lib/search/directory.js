/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Provides A base tenant directory.js
 * @module alice-core/lib/search/directory
 * @author Eric Satterwhite
 * @since 1.9.0
 * @requires events
 * @requires alice-conf
 * @requires alice-log
 * @requires alice-stdlib/class
 * @requires alice-stdlib/class/options
 */

var events  = require( 'events' )                     // node events module
  , conf    = require( 'alice-conf' )                 // alice conf loader
  , logger  = require( 'alice-log' )                  // alice logging module
  , Class   = require( 'alice-stdlib/class' )         // alice Class implementation
  , Options = require( 'alice-stdlib/class/options' ) // Class options mixin
  , interpolate = require( 'alice-stdlib/string' ).interpolate // Class options mixin
  , Tenant  = require('alice-catalog/models/tenant' )           // Tenant Schema
  ;

/**
 * The provides basic functionality to search tenants with specific critera
 * @constructor
 * @alias module:alice-core/lib/search/directory
 * @param {Object} options instance configuration options
 * @param {String} [options.tenant=null] The tenant scope of the instance search
 * @param {String} [options.country='US'] The country scope of the instance search
 * @mixes module:alice-stdlib/class/options
 * @fires module:alice-core/lib/search/directory#error
 * @fires module:alice-core/lib/search/directory#data
 * @example var x = new Directory({
    tenant:'k8'
    ,country:'US'
 });
 */
module.exports = new Class(/* @lends module:alice-core/lib/search/directory.prototype */{
    inherits: events.EventEmitter
    ,mixin:[ Options ]
    ,options:{
        type:null
        ,country:'US'
    }

    , constructor: function( options ){
        this.setOptions( options )
    }
    /**
     * Returns list of states for currently registerd tenants in a given country. Was StateListingByCountry
     * @method module:alice-core/lib/search/directory#states
     * @param {String} tenant The type of tenant to pin the query to ( k8, hs, college )
     * @param {String} [county='US'] The Country to pin the query to
     * @param {Function} [callback] Callback to execute when data is returned. Method Signature( err, data )
     **/
    , states: function states(type, country, callback ){
        var that = this;
        var opts = this.options
        logger.info('looking up states for %s [ %s ]', country, type)
        type = ( type || opts.type ).toLowerCase();
        country = ( country || opts.country ).toUpperCase();


        Tenant.aggregate([
            {
              $project:{
                    state:{$toUpper:'$state'}
                    ,country:"$country"
                    ,tenantType:'$tenantType'
              }
            },{
                $match:{
                    country:country
                  , tenantType:type
                  , state:{$ne:""}
                }
            },{
                $project:{
                    state:'$state'
                    ,letter:{$substr:["$state",0,1]}
                    ,oid:'$_id'
                    ,tenantType:'$tenantType'
                    ,country:"$country"

                }
            },{
              $group:{
                  _id:{letter:'$letter', state:'$state'}
                  ,tenantType:{$first:'$tenantType'}
                  ,country:{$first:'$country'}
               }
            },{
              $project:{
                  state:'$_id.state'
                  ,country:'$country'
                  ,tenantType:'$tenantType'
              }
            },{
              $sort:{state:1}
            },{
                $group:{
                   _id:{letter:'$_id.letter'}
                    ,items:{
                        $push:{
                            name:'$_id.state'
                            ,country:'$country'
                            ,tenantType:'$tenantType'
                            ,url:{$concat:['/directory', '/', '$tenantType', '/', '$country', '/', '$_id.state']}
                        }
                    }
                }
            },{
               $project:{
                    letter:'$_id.letter'
                   , items:'$items'
                   , _id:0
               }
            },{$sort:{letter:1}}
        ],this.end.bind( this, callback ) );
    }

    /**
     * Returns a list of cities for all of the current registered tenants. was CityListingByState
     * @method module:alice-core/lib/search/directory#cities
     * @param {String} type
     * @param {String} [country='US']
     * @param {String} state
     * @param {Function} [callback] Callback to execute when data is returned. Method Signature( err, data )
     **/
    , cities: function cities(type, country, state, callback ){
        var that = this;
        var opts = this.options
        type = ( type || opts.type ).toLowerCase();
        country = ( country || opts.country ).toUpperCase();
        logger.info('looking up cities for %s,%s [ %s ]', country, state, type)

        Tenant.aggregate([
            {
                $match:{
                    country:country
                  , tenantType:type
                  , state: state
                  , city:{$ne:""}
                  , name:{$ne:""}
                }
            },{
              $project:{oid:'$_id', city:"$city", state:"$state", tenantType:'$tenantType', country:'$country'}
            },{
              $group:{
                  _id:{ city:'$city', state:'$state'}
                  ,tenantType:{$first:'$tenantType'}
                  ,country:{$first:'$country'}
               }
            },{
                $project:{
                    city:'$_id.city'
                    , state:'$_id.state'
                    ,_id:0
                    , letter:{$substr:['$_id.city',0,1]}
                    , tenantType:'$tenantType'
                    , country:"$country"
                }
            },{
              $sort:{city:1}
            },{
              $group:{
                    _id:{letter:{$substr:['$city',0,1]}}
                    ,items:{
                        $push:{
                            name:'$city'
                            , state:'$state'
                            , tenantType:'$tenantType'
                            , country:'$country'
                            ,url:{$concat:['/directory', '/', '$tenantType', '/', '$country', '/', '$state', '/', '$city']}

                        }
                    }
              }
            },{
                $project:{
                    letter:'$_id.letter'
                    ,items:'$items'
                    ,_id:0
                }
            },{$sort:{letter:1}}
        ],this.end.bind( this, callback  ) );
    }


    /**
     * Searches for tenants in a specific state. was TenantListingByState
     * @method module:alice-core/lib/search/directory#byState
     * @param {String} type the type of tenant to look up
     * @param {String} [country] the country to look up tenants in
     * @param {String} state The state to lookup tenants in
     * @param {Function} [callback] Callback to execute when data is returned. Method Signature( err, data )
     **/
    , byState: function byState(type, country, state, callback){
        var opts = this.options
          , that = this;
        type = ( type || opts.type ).toLowerCase();
        country = ( country || opts.country ).toUpperCase();
        state = ( state || opts.state ).toUpperCase();
        logger.info('looking up tenants in %s [ %s ]', state, type)
        Tenant
            .find({
                 state: state
                , country:country
                , tenantType: type
            })
            .select('tenantType name mascot longName city state country url')
            .sort('longName city')
            .exec(this.end.bind( this, this.formatTenant.bind( this,callback ) ) );
    }

    /**
     * Searches for a tenant in a specific city. was TenantListingByCity
     * @method module:alice-core/lib/search/directory#byCity
     * @param {String} type the type of tenant to search for ( k8, hs, college)
     * @param {String} [country='US'] The country to search for tenants in
     * @param {String} state The state to find cities in
     * @param {String} city The name of the city to look up
     * @param {Function} [callback] Callback to execute when data is returned. Method Signature( err, data )
     **/
    , byCity: function ByCity( type, country, state, city, callback ){
        var opts = this.options
          , that = this;

        logger.info('looking up tenants in %s, %s [ %s ]', city, state, type)
        type = ( type || opts.type ).toLowerCase();
        country = ( country || opts.country ).toUpperCase();
        state = ( state || opts.state ).toUpperCase();
        city = ( city || opts.city );

        Tenant
            .find({
                 state: state
                , city: city
                , country:country
                , tenantType: type
            })
            .sort('longName')
            .select('tenantType name mascot longName city state country url')
            .exec(this.end.bind( this, this.formatTenant.bind( this,callback ) ) );
    }

    , end: function end( callback, err , data ){
        if( err ){
            /**
             * @module:alice-core/lib/search/directory#error
             * @event
             * @param {TYPE} error the error object returned from the driver
             **/
             logger.error(
                'directory search encountered an error %s'
                , err.message,{
                    stack: logger.exception.getAllInfo( err )
                } )
            this.emit('error', err )
        } else{
            /**
             * @module:alice-core/lib/search/directory#data
             * @event
             * @param {TYPE} data results from search query
             **/
            this.emit('data', data )
        }
        return callback && callback( err, data );
    }

    /**
     * this is necessary, because Mongo can't convert an Object ID to a string inside an aggregate
     * @method module:alice-catalog/lib/search/directory#formatTenant
     */
    , formatTenant: function formatTenant( callback, err, results ){
        var groupPayload = []
          , index_cache = {}
          , payloadLength = results.length
          , letter
          , idx
          , i
          ;
        if(!err ){

            for(i=0; i < payloadLength; i++){
                letter = results[i].name[0];

                if( index_cache.hasOwnProperty( letter ) ){
                    groupPayload[ index_cache[letter] ].items.push(  results[i].toObject({virtuals:true}) )
                }else{
                    idx = groupPayload.push({
                        letter:letter
                      , items:[ results[i].toObject({ virtuals:true }) ]
                    });
                    index_cache[letter] = idx-1;
                }
            }
        }

        index_cache = {}
        results = null;

        return callback && callback( err, groupPayload )
    }
});
