/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * Collector module that exposed library functions and classes
 * @module alice-catalog/lib
 * @author Eric satterwhite
 * @since 0.1.0
 * @requires catalogItem
 * @requires mascotloader
 * @requires productloader
 */


var catalogItem   = require( './catalogItem' )  // Catalog Item DTO class
  , mascotloader  = require( './mascotloader' )  // Tenant mascot loader function
  , productloader = require( './productloader' )  // Product loader function
  ;

exports.mascotloader = mascotloader
exports.proeuctloader = productloader
exports.catalogItem = catalogItem
