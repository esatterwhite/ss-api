var chroma = require('chroma-js'),
    _ = require('underscore');

var colorMatchHash = {};
var wackyColors = ["FFFFFF","000000","008FD2","EA0000","A46F75","000064","5B186E","00A000","AAB5CB","0036D9","BEBEBE","75B2DD","1C3031","774400","008080","960000","2B372D","FF8000","C8C8C8","FFD700","2A2A36","FFFF00","2D4430","24DDCB","215E21","993366", "FFCCAE","E100E1","FDB6C1","DBCCC4","C0AE96","A7BAC0","A1C36C","DFD1A0","8DA0B1"];
wackyColors.sort(function (a, b) {
  return chroma('#' + b).hsl()[0] - chroma('#' + a).hsl()[0];
});
function chooseColors(colorMatchArray) {
  if (colorMatchArray.length > 2) return [colorMatchArray[2]];
  else return [colorMatchArray[0]];
}
function colorMatch(product, tenant, full) {
    var threshold = .15;
    var tenantColors = ['000000', 'FFFFFF', tenant.primaryColor.hex, tenant.secondaryColor.hex];
    if (tenant.hasOwnProperty('teritaryColor')) tenantColors.push(tenant.teritaryColor.hex);
    var hashId = product._id + tenantColors.join("");
    if (colorMatchHash.hasOwnProperty(hashId)) {
      if (full) return colorMatchHash[hashId];
      else return chooseColors(colorMatchHash[hashId]);
    }
    var productColors = _.map(product.colors, function (color) { return color.hexColor.substr(1) });
    var matchObj = {};
    for (var i = 0; i < productColors.length; i++) {
        var productHsl = chroma('#' + productColors[i]).hsl();
        for (var j = 0; j < tenantColors.length; j++) {
            var matching = tenantColors[j];
            var tenantHsl = chroma('#' + matching).hsl();
            var combinedDifference = (Math.abs(tenantHsl[2] - productHsl[2]));
            if (!isNaN(tenantHsl[0]) && !isNaN(productHsl[0])) {
                var tenantHue = tenantHsl[0]/360;
                var productHue = productHsl[0]/360;
                var shortWheel = Math.abs(tenantHue - productHue)*3;
                var longWheel =  (1 - Math.abs(tenantHue - productHue))*3;
                if (shortWheel < longWheel) {
                    combinedDifference +=  shortWheel;
                } else {
                    combinedDifference += longWheel;
                }
            } else if (!(isNaN(tenantHsl[0]) && isNaN(productHsl[0])))  {
                continue;
            }
            if (combinedDifference < threshold && (!matchObj.hasOwnProperty(matching) || matchObj[matching].score > combinedDifference)) {
                matchObj[matching] = {score: combinedDifference, color: productColors[i]};
            }
        }
    }
    var returnable =  _.uniq(_.map(matchObj, function (value, key) { return value.color; }));
    if (full) return returnable;
    else return chooseColors(returnable);
}
var decalColorsHash = {};
function generateDecalColors(productColor, tenant, decal) {
    var clrp, clrs, clrt;
    var productChroma = chroma('#' + productColor);
    var productHsl = productChroma.hsl();
    var productRgb = productChroma.rgb();
    var hashId = productColor  + decal.colors.clrp + decal.colors.clrs + decal.colors.clrt + tenant.primaryColor.hex  + tenant.secondaryColor.hex + decal.colors.whiteContrast;
    if (tenant.hasOwnProperty('teritaryColor')) hashId += tenant.teritaryColor.hex;
    if (decalColorsHash.hasOwnProperty(hashId))  { return decalColorsHash[hashId]; }
    if( Math.abs(productRgb[0] - productRgb[1]) + Math.abs(productRgb[1] - productRgb[2]) > 10) { // test if colored
      /*
        if (productHsl[2] > .6) {
            var baseColor = '#000000';
            var secondaryColor = '#FFFFFF'; //baseColor;
        } else {
            var baseColor = '#FFFFFF';
            var secondaryColor = '#000000'; //;baseColor;
        }
        if (chrome
        */
      var pc = '#' + tenant.primaryColor.hex,
          sc = '#' + tenant.secondaryColor.hex,
          baseColor,
          secondaryColor,
          thirdColor;
      if (chroma.contrast(pc, productColor) > 4) {
        baseColor = pc;
        secondaryColor = pc;
        thirdColor = pc;
      } else if (chroma.contrast(sc, productColor) > 4) {
        baseColor = sc;
        secondaryColor = sc;
        thirdColor = sc;
      }  else if (productHsl[2] > .6) {
            var baseColor = '#000000';
            var secondaryColor = '#FFFFFF'; //baseColor;
            var thirdColor = '#929292';
        } else {
            var baseColor = '#FFFFFF';
            var secondaryColor = '#000000'; //;baseColor;
            var thirdColor = '#929292';
        }
    }
    else {
        var baseColor = '#' + tenant.primaryColor.hex;
        var secondaryColor = '#' + tenant.secondaryColor.hex;
        var thirdColor = ((productHsl[2] > .5) ? '#000000': '#ffffff');
        if (chroma.contrast(baseColor, productColor) < 2) {
            baseColor = secondaryColor;
        }
        if (chroma.contrast(secondaryColor, productColor) < 2) {
            if (baseColor == secondaryColor) {
                baseColor = thirdColor;
            }
            secondaryColor = thirdColor;
        }
    }
    var returnable = generateDecalColorObject(baseColor, secondaryColor, decal, productColor);
    decalColorsHash[hashId] = returnable;
    return returnable;
}
function generateDecalColorObject(primary, secondary, decal, productColor) {
    var clrp = primary.substr(1),
        clrs = ((primary== secondary) ? chroma(chroma.interpolate(secondary, productColor, decal.colors.clrs)).hex().substr(1) : secondary.substr(1) ) ,
        clrt = secondary.substr(1), // No Teritary currently ... chroma.interpolate(primary, productColor, decal.colors.clrt).toString().substr(1),
        clrw = 'ffffff',
        clrb = '000000';
    if (clrp.toLowerCase() == 'ffffff') clrw = '929694';
    if (clrp == '000000' || (productColor && chroma.contrast(productColor, '000000') < 3)) clrb = '929694';
    var returnable = { clrp: clrp, clrs: clrs, clrt: clrt, clrw: clrw, clrb: clrb};
    if (decal.colors.whiteContrast && returnable[decal.colors.whiteContrast].toLowerCase() == 'ffffff') {
      /*if (decal.colors.whiteContrast == 'clrs') { returnable[decal.colors.whiteContrast] = clrp; }
      else if (decal.colors.whiteContrast == 'clrp') { returnable[decal.colors.whiteContrast] = clrw; }
      else returnable[decal.colors.whiteContrast] = '000000'; */
      if (clrw != 'ffffff') returnable[decal.colors.whiteContrast] = clrw;
      else returnable[decal.colors.whiteContrast] = '929694';
    }
    return returnable;
}
function generateFullDecalColorSpectrum(product, tenant, decal) { // Get decal options for every product color
    var chosenColors = colorMatch(product, tenant, true), chosenColorsObject = {};
    for (var i = 0; i < chosenColors.length; i++) {
        chosenColorsObject[chosenColors[i]] = generateDecalColors(chosenColors[i], tenant, decal, chosenColors);
    }
    var wackyOptions = [];
  for (var i =0; i < wackyColors.length; i++) {
    var c = '#' + wackyColors[i];
    wackyOptions.push(generateDecalColorObject(c, c, decal));
  }
      /*// generate an array of White, Black, Primary, Secondary with clrs appropriately set
        generateDecalColorObject('#FFFFFF', '#FFFFFF', decal),
        generateDecalColorObject('#000000', '#000000', decal)];
    if (tenant.primaryColor.hex != 'FFFFFF' && tenant.primaryColor.hex != '000000') wackyOptions.push(generateDecalColorObject('#' + tenant.primaryColor.hex, '#' + tenant.primaryColor.hex, decal));
    if (tenant.secondaryColor.hex != 'FFFFFF' && tenant.secondaryColor.hex != '000000')   wackyOptions.push(generateDecalColorObject('#' + tenant.secondaryColor.hex, '#' + tenant.secondaryColor.hex, decal));*/
    return {chosen: chosenColorsObject, wacky: wackyOptions};
}

module.exports = {
    colorMatch: colorMatch,
    generateDecalColors: generateDecalColors,
    generateDecalColorObject: generateDecalColorObject,
    generateFullDecalColorSpectrum: generateFullDecalColorSpectrum
};
