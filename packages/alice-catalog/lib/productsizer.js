var http          = require('http')
, xml2js        = require('xml2js')
, Case          = require('case')
, _             = require('underscore')
, async         = require('async')
, productSchema = require('../models/product')
, staticData    = require('../fixtures/products')
, util               = require( 'util' )
, Boom               = require( 'boom' )
;


function iterateThroughData(callback) {
  /*
    var waiting = 0, products = [];
    var pendingCallback = function (err, product) {
        waiting -= 1;
        products.push(product);
        if (waiting == 0) callback(false, products );
    };
    for (var i =0; i <  staticData.length; i++) {
        var manualData = staticData[i];
        waiting += 1;
        makeSpreadshirtApiRequest( manualData, pendingCallback);
    }*/
  async.eachSeries(staticData, function (dataEntry, localCb) {
    makeSpreadshirtApiRequest(dataEntry, localCb);
  }, function (err) {
    if (err) { console.log("errocurred"); }
    callback(false, "All done");

  });
}

function makeSpreadshirtApiRequest(manualData, topCallback) {
  var options = {
    host: 'api.spreadshirt.com',
    path: '/api/v1/shops/509490/productTypes/' + manualData.spreadshirtId,
  };

  callback = function(response) {
    var str = '';

    //another chunk of data has been recieved, so append it to `str`
    response.on('data', function (chunk) {
      str += chunk;
    });

    //the whole response has been recieved, so we just print it out here
    response.on('end', function () {
      xml2js.parseString(str, function (err, data) {
        if (err) {
          console.warn("XML error: " + err);
          console.warn(str);
        } else {
          var product = getSizes(data, manualData, topCallback);
        }
      });
    });
  }
  http.request(options, callback).end();
}
function getSizes(ssData, manualData, callback) {
  var sizes = [], productType = ssData.productType.$.id;
  for (var i = 0; i < ssData.productType.sizes[0].size.length; i++) {
    var size = ssData.productType.sizes[0].size[i];
    var measures = [];
    for (var j = 0; j < size.measures[0].measure.length; j++) {
      var measure = size.measures[0].measure[j];
      measures.push({
        name: measure.name[0],
        value: measure.value[0]._,
        unit: measure.value[0].$.unit
      });
    }
    sizes.push({
      "name": size.name[0],
      "manufacturerId": size.$.id,
      "measurements": measures
    });
  }
  var findSchema = {
    "frontImage.imageRef": manualData.views.front
  };
  productSchema.find(findSchema, function (err, products) {
    if (err) {
      console.log("ERR: " + JSON.stringify(err));
      return callback(true, false);
    }/*(else  if (products.length != 1) {
      console.log("Unexpected product length [" + manualData.views.front + "]: " +  products.length);
      return callback(false, true);
    }*/
    async.eachSeries(products, function (product, pCallback) {
      product.sizes = sizes;
      product.manufacturer.internalType = productType;

      product.markModified('sizes');
      product.markModified('manufacturer.internalType');
      product.save(function (err) {
        pCallback();
      });
    }, function () {
      callback(false, true);
    });
    //console.log("I wanna put " + sizes.length + " on " + p.manufacturer.internalId);
    //console.log(JSON.stringify(sizes));

    //return callback(false, true);
  });
}

module.exports = function Get(params, callback) {
  if (params.secret != "debug") return callback(new Boom.notFound( util.format( 'No Tenant with ID %s found') ), false);
  else {
    iterateThroughData(callback);
    //callback(false, "Society is fundamentally flawed");
  }
}
