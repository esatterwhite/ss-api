var _ = require('underscore'),
    Tenant = require('alice-catalog/models/tenant'),
    async              = require( 'async' ),
    staticData = require('../fixtures/lrgMascots');


var editedNum, tooMany, notEnough;
function iterateThroughData(callback) {
  /* var waiting = 0, tenants = [];
    var pendingCallback = function (err, tenant) {
        waiting -= 1;
        tenants.push(tenant);
        if (waiting == 0) callback(false, "Edit ok: " + tenants.length);
    };
    for (var i =0; i <  staticData.length; i++) {
        var manualData = staticData[i];
        waiting += 1;
        tenantEdit(manualData, pendingCallback);
    }*/
  var i = 0;
  editedNum = 0;
  tooMany = 0;
  tooManyArr = [];
  notEnoughArr = [];
  notEnough = 0;
  var iterateCallback = function () {
    i++;
    if (i < staticData.length) tenantEdit(staticData[i], iterateCallback);
    else {
      var msg =  "Edit OK: " + editedNum + "/" + staticData.length + " Not Enough: " + notEnough + " Too many: " + tooMany;
      console.log(msg);
      callback(false, msg);
    }
  }
  tenantEdit(staticData[i], iterateCallback);
}
function tenantEdit (tenantProps, callback) {
  // {"state":"AL","zip":"35023","school":"Hueytown High School-","filename":"autos7-AL-35023-01_Hueytown_High_School-1"}
  var searchParams = {tenantType: "k12", zip: tenantProps.zip, state: tenantProps.state, name: tenantProps.school};
  var t = Tenant.find(searchParams).exec(function (err, foundTenants) {
    if (foundTenants.length > 1){
      //console.log("Too many tenants: " + JSON.stringify(searchParams));
      for (var i = 0; i < foundTenants.length; i++) {
        if ((foundTenants[i].mascot.name != foundTenants[0].mascot.name) || foundTenants[0].mascot.name == "")  {
          //console.log(foundTenants[i].mascot.name + " vs " + foundTenants[0].mascot.name);
          tooMany+= foundTenants.length;
          tooManyArr.push(searchParams);
          callback();
          return;
        }
      }
      //console.log("All mascot names were: " + foundTenants[0].mascot.name + "(" + tenantProps.filename + ")");
    }
    else if (foundTenants.length < 1 || !foundTenants)
    {
      //console.log("Not enough tenants: " + JSON.stringify(searchParams));
      notEnough++;
      notEnoughArr.push(searchParams);
      callback();
      return;
    }
    var saveArr = [];
    for (var i = 0; i < foundTenants.length; i++) {
      foundTenants[i].lrg.isLicensed = true;
      foundTenants[i].lrg.licenseName = tenantProps.filename;
      foundTenants[i].markModified('lrg.isLicensed');
      foundTenants[i].markModified('lrg.licenseName');
      saveArr.push(foundTenants[i].save.bind(foundTenants[i]));
    }
    async.parallel(saveArr, function (err, results) {
      if (err)  {
        throw err;
      } else {
        editedNum+=foundTenants.length;
        callback();
      }
    });
  });
}

module.exports = function Get(params, callback) {
  if (params.secret != "debug") return callback("No Secret", false);
  else {
    iterateThroughData(callback);
    //callback(false, "Society is fundamentally flawed");
  }
}
