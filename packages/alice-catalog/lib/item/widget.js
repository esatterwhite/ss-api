/*jshint laxcomma: true, smarttabs: true, node: true*/
'use strict';
/**
 * Generates data for a
 * @module alice-catalog/lib/item/widget
 * @author Eric Satterwhite
 * @since 1.2.1
 * @requires qs
 * @requires alice-log
 * @requires alice-stdlib/class
 * @requires alice-stdlib/class/options
 * @requires alice-stdlib/class/parent
 * @requires alice-stdlib/url
 * @requires alice-catalog/lib/item/product
 */
var  qs                = require( 'qs' )
	, conf             = require( 'alice-conf' )
	, Class            = require( 'alice-stdlib/class' )
	, Options          = require( 'alice-stdlib/class/options' )
	, Parent           = require( 'alice-stdlib/class/parent' )
	, url              = require( 'alice-stdlib/url' )
	, Item             = require('./product')
	, ITEM_HEIGHT      = 203
	, IMAGE_PROTOCOL
	, WidgetItem
	;

IMAGE_PROTOCOL = conf.get('DEFAULT_PROTOCOL');
/**
 * Catalog item used to populate front end widgets
 * @constructor
 * @alias module:alice-catalog/lib/item/widget
 * @extends module:alice-catalog/lib/item/product
 * @param {Object} options
 * @param {?module:alice-catalog/models/product} [options.product] Product instance used to render image layers
 * @param {String} [options.color=FFFFFF] Base color of the Decal images
 * @param {Object} options.decalAdditions Object defining the layering options to enable
 * @param {module:alice-catalog/models/decal} options.decal Decal instance used to generate image layers
 * @param {module:alice-catalog/models/tenant} options.tenant Tenant instance
 * @param {Object} options.decalColors
 * @param {String} options.decalColors.clrp hex code, sans `#` representing the primary color of the decal mascot
 * @param {String} options.decalColors.clrs hex code, sans `#` representing the secondary color of the decal mascot
 * @param {String} options.decalColors.clrt hex code, sans `#` representing the tertiary color of the decal mascot
 * @param {String} options.decalColors.clrb hex code, sans `#` representing the black color of the decal mascot
 * @param {String} options.decalColors.clrw hex code, sans `#` representing the white color of the decal mascot
 */
module.exports = WidgetItem = new Class(/* @lends module .THING.prototype */{
	inherits:Item
	,mixin:[ Options, Parent ]
	,options:{
		product        : null // optional
	 , color          : null // optional
	 , decalAdditions : null // required
	 , decal          : null // required
	 , tenant         : null // required
	 , decalColors    : null // required
	}
	,constructor: function( options ){
		this.setOptions( options );
	}

	/**
	 * Generates the layers containing s7 urls to render a UI view
	 * @method module:alice-catalog/lib/item/product#generateView
	 * @param {String} viewtype The type of view to generate (`details`, `product`)
	 * @param {module:alice-catalog/models/decal} decal ...
	 * @param {Number} height The height of the product decal
	 * @param {Boolean} printview specifiy if to include a print view laer
	 **/
	,generateView: function generateView(productView, decal, height, printView, widgetView) {
		var layers          = []
		  , viewColor       = this.viewColor
		  , tenant          = this.options.tenant
		  , tenantName      = encodeURIComponent( tenant.name || tenant.longName )
		  , mascotName      = encodeURIComponent( tenant.mascot.name || 'Team' )
		  , inclusions      = decal && decal.inclusions || {}
		  , decalAdditions  = this.additions
		  , qualityString   = '&qlt=80,0&resmode=bilin'
		  , quality         = '80,0'
		  , resmode         = 'bilin'
		  , additionString  = '&'
      , width = 243
		  , mascotColors
		  , tenantNameSize
		  , mascotNameSize
		  ;

    //var cropObj = [235,332,556,730], cropMod = 100; //&resmode=sharp2
    var cropObj = [235, 50, 570, 850];
    var swatchCrop = [50,0,957,1380];
		mascotColors  = this.options.decalColors;
		tenantNameSize = this.tenantSizes( tenantName.length, decal && decal.lengthLimits.tenant, {cutoff_medium: 16, cutoff_large: 16});
		mascotNameSize = this.mascotSizes( mascotName.length, decal && decal.lengthLimits.mascot );

		if (height === '{{HEIGHT}}' || height > 400) {
			qualityString = '&qlt=95,0&resmode=sharp2';
		}

		inclusions = this.inclusions( decalAdditions );
		additionString += decodeURIComponent( url.qs.legacy.stringify( inclusions ) );
    if (widgetView) {
         //layers = IMAGE_PROTOCOL + '://a248.e.akamai.net/f/248/9086/10h/s7d7.scene7.com/ir/render/spiritshopRender/' + productView + '?wid=' + '261' + qualityString + '&fmt=png&resmode=sharp2&obj=main/color&color=' + viewColor+ '&obj=main/imprint&decal&res=100&show&src=fxg{spiritshop/' + decal.s7id + '?setAttr.txt_t_nm_MED={visible=' + tenantNameSize.medium +'}&setAttr.txt_t_nm_LONG={visible=' + tenantNameSize.large+'}&setAttr.txt_t_nm_SHRT={visible=' + tenantNameSize.small+'}&setAttr.txt_t_l_MED={visible=' + mascotNameSize.medium + '}&setAttr.txt_t_l_SHRT={visible=' + mascotNameSize.small +'}&setAttr.txt_t_l_LONG={visible=' + mascotNameSize.large + '}&$txttl=' + mascotName + ' &$txttnm=' + tenantName  + additionString +  '&setAttr.clrp={baseColorValue=%23' + mascotColors.clrp + '}&setAttr.clrs={baseColorValue=%23' + mascotColors.clrs + '}&setAttr.clrt={baseColorValue=%23' + mascotColors.clrt + '}&setAttr.clrw={baseColorValue=%23' + mascotColors.clrw + '}&setAttr.clrb={baseColorValue=%23' + mascotColors.clrb + '}}';
            layers = IMAGE_PROTOCOL + '://a248.e.akamai.net/f/248/9086/10h/s7d7.scene7.com/is/image/spiritshop?wid=' + width + '&fmt=png&resmode=sharp2&src=ir{spiritshopRender/' + productView + '?obj=main/color&color=' + viewColor+ '&obj=main/imprint&decal&res=100&show&src=fxg{spiritshop/' + decal.s7id + '?setAttr.txt_t_nm_MED={visible=' + tenantNameSize.medium +'}&setAttr.txt_t_nm_LONG={visible=' + tenantNameSize.large+'}&setAttr.txt_t_nm_SHRT={visible=' + tenantNameSize.small+'}&setAttr.txt_t_l_MED={visible=' + mascotNameSize.medium + '}&setAttr.txt_t_l_SHRT={visible=' + mascotNameSize.small +'}&setAttr.txt_t_l_LONG={visible=' + mascotNameSize.large + '}&$txttl=' + mascotName + ' &$txttnm=' + tenantName  + additionString +  '&setAttr.clrp={baseColorValue=%23' + mascotColors.clrp + '}&setAttr.clrs={baseColorValue=%23' + mascotColors.clrs + '}&setAttr.clrt={baseColorValue=%23' + mascotColors.clrt + '}&setAttr.clrw={baseColorValue=%23' + mascotColors.clrw + '}&setAttr.clrb={baseColorValue=%23' + mascotColors.clrb + '}}}&crop=' + swatchCrop.toString() + '&resmode=sharp2';
    } else {
        layers = IMAGE_PROTOCOL + '://a248.e.akamai.net/f/248/9086/10h/s7d7.scene7.com/is/image/spiritshop?wid=' + width + '&fmt=png&resmode=sharp2&src=ir{spiritshopRender/' + productView + '?obj=main/color&color=' + viewColor+ '&obj=main/imprint&decal&res=100&show&src=fxg{spiritshop/' + decal.s7id + '?setAttr.txt_t_nm_MED={visible=' + tenantNameSize.medium +'}&setAttr.txt_t_nm_LONG={visible=' + tenantNameSize.large+'}&setAttr.txt_t_nm_SHRT={visible=' + tenantNameSize.small+'}&setAttr.txt_t_l_MED={visible=' + mascotNameSize.medium + '}&setAttr.txt_t_l_SHRT={visible=' + mascotNameSize.small +'}&setAttr.txt_t_l_LONG={visible=' + mascotNameSize.large + '}&$txttl=' + mascotName + ' &$txttnm=' + tenantName  + additionString +  '&setAttr.clrp={baseColorValue=%23' + mascotColors.clrp + '}&setAttr.clrs={baseColorValue=%23' + mascotColors.clrs + '}&setAttr.clrt={baseColorValue=%23' + mascotColors.clrt + '}&setAttr.clrw={baseColorValue=%23' + mascotColors.clrw + '}&setAttr.clrb={baseColorValue=%23' + mascotColors.clrb + '}}}&crop=' + cropObj.toString() + '&resmode=sharp2';
    }
		return layers;
	}
	,identifiers: function identifiers(){
		return { obj: {
			hasMascot: this.personalization.arttm
			,activity: this.options.decalAdditions.category
		} };
	}
	,toJSON: function(){
		var name = this.name;
		var product = this.options.product;
		var decal = this.options.decal;
		return {

		    product          : product
		  , decal            : {name:decal.name, s7id:decal.s7id}
		  , widgetView       : {
				product: this.generateView(product.frontImage.imageRef, decal, this.height, false, false),
				swatch: this.generateView(product.swatch, decal, this.height, false, true)
		  }
		  , linkurl          : this.linkUrl
          , category         : this.additions.category
		};
	}
});

Object.defineProperties(WidgetItem.prototype,{
	/**
	 * @instance
	 * @name name
	 * @memberof module:alice-catalog/lib/item/widget
	 * @property {String} name The name of the object being generated based on the feching parameter
	 */
	name: {
		get: function(){
			return this.options.decal.name;
		}
	}

	/**
	 * @instance
	 * @name height
	 * @memberof module:alice-catalog/lib/item/widget
	 * @property {Number} Height A Front end concern
	 */
	,height:{
		get:function( ){
			return ITEM_HEIGHT;
		}
	}
})
