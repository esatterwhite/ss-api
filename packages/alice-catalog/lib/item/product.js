/*jshint laxcomma: true, smarttabs: true, node: true*/
'use strict';
/**
 * Generates data for a
 * @module alice-catalog/lib/item/product
 * @author Eric Satterwhite
 * @since 1.2.1
 * @requires events
 * @requires joi
 * @requires alice-log
 * @requires alice-stdlib/class
 * @requires alice-stdlib/class/options
 * @requires alice-stdlib/url
 * @requires alice-stdlib/lang
 * @requires alice-catalog/lib/constants
 * @requires alice-catalog/lib/s7
 * @requires alice-catalog/models/decal
 */
var  qs        = require( 'qs' )
   , conf      = require( 'alice-conf' )
   , Class     = require( 'alice-stdlib/class' )
   , Options   = require( 'alice-stdlib/class/options' )
   , Parent    = require( 'alice-stdlib/class/parent' )
   , url       = require( 'alice-stdlib/url' )
   , date      = require('alice-stdlib/date')
   , Item      = require('./base')
   , constants = require('../constants')
   , S7        = require('../s7')
   , sizing     = require('../sizing')
   , number    = require( 'alice-stdlib/number' )
   , ProductItem
   ;

var  IMAGE_PROTOCOL = conf.get('DEFAULT_PROTOCOL');

/**
 * Description
 * @constructor
 * @alias module:alice-catalog/lib/item/product
 * @extends module:alice-catalog/lib/item/base
 * @param {TYPE} param
 */
module.exports = ProductItem = new Class(/* @lends module .THING.prototype */{
	inherits:Item
	,mixin:[ Options, Parent ]
	,options:{
		 product:null
		,decal:null
		,tenant:null
		,decalColors:null
		,color:null
	}
	,constructor: function( options ){
		this.setOptions( options );
	}

	,identifiers: function identifiers(){
		return { obj: this.productIdentifiers( ) };
	}

	,productIdentifiers: function productIdentifiers( ){

		return {
			color   : this.color
		  , gender  : this.options.product.gender
		  , price   : this.options.product.unitPrice
		  , category: this.options.product.search.tags
		};
	}

	, inclusions: function( additions ){
		var inclusions = {}
		   , tenant  = this.options.tenant
		   , personalization = this.personalization
		   , result
		   ;

		for( var key in personalization ){
		  if( personalization[ key ]  ){
			  switch( key ){
				  case constants.ART_MASCOT:
					result = additions[ constants.ART_MASCOT ];
					break;

				  case constants.ART_ACTIVITY:
					result = additions.s7Id;
					break;

				  case constants.TEXT_ACTIVITY_NAME:
					result = additions.category;
					break;

				  case constants.TEXT_TENANT_CITY:
					result = tenant.city;
					break;

				  case constants.TEXT_TENANT_STATE:
					result = tenant.stateName;
					break;

				  case constants.TEXT_TENANT_TYPE:
					result = tenant.displayType;
					break;

				  case constants.TEXT_TENANT_ACRONYM:
					result = tenant.acronym;
					break;

				  case constants.TEXT_TENANT_NAME:
					result = tenant.name.trim();
					break;
				  case constants.TEXT_DATE_ONE:
					result = date.format( new Date(), personalization[ key ] );
					break;

				  case constants.TEXT_DATE_TWO:
					result = date.format( date.increment( new Date(), 'year', 1) , personalization[ key ] );
					break;

				  default:
					continue;
			}
			inclusions[ '$' + key ] = result;
		  }
		}
	  return inclusions;
	}

	/**
	 * Generates the layers containing s7 urls to render a UI view
	 * @method module:alice-catalog/lib/item/product#generateView
	 * @param {String} viewtype The type of view to generate (`details`, `product`)
	 * @param {module:alice-catalog/models/decal} decal ...
	 * @param {Number} height The height of the product decal
	 * @param {Boolean} printview specifiy if to include a print view laer
	 **/
	,generateView: function generateView(productView, decal, height, printView, widgetView) {
		var layers          = []
		  , viewColor       = this.viewColor
		  , tenant          = this.options.tenant
		  , tenantName      = encodeURIComponent( tenant.name || tenant.longName )
		  , mascotName      = encodeURIComponent( tenant.mascot.name || 'Team' )
		  , inclusions      = decal && decal.inclusions || {}
		  , decalAdditions  = this.additions
		  , qualityString   = '&qlt=80,0&resmode=bilin'
		  , quality         = '80,0'
		  , resmode         = 'bilin'
		  , additionString  = '&'
		  , mascotColors
		  , tenantNameSize
		  , mascotNameSize
		  ;

		if ( !!this.options.decalColors ) {
			mascotColors = this.options.decalColors;
		} else {
			mascotColors = {
				clrp: tenant.primaryColor.hex || 'ffffff',
				clrs: tenant.secondaryColor.hex || 'ffffff',
				clrt: (tenant.teritaryColor) ? tenant.teritaryColor.hex : 'ffffff',
				clrw: 'ffffff',
				clrb: '000000'
			};
	   }


		tenantNameSize = this.tenantSizes( tenantName.length, decal && decal.lengthLimits.tenant, {cutoff_medium: 16, cutoff_large: 16});
		mascotNameSize = this.mascotSizes( mascotName.length, decal && decal.lengthLimits.mascot );

		if (height === '{{HEIGHT}}' || height > 400) {
			qualityString = '&qlt=95,0&resmode=sharp2';
			quality       = '95,0';
			resmode       = 'sharp2';
		}

		inclusions = this.inclusions( decalAdditions );

		additionString += decodeURIComponent( url.qs.legacy.stringify( inclusions ) );
		layers.push( new S7.url.render( productView, {hei:height, qlt: quality, obj: 'main/color', color: viewColor, sharp:1 , rs: 'U1', pos   : '0,0'} ));
		decal && layers.push(IMAGE_PROTOCOL + '://a248.e.akamai.net/f/248/9086/10h/s7d7.scene7.com/ir/render/spiritshopRender/' + productView + '?hei=' + height + qualityString + '&obj=main/color&color=' + viewColor+ '&obj=main/imprint&decal&show&src=fxg{spiritshop/' + decal.s7id + '?setAttr.txt_t_nm_MED={visible=' + tenantNameSize.medium +'}&setAttr.txt_t_nm_LONG={visible=' + tenantNameSize.large+'}&setAttr.txt_t_nm_SHRT={visible=' + tenantNameSize.small+'}&setAttr.txt_t_l_MED={visible=' + mascotNameSize.medium + '}&setAttr.txt_t_l_SHRT={visible=' + mascotNameSize.small +'}&setAttr.txt_t_l_LONG={visible=' + mascotNameSize.large + '}&$txttl=' + mascotName + ' &$txttnm=' + tenantName  + additionString +  '&setAttr.clrp={baseColorValue=%23' + mascotColors.clrp + '}&setAttr.clrs={baseColorValue=%23' + mascotColors.clrs + '}&setAttr.clrt={baseColorValue=%23' + mascotColors.clrt + '}&setAttr.clrw={baseColorValue=%23' + mascotColors.clrw + '}&setAttr.clrb={baseColorValue=%23' + mascotColors.clrb + '}}&fmt=png-alpha&sharp=1&res=100&rs=U1&pos=0,0&req=object');

		return layers;
	}

	, tenantSizes: function (/* len, inclusions, overrides */) {
		return this.sizes.apply(this, arguments);
	}

	, mascotSizes: function (/* len, inclusions, overrides */) {
		return this.sizes.apply(this, arguments);
	}

	/**
	 * Determines size restrictions for tenant name layer inclusion.
	 * @method module:alice-catalog/lib/item/product#sizes
	 * @param {Number} length The length a string to be included
	 * @param {String[]} inclusions An array allowable sizes the tenant name maybe be included on a specific decal.
	 * Tshese values generally come from `Decal.lengthLimits`
	 * @param {Object} [overrides] Values to be used in place of calculated values
	 * @param {Boolean} [overrides.small=null] If specified, will be used as the small inclusion value
	 * @param {Boolean} [overrides.medium=null] If specified, will be used as the medium inclusion value
	 * @param {Boolean} [overrides.large=null] If specified, will be used as the large inclusion value
	 * @param {Number} [overrides.cutoff_small=9] If specified, this value will be used to denote when a string is too long for short text inclusions. Only used if the  small value was not specified
	 * @param {Number} [overrides.cutoff_medium=9] If specified, this value will be used to denote when a string is too long for medium text inclusions. Only used if the  medium value was not specified
	 * @param {Number} [overrides.cutoff_large=9] If specified, this value will be used to denote when a string meets the requirement for large text inclusions. Only used if the  large value was not specified
	 * @return {SizeObject}
	 * @example
var ci = new CatalogItem({ ... })
ct.sizes(10, ['MED', 'LONG']) // {small:false, med: false, long: true}
	 */
	, sizes: function tenatSizes( len, inclusions, overrides ){
		var allowShort
		  , allowMed
		  , allowLong
		  , showingSmall
          , size
          ;

        inclusions = inclusions || [];
        overrides  = overrides  || {};

        size = sizing.calculateSize( len, inclusions )
        
        allowShort = size === constants.SIZE_SHORT;
        allowMed   = size === constants.SIZE_MEDIUM;
        allowLong  = size === constants.SIZE_LONG;
		
		return {
			 small  : allowShort
		   , medium : allowMed  && !showingSmall
		   , large  : allowLong
		};
	}
	,toJSON: function toJSON( ){
		var name = this.name;
		var product = this.options.product;

		return {
			identifiers      : this.identifiers()
		  , product          : product
		  , name             : name
		  , color            : this.color
		  , s7id             : name
		  , frontView        : this.generateView( product.frontImage.imageRef, this.options.decal, this.height )
		  , price            : this.price
		  , productUrl       : this.productUri
		  , linkurl          : this.linkUrl
		  , display_order    : this.display_order
		  , fmtPrice         : this.formattedPrice
		};
	}
});


Object.defineProperties( ProductItem.prototype,{
	/**
	 * @instance
	 * @name productUri
	 * @memberof module:alice-catalog/lib/item/product
	 * @property {String|Boolean} productUri The uri used to find an activity for a product
	 */
	productUri:{
		get: function(){
			return 'activities?' + qs.stringify({
				product:''+this.options.product._id
			  , color:this.options.color
			  , additions:this.options.decalAdditions
			});
		}
	}

	,name:{
		get: function(){
			return this.options.product.name;
		}
	}
	/**
	 * @instance
	 * @name color
	 * @memberof module:alice-catalog/lib/item/product
	 * @property {String} color Returns the currently configured hex color
	 */
	,color:{
		get: function(){
			return this.options.color;
		}
	}

	/**
	 * @instance
	 * @name price
	 * @memberof module:alice-catalog/lib/item/product
	 * @property {Number} price returns the current price of the Product in cents
	 */
	,price:{
		get: function(){
			return this.options.product.economics.price
		}
	}
	/**
	 * @instance
	 * @name formattedPrice
	 * @memberof module:alice-catalog/lib/item/product
	 * @property {Number} formattedPrice returns the current price of the Product in Dollars
	 */
	,formattedPrice: {
		get: function () {
			return number.getDollars(this.price);
		}
	}

	/**
	 * @instance
	 * @name shortDescription
	 * @memberof module:alice-catalog/lib/item/product
	 * @property {String} shortDescription Returns the short verson of the decription of the current product
	 */
	,shortDescription:{
		get: function(){
			return this.options.product.shortDescription
		}
	}

	/**
	 * @instance
	 * @name fullDescription
	 * @memberof module:alice-catalog/lib/item/product
	 * @property {String} fullDescription returns the full description of the current product
	 */
	,fullDescription:{
		get: function( ){
			return this.options.product.fullDescription;
		}
	}

	/**
	 * @property {Number|String} height returns the height of the rendering area on the UI
	 **/
	,height:{
		get: function( ){
			return 523;
		}
	}

	/**
	 * @property {String} viewColor returns the hex code currently configured color
	 **/
	,viewColor:{
	    get: function(){
	        return  this.options.color || 'ffffff';
	    }
	}
});
