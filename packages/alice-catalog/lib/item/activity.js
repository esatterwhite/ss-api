/*jshint laxcomma: true, smarttabs: true, node: true*/
'use strict';
/**
 * Generates data for a
 * @module alice-catalog/lib/item/activity
 * @author Eric Satterwhite
 * @since 1.2.1
 * @requires events
 * @requires joi
 * @requires alice-log
 * @requires alice-stdlib/class
 * @requires alice-stdlib/class/options
 * @requires alice-stdlib/url
 * @requires alice-stdlib/lang
 * @requires alice-catalog/lib/constants
 * @requires alice-catalog/lib/s7
 * @requires alice-catalog/models/decal
 */
var  qs        = require( 'qs' )
	, conf      = require( 'alice-conf' )
	, Class     = require( 'alice-stdlib/class' )
	, Options   = require( 'alice-stdlib/class/options' )
	, Parent    = require( 'alice-stdlib/class/parent' )
	, url       = require( 'alice-stdlib/url' )
	, date      = require('alice-stdlib/date')
	, Item      = require('./product')
	, constants = require('../constants')
	, S7        = require('../s7')
	, number    = require( 'alice-stdlib/number' )
	, DEFAULT_SWATCH = '8ea1c2ded7d44b67beb034e5eab9246d'
	, ITEM_HEIGHT = 523

	, ITEM_DESCRIPTION = 'Click on this Decal to put it on your apparel item of choice!'
	, ActivityItem
	;

var  IMAGE_PROTOCOL = conf.get('DEFAULT_PROTOCOL');
/**
 * Description
 * @constructor
 * @alias module:alice-catalog/lib/item/activity
 * @param {TYPE} param
 */
module.exports = ActivityItem = new Class(/* @lends module .THING.prototype */{
	inherits:Item
	,mixin:[ Options, Parent ]
	,options:{
		product        : null // optional
	 , color          : null // optional
	 , decalAdditions : null // required
	 , decal          : null // required
	 , tenant         : null // required
	 , decalColors    : null // required
	}
	,constructor: function( options ){

		this.setOptions( options )
	}

	/**
	 * Generates the layers containing s7 urls to render a UI view
	 * @method module:alice-catalog/lib/item/product#generateView
	 * @param {String} viewtype The type of view to generate (`details`, `product`)
	 * @param {module:alice-catalog/models/decal} decal ...
	 * @param {Number} height The height of the product decal
	 * @param {Boolean} printview specifiy if to include a print view laer
	 **/
	,generateView: function generateView(productView, decal, height, printView, widgetView) {
		var layers          = []
		  , viewColor       = this.viewColor
		  , tenant          = this.options.tenant
		  , tenantName      = encodeURIComponent( tenant.name || tenant.longName )
		  , mascotName      = encodeURIComponent( tenant.mascot.name || 'Team' )
		  , inclusions      = decal && decal.inclusions || {}
		  , decalAdditions  = this.additions
		  , qualityString   = '&qlt=80,0&resmode=bilin'
		  , quality         = '80,0'
		  , resmode         = 'bilin'
		  , additionString  = '&'
		  , mascotColors
		  , tenantNameSize
		  , mascotNameSize
		  ;

		mascotColors  = this.options.decalColors;
		tenantNameSize = this.tenantSizes( tenantName.length, decal && decal.lengthLimits.tenant, {cutoff_medium: 16, cutoff_large: 16});
		mascotNameSize = this.mascotSizes( mascotName.length, decal && decal.lengthLimits.mascot );

		if (height === '{{HEIGHT}}' || height > 400) {
			qualityString = '&qlt=95,0&resmode=sharp2';
			quality       = '95,0';
			resmode       = 'sharp2';
		}

		inclusions = this.inclusions( decalAdditions );

		additionString += decodeURIComponent( url.qs.legacy.stringify( inclusions ) );
		layers.push( new S7.url.render( productView, {hei:height, qlt: quality, obj: 'main/color', color: viewColor, sharp:1 , rs: 'U1', pos   : '0,0'} ));
		decal && layers.push(IMAGE_PROTOCOL + '://a248.e.akamai.net/f/248/9086/10h/s7d7.scene7.com/ir/render/spiritshopRender/' + productView + '?hei=' + height + qualityString + '&obj=main/color&color=' + viewColor+ '&obj=main/imprint&decal&show&src=fxg{spiritshop/' + decal.s7id + '?setAttr.txt_t_nm_MED={visible=' + tenantNameSize.medium +'}&setAttr.txt_t_nm_LONG={visible=' + tenantNameSize.large+'}&setAttr.txt_t_nm_SHRT={visible=' + tenantNameSize.small+'}&setAttr.txt_t_l_MED={visible=' + mascotNameSize.medium + '}&setAttr.txt_t_l_SHRT={visible=' + mascotNameSize.small +'}&setAttr.txt_t_l_LONG={visible=' + mascotNameSize.large + '}&$txttl=' + mascotName + ' &$txttnm=' + tenantName  + additionString +  '&setAttr.clrp={baseColorValue=%23' + mascotColors.clrp + '}&setAttr.clrs={baseColorValue=%23' + mascotColors.clrs + '}&setAttr.clrt={baseColorValue=%23' + mascotColors.clrt + '}&setAttr.clrw={baseColorValue=%23' + mascotColors.clrw + '}&setAttr.clrb={baseColorValue=%23' + mascotColors.clrb + '}}&fmt=png-alpha&sharp=1&res=100&rs=U1&pos=0,0&req=object');

		return layers;
	}
	,identifiers: function identifiers(){
		return { obj: {
			hasMascot: this.personalization.arttm
			,activity: this.options.decalAdditions.category
		} };
	}
	,toJSON: function(){
		var name = this.name;
		var product = this.options.product;
		var decal = this.options.decal;
		return {
			identifiers       : this.identifiers()
		  , product          : product
		  , name             : name
		  , color            : this.color
		  , s7id             : name
		  , decal            : {name:decal.name, s7id:decal.s7id}
		  , swatchView       : this.swatchView = this.generateView(product ? product.swatch : DEFAULT_SWATCH, this.options.decal, ITEM_HEIGHT )
		  , productUrl       : product && this.productUri
		  , linkurl          : this.linkUrl
		  , display_order    : this.display_order
          , category         : this.additions.category
		};
	}
});

Object.defineProperties(ActivityItem.prototype,{
	/**
	 * @instance
	 * @name fullDescription
	 * @memberof module:alice-catalog/lib/item/activity
	 * @property {String} fullDescription returns the full description of the current product
	 */
	fullDescription:{
		 get: function( ){
			  return ITEM_DESCRIPTION;
		 }
	}

	/**
	 * @instance
	 * @name name
	 * @memberof module:alice-catalog/lib/item/activity
	 * @property {String} name The name of the object being generated based on the feching parameter
	 */
	,name: {
		get: function(){
			return this.options.decal.name;
		}
	}

	/**
	 * @instance
	 * @name price
	 * @memberof module:alice-catalog/lib/item/product
	 * @property {Number} price returns the current price of the Product in cents
	 */
	,price:{
		get: function(){
			return 0
		}
	}
})
