var http          = require('http')
  , xml2js        = require('xml2js')
  , Case          = require('case')
  , _             = require('underscore')
  , productSchema = require('../models/product')
  , staticData    = require('../fixtures/products')
  ;


function iterateThroughData(callback) {
    var waiting = 0, products = [];
    var pendingCallback = function (err, product) {
        waiting -= 1;
        products.push(product);
        if (waiting == 0) callback(false, Jproducts );
    };
    for (var i =0; i <  staticData.length; i++) {
        var manualData = staticData[i];
        waiting += 1;
        makeSpreadshirtApiRequest(manualData.spreadshirtId, manualData, pendingCallback);
    }
}

function makeSpreadshirtApiRequest(id, manualData, topCallback) {
    console.log("Requesting");
  return;
    var options = {
        host: 'api.spreadshirt.com',
        path: '/api/v1/shops/509490/productTypes/' + id,
    };

    callback = function(response) {
        var str = '';

        //another chunk of data has been recieved, so append it to `str`
        response.on('data', function (chunk) {
            str += chunk;
        });

        //the whole response has been recieved, so we just print it out here
        response.on('end', function () {
            xml2js.parseString(str, function (err, data) {
                if (err) {
                    console.warn("XML error: " + err);
                    console.warn(str);
                } else {
                    var product = createProductSchema(data, manualData);
                    product.save();
                    topCallback(false, product);
                }
            });
        });
    }
    http.request(options, callback).end();
}
function createProductSchema(ssData, manualData) {
    var colors = [];
    for (var i = 0; i < ssData.productType.appearances[0].appearance.length; i++) {
        var appearance = ssData.productType.appearances[0].appearance[i];
        colors.push({
            name: Case.title(appearance.name[0]),
            hexColor: appearance.colors[0].color[0]._,
            manufacturerId: appearance.$.id
        });
    }
    var tags = manualData.subtype.split(", ");
    var product = new productSchema({
        name: ssData.productType.name[0],
        brand: ssData.productType.brand[0],
        shortDescription: ssData.productType.shortDescription[0],
        fullDescription: ssData.productType.description[0],
        search: {tags: tags},
        economics: {
            productCost: ssData.productType.price[0].vatIncluded[0],
            productMSRP: ssData.productType.price[0].vatIncluded[0],
            price:  ssData.productType.price[0].vatIncluded[0]*1.25,
        },
        frontImage:{
            imageRef: manualData.views.front,
        },
        backImage: {
            imageRef: manualData.views.back,
        },
        swatch: manualData.swatch,
        manufacturer: {
            name: "Spreadshirt",
            internalId: manualData.spreadshirtId,
        },
        colors: colors,
        recordstatus: 'active',
        lifecycle: {
            availableOn: '1/1/2014'
        },
        gender: manualData.gender,
    });

    //product.colors = colors;
    return product;

}

module.exports = function Get(params, callback) {
    console.log("PRODUCT LOADER SERVICE")
    if (params.secret != "debug") return callback("No Secret", false);
    else {
        iterateThroughData(callback);
        //callback(false, "Society is fundamentally flawed");
    }
}
