/*
 * Created by Ryan Fisch on 5/12/2014.
 */
var EventEmitter = require('events').EventEmitter;

var PromotionsManager = function () {
    EventEmitter.call(this);
};

PromotionsManager.prototype = Object.create(EventEmitter.prototype);

PromotionsManager.prototype.GetHeroPromotion = function (scope, user) {
    //TODO: Do some type of look up into configuration or repository
    //      based on the scope of the call request and the active
    //      user if known.
    //      mimicking async to have emit event handler
    
    var bob = this;
    bob.emit('GetHeroPromotionDone', { promo: "this is a sample promo %link% messages", link: "http://google.com" });
};

PromotionsManager.prototype.GetWidgets = function (scope, tenantType, user) {
    //TODO: Do some type of look up into configuration or repository
    //      based on the scope of the call request and the active
    //      user if known.
    //      mimicking async to have emit event handler
    var bob = this;
    var widgets = [];
    var debugWidgets = [{
            row: [
                {
                    "size": "col-md-4 col-sm-6", content: [{ media: 'state', size: 'sm' }],
                },
                {
                    "size": "col-md-4 col-sm-6",
                    content: [
                        { size: 'sm', media: 'image', url: '/images/corporate/Corp_widget-products.jpg' },
                        { size: 'sm', media: 'image', url: '/images/corporate/Corp_widget-boosters.png', href: 'http://fundraising.spiritshop.com' },
                        { size: 'sm', media: 'image', url: '/images/corporate/Royalties.jpg', href: "http://support.spiritshop.com/hc/en-us/articles/201100305-Become-a-School-Partner" }
                    ]
                },
                {
                    "size": "col-md-4 col-sm-6", content: [
                        { size: 'sm', media: 'image', url: '/images/corporate/Corp_widget-activity.jpg', href: '/bulk-orders' },
                        { size: 'sm', media: 'image', url: '/images/corporate/Video-WhyShop.png', href: "http://support.spiritshop.com/hc/en-us/articles/203224449-Why-Choose-Spirit-Shop" },
                        { size: 'sm', media: 'image', url: '/images/corporate/Partners.jpg', href: "http://support.spiritshop.com/hc/en-us/articles/203224189-Media-Affiliate-Partners" }
                    ]

                },
            ]
        }, {
            row: [
                {
                    size: "col-sm-4",
                    content: [
                        { size: 'sm', media: 'image', url: '/images/corporate/Customize.png' }
                    ]
                },
                {
                    size: "col-sm-4",
                    content: [
                        { size: 'sm', media: 'image', url: '/images/corporate/Shipping.png' }
                    ]
                },
                {
                    size: 'col-sm-4',
                    content: [
                        { size: 'sm', media: 'image', url: '/images/corporate/Satisfaction.png' }
                    ]
                }
            ]

        }];
    
    bob.emit('GetWidgetsDone', debugWidgets);
};

module.exports = PromotionsManager;
