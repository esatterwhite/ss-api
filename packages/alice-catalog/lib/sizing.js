/*jshint laxcomma: true, smarttabs: true, node: true*/
'use strict';
/**
 * sizing.js
 * @module sizing.js
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires util
 * @requires debug
 * @requires alice-catalog/lib/constants
 */
var util       = require( 'util' )
  , debug      = require( 'debug' )('alice:lib:sizing')
  , constants  = require( '../lib/constants' )
  , number     = require( 'alice-stdlib/number' )
  , calculateSize
  , getters
  , cutoff
  ;


cutoff = {
	 "MIN":constants.LENGTH_MIN
	,"SHRT":constants.LENGTH_SHORT
	,"MED":constants.LENGTH_MEDIUM
	,"LONG":constants.LENGTH_LONG
};


/**
 * determines the inclusion size bucket a particular string falls into
 * @name calculateSize
 * @function
 * @memberof module:alice-catalog/lib/sizing
 * @param {Number} length The number of characters in the string
 * @return {String|Null} Size. The string representation of the inclusion bucket the given string falls into. Null if it doesn't meet any of the requirements
 **/
exports.calculateSize = function( len ){
  var size;

  len = typeof len === 'string' ? len.length : len;

  if( number.between( len, cutoff.MIN, cutoff.SHRT ) ){
	  size = constants.SIZE_SHORT;
  } else if ( number.between(len, cutoff.SHRT, cutoff.MED )){
	  size = constants.SIZE_MEDIUM;
  } else if( number.between( len, cutoff.MED, cutoff.LONG, 1 ) ){
	  size = constants.SIZE_LONG;
  } else {
	  size = null
  }
  return size;
};


/**
 * DESCRIPTION
 * @name tenant
 * @function
 * @memberof module:alice-catalog/lib/sizing
 * @param {TYPE} NAME
 * @param {TYPE} NAME
 * @return
 **/
exports.tenant = function( name, limits ){
	var len = typeof name === 'string' ? name.length: name
	  , size
	  ;

	if( !limits.length ){
		return true;
	}
	size = exports.calculateSize( len )
	return limits.indexOf( size ) >= 0;
};

/**
 * DESCRIPTION
 * @name mascot
 * @function
 * @memberof module:alice-catalog/lib/sizing
 * @param {TYPE} NAME
 * @param {TYPE} NAME
 * @return
 **/
exports.mascot = function( name, limits ){
	var len = typeof name === 'string' ? name.length : name
	  , size
	  ;

	if( !limits.length ){
		return true;
	}
	size = exports.calculateSize( len );
	return limits.indexOf( size ) >= 0;
};
