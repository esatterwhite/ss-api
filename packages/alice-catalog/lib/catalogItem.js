/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * Responsible for generation individaul items for a tenant and activity catalog
 * @module alice-catalog/lib/item
 * @author Ryan Fisch
 * @author Robert Gable
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires events
 * @requires joi
 * @requires alice-log
 * @requires alice-stdlib/class
 * @requires alice-stdlib/class/options
 * @requires alice-stdlib/url
 * @requires alice-stdlib/lang
 * @requires alice-catalog/lib/constants
 * @requires alice-catalog/lib/s7
 * @requires alice-catalog/models/decal
 */

var  events    = require( 'events' )
   , qs        = require( 'qs' )
   , conf    = require( 'alice-conf' )
   , logger    = require( 'alice-log' )
   , Class     = require( 'alice-stdlib/class' )
   , Options   = require( 'alice-stdlib/class/options' )
   , url       = require( 'alice-stdlib/url' )
   , date      = require('alice-stdlib/date')
   , clone     = require( 'alice-stdlib/lang' ).clone
   , constants = require('./constants')
   , Decal     = require('../models/decal')
   , S7        = require('./s7')
   , number    = require( 'alice-stdlib/number' )
   , RENDER    = 'ir'
   , SERVE     = 'is'
   , EMPTY_ARRAY = []
   , Item
   , itemschema
   , SceneUrl
   ;


/**
 * Description
   ;

/**
 * Represents an individual Item in a tenant specific catalog
 * @alias module:alice-catalog/lib/item
 * @constructor
 * @extends EventEmitter
 * @mixes module:alice-stdlib/class
 * @mixes module:alice-stdlib/class/options
 * @param {Object} options Instance configuration options
 * @param {String} [options.color=FFFFFF] the hexcode of the decal item
 */
var testdebug = true;
Item = new Class(/* @lends module:alice-catalog/lib/catalogItem.prototype */{
    inherits:events.EventsEmitter
    ,mixin:[ Options ]
    ,options:{
        product: null
      , color: 'FFFFFF'
      , decal: null
      , decalAdditions: null
      , tenant: null
      , decalColors: null
      , fetching:null // activities, details, products
    }

    ,constructor: function( options ){
        var height
          , product
          , fetching
          , decal
          , tenant
          ;

        // apply over-rides
        this.setOptions( options );

        product           = this.options.product;
        tenant            = this.options.tenant ;
        fetching          = this.options.fetching;
        decal             = this.options.decal;
        height            = this.height;


        this.frontView = this.generateView( product && product.frontImage.imageRef || null, decal, height);
        this.printView = this.generateView('', decal, 3990, true);
        if (fetching == "widget") {
          this.widgetView = {
            product: this.generateView(product.frontImage.imageRef, decal, height, false, true),
            swatch: this.generateView(product.swatch, decal, height, false, true)
          };
        } else {
          this.widgetView = {};
        }

        if (product) {
            if( decal ){
                this.swatchView = this.generateView(product.swatch, decal, height);
            }

            if( this.options.fetching === 'details' ){

                decal.name = this.formatDecalName();

                this.backView = this.generateView(
                    product.backImage.imageRef
                    , false
                    , height
                );

                this.backViewWithDecal = this.generateView(
                    product.backImage.imageRef
                    , new Decal({s7id: '{{DECAL}}' })
                    , height
                );
            }
        } else if (decal) {
            this.swatchView = this.generateView(
                '8ea1c2ded7d44b67beb034e5eab9246d'
                , decal
                , height
            );

        }
    }

    /**
     * creates an metadata object about the product item being generated
     * @method module:alice-catalog/lib/item#generateIdentifiers
     * @return {Object} identifiers Returns the idetifiers as an object and query string
     **/
    , generateIdentifiers: function generateIdentifiers() {
        var identifierStr  = '';
        var identifiers = {
            color: false,
            gender: false,
            category: false,
            price: false,
            hasMascot: false,
            activity: false
        };

        this.productIdentifiers( identifiers );
        this.decalIdentifiers( identifiers );

        for (var i in identifiers) {
            identifierStr += ' data-filter' + i + '="' + identifiers[i] + '"';
        }

        return {obj: identifiers, attrs: identifierStr};
    }

    /**
     * Augments an identifier object with product information, if a product has been specified
     * @method module:alice-catalog/lib/item#productIdentifiers
     * @param {Object} identifiers Augments an identifiers object
     * @return {Object} Object containing product identifiers
     **/
    ,productIdentifiers: function( obj ){
        if( this.options.product ){
            obj.color    = this.color;
            obj.gender   = this.options.product.gender;
            obj.price    = this.options.product.unitPrice;
            obj.category = this.options.product.search.tags;
        }
        return obj;
    }

    , inclusions: function( additions, ignoreDetails ){
        var inclusions = {}
           , details = this.options.fetching === 'details' && !ignoreDetails
           , tenant  = this.options.tenant
           , personalization = this.personalization
           , result
        for( var key in personalization ){
          // The SPECIALEST case; customizable decals
          if (key == 'txto') {
             if (!details) continue;
             var customFields = parseInt(personalization[key]);
             if (isNaN(customFields) || !customFields) continue;
             for (var i =0; i < customFields; i++) {
               inclusions['$txto' + (i+1)] = "{{TXTO" + (i+1) + "}}";
            }
            continue;
          }
          if( personalization[ key ]  ){
            if( details ){
              result = "{{" + key + "}}"
            } else {
              switch( key ){
                  case constants.ART_MASCOT:
                    result = additions[ constants.ART_MASCOT ]
                    break;

                  case constants.ART_ACTIVITY:
                    result = additions.s7Id
                    break;

                  case constants.TEXT_ACTIVITY_NAME:
                    result = additions.category;
                    break;

                  case constants.TEXT_TENANT_CITY:
                    result = tenant.city
                    break;

                  case constants.TEXT_TENANT_STATE:
                    result = tenant.stateName;
                    break;

                  case constants.TEXT_TENANT_TYPE:
                    result = tenant.displayType
                    break;

                  case constants.TEXT_TENANT_ACRONYM:
                    result = tenant.acronym
                    break;

                  case constants.TEXT_TENANT_NAME:
                    result = tenant.name.trim();
                    break;
                  case constants.TEXT_DATE_ONE:
                    result = date.format( new Date(), personalization[ key ] )
                    break

                  case constants.TEXT_DATE_TWO:
                    result = date.format( date.increment( new Date(), 'year', 1) , personalization[ key ] )
                    break;

                  default:
                    continue;
              }
            }
            inclusions[ "$" + key ] = result
          }
        }
      return inclusions;
    }
    /**
     * Augments an identifiers object with decal specific information
     * @method module:alice-catalog/lib/item#decalIdentifiers
     * @param {Object} identifiers An identifiers object to include decal information on
     * @return {Object} the augmented identifiers object
     **/
    ,decalIdentifiers: function(obj){
        if ( this.personalization.arttm ){
            obj.hasMascot = true;
        }

        if ( this.personalization.txtanm ){
            obj.activity = this.options.decalAdditions.category;

        }

        return obj;
    }


    /**
     * Determines size restrictions for tenant name layer inclusion.
     * @method module:alice-catalog/lib/item#sizes
     * @param {Number} length The length a string to be included
     * @param {String[]} inclusions An array allowable sizes the tenant name maybe be included on a specific decal.
     * Tshese values generally come from `Decal.lengthLimits`
     * @param {Object} [overrides] Values to be used in place of calculated values
     * @param {Boolean} [overrides.small=null] If specified, will be used as the small inclusion value
     * @param {Boolean} [overrides.medium=null] If specified, will be used as the medium inclusion value
     * @param {Boolean} [overrides.large=null] If specified, will be used as the large inclusion value
     * @param {Number} [overrides.cutoff_small=9] If specified, this value will be used to denote when a string is too long for short text inclusions. Only used if the  small value was not specified
     * @param {Number} [overrides.cutoff_medium=9] If specified, this value will be used to denote when a string is too long for medium text inclusions. Only used if the  medium value was not specified
     * @param {Number} [overrides.cutoff_large=9] If specified, this value will be used to denote when a string meets the requirement for large text inclusions. Only used if the  large value was not specified
     * @return {SizeObject}
     * @example
var ci = new CatalogItem({ ... })
ct.sizes(10, ['MED', 'LONG']) // {small:false, med: false, long: true}
     */
    , tenantSizes: function (len, inclusions, overrides) {
        if (this.options.fetching === 'details') {
            return {
                  small: '{{TNSML}}'
                , medium: '{{TNMED}}'
                , large: '{{TNLRG}}'
        };
        }
        return this.sizes.apply(this, arguments);
    }
    , mascotSizes: function (len, inclusions, overrides) {
        if (this.options.fetching === 'details') {
            return {
                  small: '{{MNSML}}'
                , medium: '{{MNMED}}'
                , large: '{{MNLRG}}'
            };
        }
        return this.sizes.apply(this, arguments);
    }
    ,formatDecalName: function () {
      //[mascot] [activity] [class year] [city] [state]
      var inclusions = this.inclusions( this.options.decalAdditions, true );
      var str = this.options.decal.name;
        str = str.replace("[mascot]", this.options.tenant.mascot.name );
        str = str.replace("[city]", inclusions['$' + constants.TEXT_TENANT_CITY])
        str = str.replace("[activity]", this.options.decalAdditions[constants.TEXT_ACTIVITY_NAME]);
        str = str.replace("[state]", inclusions['$' + constants.TEXT_TENANT_STATE]);
      var date = (inclusions['$' + constants.TEXT_DATE_ONE] ? inclusions['$' + constants.TEXT_DATE_ONE] : inclusions['$' + constants.TEXT_DATE_TWO]);
        str = str.replace("[class year]", date);
      return str;
    }
    , sizes: function tenatSizes( len, inclusions, overrides ){
        var allowShort
          , allowMed
          , allowLong
          , showingSmall
          ;

        inclusions = inclusions || [];
        overrides  = overrides  || {};

        allowShort = inclusions.length ? inclusions.indexOf( constants.SIZE_SHORT )  >= 0 : true;
        allowMed   = inclusions.length ? inclusions.indexOf( constants.SIZE_MEDIUM ) >= 0 : true;
        allowLong  = inclusions.length ? inclusions.indexOf( constants.SIZE_LONG )   >= 0 : true;
        showingSmall = overrides.hasOwnProperty('small')  ? overrides.small  : allowShort && len < ( overrides.cutoff_small || constants.LENGTH_SHORT );

        return {
             small  : showingSmall
           , medium : overrides.hasOwnProperty(constants.SIZE_MEDIUM) ? overrides.medium : allowMed   && len < ( overrides.cutoff_medium  || constants.LENGTH_MEDIUM ) &&!showingSmall
           , large  : overrides.hasOwnProperty(constants.SIZE_LARGE)  ? overrides.large  : allowLong  && len >= ( overrides.cutoff_large   || constants.LENGTH_MEDIUM )
        };
    }


    // TODO: This function is trying to do too much. should be broken up into more digestable peices
    /**
     * Generates the layers containing s7 urls to render a UI view
     * @method module:alice-catalog/lib/item#generateView
     * @param {String} viewtype The type of view to generate (`details`, `product`)
     * @param {module:alice-catalog/models/decal} NAME ...
     * @param {Number} height The height of the product decal
     * @param {Boolean} printview specifiy if to include a print view laer
     **/
    ,generateView: function generateView(productView, decal, height, printView, widgetView) {
        var layers          = []                                                                     //
          , viewColor       = this.viewColor                                                         //
          , tenantName      = encodeURIComponent(this.options.tenant.name)
          , mascotName      = (this.options.tenant.mascot.name) ? encodeURIComponent(this.options.tenant.mascot.name) : 'Team'
          , inclusions      = decal && decal.inclusions || {}                                        //

          , decalAdditions  = this.options.decalAdditions                                            //
          , qualityString   = '&qlt=80,0&resmode=bilin'                                              //
          , quality = '80,0'
          , resmode = 'bilin'
          , additionString  = '&'  // fxg additions for querystring
          , additionArr = []
          , mascotColors                                                                             //
          , tenantNameSize                                                                           //
          , mascotNameSize                                                                           //
          ;


        if (!tenantName) tenantName = encodeURIComponent(this.options.tenant.longName);
        if (this.options.fetching === 'details') {
            tenantName = '{{tn}}';
            mascotName = '{{mn}}';
            mascotColors = { clrp: '{{CLRP}}', clrs: '{{CLRS}}', clrt: '{{CLRT}}', clrw: '{{CLRW}}', clrb: '{{CLRB}}'};
        } else if ( !!this.options.decalColors ) {
            mascotColors = this.options.decalColors;
        } else {
            mascotColors = {
                clrp: this.options.tenant.primaryColor.hex || 'ffffff',
                clrs: this.options.tenant.secondaryColor.hex || 'ffffff',
                clrt: (this.options.tenant.teritaryColor) ? this.options.tenant.teritaryColor.hex : 'ffffff',
                clrw: 'ffffff',
                clrb: '000000'
            };
       }


       debugger;
        tenantNameSize = this.tenantSizes( tenantName.length, decal && decal.lengthLimits.tenant, {cutoff_medium: 16, cutoff_large: 16});
        mascotNameSize = this.mascotSizes( mascotName.length, decal && decal.lengthLimits.mascot );

        if (height === '{{HEIGHT}}' || height > 400) {
            qualityString = '&qlt=95,0&resmode=sharp2';
            quality       = '95,0';
            resmode       = 'sharp2';
        }

        var inclusions = this.inclusions( decalAdditions );
        additionString += decodeURIComponent( url.qs.legacy.stringify( inclusions ) );
      if (widgetView ) {
        layers = conf.get('DEFAULT_PROTOCOL') + '://a248.e.akamai.net/f/248/9086/10h/s7d7.scene7.com/ir/render/spiritshopRender/' + productView + '?hei=' + height + qualityString + '&fmt=png&resmode=sharp2&obj=main/color&color=' + viewColor+ '&obj=main/imprint&decal&res=100&show&src=fxg{spiritshop/' + decal.s7id + '?setAttr.txt_t_nm_MED={visible=' + tenantNameSize.medium +'}&setAttr.txt_t_nm_LONG={visible=' + tenantNameSize.large+'}&setAttr.txt_t_nm_SHRT={visible=' + tenantNameSize.small+'}&setAttr.txt_t_l_MED={visible=' + mascotNameSize.medium + '}&setAttr.txt_t_l_SHRT={visible=' + mascotNameSize.small +'}&setAttr.txt_t_l_LONG={visible=' + mascotNameSize.large + '}&$txttl=' + mascotName + ' &$txttnm=' + tenantName  + additionString +  '&setAttr.clrp={baseColorValue=%23' + mascotColors.clrp + '}&setAttr.clrs={baseColorValue=%23' + mascotColors.clrs + '}&setAttr.clrt={baseColorValue=%23' + mascotColors.clrt + '}&setAttr.clrw={baseColorValue=%23' + mascotColors.clrw + '}&setAttr.clrb={baseColorValue=%23' + mascotColors.clrb + '}}';
      }
        else if ( printView && decal ) {
            layers = new S7.url.serve(decal.s7id, {
                protocol                : 'http'
              , hei                     : 3990
              , dpi                     : 266
              , qlt                     : '100,0'
              , resMode                 : resmode
              , 'setAttr.txt_t_nm_MED'  : '{visible=' + tenantNameSize.medium +'}'
              , 'setAttr.txt_t_nm_LONG' : '{visible=' + tenantNameSize.large+'}'
              , 'setAttr.txt_t_nm_SHRT' : '{visible=' + tenantNameSize.small+'}'
              , 'setAttr.txt_t_l_MED'   : '{visible=' + mascotNameSize.medium + '}'
              , 'setAttr.txt_t_l_SHRT'  : '{visible=' + mascotNameSize.small +'}'
              , 'setAttr.txt_t_l_LONG'  : '{visible=' + mascotNameSize.large + '}'
              , '$txttl'                :  mascotName
              , '$txttnm'               :  tenantName
              , 'setAttr.clrp'          : '{baseColorValue=%23' + mascotColors.clrp + '}'
              , 'setAttr.clrs'          : '{baseColorValue=%23' + mascotColors.clrs + '}'
              , 'setAttr.clrt'          : '{baseColorValue=%23' + mascotColors.clrt + '}'
              , 'setAttr.clrw'          : '{baseColorValue=%23' + mascotColors.clrw + '}'
              , 'setAttr.clrb'          : '{baseColorValue=%23' + mascotColors.clrb + '}}'
              , sharp                   : 1
              , res                     : 100
              , rs                      : 'U1'
              , pos                     : '0,0'
              , fmt                     : 'png-alpha'
              , path                    : '/is/agm/spiritshop/'
              , hostname                : 's7d7.scene7.com'
              , additionObj: inclusions
            });

        } else if (productView && decal) {
            // product render
            layers.push(new S7.url.render({
                    path    : '/ir/render/spiritshopRender/' + productView
                  , hei     : height
                  , obj     : 'main/color'
                  , color   : viewColor
                  , sharp   : 1
                  , rs      : 'U1'
                  , pos     : '0,0'
                  , qlt     : quality
                  , resMode : resmode
                  , resmode :'sharp2'
                })
            );
            // FIXME: This url has nasty FXG filter objects in it and it is difficult to encode correctly. So doing it by hand --ERS
          layers.push(conf.get('DEFAULT_PROTOCOL') + '://a248.e.akamai.net/f/248/9086/10h/s7d7.scene7.com/ir/render/spiritshopRender/' + productView + '?hei=' + height + qualityString + '&obj=main/color&color=' + viewColor+ '&obj=main/imprint&decal&show&src=fxg{spiritshop/' + decal.s7id + '?setAttr.txt_t_nm_MED={visible=' + tenantNameSize.medium +'}&setAttr.txt_t_nm_LONG={visible=' + tenantNameSize.large+'}&setAttr.txt_t_nm_SHRT={visible=' + tenantNameSize.small+'}&setAttr.txt_t_l_MED={visible=' + mascotNameSize.medium + '}&setAttr.txt_t_l_SHRT={visible=' + mascotNameSize.small +'}&setAttr.txt_t_l_LONG={visible=' + mascotNameSize.large + '}&$txttl=' + mascotName + ' &$txttnm=' + tenantName  + additionString +  '&setAttr.clrp={baseColorValue=%23' + mascotColors.clrp + '}&setAttr.clrs={baseColorValue=%23' + mascotColors.clrs + '}&setAttr.clrt={baseColorValue=%23' + mascotColors.clrt + '}&setAttr.clrw={baseColorValue=%23' + mascotColors.clrw + '}&setAttr.clrb={baseColorValue=%23' + mascotColors.clrb + '}}&fmt=png-alpha&sharp=1&res=100&rs=U1&pos=0,0&req=object');
        } else if (productView && !decal) {
            layers.push( new S7.url.render( productView, {hei:height, qlt: quality, obj: 'main/color', color: viewColor, sharp:1 , rs: 'U1', pos   : '0,0'} ));
        } else if (!productView && decal) {
            layers.push(
                 new S7.url.serve(
                    decal.s7id
                    ,{
                        '$txttl'                 : mascotName
                        ,'$txttnm'               : tenantName
                        ,'setAttr.txt_t_nm_MED'  : '{visible=' + tenantNameSize.medium+'}'
                        ,'setAttr.txt_t_nm_LONG' : '{visible=' + tenantNameSize.large+'}'
                        ,'setAttr.txt_t_nm_SHRT' : '{visible=' + tenantNameSize.small+'}'
                        ,'setAttr.txt_t_l_MED'   : '{visible=' + mascotNameSize.medium + '}'
                        ,'setAttr.txt_t_l_SHRT'  : '{visible=' + mascotNameSize.small +'}'
                        ,'setAttr.txt_t_l_LONG'  : '{visible=' + mascotNameSize.large + '}'
                        ,'setAttr.clrp'          : '{baseColorValue=%23' + mascotColors.clrp + '}'
                        ,'setAttr.clrs'          : '{baseColorValue=%23' + mascotColors.clrs + '}'
                        ,'setAttr.clrt'          : '{baseColorValue=%23' + mascotColors.clrt + '}'
                        ,'setAttr.sW'            : '{baseColorValue=%23' + mascotColors.clrw + '}'
                        ,'setAttr.sB'            : '{baseColorValue=%23' + mascotColors.clrb + '}'
                        ,fmt                     : 'png-alpha'
                        ,hei                     : height
                    }
                )
            );
        }
        return layers;
    }

    /**
     * Defines a serialization structure
     * @private
     * @method module:alice-catalog/lib/item#toJSON
     **/
    ,toJSON: function toJSON(  ){
        var returnObj = {
            product          : this.options.product
          , decal            : this.options.decal
          , template         : this.options.decal
          , spectrum         : this.options.spectrum
          , name             : this.name
          , s7id             : this.name
          , color            : this.color
          , price            : this.price
          , fmtPrice         : this.formattedPrice
          , linkurl          : this.linkUrl
          , identifiers      : this.generateIdentifiers()
          , mascot           : this.personalization.arttm
          , productUrl       : this.productUri
          , decalUrl         : this.decalUri
          , frontView        : this.frontView
          , swatchView       : this.swatchView || []
          , backView         : this.backView
          , backDecalView    : this.backViewWithDecal
          , shortDescription : this.shortDescription
          , display_order    : this.display_order
          , category         : this.additions.category
          , widgetView       : this.widgetView
          , debug            :['foo','bar','baz']
        };
        if (this.options.fetching == "details") {
            returnObj.personalization = this.options.decal.personalization;
            returnObj.additions = this.additions;
            returnObj.fullDescription  = this.fullDescription;
            returnObj.printView = this.printView
        }
        return returnObj;
    }
});

Object.defineProperties( Item.prototype, {
    /**
     * @memberof module:alice-catalog/lib/item
     * @property {String} linkUrl returns the full url for an item
     */
    linkUrl:{
        get: function(){
            var product = this.options.product
              , decal = this.options.decal
              , personal = this.personalization
              , urifragment = ''
              , personalization
              ;

            if ( product && decal) {

                // FIXME:  This seems wrong and too rigid.
                // This cant be the only possible out comes. -ERS
                if ( personal['artad'] ){
                    urifragment = "/a/"
                    personalization = 'artad'
                } else if ( personal['txtanm'] ) {
                    urifragment = '/t/';
                    personalization = 'txtanm';
                }

                urifragment += encodeURIComponent( this.options.decalAdditions[ personalization ] || '' );

                return url.join(
                   'product'
                  , product._id
                  ,  decal.s7id
                  , this.color
                  ,  urifragment
                );
            } else if (product) {
                return this.productUri;
            } else if (decal) {
                return this.decalUri;
            }
        }
    }
    /**
     * @property {String|Boolean productUri The uri used to find an activity for a product
     */
    ,productUri:{
        get: function(){
            if (this.options.product) {
              return 'activities?' + qs.stringify({ product:this.options.product._id.toString(), color:this.options.color, additions:this.options.decalAdditions });

            } else {
                return null;
            }
        }
    }
    /**
     * @property {String|Boolean decalUri url use to get products with specific decal additions
     */
    ,decalUri:{
        get: function(){
            if (this.options.decal) {
              return 'products?' + qs.stringify({decal:this.options.decal.s7id, additions:this.options.decalAdditions});
            } else  {
                return null;
            }
        }
    }

    /**
     * @property {String} name The name of the object being generated based on the feching parameter
     */
    ,name: {
        get: function(){
            switch(this.options.fetching){
                case 'product':
                    return this.options.product.name;
                case 'activities':
                    return this.options.decal.name;
                default:
                    return this.options.product && this.options.product.name || '';
            }
        }
    }

    /**
     * @property {Object} personalization returns the currect set of personalization flags for decal rendering
     */
    ,personalization:{
        get: function(){
            return this.options.decal && this.options.decal.personalization || {};
        }
    }
    /**
     * @property {String} color Returns the currently configured hex color
     */
    ,color:{
        get: function(){
            return this.options.color;
        }
    }
    /**
     * @property {String} gender
     */
    ,gender:{
        get: function(){}
    }
    /**
     * @property {Number} price returns the current price of the Product in Dollars
     */
    ,price:{
        get: function(){
            return this.options.product && ( this.options.product.economics.price  );
        }
    }
    ,formattedPrice: {
        get: function () {
            return this.options.product && ( number.getDollars(this.options.product.economics.price));
        }
    }
    /**
     * @property {String} category
     */
    ,category:{
        get: function(){}
    }
    /**
     * @property {String} shortDescription Returns the short verson of the decription of the current product
     */
    ,shortDescription:{
        get: function(){
            return (this.options.product) ? this.options.product.shortDescription: false;
        }
    }

    ,additions:{
      get:function(){
        return this.options.decalAdditions ? this.options.decalAdditions : {};
      }
    }
    /**
     * @property {String} fullDescription returns the full description of the current product
     */
    ,fullDescription:{
        get: function( ){
            return (this.options.fetching === 'activities') ?  'Click on this Decal to put it on your apparel item of choice!' : this.options.product ?  this.options.product.fullDescription : '';
        }
    }

    /**
     * @property {String} viewColor returns the hex code currently configured color
     **/
    ,viewColor:{
        get: function(){
            return this.options.fetching === 'details' ? '{{PRODUCTCOLOR}}' : this.options.color || 'ffffff';
        }
    }

    /**
     * @property {Number|String} height returns the height of the rendering area on the UI
     **/
    ,height:{
        get: function( ){
            return this.options.fetching === 'details' ? '{{HEIGHT}}' : 523;
        }
    }

    ,display_order:{
      get:function(){
        return this.options.decal ? this.options.decal.display_order : 0;

      }
    }
});

module.exports = Item;
