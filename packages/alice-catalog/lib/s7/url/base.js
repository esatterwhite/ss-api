/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Abstraction around creating complex urls for scene scene7
 * @module alice-catalog/lib/s7/url/base
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires alice-stdlib/url
 * @requires alice-stdlib/class
 * @requires alice-conf
 * @requires alice-logger
 * @requires alice-stdlib/class/options
 * @requires alice-stdlib/class/parent
 * @requires alice-stdlib/lang/clone
 */

var Class   = require( 'alice-stdlib/class' )
  , logger  = require( 'alice-log' )
  , conf    = require( 'alice-conf' )
  , url     = require( 'alice-stdlib/url' )
  , Options = require( 'alice-stdlib/class/options' )
  , Parent  = require( 'alice-stdlib/class/parent' )
  , clone   = require( 'alice-stdlib/lang' ).clone
  , SceneUrl
  ;

/**
 * @constructor
 * @alias module:alice-catalog/lib/s7/url/base
 * @param {Object} options
 */
SceneUrl = new Class({
	mixin: [ Options, Parent ]

	,options:{
		protocol: conf.get('DEFAULT_PROTOCOL')
		,path:'/'
		,hostname:'a248.e.akamai.net/f/248/9086/10h/s7d7.scene7.com'
	}

	,constructor: function(  ){
		var options;

		if( arguments.length == 1 ){
			options = arguments[0]
		}else{
			this.id = arguments[0]
			options = arguments[1]
		}

		this.setOptions( options )
	}

	/**
	 * Generates a string representation of URL object. Which is a fully qualified url to a scene 7 object
	 * @method module:alice-catalog/lib/s7/url/base#toString
	 * @return {String} fully qualified url to a scene seven object
	 **/
	,toString: function(){
		var opts = clone( this.options )
        ,additions = this.options.additionObj;
		delete opts['hostname'];
		delete opts['protocol'];
		delete opts['path'];
		delete opts['auth'];
		delete opts['slashes'];
		delete opts['href'];
		delete opts['port'];
		delete opts['search'];
		delete opts['hash'];
    delete opts['additionObj'];
    if (additions) {
      for (var i in additions) {
         opts[i] = additions[i];
      }
    }
		return decodeURIComponent( url.format({
			protocol: this.options.protocol
			,hostname: this.options.hostname
			,pathname: url.join( this.options.path, this.id )
			,query:opts
		}) )
	}
	/**
	 * creates an object suitable for JSON serialization
	 * @method module:alice-catalog/lib/s7/url/base#toString
	 * @return {String} fully qualified url to a scene seven object
	 **/
	,toJSON: function(){
		return "" + this;
	}
});

module.exports = SceneUrl
