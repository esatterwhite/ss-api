/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Abstraction around creating complex urls for scene scene7
 * @module alice-catalog/lib/s7/url/render
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires alice-stdlib/url
 * @requires alice-stdlib/class
 * @requires alice-stdlib/class/options
 * @requires alice-stdlib/class/parent
 * @requires alice-stdlib/lang/clone
 */

var Class   = require( 'alice-stdlib/class' )
  , logger  = require( 'alice-log' )
  , url     = require( 'alice-stdlib/url' )
  , Options = require( 'alice-stdlib/class/options' )
  , Parent  = require( 'alice-stdlib/class/parent' )
  , clone   = require( 'alice-stdlib/lang' ).clone
  , SceneUrl = require('./base')
  ;

/**
 * @constructor
 * @alias module:alice-catalog/lib/s7/url/render
 * @param {Object} options
 */
var SceneRenderUrl = new Class({
    inherits: SceneUrl
    ,options:{
        path:'/ir/render/spiritshopRender'
    }
});

module.exports = SceneRenderUrl
