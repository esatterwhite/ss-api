/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Abstraction around creating complex urls for scene scene7
 * @module alice-catalog/lib/s7/url/serve
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires alice-stdlib/url
 * @requires alice-stdlib/class
 * @requires alice-stdlib/class/options
 * @requires alice-stdlib/class/parent
 * @requires alice-stdlib/lang/clone
 */

var Class   = require( 'alice-stdlib/class' )
  , logger  = require( 'alice-log' )
  , url     = require( 'alice-stdlib/url' )
  , Options = require( 'alice-stdlib/class/options' )
  , Parent  = require( 'alice-stdlib/class/parent' )
  , clone   = require( 'alice-stdlib/lang' ).clone
  , SceneUrl = require( './base' )
  ;

/**
 * @constructor
 * @alias module:alice-catalog/lib/s7/url/serve
 * @param {Object} options
 */
var SceneServeUrl = new Class({
    inherits: SceneUrl
    ,options:{
         path:'/is/agm/spiritshop/'
        ,hei                     : 3990
        ,dpi                     : 266
        ,qlt                     : '100,0'
        ,sharp                   : 1
        ,res                     : 100
        ,rs                      : 'U1'
        ,pos                     : '0,0'
        ,resmode                 : 'sharp'
        ,'$txttl'                : null
        ,'$txttnm'               : null
        ,'setAttr.txt_t_nm_MED'  : ""
        ,'setAttr.txt_t_nm_LONG' : ""
        ,'setAttr.txt_t_nm_SHRT' : ""
        ,'setAttr.txt_t_l_MED'   : ""
        ,'setAttr.txt_t_l_SHRT'  : ""
        ,'setAttr.txt_t_l_LONG'  : ""
        ,'setAttr.clrp'          : ""
        ,'setAttr.clrs'          : ""
        ,'setAttr.clrt'          : ""
        ,'setAttr.clrw'          : ""
        ,'setAttr.clrb'          : ""
        ,fmt                     : 'png-alpha'
      ,additionObj : {}
    }
    ,constructor: function( ){
        Array.prototype.unshift.call( arguments, 'constructor' )
        this.parent.apply(this, arguments );
    }

})
module.exports = SceneServeUrl;
