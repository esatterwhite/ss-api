/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Configuration options for alice-catalog
 * @module module:alice-catalog/conf
 * @author Eric Satterwhite
 * @since 0.1.0
 */

exports.DEFAULT_PROTOCOL = 'http'
