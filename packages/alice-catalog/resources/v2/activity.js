/*jshint laxcomma: true, node: true, smarttabs: true*/
'use strict';
/**
 * Quick and dirty CRUD endpoint for dealing with activities
 * @module alice-catalog/resources/v2/activity
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires http
 * @requires joi
 * @requires url
 * @rquires boom
 * @requires alice-web/lib/paginator
 * @requires alice-stdlib/collection
 * @requires alice-core/lib/db/schema/toJoi
 * @requires alice-core/lib/db/schema/validators/defaults
 * @requires alice-catalog/models/activity
 */

var http       = require('http')
  , joi        = require( 'joi' )
  , Boom       = require( 'boom' )
  , url        = require('url')
  , Paginator  = require('alice-web/lib/paginator')
  , forEach    = require('alice-stdlib/collection').forEach
  , toJoi      = require('alice-core/lib/db/schema/toJoi')
  , validators = require('alice-core/lib/db/schema/validators/defaults')
  , Activity   = require('alice-catalog/models/activity')
  , queryset   = Activity.find().limit(25).toConstructor()
  ;

var CODES =[];

exports.get_list = {
	method:"GET"
	,path:'/activity'
	,config:{
		jsonp:'callback'
		,description:"Look up tenants matching specific criteria"
		,tags:['api']
		,plugins:{
			'hapi-swagger':{
				responseMessages:CODES
			}
		}

		,handler: function tenant_get( request, reply ){
			var query = new queryset();
			query.model.count(function( err, count ){

				if( err ){
					return reply(new Boom.wrap( err ) );
				}

				query
				.skip( request.query.offset || 0 )
				.limit( request.query.limit )
				.exec(function( err, results ){
					var paginator = new Paginator({
						req:request
						,res:reply
						,objects:results
						,count: count
						,limit:request.query.limit
						,offset:request.query.offset
					});
					
					reply( paginator.page() );
				});

			})
		}
		,validate:{
			query: validators.query.clone()
		}
	}
};

exports.get_detail = {
	method:'GET'
	,path:'/activity/{activity_id}'
	,config:{
		description:"Find a specific activity by id"
		,tags:['api']

		,validate:{
			query: validators.query.clone()
			,params:{
				activity_id:joi.string().length(24).alphanum().required().description("Mongo Object ID String")
			}
		}
		,handler: function tenant_by_id( request, reply ){
			var query = new queryset();

			query.findOne({_id:request.params.activity_id}, function( err, t ){
				reply( err && new Boom.wrap( err ) || t.toJSON() );
			});
		}
	}
};


exports.post_list = {
	method:'POST'
	,path:'/activity'
	,config:{
		description:"Creates a new activity instance"
		,tags:['api']
		,validate:{
			query: validators.query.clone()
			,payload:toJoi( Activity.schema )
		}
		,handler: function post_list( request, reply ){
			// NOTE - This doesn't save. Just validates payload
			// creates a new instances and returns it.
			new Activity(request.payload).save( function( e, activity ){

				return reply( activity.toJSON() ).created()
			})
		}
	}
}

