/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * product API Endpoint definitions
 * @module alice-catalog/resources/v2/product
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires joi
 * @requires url
 * @requires tastypie/lib/serializer
 * @requires alice-stdlib/class
 * @requires alice-stdlib/lang
 * @requires alice-core/lib/db/schema/toJoi
 * @requires alice-web/lib/paginator
 * @requires alice-catalog/models/product
 */

var joi        = require( 'joi' )
  , url        = require('url')
  , Class      = require('alice-stdlib/class')
  , Serializer = require('tastypie/lib/serializer')
  , clone      = require('alice-stdlib/lang').clone
  , toJoi      = require('alice-core/lib/db/schema/toJoi')
  , Paginator  = require('alice-web/lib/paginator')
  , Product    = require('../../models/product')
  , queryset   = Product.find().limit(25).toConstructor()
  ;


var default_params = joi.object({
	limit:joi.number().min(0).default(25).description("the number of records to return")
  , offset:joi.number().min(0).default(0).description("the starting page number")
  , format:joi.string().alphanum().allow('json','xml','jsonp')
  , callback:joi.string().alphanum().description('query parameter used to force a jsonp response')
  , apikey:joi.string().guid()
}).unknown()

exports.get_list = {
	method:'GET'
	,path:'/product'
	,config:{
		description:"Find products matching a specific criteria"
		,tags:['api']
		,validate:{
			query: default_params.clone()
		}
		,handler:function( request, reply ){
			var query = new queryset()
			query.model.count( function( err, count ){
				if( err ){
					return reply( err )
				}

				query
					.skip( request.query.offset || 0 )
					.limit( request.query.limit )
					.exec(function( e, results ){
						var paginator = new Paginator({
							req:request
							,res:reply
							,objects:results
							,count: count
							,limit:request.query.limit
							,offset:request.query.offset
							,collectionName:'data'
						});
						
						reply( paginator.page() )
					})
			}) 
		}
	}
}
