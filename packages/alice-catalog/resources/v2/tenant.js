/*jshint laxcomma: true, node: true, smarttabs: true*/
'use strict';
/**
 * Second version of tenant endpoints with full Crud operations, paging and other standard API features v1 does not implement
 * @module alice-catalog/resources/v2/tenant 
 * @author Eric Satterwhite
 * @since 0.2.0
 * @requires joi
 * @requires url
 * @requires http
 * @requiers alice-catalog/models/tenant
 * @requiers alice-stdlib/class
 * @requires alice-stdlib/lang
 * @requires alice-stdlib/collection
 * @requires alice-core/lib/db/schema/toJoi
 */

var joi        = require( 'joi' )
  , http       = require('http')
  , url        = require('url')
  , Boom       = require( 'boom' )
  , Tenant       = require('alice-catalog/models/tenant')
  , toJoi        = require('alice-core/lib/db/schema/toJoi')
  , validators = require('alice-core/lib/db/schema/validators/defaults')
  , Class        = require('alice-stdlib/class')
  , typecast     = require('alice-stdlib/string').typecast
  , toArray      = require('alice-stdlib/lang').toArray
  , clone        = require('alice-stdlib/lang').clone
  , isFunction   = require('alice-stdlib/lang').isFunction
  , forEach      = require('alice-stdlib/collection').forEach
  , namespace    = require('alice-stdlib/object').namespace
  , Paginator    = require('alice-web/lib/paginator')
  , queryset     = Tenant.find().limit(25).toConstructor()
  , orderExp     = /^(\-)?([\w]+)/
  , terms
  , applySorting
  , buildFilters
  , querydefaults
  ;

terms = {
	'gt'          : '$gt'
  , 'gte'         : '$gte'
  , 'in'          : '$in'
  , 'lt'          : '$lt'
  , 'lte'         : '$lte'
  , 'ne'          : '$ne'
  , 'nin'         : '$nin'
  , 'regex'       : '$regex'
  , 'all'         : '$all'
  , 'size'        : '$size'
  , 'match'       : '$elemMatch'
  , 'contains'    : { key:'$regex', value: function( term ){ return new RegExp( term )}}
  , 'icontains'   : { key:'$regex', value: function( term ){ return new RegExp(term, 'i')}}
  , 'startswith'  : { key:'$regex', value: function( term ){ return new RegExp( '^' + term ) }}
  , 'istartswith' : { key:'$regex', value: function( term ){ return new RegExp( '^' + term, 'i' )}}
  , 'endswith'    : { key:'$regex', value: function( term ){ return new RegExp( term + '$' ) }}
  , 'iendswith'   : { key:'$regex', value: function( term ){ return new RegExp( term + '$', 'i') }}
}


querydefaults = validators.query._inner.children.map(function( c ){ return c.key })
var paths = Object.keys( Tenant.schema.paths );

var allowablepaths = paths.filter( function( p ){
	return ( p !== '_id' && p !== '__v');
});

// this is actually significantly faster than Array#map
function quickmap(array, mapFunction) {
  var arrayLen = array.length;
  var newArray = new Array(arrayLen);
  for(var i = 0; i < arrayLen; i++) {
    newArray[i] = mapFunction(array[i], i, array);
  }
  return newArray;
}
    
function join( array, sep ){
	return quickmap( array, function( i ){
		return i.key ? i.key : i;
	}).join( sep )
}

/**
 * set "nested" object property
 */
function set(obj, prop, val){
    var parts = (/^(.+)\_\_(.+)$/).exec(prop);
    if (parts){
        namespace(obj, parts[1])[parts[2]] = val;
    } else {
        obj[prop] = val;
    }

    return obj;
}

applySorting = function( mquery, rquery ){
	var ordering = {};
	toArray( rquery.orderby ).forEach( function( param ){
		var bits = orderExp.exec( param );
		
		if( !bits ){
			return;
		}

		ordering[ bits[2] ] = bits[1] ? -1 : 1;
	});

	mquery.sort( ordering );
	return mquery;
}

buildFilters = function( obj ){
	var remaining = {};
	for( var key in obj ){
		var bits = key.split('__')
		   , filter = {}
           , value
           , fieldname
           , filtertype
           , last 

        value     = obj[key];
		fieldname = bits.shift();
		
		bits = quickmap(bits, function( bit ){
			if( terms.hasOwnProperty( bit ) ){
				return terms[ bit ];
			}
			throw new Error("unknown filter type " + bit );
		});

		last = bits[ bits.length - 1 ];
		// should be defined on resource instance
		if( allowablepaths.indexOf( fieldname ) >=0 ){
			filter = bits.length ? set( filter, join( bits, '__' ),  isFunction( last.value ) ? last.value( value ) : typecast( value ) ) : typecast( value );
			remaining[ fieldname ] = filter;
		}
	}
	return remaining;
};

var CODES =[] 
forEach( http.STATUS_CODES, function(v,k){
	if( k > 399 ){
		CODES.push({code:k,message:v})
	}
})

exports.get_list = {
	method:"GET"
	,path:'/tenant'
	,config:{
		description:"Look up tenants matching specific criteria"
		,tags:['api']
		,plugins:{
			'hapi-swagger':{
				responseMessages:CODES
			}
		}
		,response:{
			schema: validators.response.clone()
		}
		,handler: function tenant_get( request, reply ){
			var query = new queryset();
			query.model.count(function( err, count ){
				if( err ){
					return reply(err)
				}

				query
				.skip( request.query.offset || 0 )
				.limit( request.query.limit )

				applySorting( query, request.query )
				
				var values = buildFilters( request.query )

				query
					.where( values )

				query
					.exec(function( err, results ){
						var paginator = new Paginator({
							req:request
							,res:reply
							,objects:results
							,count: count
							,limit:request.query.limit
							,offset:request.query.offset
						});
						
						reply( paginator.page() )
					});

			})
		}
		,validate:{
			query: validators.query.clone()
		}
	}
};

exports.get_detail = {
	method:'GET'
	,path:'/tenant/{tenant_id}'
	,config:{
		handler: function tenant_by_id( request, reply ){
			var query = new queryset();

			query.findOne({_id:request.params.tenant_id}, function( err, t ){
				var o  = t.toObject({getters:true, virtuals:true})
				console.log( o )
				reply( err || o )
			})
		}
		,plugins:{
			'hapi-swagger':{
				responseMessages:CODES
			}
		}
		,description:"Find a specific tenant by id"
		,tags:['api']
		,validate:{
			query: validators.query.clone()
			,params:{
				tenant_id:joi.string().length(24).alphanum().required().description("Mongo Object ID String")
			}
		}
	}
};
exports.put_list={
	method:'PUT'
	,path:'/tenant/{tenant_id}'
	,config:{
		description:"Modify an existing tenant"
		,plugins:{
			'hapi-swagger':{
				responseMessages:CODES
			}
		}
		,response:{
			schema: validators.response.clone()
		}
		,tags:['crud','tenant','core', 'api']

		,validate:{
			query: validators.query.clone()
			,payload:toJoi( Tenant.schema )
			,params:{
				tenant_id:joi.string().length(24).alphanum().required().description("Mongo Object ID String")
			}
		}
		,handler:function put_list( request, reply ){
			var query = new queryset();

			query.findOne({_id:request.params.tenant_id}, function( err, t ){
				reply( err || t )
			})
		}
	}
};

exports.post_list = {
	method:'POST'
	,path:'/tenant'
	,config:{
		description:"Creates a new tenant instance"
		,tags:['crud', 'tenant', 'api']

		,handler: function post_list( request, reply ){
			var t = new Tenant(request.payload).toObject({ getters:true, virtuals: true })
			t.save(function(err){
				return err ? reply( err ) : reply( t ).created()
			});

		}
		,validate:{
			query: validators.query.clone()
			,payload:toJoi( Tenant.schema )
		}
	}
}

