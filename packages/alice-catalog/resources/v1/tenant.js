/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Reimplementing removed stuff
 * @module alice-catalog/resources/v1/tenant
 * @author Robert Gable
 * @requires joi
 * @requires url
 * @requires http
 * @requiers alice-catalog/models/tenant
 * @requiers alice-stdlib/class
 * @requires alice-stdlib/lang
 * @requires alice-stdlib/collection
 * @requires alice-core/lib/db/schema/toJoi
 */


var joi       = require( 'joi' )
, Tenant    = require('alice-catalog/models/tenant')
, Class     = require('alice-stdlib/class')
, queryset  = Tenant.find().limit(25).toConstructor()
, Paginator = require('alice-web/lib/paginator')
, url       = require('url')
, clone     = require('alice-stdlib/lang').clone
, toJoi     = require('alice-core/lib/db/schema/toJoi')
, forEach   = require('alice-stdlib/collection').forEach
, http      = require('http')
, Boom               = require( 'boom' )


;
exports.tenantLookup = {
  method: "GET"
  ,path: "/tenant/{tenantId}"
  ,config: {
    description: "Returns a tenant using a normal ID like a normal website where normal things happen"
    ,tags:['api']
    ,validate: {
      query: joi.object({
        apikey:joi.string().guid()

      })
      ,params:joi.object({
                tenantId:joi.string().length(24).alphanum().description('Mongo ID For tenant').required()
      })
    }
  }
  ,handler: function (request, reply) {
    Tenant.findById(request.params.tenantId, function (err, tenant) {
      //logger.info("Tenant - Looking Tenant up by Legacy Id");
      if (!tenant) {
        return reply(new Boom.notFound("Tenant with Matching not Found"));
      }
      reply({
        tenant: tenant
      });
    });
  }
};
exports.legacyLookup = {
  method: "GET"
  ,path: "/tenant/action/legacyLookup/{legacySystemId}"
  ,config: {
    description: "Returns a tenant using legacy id"
    ,tags:['api']
    ,validate: {
      query: joi.object({
        apikey:joi.string().guid()

      })
      ,params:joi.object({
        legacySystemId:joi.number().integer().required().description("Legacy System Id")

      })
    }
  }
  ,handler: function (request, reply) {
    Tenant.find({"legacySystemId": request.params.legacySystemId}, function (err, tenant) {
      //logger.info("Tenant - Looking Tenant up by Legacy Id");
      if (!tenant.length) {
        return reply(new Boom.notFound("Tenant with Matching not Found"));
      }
      reply({
        tenant: tenant
      });
    });
  }
};

exports.affiliateLookup = {
  method: "GET"
  ,path: "/tenant/action/affiliateLookup/{affiliateId}/{externalId}"
  ,config: {
    description: "Returns a tenant using our affiliates' information"
    ,tags: ["api"]
    ,validate: {
      query: joi.object({
        apikey:joi.string().guid()

      })
      ,params:joi.object({
        affiliateId:joi.string().required().description("Affiliate Id")
        ,externalId: joi.string().required().description("External Id")

      })
    }
  }
  ,handler: function (request, reply) {
      Tenant.findOne({ "affiliateMaps": { $elemMatch: { $and : [{ "affiliateId" : request.params.affiliateId }, { "externalId" : request.params.externalId }] } } }, function (err, tenant) {
      if (!tenant) {
        return reply (new Boom.notFound("Tenant with Matching Data not found"));
      }
      reply ({tenant: tenant});
    });
  }

};

//exports.affiliateLookup = {};
