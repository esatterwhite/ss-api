/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * handles tenant school lookup functionality
 * @module alice-catalog/resources/v1/directory
 * @author Ryan Fish
 * @author Eric satterwhite
 * @since 0.1.0
 * @requires joi
 * @requires alice-log
 * @requires alice-catalog/lib/search/directory
 */
var DirectorySearch  = require('../../lib/search/directory') // internal directory search library
  , joi              = require('joi')                        // joi Object validation
  , logger           = require('alice-log')                  // standard logging module
  , directory        = new DirectorySearch()                 // and instance of the directory search class
  , STATES           = 0                                     // constant value for representing state filter
  , CITIES           = 2                                     // constant value for represnting a city filter
  , TENANT_BY_CITY   = 6                                     // constant value representing a tenant look up by city
  , TENANT_BY_STATE  = 10                                    // constant value for representing a tenant look up by state
  ;


function directoryHandler(callback, err, payload ){

	payload = {
		data:payload
		,meta:{
			count:payload && payload.length
			,next:null
			,previous:null
		}
	}
	callback(err || payload);
}

// FIXME this should be an action on the tenant resource
// /tenant/directory?params=1
// or if their is more than one directory type - directory/{type}

/**
 * DESCRIPTION
 * @property {String} method=GET
 * @property {String} path=/directory/query
 * @property {Object} query Query string parameters 
 * @property {String} [query.ttype] tenant type to look up. k12 or collegiate
 * @property {String} [query.ctry] A country abbreviation to filter results by
 * @property {String} [query.st] A State abbreviation to filter results by
 * @property {Boolean|Number} tenants True return tenants for a particular state or city
 * @property {String} apikey A valid apikey to authenticate the request against
 **/
exports.get_list = {
	method:'GET'
	,path:'/directory/query'
	,config:{
		tags:['api']
		,description:"Geographically aggregates Tenant data by city state or county"
		,validate:{
			query:joi.object({
				ttype:joi.string().allow('k12','collegiate')
			  , ctry:joi.string().uppercase().optional()
			  , st:joi.string().optional().uppercase()
			  , tenants:joi.alternatives([joi.boolean(), joi.number().allow(1,0) ]).optional()
			  , city: joi.string().optional()
			  , apikey: joi.string().guid()
			})
		}
		,handler: function(request, reply){
			var query            = request.query
			  , type             = query.ttype
			  , country          = query.ctry
			  , state            = query.st
			  , city             = query.city
			  , searchIdentifier
			  , showTenants
			  ;


			searchIdentifier = 0;
			showTenants = !!query.tenants;

			// calulate search option.
			searchIdentifier += state ? Math.pow(2,1) : 0;
			searchIdentifier += city ? Math.pow(2,2) : 0;
			searchIdentifier += showTenants ?  Math.pow(2,3) : 0;

			// Overkill but in case we add more permutationscl
			logger.debug("search identifiery %s", searchIdentifier, query );
			switch(searchIdentifier){
				case STATES:
					directory.states(
						type
						, country
						, directoryHandler.bind( null, reply )
					);
					break;
				case CITIES:
					directory.cities(
						type
						, country
						, state
						, directoryHandler.bind( null, reply )
					);
					break;
				case TENANT_BY_CITY:
					//select tenants by city
					directory.byCity(
						type
						, country
						, state
						, city
						, directoryHandler.bind(null, reply )
					);
					break;
				case TENANT_BY_STATE:
					//select all tenants by state
					directory.byState(
						type
						, country
						, state
						, directoryHandler.bind(null, reply )
					);
					break;
			   default:
					directory.cities(
						type
						, country
						, state
						, directoryHandler.bind( null, reply )
					);
			}
		}
	}
}
