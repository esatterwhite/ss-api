/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Defines The API Resource for searching data
 * @module alice-catalog/resources/v1/autocomplete
 * @author Eric Satterwhite
 * @author Ryan Fisch
 * @since 0.1.0
 * @requires businesslayer/autocompletesearcher
 * @requires joi
 */

var autocompletesearcher = require('../../lib/search/autocomplete')
  , joi = require('joi')

/**
 * Defines GET Handler which Fetches tenant information from searchly
 * @property {String} [method=GET]
 * @property {String} [path=autocomplete]
 * @property {Object} validate Endpoint validation rules
 * @property {Object} validate.query Query string validation rules
 * @property {String} validate.query.term Exact search term to search for
 * @property {String} [validate.query.apikey] Account authentication key
 * @property {String} validate.query.ttype Restricts the search to a specific tenant types ( k8, hs, college )
 * @property {String} validate.query.scope Search scope. Can be *city*, *state*, or *tenant*
 * @property {Number} [validate.query.take=100] Maximume page size for data
 * @property {Number} [validate.query.page=1] Starting page number
 **/
exports.get_list = {
    method:'GET'
    ,path:'/autocomplete'
    ,config:{
    	description:"search tenants matching specified criteria through elasticsearch"
    	,tags:['api']
    	,validate:{
    		query:{
    			term:joi.any().required().description('The search term')
    			,apikey:joi.string().guid().description("Api key for account authentication")
    			,ttype:joi.string().description("tenant type code")
    			,scope:joi.string().allow('city','state','tenant')
    			,take:joi.number().default( 100 )
    			,page:joi.number().default( 1 )
    		}
    	}
    	,handler: function( request, reply ){
    		 var searcher = new autocompletesearcher();
    		 searcher.once('done', function (data) {
    		     reply(data);
    		 }).once('error', function (error) {
    		     reply(error);
    		 });
    		searcher.search(request.query.term, request.query.ttype, request.query.scope, request.query.take, request.params.page);
    	}
    }
};

