
var product = require('../../lib/productloader')
  , productsizer = require('../../lib/productsizer')
  , mascot  = require('../../lib/mascotloader')
  , generateEra  = require('../../lib/eragenerator')
  , Boom = require( 'boom' )
  , joi = require('joi')


exports.generate_era = {
	method:'GET'
	,path:'/eragenerator'
	,config:{
		tags:['api']
		,validate:{
			query:{
				secret:joi.string().optional()
				,apikey:joi.string().guid().optional()
			}
		}
		,handler: function get_generate_era( request, reply ){
			generateEra(function(err, msg ){
				reply( err && Boom.wrap( err ) || msg ).type('text/plain')
			})
		}
	}
};

exports.load_product = {
	method:'GET'
	,path:'/productloader'
	,config:{
		tags:['api']
		,validate:{
			query:{
				apikey:joi.string().guid().optional()
				,secret:joi.string().optional()
			}
		}
		,handler: function get_load_product( request, reply ){
			product(function(err, product){
				reply(err && Boom.wrap( err ) || products )
			})
		}
	}
};

exports.size_product = {
	method:'GET'
	,path:'/productsizer'
	,config:{
		tags:['api']
		,validate:{
			query:{
				apikey:joi.string().guid().optional()
				,secret:joi.string().optional()
			}
		}
		,handler: function get_load_product( request, reply ){
      productsizer(request.query, function(err, product){
				reply(err && Boom.wrap( err ) || "All done!" )
			})
		}
	}
};

exports.load_mascot = {
	method:'GET'
	,path:'/mascotloader'
	,config:{
		tags:['api']
		,description:'Associates mascots with Existing tenants and saves them'
		,validate:{
			query:{
				apikey:joi.string().guid().optional()
				,secret:joi.string().optional()
			}
		}
		,handler:function get_load_mascot( request, reply ){
			mascot(request.query, function(err, msg){
				reply(err && Boom.wrap( err ) || msg ).type('text/plain')
			})
		}
	}
};
