/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Product specific endpoints
 * @module alice-catalog/resources/v1/product
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires util
 * @requires mongoose
 * @requires joi
 * @requires alice-catalog/models/product
 * @requires alice-catalog/models/decal
 */
var util        = require('util')
  , mongoose    = require('mongoose')
  , joi         = require('joi')
  , Product     = require('alice-catalog/models/product')
  , decalSchema = require('alice-catalog/models/decal')
  , Boom        = require( 'boom' )
  , ObjectId    = mongoose.Types.ObjectId
  ;


/**
 * returns Default product listing 
 * @property {String} [method=GET]
 * @property {String} [path=/product]
 * @property {Object} validate Endpoint validation rules
 * @property {Object} validate.query Query string validation rules
 * @property {String} [validate.query.apikey] Account authentication key
 **/
exports.get_list = {

    method:'GET'
    ,path:'/product'
    ,config:{
        description:'List all products'
        ,tags:['api']
        ,validate:{
            query:{
                apikey:joi.string().guid().optional()
            }
        }
        ,handler: function get_product_list( request, reply ){

            Product.find(function (err, products) {
                reply(err && Boom.wrap( err ) || products);
            })
        }   
    }
}

/**
 * returns a product by a specific id
 * @property {String} [method=GET]
 * @property {String} [path=/product/product_id]
 * @property {Object} validate Endpoint validation rules
 * @property {Object} validate.query Query string validation rules
 * @property {Object} validate.params url parameters
 * @property {Object} validate.params.product_id The id of a product to lookup
 * @property {String} [validate.query.apikey] Account authentication key
 **/
exports.get_detail = {
    method:'GET'
    ,path:'/product/{product_id}'
    ,config: {
        description:'Find product by ID'
        ,tags:['api']
        ,validate:{
            query:{
                apikey:joi.string().guid().optional()
            }
            ,params:{
                product_id:joi.string().length(24).alphanum().description('Mongo ID For product').required()
            }
        }
        ,handler:function get_product_detail( request, reply ){

            Product.findById(request.params.product_id, function (err, product) { 
                reply(err && Boom.wrap( err ) || product);
            });
        }
    }
};
