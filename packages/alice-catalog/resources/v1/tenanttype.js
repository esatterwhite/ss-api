/*
 * Created by Ryan Fisch on 5/12/2014.
 */
var joi                = require('joi'),
    Boom                = require('boom'),
    Promo   = require('alice-catalog/lib/promotion'),
    TenantTypeLanding  = require('../../../../models/viewmodels/tenanttypelanding'),
    constants          = require('alice-core/lib/constants')
    ;

exports.get_viewmodel = {
    method:'GET'
    ,path:'/tenanttypelanding/getviewmodel'
    ,config:{
        description:"Returns a tenant specific view model (?)"
        ,tags:['api']
        
        ,validate:{
            query:{
                userid:joi.string().required().default('guest').description("id of the user making the request")
                ,ttype:joi.string().required().default('hs').description("Type of tenant to lookup")
                ,apikey:joi.string().guid()
            }
        }

        ,handler: function( request, reply ){
            var userId     = request.query.userid||'guest'
              , tenantType = request.query.ttype || 'hs'
              , model      = new TenantTypeLanding({tenantType: tenantType})
              , pm         = new Promo() // TODO: get promos for this specific tenant type = tenantType, and any users or globals
            ;

            pm.once('GetHeroPromotionDone', function (data) {
                model.subNavigationPromo = data;
                pm.GetWidgets(constants.TENANT_TYPE_LANDING, tenantType, userId);
            });

            pm.once('GetWidgetsDone', function (data) {
                model.widgets = data;
                reply(model);
            }).once('error', function (err) {
                  reply( Boom.wrap( err ) );
            }).GetHeroPromotion(constants.TENANT_TYPE_LANDING, userId);
        }
    }
};
