/*jshint laxcomma: true, smarttabs: true, node: true*/
'use strict';
/**
 * promotion.js
 * @module alice-catalog/resources/v1/promotion
 * @author Eric satterwhite
 * @since 2.0.0
 * @requires util
 * @requires joi
 * @requires alice-log
 * @requires alice-conf
 * @requires alice-web/lib/paginator.hapi
 * @requires alice-core/lib/db/schema/toJoi
 * @requires alice-core/lib/db/schema/validators/defaults
 * @requires alice-catalog/models/promotion
 */
var util       = require( 'util' )
  , joi        = require( 'joi' )
  , Boom       = require( 'boom' )
  , logger     = require( 'alice-log' )
  , conf       = require( 'alice-conf' )
  , merge      = require('alice-stdlib/object').merge
  , Paginator  = require('alice-web/lib/paginator')
  , toJoi      = require( 'alice-core/lib/db/schema/toJoi' )
  , validators = require( 'alice-core/lib/db/schema/validators/defaults' )
  , Promotion  = require( '../../models/promotion' )
  , OIDTest    = /^[0-9a-fA-F]{24}$/
  , queryset
  ;

queryset = Promotion.find().limit(25).toConstructor();

/**
 * ### GET /api/v1/promotion
 * defines the listing endpoint for catalog promotion items
 * @property {String} [method=GET]
 * @property {String} [path=/promotion]
 * @property {Object} validate Endpoint validation rules
 * @property {Object} validate.query Query string validation rules
 * @property {Number} [validate.query.limit=25] Limits the number for results per page of data.
 * @property {Number} [validate.query.offset=0] starting page number for listing endpoints
 * @property {String} [validate.query.format=json] ad hoc serialization formats. Allows `json`, `xml`, `jsonp`
 * @property {String} [validate.query.callback] Used for jsonp callback methods
 * @property {String} [validate.query.apikey] Used for adhoc account auth access
 * @property {String} [validate.query.order] used to sort collections of resources
 * @example curl -XGET -H "Accept: application/json" http://localhost:3001/api/v1/promotion?limit=0&order=-code
{
	"meta": {
	    "count": 15,
	    "limit": 25,
	    "next": null,
	    "offset": 0,
	    "previous": null
	}.
    "data": [
        {
            "__v": 0,
            "_id": "54de46457241d12d4445d2a7",
            "code": "SS191s",
            "description": "Hello world",
            "end": null,
            "promotion_id": "54de46457241d12d4445d2a7",
            "rate": 100,
            "start": null,
            "type": "percentage",
            "used": "2015-02-13T22:21:48.363Z",
            "volatile": true
        }
        ...
    ]
}
 **/
exports.get_list = {
	method:'get'
	,path:'/promotion'
	,config:{
		tags:['api']
		,validate:{
			query: validators.query.clone().keys({
				all:joi.boolean().default(false)
			})
		}
		,handler:function( request, reply){
			var queryset = Promotion.live( request.query.all ).toConstructor()
			
			new queryset().count(function( cerr, count ){

				var query = new queryset();
				
				query
					.skip( request.query.offset || 0 )
					.limit( request.query.limit );

				query.exec(function(err, results ){

					// generate a page
					results = new Paginator({
					  req: request
					  ,res: reply
					  ,count: count
					  ,limit:request.query.limit
					  ,offset:request.query.offset
					  ,objects: results
					}).page();

					reply(
						err ? new Boom.wrap( err ) : results
					);
				});
			})
		}
	}
};

/**
 * ### GET /api/v1/promotion/:id
 * defines the listing endpoint for catalog promotion items
 * @property {String} [method=GET]
 * @property {String} [path=/promotion/{promotion_id}] A mongo ObjectID of the promotion, or the promotion code of the specific promotion to look up
 * @property {Object} validate Endpoint validation rules
 * @property {Object} validate.query Query string validation rules
 * @property {String} [validate.query.format=json] ad hoc serialization formats. Allows `json`, `xml`, `jsonp`
 * @property {String} [validate.query.callback] Used for jsonp callback methods
 * @property {String} [validate.query.apikey] Used for adhoc account auth access
 * @example curl -XGET -H "Accept: application/json" http://localhost:3001/api/v1/promotion/54de46457241d12d4445d2a7
 {
	"__v": 0,
	"_id": "54de46457241d12d4445d2a7",
	"code": "SS191s",
	"description": "Hello world",
	"end": null,
	"promotion_id": "54de46457241d12d4445d2a7",
	"rate": 100,
	"start": null,
	"type": "percentage",
	"used": "2015-02-13T22:21:48.363Z",
	"volatile": true
 }
 * @example curl -XGET -H "Accept: application/json" http://localhost:3001/api/v1/promotion/SS191s
{
	"__v": 0,
	"_id": "54de46457241d12d4445d2a7",
	"code": "SS191s",
	"description": "Hello world",
	"end": null,
	"promotion_id": "54de46457241d12d4445d2a7",
	"rate": 100,
	"start": null,
	"type": "percentage",
	"used": "2015-02-13T22:21:48.363Z",
	"volatile": true
}
 **/
exports.get_detail = {
	method:'get'
	,path:'/promotion/{promotion_id}'
	,config:{
		tags:['api']
		,description:'Get a specific promotion by ID'
		,notes:'Promotion Id may be the PK of a promotion, or the promotion code'
		,validate:{
			query:validators.query.clone()
			,params:{
				promotion_id:joi.string().required().description("A Valid mongo ID, or a promotion code")
			}
		}
		,handler:function( request, reply){
			var query  // instance of Promotion query
			  , isOID  // boolean value denoting if parameter looks like a mongo oid
			  , id     // alias to promotion_id request parameter
			  ;

			query = new queryset();
			id    = request.params.promotion_id;
			isOID = !!OIDTest.exec( id );
			query = isOID ? Promotion.findById(id) : query.findOne({code:id});
			query.exec(function(err, results ){
				reply(
					err ? new Boom.wrap( err ) : results ? results : new Boom.notFound("No Matching promotion found")
				);
			});
		}
	}
};

/**
 * ### DELETE /api/v1/promotion/:id
 * Deletes a promotion by ID
 * @property {Object} validate Endpoint validation rules
 * @property {Object} validate.query Query string validation rules
 * @property {String} [validate.query.format=json] ad hoc serialization formats. Allows `json`, `xml`, `jsonp`
 * @property {String} [validate.query.callback] Used for jsonp callback methods
 * @property {String} [validate.query.apikey] Used for adhoc account auth access
 * @example curl -XDELETE -H "Accept: application/json" http://localhost:3001/api/v1/promotion/54de46457241d12d4445d2a7
 **/
exports.delete_detail = {
	method:'delete'
	,path:'/promotion/{promotion_id}'
	,config:{
		tags:['api']
		,description:'Deletes an exiting promotion by id'
		,validate:{
			query:validators.query.clone()
			,params:{
				promotion_id:joi.string().length(24).alphanum().required().description("Primary key of the promotion to delete")
			}
		}
		,plugins:{
			'hapi-swagger':{
				responseMessages:[{
					code:204, message:"Request succeeded with no content in response"
				},{
					code:404, message:"No document with requested id was found."
				}]
			}
		}
		,handler:function( request, reply){
			var query = new queryset()
			query = query.model.findByIdAndRemove(request.params.promotion_id);
			query.exec(function( err, doc ){
				var resp = reply(
					err ? new Boom.wrap( err ) : doc ? undefined : new Boom.notFound(util.format('Promotion with id %s not found', request.params.promotion_id))
				);
				resp.code && resp.code( 204 );
			});
		}
	}
};


/**
 * ### PUT /api/v1/promotion/:id
 * Replaces the values of a document with supplied values by promotion id
 * @property {Object} validate Endpoint validation rules
 * @property {Object} validate.query Query string validation rules
 * @property {String} [validate.query.format=json] ad hoc serialization formats. Allows `json`, `xml`, `jsonp`
 * @property {String} [validate.query.callback] Used for jsonp callback methods
 * @property {String} [validate.query.apikey] Used for adhoc account auth access
 * @example curl -XDELETE -H "Accept: application/json" http://localhost:3001/api/v1/promotion/54de46457241d12d4445d2a7
 **/
exports.put_detail = {
	method:'put'
	,path:'/promotion/{promotion_id}'
	,config:{
		tags:['api']
		,description:'Update an existing Promotion by ID'
		,validate:{
			query: validators.query.clone()
			,payload: toJoi( Promotion.schema )
			,params:{
				promotion_id:joi.string().alphanum().length( 24 ).required().description("Primary key of the promotion to delete")
			}
		}

		,handler:function( request, reply){
			var query = new queryset()
			  , id = request.params.promotion_id
			  ;

			Promotion.findById( id, function( err, promo ){
				if( err || !promo ){
					return reply(
						err ? new Boom.wrap( err ) : new Boom.notFound(util.format('Promotion with id %s not found'), id)
					)
				}
				merge( promo, request.payload );
				promo.save(function( err ){
					var resp = reply(
						err ? new Boom.wrap( err ) : promo
					)

					resp.code && resp.code( 200 )
				});
			});
		}
	}
};


/**
 * ### POST /api/v1/promotion/:id
 * Creates a new promotion
 * @property {Object} validate Endpoint validation rules
 * @property {Object} validate.query Query string validation rules
 * @property {String} [validate.query.format=json] ad hoc serialization formats. Allows `json`, `xml`, `jsonp`
 * @property {String} [validate.query.callback] Used for jsonp callback methods
 * @property {String} [validate.query.apikey] Used for adhoc account auth access
 * @example curl -XPOST -d '{'rate' : 15, 'code' : 'GEN-PROMOSPEC', 'type' : 'percentage', 'volatile' : false, 'end' : null, 'start' : null, }'
 * -H "Content-Type: application/json" -H "Accept: application/json" - http://localhost:3001/api/v1/promotion
 **/
exports.post_list = {
	method:'post'
	,path:'/promotion'
	,config:{
		tags:['api']
		,description:'Create new instance'
		,validate:{
			query: validators.query.clone()
			,payload:toJoi( Promotion.schema )
		}
		,handler:function( request, reply){
			var query = new queryset()
			Promotion.create(request.payload, function( err, doc ){
				if(err){
					return reply( new Boom.wrap( err ) )
				}
				var resp = reply( doc )
				resp.created( request.path + '/' + doc._id )
			})
		}
	}
};


/**
 * ### PUT /api/v1/promotion/:id
 * Performace a partial replace in place on a specific promotion. e.g. Only the values provided will be updated
 * @property {Object} validate Endpoint validation rules
 * @property {Object} validate.query Query string validation rules
 * @property {String} [validate.query.format=json] ad hoc serialization formats. Allows `json`, `xml`, `jsonp`
 * @property {String} [validate.query.callback] Used for jsonp callback methods
 * @property {String} [validate.query.apikey] Used for adhoc account auth access
 * @example curl -XDELETE -H "Accept: application/json" http://localhost:3001/api/v1/promotion/54de46457241d12d4445d2a7
 **/
exports.patch_detail = {
	method:'patch'
	,path:'/promotion/{promotion_id}'
	,config:{
		tags:['api']
		,description:'Update an existing Promotion by ID'
		,validate:{
			query: validators.query.clone()
			,payload: toJoi( Promotion.schema, true )
			,params:{
				promotion_id:joi.string().alphanum().length( 24 ).required()
			}
		}
		,handler:function( request, reply){
			var query = new queryset()
			  , id = request.params.promotion_id
			  ;

			Promotion.findById( id, function( err, promo ){
				if( err || !promo ){
					return reply(
						err ? new Boom.wrap( err ) : new Boom.notFound(util.format('Promotion with id %s not found'), id)
					);
				}
				merge( promo, request.payload )
				promo.save(function( err ){
					var resp = reply(
						err ? new Boom.wrap( err ) : promo
					);

					resp.code && resp.code( 200 );
				});
			});
		}
	}
};
