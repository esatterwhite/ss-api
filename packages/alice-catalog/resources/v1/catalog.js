  /*jshint camelcase:false, laxcomma: true, smarttabs: true, node: true*/
 'use strict';
 /**
  * Rest Interface for the Catalog sepcific endpoints
  * @module alice-catalog/resources/v1/catalog
  * @author Eric Satterwhite
  * @author Ryan Fisch
  * @since 0.1.0
  * @requires util
  * @requires joi
  * @requires async
  * @requires alice-log
  * @requires alice-catalog/lib/colors
  * @requires alice-catalog/lib/catalogItem
  * @requires alice-catalog/models/tenant
  * @requires alice-catalog/models/product
  * @requires alice-catalog/models/decal
  * @requires alice-catalog/models/mascot
  * @requires alice-catalog/models/activity
  */

var util               = require( 'util' )                    // node util for formatting
  , joi                = require( 'joi' )                     // joi validation lib for validating API endpoints
  , async              = require( 'async' )                   // async module for control flow
  , logger             = require( 'alice-log' )               // logger Standard logging module
  , Boom               = require( 'boom' )
  , array              = require( 'alice-stdlib/array')
  , lang               = require( 'alice-stdlib/lang' )
  , operator           = require( 'alice-stdlib/operator' )
  , object             = require( 'alice-stdlib/object' )
  , typecast           = require( 'alice-stdlib/string' ).typecast
  , validators         = require( 'alice-core/lib/db/schema/validators/defaults' )
  , Paginator          = require( 'alice-web/lib/paginator.hapi' )
  , colours            = require( '../../lib/colors' )        // internal colours lib for interpolation of colors
  , interpolate        = require( '../../lib/interpolate')    // library for interpolating catalog items
  , catalogItem        = require( '../../lib/catalogItem' )   // interal catalogItem for generating image layer definitions for S7
  , Tenant             = require( '../../models/tenant' )     // Tenant Model
  , Product            = require( '../../models/product' )    // Product model
  , Decal              = require( '../../models/decal' )      // Decal model
  , map                = array.quickmap
  , contains           = array.contains
  , intersection       = array.intersection
  , compact            = array.compact
  , clone              = lang.clone // standard clone function
  , isFunction         = lang.isFunction // standard isFunction function
  , namespace          = object.namespace
  , get                = object.get
  , EMPTY_ARRAY        = []                                   // static empty array for fallback value
  , FILTER_SEP         = '__'
  , orderExp           = /^(\-)?([\w]+)/
  , terms
  , buildFilters
  , catalogQuery
  , querydefaults
  ;

terms = {
  'gt'            : operator.gt
  , 'exact'       : operator.eq
  , 'iexact'      : function( term, match ){ return new RegExp( match, 'i').test( term );}
  , 'gte'         : operator.gte
  , 'lt'          : operator.lt
  , 'lte'         : operator.lte
  , 'ne'          : operator.stneq
  , 'nin'         : function( term, match ){ return !contains( term, match ); }
  , 'size'        : function( term, match ){ return term.length === match; }
  , 'all'         : function( term, match ){
                      return intersection(match,term).length == term.length;
                    }
  , 'regex'       : function( term, match ){ return false; }
  , 'in'          : function( term, match ){ return contains( term, match ); }
  , 'contains'    : function( term, match ){ return new RegExp( match ).test( term ); }
  , 'icontains'   : function( term, match ){ return new RegExp( match, 'i').test( term ); }
  , 'startswith'  : function( term, match ){ return new RegExp( '^' + match ).test( term ); }
  , 'istartswith' : function( term, match ){ return new RegExp( '^' +  match, 'i' ).test(term ); }
  , 'endswith'    : function( term, match ){ return new RegExp(  match + '$' ).test(term ); }
  , 'iendswith'   : function( term, match ){ return new RegExp( match + '$', 'i').test( term ); }
};

catalogQuery =
    validators
          .query
          .clone()
          .keys({
              longName       : joi.string().optional().description('Full name of the tenant')
            , state          : joi.string().optional().description('State the tenant resides in')
            , city           : joi.string().optional().description('City the tenant resides in')
            , name           : joi.string().optional().description('Short name of the tenant')
            , mascot         : joi.string().optional().description('Related masot id')
            , legacySystemId : joi.string().optional().description('Id of tenant in legacy database')
            , decal          : joi.string().optional().allow('').description('Scene seven id for a decal')
            , additions      : joi.object()
            , category       : joi.string().optional()
            , product        : joi.string().optional().length( 24 ).alphanum().description('Mongo ID of related product')
            , color          : joi.string().optional()
            , fetch          : joi.string().required().valid('products', 'activities').description('determines the type of catalog items to return')
            // , activity       : joi.string().when('fetch',{is:'activities', then:joi.required(), otherwise:joi.optional() }).description('required when fetch is set to activities. Activity should be in title case')
          });

querydefaults = catalogQuery._inner.children.map(function( c ){ return c.key; });

buildFilters = function ( type, obj ){
  var remaining = {};
  var paths = Object.keys( type );
  var allowablepaths = paths.filter( function( p ){
    return (( p !== '_id' && p !== '__v'  && querydefaults.indexOf( p ) === -1) || p == "category" );
  });
  for( var key in obj ){

    var bits = key.split( FILTER_SEP )
       , filtertype = terms.exact
       , value
       , fieldname
       ;
    value     = obj[key];
    fieldname = bits.shift();

    bits = map(bits, function( bit ){
      if( terms.hasOwnProperty( bit ) ){
        return terms[ bit ];
      }
      return bit;
      // throw new Error("unknown filter type " + bit );
    });
    filtertype = bits.length > 0 && isFunction( bits[ bits.length -1 ] )  ? bits.pop() : filtertype

    if( allowablepaths.indexOf( fieldname ) >=0 ){
      bits.unshift( fieldname );
      fieldname = compact( bits ).join('.');
      remaining[ fieldname ] = {value: typecast( value ), test:filtertype};

    }
  }
  return remaining;
};



// TODO: add conditional validation for activities. when activities, need products and color params
/**
 * Defines GET Handler which Fetches tenant information from searchly
 * @property {String} [method=GET]
 * @property {String} [path=catalog/{tenant_id}]
 * @property {Object} validate Endpoint validation rules
 * @property {Object} validate.params Url Parameter validation rules
 * @property {String} validate.params.tenant_id Unique ID of a tenant to generate a catalog for
 * @property {Object} validate.query Query string validation rules
 * @property {String} [validate.query.apikey] Account authentication key
 * @property {String} validate.query.term Exact search term to search for
 * @property {String} validate.query.ttype Restricts the search to a specific tenant types ( k8, hs, college )
 * @property {String} validate.query.scope Search scope. Can be *city*, *state*, or *tenant*
 * @property {String} validate.query.fetch Dictates the type of catalog ( products or activites )
 * @property {String} [validate.query.longName] filter tenants by the specified longName value
 * @property {String} [validate.query.state] filter tenants by the specified state value
 * @property {String} [validate.query.city] filter tenants by the specified city value
 * @property {String} [validate.query.name] filter tenants by the specified name value
 * @property {String} [validate.query.mascot] filter tenants by the specified mascot value
 * @property {String} [validate.query.legacySystemId] filter tenants by the specified legacySystemId value
 * @property {String} [validate.query.apikey] filter tenants by the specified apikey value
 * @property {String} [validate.query.decal] filter tenants by the specified decal value
 * @property {String} [validate.query.product] filter tenants by the specified product value
 * @property {String} [validate.query.color] filter tenants by the specified color value
 **/
exports.get_tenant_catalog = {
    method:'GET'
    ,path:'/catalog/{tenant_id}'
    ,config:{
        description:'look up tenant products or activities...'
        ,tags:['api']
        ,validate:{
            query:catalogQuery.clone().keys().unknown()
            ,params:joi.object({
                tenant_id:joi.string().alphanum().required()
                ,id:joi.string().optional().length(24).alphanum()
            }).rename('tenant_id', 'id', { alias:true })
        }

        // when fetch is activities, product and color is passed.
        ,handler: function( request, reply ){
          var q = catalogQuery.clone().unknown()
            var query = clone( request.query );
            async.series({
              items:function( cb ){
                if (query.fetch === 'products') {
                  interpolate.products( request.params.id, query.decal, query.additions, cb );
                } else if ( query.fetch === 'activities') {
                  interpolate.activities( request.params.id, query.product, query, cb );
                }
              }
            }, function( err, results ){
                var filters  // constructed filters object
                  , payload  // data set to return
                  ;

              if( err ){
                logger.error(err.message, logger.exception.getAllInfo( err ) )
                return reply( err )
              }

                // generate applicable filters
                filters = buildFilters( catalogItem.prototype.toJSON(), request.query );
                logger.info('applying catalog filters for %s', request.params.id, filters );

                payload = results.items.items;
                // apply filters
                payload = payload.filter( function( item ){
                    var content // the Data object to inspect
                      , valid   // if data object passes all filter checks
                      , filter
                      ;

                    content = item.toJSON ? item.toJSON() : item; // we need to check the rendered data in this case... not ideal
                    valid   = true;          // innocent until proven guilty.

                    for( var key in filters ){
                      filter = filters[key];
                      valid = filter.test( get( content, key ), filter.value);
                      if( !valid ){
                        return false;
                      }
                    }
                    return true;
                });

                logger.debug('Paging catalog items')

                // generate a page
                payload = new Paginator({
                  req: request
                  ,res: reply
                  ,objects: payload
                }).page();


                // Shape data as it was previously.
                payload.data = {
                  tenant:results.items.tenantInfo
                  ,items: payload.data
                };

                logger.debug('catalog items complete')

                // done
                reply(payload);
            });
        }
    }
};

// TODO: add conditional validation for activities. when activities, need products and color params
/**
 * Defines GET Handler which Fetches tenant information from searchly
 * @property {String} [method=GET]
 * @property {String} [path=catalog/{partner}/{partner_id}]
 * @property {Object} validate Endpoint validation rules
 * @property {Object} validate.params Url Parameter validation rules
 * @property {String} validate.params.tenant_id Unique ID of a tenant to generate a catalog for
 * @property {Object} validate.query Query string validation rules
 * @property {String} [validate.query.apikey] Account authentication key
 * @property {String} validate.query.term Exact search term to search for
 * @property {String} validate.query.ttype Restricts the search to a specific tenant types ( k8, hs, college )
 * @property {String} validate.query.scope Search scope. Can be *city*, *state*, or *tenant*
 * @property {String} validate.query.fetch Dictates the type of catalog ( products or activites )
 * @property {String} [validate.query.longName] filter tenants by the specified longName value
 * @property {String} [validate.query.state] filter tenants by the specified state value
 * @property {String} [validate.query.city] filter tenants by the specified city value
 * @property {String} [validate.query.name] filter tenants by the specified name value
 * @property {String} [validate.query.mascot] filter tenants by the specified mascot value
 * @property {String} [validate.query.legacySystemId] filter tenants by the specified legacySystemId value
 * @property {String} [validate.query.apikey] filter tenants by the specified apikey value
 * @property {String} [validate.query.decal] filter tenants by the specified decal value
 * @property {String} [validate.query.product] filter tenants by the specified product value
 * @property {String} [validate.query.color] filter tenants by the specified color value
 **/
exports.get_partner_catalog = {
    method: 'GET'
    ,path: '/catalog/{partner}/{partner_id}'
    ,config: {
        description: 'look up partner products or activities...'
        ,tags: ['api']
        ,validate: {
            query: joi.object({
                longName       : joi.string().optional().description('Full name of the tenant')
                , state          : joi.string().optional().description('State the tenant resides in')
                , city           : joi.string().optional().description('City the tenant resides in')
                , name           : joi.string().optional().description('Short name of the tenant')
                , mascot         : joi.string().optional().description('Related masot id')
                , legacySystemId : joi.string().optional().description('Id of tenant in legacy database')
                , apikey         : joi.string().guid().optional().description('Api key used for account authentication')
                , decal          : joi.string().optional().allow('').description('Scene seven id for a decal')
                , additions      : joi.object()
                , product        : joi.string().optional().length(24).alphanum().description('Mongo ID of related product')
                , color          : joi.string().optional()
                , fetch          : joi.string().required().allow('products', 'activities').description('determines the type of catalog items to return')
              // , activity       : joi.string().when('fetch',{is:'activities', then:joi.required(), otherwise:joi.optional() }).description('required when fetch is set to activities. Activity should be in title case')
            })
            ,params: joi.object({
                partner: joi.string().alphanum()
                ,partner_id: joi.string().min(36).max(36)
                ,id: joi.string().min(36).max(36)
            }).rename('partner_id', 'id', { alias: true })
        }
        ,handler: function (request, reply) {
            // when fetch is activities, product and color is passed.
            var query = clone(request.query);

            async.waterfall([
                // Load the partner school by id
                function (callback) {
                    Tenant.findOne({ 'affiliateMaps': { $elemMatch: { $and : [{ 'affiliateId' : request.params.partner }, { 'externalId' : request.params.id }] } } }, function (err, tenant) {
                        if (!tenant) {
                            callback(null, '54ab7b8afe3d5a31b4f895b9');
                        } else {
                            callback(null, tenant.id);
                        }
                    });
                },
                // Load the partner catalog
                function (tenantId, callback) {
                    if (query.fetch === 'products') {
                        interpolate.products(tenantId, query.decal, query.additions, reply);
                    } else if (query.fetch === 'activities') {
                        interpolate.activities(tenantId, query.product, query, reply);
                    }
                    callback(null, 'done');
                }
            ], function (err, result) {
                // Done
            });
        }
    }
};

exports.widget = {
 method:'GET'
    ,path:'/widget/{tenant_id}'
    ,config:{
        description:'look up tenant products or activities...'
        ,tags:['api']
        ,validate:{
            params:joi.object({
                tenant_id:joi.string().alphanum().required()
                ,id:joi.string().optional().length(24).alphanum()
            }).rename('tenant_id', 'id', { alias:true })
        }

  ,handler: function( request, reply ){
            var query = clone( request.query );
            async.series({
              items:function( cb ){
                  interpolate.widgets( request.params.id,query, cb );
              }
            }, function( err, results ){
                var filters  // constructed filters object
                  , payload  // data set to return
                  ;
              var debug = true;
                // generate applicable filters
                filters = buildFilters( catalogItem.prototype.toJSON(), request.query );
                logger.info('applying catalog filters for %s', request.params.id, filters )

                payload = results.items.items;
                // apply filters
                payload = payload.filter( function( item ){
                    var content // the Data object to inspect
                      , valid   // if data object passes all filter checks
                      , filter
                      ;

                    content = item.toJSON ? item.toJSON():item; // we need to check the rendered data in this case... not ideal
                    valid   = true;          // innocent until proven guilty.

                    for( var key in filters ){
                      filter = filters[key];
                      valid = filter.test( get( content, key ), filter.value);
                      if( !valid ){
                        return false;
                      }
                    }
                    return true;
                });

                logger.debug('Paging catalog items')

                // generate a page
                payload = new Paginator({
                  req: request
                  ,res: reply
                  ,objects: payload
                }).page();


                // Shape data as it was previously.
                payload.data = {
                  tenant:results.items.tenantInfo
                  ,items: payload.data
                };

                logger.debug('catalog items complete')

                // done
                reply(payload);
            });
        }
    }
};
// FIXME: this should go in the product resource as product/{id}

/**
 * Returns
 * @property {String} [method=GET]
 * @property {String} [path=/catalog/action/productDetails]
 * @property {Object} validate Endpoint validation rules
 * @property {Object} validate.query Query string validation rules
 * @property {String} [validate.query.apikey] Account authentication key
 * @property {String} [validate.query.product] Product ID to look for
 * @property {String} [validate.query.tenant] Tenant ID to restict product generation from
 * @property {String} [validate.query.color] Default color of product to place in details
 * @property {String} [validate.query.artad] Seems mostly unused ?
 * @property {String} [validate.query.txtanm] Seems mostly unused ?
 **/
exports.get_product_detail = {
    method:'GET'
    ,path:'/catalog/action/productDetails'
    ,config:{
        description:'Returns detailed information about a unique product'
        ,tags:['api']
        ,validate:{
            query:joi.object({
                apikey:joi.string().guid()
                ,product:joi.string().length(24).alphanum().description('Mongo ID of related base product')
                ,tenant:joi.string().length(24).alphanum().required().description('Mongo id of related tenant')
                ,color:joi.string().invalid('undefined', 'null', '' ).regex(/[0-9A-Fa-f]{6}/ ,'hex code ( RRGGBB )').description('base color for product')
                ,decal:joi.string().description('Mongo ID of related decal instance')
                ,artad:joi.string().description('This looks unused by anything')
                ,txtanm:joi.string().description('This also looks unused')
            })
        }
        ,handler: function( request, reply ){

            Tenant.findById(request.query.tenant,function(err, tenant ){
                logger.info('catalog - looking tenant up by id', request.query.tenant);

                if (!tenant) {
                    return reply( new Boom.notFound('Tenant with matching not found'));
                }

                async.waterfall([
                  function getProduct( callback ){
                    var product_id = request.query.product

                    if( !product_id ){
                      return callback(new Boom.badRequest("product is a required query parameter"),null)
                    }

                    Product.findById(product_id, function( err, product ){
                      err = err ? Bomm.wrap( err ) : err
                      err = !product ? new Boom.notFound( util.format('Product with id %s not found',product_id) ) : err
                      callback( err, product )
                    });
                  }
                  , function getDecal( product, callback ){
                        var decal_id = request.query.decal;

                        if( !decal_id ){
                          return callback(new Boom.badRequest("decal is a required query parameter"), null)
                        }

                        Decal.findBySceneId(decal_id, function (err, decal) {
                            err = err ? Bomm.wrap( err ) : err;
                            err = !decal ? new Boom.notFound( util.format('Product with id %s not found',decal_id) ) : err;
                            callback( err, product, decal );
                        });
                  }
                  , function additions( product, decal, callback ){
                      interpolate.additions(decal, tenant, request.query, function (err, additions) {
                            callback(err, product, decal, additions );
                        });
                  }
                  , function getBackDecal( product, decal, additions, callback ){
                        Decal.back(function (err, backDecals) {
                            callback( err, product, decal, additions, backDecals );
                        });
                  }
                ],function( err, product, decal, additions, backDecals ){
                    reply(err || {
                        tenant: tenant
                      , backDecals: backDecals
                      , item: new catalogItem({
                            product        : product
                          , color          : request.query.color
                          , decalAdditions : additions
                          , decal          : decal
                          , spectrum       : colours.generateFullDecalColorSpectrum(product, tenant, decal)
                          , decalColors    : colours.generateDecalColors(request.query.color, tenant, decal)
                          , tenant         : tenant
                          , fetching       : 'details'
                         })
                    });
                });
            });
        }
    }
};
