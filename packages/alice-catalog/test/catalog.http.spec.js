var assert    = require('assert')
  , crypto    = require('crypto')
  , util      = require('util')
  , async     = require('async')
  , server    = require( 'alice-web' )
  , conf      = require('alice-conf')
  , Tenant    = require('alice-catalog/models/tenant')
  , Promotion = require('alice-catalog/models/promotion')
  , token     = require('alice-auth/lib/token')
  , secret    = conf.get('jwtToken:secret')
  , url       = require('alice-stdlib/url')
  , hash
  , key
  ;


hash = crypto.createHash('sha1');
hash.update( crypto.randomBytes(1028) );
key  = hash.digest('hex');


describe('resources', function(){
	describe('v1', function(){
		describe('catalog', function(){
			var query, payload, authtoken

			before(function( done ){
				payload = token.tokenize( key )
				authtoken = token.encode( payload, secret )

				query = {
					decal: 'ss_0018',
					additions: {
						_id: 'Band',
					    s7Id: 'art_activity_band_002',
					    artad: 'art_activity_band_002',
					    category: 'none',
					    txtanm: 'Band',
					    arttm: 'false'
					},
					fetch: 'products',
				}


				server.start( done );

			});

			after(function( done ){
				server.stop( done )
			});

			it('should 404 for bad tenant ids', function( done ){
				var t = new Tenant();

				server.inject({
					url:util.format('/api/v1/catalog/%s?%s', t._id, url.qs.stringify( query ) )
				  , method:'get'
				  , headers:{
						Authorization:'Bearer ' + authtoken
					}
				}, function( response ){
					assert.equal( response.statusCode, 404 );
					done()
				});
			});

			it('should 200 for existing tenants', function( done ){
				Tenant.findOne(function( err, t ){
					server.inject({
						url:util.format('/api/v1/catalog/%s?%s', t._id, url.qs.stringify( query ) )
					  , method:'get'
					  , headers:{
							Authorization:'Bearer ' + authtoken
						}
					}, function( response ){
						assert.equal( response.statusCode, 200);
						done()
					});
				})
			})
		});

		describe('promotion', function(){
			var query, payload, authtoken

			before(function( done ){
				payload = token.tokenize( key )
				authtoken = token.encode( payload, secret )
				server.start( done );
			});

			after(function( done ){
				server.stop( done )
			});
			var promo = {
			  "description": "HTTP Test",
			  "volatile": false,
			  "rate": 50,
			  "code": "HTTPTEST"
			}
			,promo_id
			;


			it('should accept A POST request', function( done ){
				server.inject({
					url:'/api/v1/promotion'
				  , method:'POST'
				  , headers:{
						Authorization:'Bearer ' + authtoken
					}
				  , payload: promo
				}, function( response ){
					var details = response.result;
					assert.equal( response.statusCode, 201 );
					assert.equal( promo.description, details.description);
					assert.equal( promo.code, details.code );
					promo_id = details._id;
					done();
				});
			});

			it('should modify selective fields with patch', function( done ){
				server.inject({
					url:'/api/v1/promotion/' + promo_id
				  , method:'PATCH'
				  , headers:{
						Authorization:'Bearer ' + authtoken
					}
				  , payload: {code:"PATCHTEST"}
				}, function( response ){
					var details = response.result;
					assert.equal( response.statusCode, 200 );
					assert.equal( promo.description, details.description);
					assert.equal( details.code, 'PATCHTEST' );

					Promotion.get(promo_id, function( err, doc ){
						assert.equal( '' + doc._id, '' + promo_id);
						assert.equal( doc.code, 'PATCHTEST');
						done();
					});

				});
			})

			it('should remove documents by id with DELETE', function( done ){
				server.inject({
					url:'/api/v1/promotion/' + promo_id
				  , method:'DELETE'
				  , headers:{
						Authorization:'Bearer ' + authtoken
					}
				}, function( response ){
					var details = response.result;
					assert.equal( response.statusCode, 204 );
					Promotion.get(promo_id, function( err, doc ){
						assert.strictEqual( doc, null );
						done();
					});

				});
			})
		})
	})
})
