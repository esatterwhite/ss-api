var assert = require('assert')
  , Directory = require("../lib/search/directory");

  describe("Directory", function( ){
  	describe('~city lookup', function( ){
  		var directory = new Directory();
  		it('should lookup cities by name', function( done ){
  			directory.cities('k12', 'US', 'WI', function( err, data ){
  				assert.notEqual( data.length, 0);
  				done();
  			});
  		});

  		it('should fallback to options if tenant not specified', function( done ){
  			var d = new Directory({
  				type:'hs'
  			});

  			d.cities(null, 'US', 'FL',  function( err, data ){
  				data = data.filter(function( d ){
  					d.tenantType !=='hs';
  				});
  				assert.equal( data.length, 0, 'should not retrurn any non HS tenants' );
  				done();
  			});
  		});

  		it('should emit a data event', function( done ){
  			directory.once('data', function(){
  				done();
  			});
  			directory.cities('k12', 'US', 'FL');
  		});
  	});

  	describe('~state lookup', function(){
  		var directory = new Directory();
  		it('should lookup States by name', function( done ){
  			directory.states('k12', 'US', function( err, data ){
  				assert.notEqual( data.length, 0);
  				done();
  			});
  		});

  		it('should accept a callback', function( done ){
  			directory.states('k12', 'US', function( err, data ){
  				assert.notEqual( data, null);
  				done();
  			});
  		});

  		it('should emit a data event', function( done ){
  			directory.once('data', function(){
  				done();
  			});
  			directory.states('k12', 'US');
  		});

  		it('should fallback to options if tenant not specified', function(done){
  			var d = new Directory({
  				type:'hs'
  			});

  			d.states(null, 'US', function( err, data ){
  				data = data.filter(function( d ){
  					data.tenantType !=='hs';
  				});
  				assert.equal( data.length, 0, 'should not retrurn any non HS tenants' );
  				done();
  			});
  		});
  	});

  	describe('Tenant Lookup', function( ){
  		var directory = new Directory();
  		describe('#byCity', function(){
  			it('it should find tenants',function( done ){
  				directory.byCity('k12', 'US', 'FL', 'Miami', function( err, data ){
	  				assert.notEqual( data.length, 0);
	  				data = data.filter(function( d ){
	  					return d.tenantType != 'k12';
	  				});

	  				assert.notEqual(data.length, 0 );
	  				done();
  				});
  			});
  		});

  		describe('#byState', function(){
  			it('it should find tenants',function( done ){
				directory.byState('k12', 'US', 'FL' , function( err, data ){
					assert.notEqual( data.length, 0);
					data = data.filter(function( d ){
						return d.tenantType != 'k12';
					});

					assert.notEqual(data.length, 0 );
					done();
				});
  			});
  		});
  	});
  });
