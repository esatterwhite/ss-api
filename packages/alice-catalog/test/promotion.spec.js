/*jshint laxcomma: true, smarttabs: true, node: true, mocha: true*/
'use strict';
/**
 * promotion.spec.js
 * @module alice-cart/test/promotion.spec.js
 * @author Eric Satterwhite
 * @since 0.2.1
 * @requires assert
 * @requires util
 * @requires async
 * @requires Promotion
 * @requires alice-stdlib/date
 */

var assert    = require( 'assert' )
  , util      = require( 'util' )
  , async     = require( 'async' )
  , Promotion = require( '../models/promotion' )
  , date      = require('alice-stdlib/date')
  ;

describe('alice-cart', function(){
	describe('models',function(){
		describe('Promotion', function(){
			var general, single, future, ended, flat;

			before( function( done ){
				general = new Promotion({
				    'rate' : 15,
				    'code' : 'GEN-PROMOSPEC',
				    'type' : 'percentage',
				    'volatile' : false,
				    'end' : null,
				    'start' : null,
				});
				flat = new Promotion({
				    'rate' : 1000,
				    'code' : 'FLAT-PROMOSPEC',
				    'type' : 'flat',
				    'volatile' : false,
				    'end' : null,
				    'start' : null,
				});
				single = new Promotion({
				    'description' : 'Hello world',
				    'rate' : 100,
				    'code' : 'VOL-PROMOSPEC',
				    'type' : 'percentage',
				    'volatile' : true,
				});

				future = new Promotion({
					'rate' : 20,
					'code' : 'FUT-PROMOSPEC',
					'type' : 'percentage',
					'volatile' : false,
					'end' : null,
					'start' : date.utc( date.increment( new Date(), 'day', 1) ).toDate()
				});

				ended = new Promotion({
					'rate' : 20,
					'code' : 'END-PROMOSPEC',
					'type' : 'percentage',
					'volatile' : true,
					'end' : date.utc( date.decrement( new Date(), 'day', 1) ).toDate(),
					'start' : null
				});

				Promotion.remove(function(){
					async.parallel([
						general.save.bind(general)
						,single.save.bind(single)
						,flat.save.bind(flat)
						,future.save.bind(future)
						,ended.save.bind(ended)
					], function(err, results ){
						done()
					})
				})
			});

			after( function( done ){
				general.remove(function(){
					single.remove(function(){
						done();
					});
				});
			});


			describe('#get', function(){
				it('should lookup promotions by code', function(done){
					Promotion.get( general._id, function( err, promo ){
						assert.equal( "" + general._id, "" + promo._id)
						done();
					});
				});
				it('should look up prmotions by ID', function( done ){
					Promotion.get( single.code, function( err, promo ){
						assert.equal( "" + single._id, "" + promo._id)
						done();
					});
				});
			});

			describe('#discount',function(){
				it('should return the discount ammount as a percentage', function(){
					assert.equal( general.discount( 1995 ),  Math.round( 1995 * ( general.rate / 100 ) ) )
				});
				it('should return the discount ammount as a flat amout', function(){
					assert.equal( flat.discount( 1995 ), flat.rate )
				});
			});
			describe('#cost',function(){
				it('should return the cost ammount as a percentage', function(){
					assert.equal( general.cost( 1995 ),  Math.round(1995 - (  1995 * ( general.rate / 100 ) ) ) )
				});
				it('should return the cost ammount as a flat amout', function(){
					assert.equal( flat.cost( 1995 ), 1995 - flat.rate );
				});
			})
			describe('#calculate',function(){
				it('should return a DiscountPrice Object', function( ){
					var prices = general.calculate( 1880 );
					assert.ok( prices.hasOwnProperty( 'cost' ) );
					assert.ok( prices.hasOwnProperty( 'discount' ) );
				});

				it('should throw an error for propmotions that have ended', function(){
					assert.throws(function(){
						ended.calculate( 1999 );
					});
				});

				it('should throw for promotions that have not started', function(){
					assert.throws(function(){
						future.calculate( 1999 );
					});
				});
			});

			describe('#live', function(){
				it('should only return active promos', function( done ){
					Promotion.live( function( err, promos ){
						assert.equal( promos.length, 3 )
						done()
					})
				})
			})

			describe('#apply',function(){
				it('should mark volatile promos as used', function( done ){
					var prices;

					prices = single.apply(1495, function( err, promo ){
						assert.ok( promo.used )
						done();
					})
				})
				it('should throw for previously used single promos', function( done ){
					assert.throws( function(){
						single.apply( 1944 )
					});
					done()
				})
			})

		});
	});
});
