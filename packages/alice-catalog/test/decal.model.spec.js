var assert = require( 'assert' )
var Decal = require("../models/decal")
var constants = require("../lib/constants")
describe('Decal Model', function(){

	describe('~Length Restrictions', function(){
		describe('#calculateSize', function( ){
			it("should resolve SHORT between 3 & 8", function( done ){
				Decal.findOne(function(e,d){
					assert.equal( d.calculateSize( 3 ), constants.SIZE_SHORT )
					assert.equal( d.calculateSize( 4 ), constants.SIZE_SHORT )
					assert.equal( d.calculateSize( 5 ), constants.SIZE_SHORT )
					assert.equal( d.calculateSize( 6 ), constants.SIZE_SHORT )
					assert.equal( d.calculateSize( 7 ), constants.SIZE_SHORT )
					assert.equal( d.calculateSize( 8 ), constants.SIZE_SHORT )
					assert.notEqual( d.calculateSize( 9 ), constants.SIZE_SHORT )
					done();
				})
			});

			it("should resolve MEDIUM between 9 & 15", function( done ){
				Decal.findOne(function(e,d){
					assert.equal( d.calculateSize( 9 ), constants.SIZE_MEDIUM, "9 should resolve to MED")
					assert.equal( d.calculateSize( 10 ), constants.SIZE_MEDIUM, "10 should resolve to MED")
					assert.equal( d.calculateSize( 11 ), constants.SIZE_MEDIUM, "11 should resolve to MED")
					assert.equal( d.calculateSize( 12 ), constants.SIZE_MEDIUM, "12 should resolve to MED")
					assert.equal( d.calculateSize( 13 ), constants.SIZE_MEDIUM, "13 should resolve to MED")
					assert.equal( d.calculateSize( 14 ), constants.SIZE_MEDIUM, "14 should resolve to MED")
					assert.equal( d.calculateSize( 15 ), constants.SIZE_MEDIUM, "15 should resolve to MED")
					assert.notEqual( d.calculateSize( 16 ), constants.SIZE_MEDIUM )
					done();
				})
			});

			it('should resolve LONG between 16 & 25', function( done ){
				Decal.findOne(function(e,d){
					Decal.findOne(function(e,d){
						assert.equal( d.calculateSize( 17 ), constants.SIZE_LONG, "9 should resolve to Long")
						assert.equal( d.calculateSize( 18 ), constants.SIZE_LONG, "10 should resolve to Long")
						assert.equal( d.calculateSize( 19 ), constants.SIZE_LONG, "11 should resolve to Long")
						assert.equal( d.calculateSize( 20 ), constants.SIZE_LONG, "12 should resolve to Long")
						assert.equal( d.calculateSize( 21 ), constants.SIZE_LONG, "13 should resolve to Long")
						assert.equal( d.calculateSize( 22 ), constants.SIZE_LONG, "14 should resolve to Long")
						assert.equal( d.calculateSize( 23 ), constants.SIZE_LONG, "15 should resolve to Long")
						assert.equal( d.calculateSize( 24 ), constants.SIZE_LONG, "15 should resolve to Long")
						assert.equal( d.calculateSize( 25 ), constants.SIZE_LONG, "15 should resolve to Long")
						done();
					})
				})
			});
		})

		describe("#sizeAllowed", function(){
			it('should only allow `tenant`, and `mascot` lookup', function( ){
				var d = new Decal();

				assert.doesNotThrow(function(){
					d.sizeAllowed('tenant', 12)
				}, 'type parameter should allow tenant')
				assert.doesNotThrow(function(){
					d.sizeAllowed('tenant', 12)
				}, 'type parameter should allow tenant')

				assert.throws(function(){
					d.sizeAllowed('foo', 12)
				}, 'type parameter should allow tenant');
			});

			it('should allow a number or string for length comparison', function(){
				var d = new Decal()
				assert.doesNotThrow(function(){
					d.sizeAllowed('tenant', "Hello world")
				}, 'name parameter should allow strings');

				assert.doesNotThrow(function(){
					d.sizeAllowed('tenant', 10)
				}, 'name parameter should allow numbers');

				assert.throws(function(){
					d.sizeAllowed('tenant', d)
				}, 'name parameter should only allow string or numbers');

			})

			describe('inclusion rules', function( ){
				it("should allow all sizes", function(){
					d = new Decal({
						lengthLimits: {
						     mascot: []
						    ,tenant: []
						}
					});

					assert.equal( d.sizeAllowed('mascot', 5 ), true )
					assert.equal( d.sizeAllowed('tenant', 5 ), true )

					assert.equal( d.sizeAllowed('mascot', 10 ), true )
					assert.equal( d.sizeAllowed('tenant', 10 ), true )

					assert.equal( d.sizeAllowed('mascot', 20 ), true )
					assert.equal( d.sizeAllowed('tenant', 20 ), true )

				})

				it("should only allow short names", function(){
					d = new Decal({
						lengthLimits: {
						     mascot: [constants.SIZE_SHORT]
						    ,tenant: [constants.SIZE_SHORT]
						}
					});

					assert.equal( d.sizeAllowed('mascot', 5 ), true )
					assert.equal( d.sizeAllowed('tenant', 5 ), true )

					assert.notEqual( d.sizeAllowed('mascot', 20 ), true )
					assert.notEqual( d.sizeAllowed('tenant', 20 ), true )

				})
				it("should only allow MEDIUM names", function(){
					d = new Decal({
						lengthLimits: {
						     mascot: [constants.SIZE_MEDIUM]
						    ,tenant: [constants.SIZE_MEDIUM]
						}
					});

					assert.equal( d.sizeAllowed('mascot', 10 ), true )
					assert.equal( d.sizeAllowed('tenant', 10 ), true )

					assert.notEqual( d.sizeAllowed('mascot', 5 ), true )
					assert.notEqual( d.sizeAllowed('tenant', 5 ), true )
				})

				it("should only allow LONG names", function(){
					d = new Decal({
						lengthLimits: {
						     mascot: [constants.SIZE_LONG]
						    ,tenant: [constants.SIZE_LONG]
						}
					});

					assert.equal( d.sizeAllowed('mascot', 'hello world this is big' ), true )
					assert.equal( d.sizeAllowed('tenant', 20 ), true );

					assert.notEqual( d.sizeAllowed('mascot', '12345' ), true );
					assert.notEqual( d.sizeAllowed('tenant', '12345' ), true );

					assert.notEqual( d.sizeAllowed('mascot', 10 ), true );
					assert.notEqual( d.sizeAllowed('tenant', 10 ), true );

				})

				it("should only allow SHORT & MEDIUM names", function(){
					d = new Decal({
						lengthLimits: {
						     mascot: [constants.SIZE_SHORT, constants.SIZE_MEDIUM]
						    ,tenant: [constants.SIZE_SHORT, constants.SIZE_MEDIUM]
						}
					});


					assert.equal( d.sizeAllowed('mascot', 5 ), true )
					assert.equal( d.sizeAllowed('tenant', 5 ), true )

					assert.notEqual( d.sizeAllowed('mascot', 20 ), true )
					assert.notEqual( d.sizeAllowed('tenant', 20 ), true )


					assert.equal( d.sizeAllowed('mascot', '12345' ), true )
					assert.equal( d.sizeAllowed('tenant', 10), true );

					assert.notEqual( d.sizeAllowed('mascot', 'hello world this is big' ), true );
					assert.notEqual( d.sizeAllowed('tenant', 'hello world this is big' ), true );

				})
			})
		})
	})
})
