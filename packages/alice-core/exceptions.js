/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * exceptions.js
 * @module alice-core/exceptions
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires alice-stdlib/exception
 * @requires alice-stdlib/class
 */

var Exception = require( 'alice-stdlib/exception' )
  , Class = require( 'alice-stdlib/class' )
  ;

/**
 * Used when Some thing has somehow been misconfigured
 * @class module:alice-core/exceptions.ImproperlyConfigured
 * @extends module:alice-stdlib/exception
 * @param {Object} [options]
 * @param {String} [options.name=ImproperlyConfigured]
 * @param {Number} [options.code=4000]
 * @param {String} [options.type=imporoperly_configured]
 */
exports.ImproperlyConfigured = new Class(/** @lends module:alice-core/exceptions.ImproperlyConfigured.prototype */{
	inherits:Exception
	,options:{
		name: 'ImproperlyConfigured'
		,code:4000
		,type:'improperly_configured'
	}
});

/**
 * Should be used when specific object was unable to be located
 * @class module:alice-core/exceptions.ObjectDoesNotExist
 * @extends module:alice-stdlib/exception
 * @param {Object} [options]
 * @param {String} [options.name=ObjectDoesNotExist]
 * @param {Number} [options.code=4001]
 * @param {String} [options.type=object_does_not_exist]
 */
exports.ObjectDoesNotExist = new Class(/** @lends module:alice-core/exceptions.ObjectDoesNotExist.prototype */{
	inherits:Exception
	,options:{
		name : 'ObjectDoesNotExist'
		,code: 4001
		,type: 'object_does_not_exist'
	}
});

/**
 * Should be used when a set of criteria returns more than one object when one object is strictly expected
 * @class module:alice-core/exceptions.MultipleObjectsRetured
 * @extends module:alice-stdlib/exception
 * @param {Object} [options]
 * @param {String} [options.name=MultipleObjectsRetured]
 * @param {Number} [options.code=4002]
 * @param {String} [options.type=multiple_objects_returned]
 */
exports.MultipleObjectsRetured = new Class(/** @lends module:alice-core/exceptions.MultipleObjectsRetured.prototype */{
	inherits:Exception
	,options:{
		name: 'MultipleObjectsRetured'
		,code:4002
		,type:'multiple_objects_returned'
	}
});

/**
 * Should be used when the requeting party does not have adequate permissions to perform a given action
 * @class module:alice-core/exceptions.PermissionDenied
 * @extends module:alice-stdlib/exception
 * @param {Object} [options]
 * @param {String} [options.name=PermissionDenied]
 * @param {Number} [options.code=4003]
 * @param {String} [options.type=permission_denied]
 */
exports.PermissionDenied = new Class(/** @lends module:alice-core/exceptions.PermissionDenied.prototype */{
	inherits:Exception
	,options:{
		name: 'PermissionDenied'
		,code:4003
		,type:'permission_denied'
	}
});

/**
 * Should be thrown when a validation routine fails to pass for any reason
 * @class module:alice-core/exceptions.ValidationException
 * @extends module:alice-stdlib/exception
 * @param {Object} [options]
 * @param {String} [options.name=ValidationException]
 * @param {Number} [options.code=4004]
 * @param {String} [options.type=validateion_failed]
 */
exports.ValidationException = new Class(/** @lends module:alice-core/exceptions.ValidationException.prototype */{
	inherits:Exception
	,options:{
		name: 'ValidationException'
		,code:4004
		,type:'validation_failed'
	}
});
/**
 * Should be thrown when a method which must be, has not been implemented
 * @class module:alice-core/exceptions.ValidationException
 * @extends module:alice-stdlib/exception
 * @param {Object} [options]
 * @param {String} [options.name=NotImplemented]
 * @param {Number} [options.code=4005]
 * @param {String} [options.type=not_implemented]
 */
exports.NotImplemented = new Class({
	inherits:Exception
	,options:{
		name :'NotImplemented'
		,code:4005
		,type:'not_implemented'
	}
})

exports.TemplateException = new Class({
	inherits:Exception
	,options:{
		name:'TemplateException'
		,code:4006
		,type:'template_render'
	}
})


exports.TemplateFilterException = new Class({
	inherits:Exception
	,options:{
		name:'TemplateFilterException'
		,code:4007
		,type:'template_filter'
	}
})
