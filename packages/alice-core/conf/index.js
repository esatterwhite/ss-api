/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Configuration options for alice-core
 * @module alice-core/conf
 * @author Eric Satterwhite
 * @since 0.2.0
 */

exports.FIXTURE_PATH = 'fixtures'
/**
 * @property {Object} mail default configuration settings for mail related operations
 * @property {String} [mail.defaultemail=noreply@spiritshop.com] default email addres which will show up in the from field if none is specified
 * @property {String} [mail.defaultsender=Spirit Shop LLC.] Name which will show up in a message's sender field if non is specified
 * @property {Object} mail.smtp default settings for plain smtp transport
 * @property {Number} [mail.smtp.port=2525] Port of remote smtp server to connect to
 * @property {String} [mail.smtp.host=localhost] Hostname or ip addres of the remote host to connect to 
 * @property {Object} mail.mandrill default settings for the mandrill smtp transport
 * @property {Number} mail.mandrill.port=25 port of at running mandrill smtp serve
 * @property {String} mail.mandrill.host=smtp.mandrillapp.com port of at running mandrill smtp serve
 **/
exports.mail = {
	defaultemail:'noreply@spiritshop.com'
	,defaultsender:'Spirit Shop LLC.'
	,transport:'console'
	,admins:[
		"eric@spiritshop.com"
	]
	,smtp:{
		host:'localhost'
		,port:2525
	}

	,mandrill:{
		host:'smtp.mandrillapp.com'
		,port:25
	}
};
