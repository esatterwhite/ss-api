/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
* The Core package provides foundational functions, helpers and base line implementations to be extended and used by the rest of the **Alice** platform
* 
* 
* ### Loading
* 
* A module that provides the base line for finding and loading various resources into the framework. 
* 
* #### Fixtures
* 
* This fixture loader, can find fixture file for a specific package, all packages, the host project, or of course all availible fixtures. In addition to locating files, it can also require & return fixtures data.
* 
* Fixture lookups can be performed with the `find` method
* ```js
* var fixtures = require('alice-core').loading.fixtures;
* 
* // project fixtures
* fixtures.find('alice'); 
* {
* 	alice:['/path/to/project/fixtures/data.json']
* }
* 
* // project fixtures, and fixtures from alice-core 
* fixtures.find('alice', 'core'); 
* {
* 	alice:['/path/to/project/fixtures/data.json']
*   , 'alice-core':[ '/path/to/project/packages/alice-core/fixtures/data.json' ] 
* }
* ```
* 
* Fixture loading is done in a similar fashion with the `load` function
* ```js
* // project fixtures
* fixtures.find('alice'); 
* {
* 	alice:[{ ... }, { ... }]
* }
* 
* // project fixtures, and fixtures from alice-core 
* fixtures.find('alice', 'core'); 
* {
* 	alice:[{ ... }, { ... } ]
*   , 'alice-core':[ { ... }, { ... }, { ... } ] 
* }
* ```
* 
* @module module:alice-core
* @author Eric Satterwhite
* @since 0.1.0
* @requires alice-core/events 
* @requires alice-core/lib/loading
* @requires alice-core/lib/db
* @requires alice-core/lib/mail
*/

/**
 * General commands for the core platform
 * @namespace commands
 * @requires alice-core/commands/import
 * @requires alice-core/commands/export
 * @requires alice-core/commands/generate
 * @requires alice-core/commands/repl
 * @requires alice-core/commands/serve
 * @requires alice-core/commands/stripe_charge
 * @requires alice-core/commands/version
 * @requires alice-core/commands/docs
 */
module.exports         = require('./events');
module.exports.loading = require('./lib/loading')
module.exports.db      = require("./lib/db")
module.exports.mail    = require('./lib/mail')
