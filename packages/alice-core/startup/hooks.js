/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * ensures that event handlers for system internal events get loaded.
 * @module alice-cart/startup/hooks
 * @author Eric Satterwhite
 * @since 0.2.0
 * @requires alice-log
 */
var logger = require( 'alice-log' )
module.exports = function( server ){
	var listeners = require('alice-core/lib/loading/listeners');
	logger.info("loading system event handlers" )
	var packages = listeners.load()
	Object.keys( packages )
		.forEach( function( pkg ){

			if( !!packages[pkg].length ){
				logger.info('hooks: loaded %s handlers', pkg )
			}
		})
}
