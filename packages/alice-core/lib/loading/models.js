/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * module that knows how to load fixture files in from the project tree
 * @module alice-core/lib/loading/models
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires alice-conf
 * @example var models = require('alice-core/lib/loading/models')
 console.log( models.find( ) )
 */

var conf          = require( 'alice-conf' )                      // alice configuration loader
  , path          = require('path')
  , debug         = require( 'debug' )('alice:loading:models')   // debuging instance
  , Class         = require('alice-stdlib/class')                // standard Class
  , Loader        = require('./loader')                          // Options mixin for Class
  , ModelLoader                                                  // Base Loader class
  , loader                                                       // Default instance of the Base loader
  ;

/**
 * Locates and loads database models from selected packages and modules
 * @constructor
 * @alias module:alice-core/lib/loading/models
 * @extends module:alice-core/lib/loading/loader
 * @param {Object} [options] Instance specific configuration options
 * @param {String} [options.searchpath=models] search path to look for files to load
 * @param {RegExp} [options.filepattern=/\.js|json/] a regular expression used to qualify files
 * @example var models = require('alice-core/lib/loading/models')
 // using the default loader 
 console.log( models.find( ) )
 console.log( models.find( 'conf', 'cache' ) )
 console.log(models.load('alice', 'alice-cache', 'conf' ) )

// using a new loader instance
 var loader = new models();
 console.log( loader.find( ) )
 console.log( loader.find( 'conf', 'cache' ) )

 console.log( loader.load('alice', 'alice-cache', 'conf' ) )

 */
ModelLoader = new Class(/* @lends module:alice-core/lib/loading/models.prototype  */{
  inherits: Loader
  ,options:{
    searchpath: 'models'
    ,filepattern: /\.js|json$/
  }
  ,constructor: function( options ){
    this.parent('constructor', options )
  }
  ,toName: function( app, pth ){
    return pth.replace(this.options.extensionpattern ,'').substr(pth.lastIndexOf( path.sep )+1)
  }
});

loader = new ModelLoader();

/**
 * Locates fixture files located in the project
 * @static
 * @param {...String} [packages] Any number of applications to load models from
 * @return Object an object where keys are app names and its value is an array of full paths to all of the fixture files that were found
*/
exports.find = function find( ){
  return loader.find.apply( loader, arguments )
}

/**
 * Returns An object containe all of the fixture objects listed by app name
 * @static
 * @example
var models = require("alice-core/lib/loading").models
models.load( 'alice', 'conf' )
 * @param {...String} [packages] Any number of applications to load models from
 * @return Object an object where keys are app names and its value is an array the fixture data to all of the fixture files that were found
 */
exports.load = function load(  ){
  return loader.load.apply( loader, arguments )
}

exports.Loader = ModelLoader;
