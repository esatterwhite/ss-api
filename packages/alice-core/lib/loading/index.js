/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * Module which houses frameworks various loading capabilities
 * @module alice-core/lib/loading
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires alice-core/lib/loading/fixtures
 * @requires alice-core/lib/loading/models
 * @requires alice-core/lib/loading/loader
 */

exports.fixtures = require('./fixtures')
exports.models   = require('./models')
exports.Loader   = require('./loader')
