/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * module that knows how to load fixture files in from the project tree
 * @module alice-core/lib/loading/templates
 * @author Eric Satterwhite
 * @since 0.2.0
 * @requires alice-conf
 * @example var templates = require('alice-core/lib/loading/templates')
 console.log( templates.find( ) )
 */

var conf          = require( 'alice-conf' )                         // alice configuration loader
  , path          = require( 'path')
  , fs            = require( 'fs')
  , nunjucks      = require( 'nunjucks')
  , debug         = require( 'debug' )('alice:loading:templates')   // debuging instance
  , Class         = require( 'alice-stdlib/class')                  // standard Class
  , get           = require( 'alice-stdlib/object').get
  , clone         = require( 'alice-stdlib/lang' ).clone            // standard clone function
  , compact       = require( 'alice-stdlib/array' ).compact         // standard compact function
  , flatten       = require( 'alice-stdlib/array' ).flatten         // standard flatten function
  , attempt       = require( 'alice-stdlib/function' ).attempt      // try/catch wrapper
  , values        = require( 'alice-stdlib/object' ).values
  , PROJECT_ROOT  = conf.get( 'PROJECT_ROOT' )                      // path to the root of the project
  , PACKAGE_PATH  = conf.get( 'PACKAGE_PATH' )                      // path to the root of the project
  , Loader        = require( '../loader' )                          // Options mixin for Class
  , filters       = require( './filters' )
  , alicecheck    = /^alice/                                        // regex to check if a key starts with the word alice
  , TemplateLoader                                                  // Base Loader class
  , loader                                                          // Default instance of the Base loader
  ;

/**
 * Locates and loads templates from selected packages and modules, also
 * @constructor
 * @alias module:alice-core/lib/loading/templates
 * @extends module:alice-core/lib/loading/loader
 * @param {Object} [options] Instance specific configuration options
 * @param {String} [options.searchpath=templates] search path to look for files to load
 * @param {RegExp} [options.filepattern=/\.js|json/] a regular expression used to qualify files
 * @example var templates = require('alice-core/lib/loading/templates')
 // using the default loader
 console.log( templates.find( ) )
 console.log( templates.find( 'conf', 'cache' ) )
 console.log(templates.load('alice', 'alice-cache', 'conf' ) )

// using a new loader instance
 var loader = new templates();
 console.log( loader.find( ) )
 console.log( loader.find( 'conf', 'cache' ) )

 console.log( loader.load('alice', 'alice-cache', 'conf' ) )

 */
TemplateLoader = new Class(/* @lends module:alice-core/lib/loading/templates.prototype  */{
	inherits: Loader
	,options:{
		searchpath: 'templates'
		,filepattern: /\.html|tpl|jna$/
		,watch:false
		,autoexcape:false
	}
	,constructor: function( options ){
		this.parent('constructor', options );
		this.template = nunjucks.configure(this.packages(),this.options);
		for(var key in filters ){
			this.template.addFilter( key, filters[key]);
		}
	}
	,toName: function( app, pth ){
		return pth.substr(pth.lastIndexOf( path.sep )+1);
	}
	,remap: function remap( loaded ){
		return {name:loaded.name, template: this.template.getTemplate( loaded.name ) };
	}
	,flat: function flat(){
		var out = Loader.prototype.flat.call(this);
		return out.map(function( t ){return t.template });
	}
	,packages: function packages( ){

		var apps = fs
				.readdirSync( PACKAGE_PATH )
				.filter( function( dir ){
					return alicecheck.test( dir );
				});

		apps.push('alice');

		var paths = apps.map(function(app){
			return app === 'alice' ?
					path.join( PROJECT_ROOT, this.options.searchpath )  :
					path.join( PACKAGE_PATH, app, this.options.searchpath )
				;
		}.bind( this ));

		return paths;
	}
});

loader = new TemplateLoader();

/**
 * Locates fixture files located in the project
 * @static
 * @param {...String} [packages] Any number of applications to load templates from
 * @return Object an object where keys are app names and its value is an array of full paths to all of the fixture files that were found
*/
exports.find = function find( ){
	return loader.find.apply( loader, arguments )
}

/**
 * Returns An object containe all of the fixture objects listed by app name
 * @static
 * @example
var templates = require("alice-core/lib/loading").templates
templates.load( 'alice', 'conf' )
 * @param {...String} [packages] Any number of applications to load templates from
 * @return Object an object where keys are app names and its value is an array the fixture data to all of the fixture files that were found
 */
exports.load = function load(  ){
	return loader.load.apply( loader, arguments )
}

exports.render = function render(){
	return loader.template.render.apply( loader.template, arguments )
}
exports.renderString = function renderString(){
	return loader.template.renderString.apply( loader.template, arguments )
}

exports.Loader = TemplateLoader;
