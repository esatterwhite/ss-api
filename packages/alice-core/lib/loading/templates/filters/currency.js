/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Template filter to convert integers into a USD formatted string
 * @module alice-core/lib/loading/templates/filters/currency
 * @author Eric satterwhite
 * @since 0.2.0
 * @requires alice-core/exceptions
 * @requires alice-stdlib/number
 * @requires alice-stdlib/lang
 */

var exceptions = require( '../../../../exceptions' )
  , number = require('alice-stdlib/number')
  , isInteger = require('alice-stdlib/lang').isInteger
  ;

module.exports = function currency( num, kwargs ){
	if(!isInteger(num)){
		throw new exceptions.TemplateFilterException({
			message:"currency filter must be an integer - Got: " + num
		});
	}

	return number.formatCurrency(num/100, kwargs )
};
