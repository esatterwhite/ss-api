/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * Base implementation of a loader. Default loads package.json files from top level directory and each package in packages
 * @module alice-core/lib/loading/loader
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires debug
 * @requires path
 * @requires fs
 * @requires fs
 * @requires alice-conf
 * @requires alice-stdlib/class
 * @requires alice-stdlib/class/options
 * @requires alice-stdlib/class/parent
 * @requires alice-stdlib/lang
 * @requires alice-stdlib/array
 * @requires alice-stdlib/function
 */

var debug           = require( 'debug' )('alice:loading:base')   // debuging instance
	, path          = require( 'path' )                          // node path module
	, fs            = require( 'fs' )                            // node fs module
	, conf          = require( 'alice-conf' )                    // alice configuration loader
	, PROJECT_ROOT  = conf.get( 'PROJECT_ROOT' )                 // path to the root of the project
	, PACKAGE_PATH  = conf.get( 'PACKAGE_PATH' )                 // path to the root of the project
	, Class         = require( 'alice-stdlib/class' )            // standard Class
	, Options       = require( 'alice-stdlib/class/options' )    // Options mixin for Class
	, Parent        = require( 'alice-stdlib/class/parent' )     // Options mixin for Class
	, clone         = require( 'alice-stdlib/lang' ).clone       // standard clone function
	, toArray       = require( 'alice-stdlib/lang' ).toArray     // standard function to convert things to arrays
	, compact       = require( 'alice-stdlib/array' ).compact    // standard compact function
	, flatten       = require( 'alice-stdlib/array' ).flatten    // standard flatten function
	, attempt       = require( 'alice-stdlib/function' ).attempt // try/catch wrapper
	, values        = require("alice-stdlib/object").values
	, glob          = require('glob')
	, minimatch     = require('glob/node_modules/minimatch')
	, alicecheck    = /^alice/                                   // regex to check if a key starts with the word alice
	, Loader                                                     // Base Loader class
	;


/**
 * And object mapping arrays of files to their associated package names
 * @typedef {Object} module:alice-core/lib/loading/loader~files
 * @property {Function} flatt reduces the object into a single array
 */

/**
 * Base implementation of A file loader. Can locate all package.json files for packages and alice 
 * @constructor
 * @alias module:alice-core/lib/loading/loader
 * @param {Object} [options] Instance specific configuration options
 * @param {String} [options.searchpath=''] search path relative to {@link module:alice-conf/lib/overrides.PACKAGE_PATH|PACKAGE_PATH} to look for package files to load
 * @param {RegExp} [options.filepattern=/package.json$/] a regular expression used to qualify files
 * @mixes module:alice-stdlib/class/options
 * @mixes module:prime-util/prime/parentize
 */
Loader = new Class(/* @lends module:alice-core/loading/loading.prototype  */{
	mixin:[Options, Parent]
	,options:{
		searchpath: ''
		,extensionpattern:/\.[\w]+$/
		,filepattern: /package\.json$/
		,recurse: true
	}
	,constructor: function( options ){
		this.paths = {};
		this.cache = {};
		this.setOptions( options );
	}
	/**
	 * Locates specific files located throught the project and loads returns their absolute path
	 * @method module:alice-core/lib/loading/loader#find
	 * @example var loader.find('core', 'conf', 'alice')
	 * @param {...String} [packages] Any number of applications to load packages from. Packages are restricted to the alice packages under the {@link module:alice-conf/lib/overrides.PACKAGE_PATH|PACKAGE_PATH} setting . the `alice-` prefix is optional
	 * @return {module:alice-core/lib/loading/loader~files} an object where keys are app names and its value is an array of full paths to all of the fixture files that were found
	*/
	,find: function find(){
		var apps  // users specified application to search for files
		  , that  // reference to this
		  , obj   // object that will be returned from function
		  ;

		apps = toArray( arguments );
		that = this;
		obj = {};

		Object.defineProperty(obj,'flat',{
			value:this.flat
		});
		if( apps.length ){
			// find them all
			apps = apps.map(function( app ){
				return alicecheck.test( app ) ? app : 'alice-' + app;
			});

		}else{
			// find specified

			apps = fs
					.readdirSync( PACKAGE_PATH )
					.filter( function( dir ){
						return alicecheck.test( dir );
					});

			apps.push('alice')
		}

		debug('loading %s', apps.join(', ') );
		apps.forEach(function( app ){
			var directoryPath = app === 'alice' ? 
					path.join( PROJECT_ROOT, that.options.searchpath )  : 
					path.join( PACKAGE_PATH, app, that.options.searchpath )
				;

			debug('checking %s', directoryPath );
			
			if( that.paths[ app ] ){
				 return obj[ app ] = that.paths[app];
			}


			if( fs.existsSync( directoryPath ) &&  fs.statSync( directoryPath ).isDirectory() ){
				that.paths[ app ] = [];


				var files = app === "alice" ? 
								glob.sync( path.join( directoryPath, "*") ) :  
								that.options.recurse ?
									glob.sync( path.join( directoryPath, "**", "*" ) ) :
									glob.sync( path.join( directoryPath, "*") )

				files
				.forEach( function( f ){

					if( ( that.options.filepattern ).test( f ) ){
						f = path.normalize( f )
						debug('found fixture %s', f );
						that.paths[app].push( {name:that.toName(app, f ), path: f }  );
					}
				});
				obj[ app ] = clone( that.paths[ app ] );
			}
		});
		return obj;
	}
	/**
	 * Similar to the find method, but returns the object returned by require rather than paths
	 * @method module:alice-core/lib/loading/loader#load
	 * @param {...String} [packages] Any number of applications to load packages from
	 * @example loader.load('core', 'conf', 'alice')
	 * @return {module:alice-core/lib/loading/loader~files} an object where keys are app names and its value is an array the fixture data to all of the fixture files that were found
	 */
	,load: function load(){
		var packages = this.find.apply(this, arguments)
		  , obj = {}
		  ;

		Object.defineProperty(obj,'flat',{
			value:this.flat
		});

		for( var key in packages ){
			this.cache[key] = this.cache[key] || [];
			if( !this.cache[key].length ){
				this.cache[key] = compact( packages[key].map( this.remap.bind(this) ) );
			}

			obj[key] = clone( this.cache[key]);
		}

		return obj;
	}

	/**
	 * The return value is used as the name of the module to be loaded Defaults to the name of the application
	 * @method module:alice-core/lib/loading/loader#toName
	 * @param {String} app The name of the application the file is associated with
	 * @param {String} path the full path of the file to be loaded
	 * @return {String} The name of the file to be loaded
	 **/
	,toName: function toName(app, path ){
		return app
	}

	,remap: function remap( loaded ){
		return attempt( require.bind( require, loaded.path ) )
	}
	/**
	 * The method used to flatten out the loader data structure to a single array
	 * @method module:alice-core/lib/loading/loader#flat
	 * @return {Array} A single arry of the loaded files or modules
	 **/
	,flat: function flat(){
		var items = values( this )
		  , out = []
		  ;

		for(var x=0,len=items.length; x<len; x++){
			out = out.concat( items[x] )
		}
		return flatten( out )
	}
	/**
	 * clears internal file cache
	 * @method module:alice-core/lib/loading/loader#reset
	 * @return {Loader} Current loader instance
	 **/

	,reset: function reset( ){
		this.cache = {};
		this.paths = {};
		return this;
	}
});
module.exports = Loader;
