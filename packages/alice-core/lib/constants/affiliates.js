var affiliates = ['hss', 'mp', 'vnn', 'sun', 'mhs', 'ss'];

var affiliateNames = [];
affiliateNames['hss'] = 'USA Today High School Sports';
affiliateNames['mp'] = 'MaxPreps';
affiliateNames['vnn'] = 'Varsity News Network';
affiliateNames['sun'] = 'Sun Times';
affiliateNames['mhs'] = 'Michigan High School';
affiliateNames['ss'] = 'ScheduleStar';
affiliateNames['gsc'] = 'Great Schools'; //NOT a true affiliate just data validation provider

module.exports.affiliateCodes = affiliates;
module.exports.affiliateNames = affiliateNames;
