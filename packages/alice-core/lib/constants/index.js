/*jshint laxcomma: true, smarttabs: true, node: true*/
'use strict';
/**
 * A collection of contant data and information used in the core module
 * @module alice-core/lib/constants
 * @author Eric Satterwhite
 * @since 0.2.0
 * @requires alice-core/lib/constants/affiliates
 * @requires alice-core/lib/constants/gender
 * @requires alice-core/lib/constants/countries
 * @requires alice-core/lib/constants/states
 */

var affiliates = require('./affiliates')
  , gender     = require('./gender')
  , countries  = require('./countries')
  , states     = require('./states')
  , constants = {}
  ;

/**
 * @memberof module:alice-core/lib/constants
 * @constant {String} TENANT_TYPE_LANDING The internal idetifier for a tenant type landing used within the directory modules
 **/
constants.TENANT_TYPE_LANDING = 'tenant_type_landing';
/**
 * @memberof module:alice-core/lib/constants
 * @constant {String} DIRECTORY_CITY The internal idetifier for cities used within the directory modules
 **/
constants.DIRECTORY_CITY  = 'directory_city'
/**
 * @memberof module:alice-core/lib/constants
 * @constant {String} DIRECTORY_STATE The internal idetifier for states used within the directory modules
 **/
constants.DIRECTORY_STATE = 'directory_state'
/**
 * @memberof module:alice-core/lib/constants
 * @constant {String} DIRECTORY_TENANT The internal idetifier for tenants used within the directory modules
 **/
constants.DIRECTORY_TENANT = 'directory_tenant'


/**
 * @memberof module:alice-core/lib/constants
 * @constant {Object} states Contains an array of all state abbreviations
 **/
constants.states         = Object.freeze( states );

/**
 * @memberof module:alice-core/lib/constants
 * @constant {Object} countries Contains a list of all countries which have partnering companies
 **/
constants.countries      = Object.freeze( countries );

/**
 * @memberof module:alice-core/lib/constants
 * @constant {Object} gender An array of all viable gender abbreviatsion
 **/
constants.gender         = Object.freeze( gender );

/**
 * @memberof module:alice-core/lib/constants
 * @constant {Object} affiliateCodes An array of all know affiliate codes
 **/
constants.affiliateCodes = Object.freeze( affiliates.affiliateCodes );

/**
 * @memberof module:alice-core/lib/constants
 * @constant {Object} affiliateNames An array of all know Affiliates
 **/
constants.affiliateNames = Object.freeze( affiliates.affiliateNames );

Object.freeze( constants )

module.exports = constants;
