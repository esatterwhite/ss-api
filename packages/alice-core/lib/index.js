/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * Provides the core functionality for the alice playform
 * @module alice-core/lib
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires alice-core/lib/loading
 */


exports.loading = require('./loading')
exports.mail = require('./mail')
