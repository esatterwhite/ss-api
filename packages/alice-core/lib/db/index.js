/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * index.js
 * @module alice-core/lib/db
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires alice-core/lib/db/connection
 */

var Connection = require( './connection' )
  , conf       = require('alice-conf')
  , clone      = require('alice-stdlib/lang').clone
  , models     = require( './models' )
  ;

exports.Connection = Connection;
exports.connection = new Connection( {databases:clone( conf.get('databases') )} );
exports.models = models;
