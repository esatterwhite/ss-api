/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * index.js
 * @module alice-core/lib/db/schema/validators/defaults
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires alice-core/lib/db/schema/validators/defaults/query
 * @requires alice-core/lib/db/schema/validators/defaults/response
 */

exports.query = require( './query' );
exports.response = require( './response' );
