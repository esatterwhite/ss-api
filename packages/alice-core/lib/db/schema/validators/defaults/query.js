/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Default querystring parameter validator for API Endpoints
 * @module alice-core/lib/db/schema/validators/defaults/query
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires joi
 */

var joi = require( 'joi' )
  , conf = require('alice-conf')
  , production = conf.get('NODE_ENV') === 'production'

module.exports = joi.object({
	/**
	 * @readonly
	 * @memberof module:alice-core/lib/db/schema/validators/defaults/query
	 * @property {Number} limit Limits the number for results per page of data. Default is `25`
	 **/	
	limit:joi.number().min(0).default(25).description("the number of records to return"),
	/**
	 * @readonly
	 * @memberof module:alice-core/lib/db/schema/validators/defaults/query
	 * @property {Number} offset starting page number for listing endpoints Default is `0`
	 **/	
	offset:joi.number().min(0).default(0).description("the starting page number"),
	/**
     * @readonly
     * @memberof module:alice-core/lib/db/schema/validators/defaults/query
     * @property {String}  format ad hoc serialization formats. Allows `json`, `xml`, `jsonp`
     **/
	format:joi.string().alphanum().allow('json','xml','jsonp'),
	/**
     * @readonly
     * @memberof module:alice-core/lib/db/schema/validators/defaults/query
     * @property {String}  callback Used for jsonp callback methods
	**/
	callback:joi.string().alphanum().description('query parameter used to force a jsonp response'),
	/**
     * @readonly
     * @memberof module:alice-core/lib/db/schema/validators/defaults/query
     * @property {String}  apikey Used for adhoc account auth access
     **/
	apikey:joi.string().guid().default(production ? null : 'c4125761-7f82-49b4-8436-3bde15dabb7e' ).description("authentication token")

	/**
     * @readonly
     * @memberof module:alice-core/lib/db/schema/validators/defaults/query
     * @property {String}  order used to sort collections of resources
     **/
	,order: joi.string().optional().description('name of a field to sort the results by')
})
