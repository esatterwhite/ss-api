/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Default validator for API resposne for listing endpoints
 * @module alice-core/lib/db/schema/validators/defaults/response
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires joi
 */

var joi = require( 'joi' );


module.exports = joi.object({
	/**
	 * @readonly
	 * @name data
	 * @memberof module:alice-core/lib/db/schema/validators/defaults/response
	 * @property {Object[]} data Array containing the requested data 
	 **/  
	 data:joi.array()
	 			.includes(joi.object())
	 			.required()
	 			.description('An array of discrete data items')

	/**
     * @readonly
     * @name meta
     * @memberof module:alice-core/lib/db/schema/validators/defaults/response
     * @property {Object} apikey Used for adhoc account auth access
     **/  
	,meta:joi.object(/** @lends module:alice-core/lib/db/schema/validators/defaults/response.meta **/{
		/**
	     * @readonly
	     * @name next
	     * @memberof module:alice-core/lib/db/schema/validators/defaults/response.meta
	     * @property {String} next A URI path the to next page in the data set
	     **/  
		next:joi.string().allow(null).description("A URI path the to next page in the data set")
		/**
		 * @readonly
		 * @name previous
		 * @memberof module:alice-core/lib/db/schema/validators/defaults/response.meta
		 * @property {String} previous A URI path to the previoius page in the data set
		 **/
		,previous:joi.string().allow(null).description('A URI path to the previoius page in the data set')
		/**
		 * @readonly
		 * @name count
		 * @memberof module:alice-core/lib/db/schema/validators/defaults/response.meta
		 * @property {Number} count The total number of items in the full data set
		 **/
		,count:joi.number().required().description("The total number of items in the full data set")
		/**
		 * @readonly
		 * @name limit
		 * @memberof module:alice-core/lib/db/schema/validators/defaults/response.meta
		 * @property {Number} limit The maximum number of elements per page
		 **/
		,limit:joi.number().required().description('The maximum number of elements per page')
		/**
		 * @readonly
		 * @name offset
		 * @memberof module:alice-core/lib/db/schema/validators/defaults/response.meta
		 * @property {Number} offset The position in the data set to start paging
		 **/
		,offset:joi.number().required().description('The position in the data set to start paging')
	}).description('Data describing the current response, containing information to make additional requests')
});
