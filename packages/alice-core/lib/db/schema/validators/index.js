/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Predefines a baseline set of Joi Validators
 * @module alice-core/lib/db/schema/validators
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires alice-core/lib/db/schema/validators/defaults/query
 */

/**
 * @property {Object} [defaults] default set of validators
 * @property {Object} [defaults.query] default query string validator
 * @property {Object} [defaults.response] default response validator
 **/
exports.defaults = {
	query:require('./defaults/query')
	,response:require('./defaults/response')
}
