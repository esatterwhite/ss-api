/**
 * Created by ss-dev-01 on 4/14/2014.
 */
'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	util = require('util');

var AuditableSchema = function (fields) {
	var originalVersion;
	Schema.apply( this, arguments )
	this.add( {createdBy: {type: String, default: ''} });
	this.add( {createdDate: {type: Date, default: Date.now()} });
	this.add( {modifiedBy: {type: String, default: ''} });
	this.add( {modifiedDate: {type: Date, default: Date.now()} });
	this.recordstatus = {type: String, enum:['pending', 'active', 'suspended', 'inactive', 'deleted'], required:true, default:'active'};

	this.pre('save', function (next) {
		var updatedAttributes = this.modifiedPaths();
		// console.log(updatedAttributes);
		next();
	});

	this.post('save', function (doc) {
		// console.log(originalVersion);
		//TODO: get context somehow from connection use db, collection, document
		// determine changed fields and store before after in audit collection.

	});
};

util.inherits(AuditableSchema, Schema)

module.exports = AuditableSchema;
