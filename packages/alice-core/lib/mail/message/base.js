/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * index.js
 * @module alice-core/lib/mail/message
 * @author Eric Satterwhite
 * @since 0.2.0
 * @requires events
 * @requires joi
 * @requires alice-stdlib/class
 * @requires alice-stdlib/class/parent
 * @requires alice-stdlib/class/options
 * @requires alice-core/lang
 * @requires alice-core/lib/mail/message/id
 */

var events    = require( 'events' )
  , joi       = require( 'joi' )
  , conf      = require( 'alice-conf' )
  , Class     = require( 'alice-stdlib/class' )
  , Parent    = require( 'alice-stdlib/class/parent' )
  , Options   = require( 'alice-stdlib/class/options' )
  , clone     = require( 'alice-stdlib/lang' ).clone
  , toArray   = require( 'alice-stdlib/lang' ).toArray
  , stripTags = require( 'alice-stdlib/string').stripHtmlTags
  , mtrim     = require( 'alice-stdlib/string').trim
  , isString  = require( 'alice-stdlib/lang').isString
  , id        = require( './id' )
  , templates = require( '../../loading/templates')
  , isArray   = Array.isArray
  , EmailMessage
  , attachmentValidator
  , addressValidator
  ;


function trim( s ){
	return mtrim( s )
}

addressValidator = joi.alternatives().try(
	joi.string().email()
	, joi.array().includes( joi.string().email() )
	, joi.object({
		name:joi.string().required()
		,address:joi.string().email().required()
	})
)

attachmentValidator = joi.object({
	filename:joi.string()
				.when('path',{is:true, then:joi.optional(), otherwise:joi.required() })
				.when('content',{is:true, then: joi.required(), otherwise:joi.optional() } )

	,content:joi.alternatives([ joi.binary(), joi.string() ] )
	,contentType:joi.string().optional()
	,path:joi.string().optional()
}).required()

/**
 * Object containing content to be send in email message
 * @typedef {Object} EmailContent
 * @property {String} html Rendered html content for email
 * @property {String} text plain text version of the html
 **/


/**
 * @constructor
 * @alias module:alice-core/lib/mail/message
 * @param {Object} options Message options
 * @param {String} options.from email address the email is originating from
 * @param {String} options.sender email address to appear in the sender field of the email
 * @param {String|String[]} options.to a comma separated list of emails or an array of emails to send the message to
 * @param {String} options.cc a comma separated list of emails or an array of emails to copy on the email
 * @param {String} options.bcc a comma separated list of emails or an array of emails to blind copy on the email
 * @param {String} options.subject The value to appear in the subject field
 * @param {String} [options.text] Plain text version of the email to send
 * @param {String} [options.html] An html version of the email to send
 * @param {String} options.replyTo An e-mail address that will appear on the Reply-To: field
 * @param {String} options.inReplyTo The message-id this message is replying
 * @param {?String} [options.references] Message-id list (an array or space separated string)
 * @param {Object} [options.headers={}] Additional headers to send with the email
 * @param {?Array} options.attachments An array of attachment objects
 * @param {?Array} options.alternatives  An array of alternative text contents
 * @param {?String} [options.messageId] An id to use for the message. If not provided, one will be auto generated
 * @param {Date} [options.date] optional Date value, current UTC string will be used if not set
 * @param {String} [options.encoding='quoated-printable'] optional transfer encoding for the textual parts
 * @param {String} [options.data={}] data to be used to render associated tempalte
 * @param {?String} [options.template=null] The name of a template to use to render the HTML content for the email
 */
EmailMessage = new Class({
	inherits: events.EventEmitter
  , mixin:[ Options, Parent ]
  , options:{
	  	from        : null
	  , sender      : null
	  , to          : null
	  , cc          : null
	  , bcc         : null
	  , subject     : ''
	  , text        : ''
	  , html        : ''
	  , replyTo     : null
	  , inReplyTo   : null
	  , references  : null
	  , headers     : {}
	  , attachments : null
	  , alternatives: null
	  , messageId   : null
	  , date        : null
	  , encoding    : null
	  , data        : {}
	  , template    : null
	}

	, constructor: function( options ){
		this.setOptions( options )
	}	

	/**
	 * Renders a serializable JSON object
	 * @private
	 * @method module:alice-core/lib/mail/message/base#toJSON
	 * @return {Object} message a fully formatted object suitable for sending and serialization 
	 **/
	, toJSON: function(){
		var content = this.render();
		return {
			from         : this.from
		  , sender       : this.sender
		  , to           : this.to
		  , cc           : this.cc
		  , bcc          : this.bcc
		  , messageId    : this.id
		  , replyTo      : this.options.replyTo
		  , inReplyTo    : this.options.inReplyTo
		  , references   : this.options.references
		  , subject      : this.subject
		  , text         : content.text
		  , html         : content.html
		  , headers      : this.processHeaders()
		  , attachments  : this.options.attachments
		  , alternatives : this.options.alternatives
		  , date         : this.options.date
		  , encoding     : this.options.encoding
		}
	}

	, toString: function(){
		return this.id
	}

	, attach: function( definition ){
		var result = attachmentValidator.validate( definition )

		if( result.error ){
			throw result.error.annotate()
		}
		(this.options.attachments = this.options.attachments ||[] ).push( definition )
	}
	/**
	 * Hook to apply some preprocessing to Email headers before they the message is sent
	 * @method module:alice-core/lib/mail/message#processHeaders
	 * @return {Object} headers
	 **/
	, processHeaders: function(){
		return this.options.headers
	}

	/**
	 * generates html and text content for message to be sent
	 * @method module:alice-core/lib/mail/message#render
	 * @return {EmailContent}
	 **/
	, render: function render(){
		var txt, html;
		if( this.options.template ){
			joi.assert(this.options.template, joi.string().required(), "template must be a string")
		}
		html = this.options.html || this.options.template ? templates.render( this.options.template, this.options.data ) : ""

		return {
			text: this.options.text || stripTags( html )
		  , html:html
		};
	}
	/**
	 * Used interinally to identify custom data structures
	 * @private
	 * @method module:alice-core/lib/mail/message/base#$familiy
	 * @return {String} A string representation of class
	 **/
	,$family: function(){
		return 'email'
	}

});

Object.defineProperties(EmailMessage.prototype,{

	/**
	 * @name payload
	 * @memberof module:alice-core/lib/mail/message
	 * @property {Object} payload An object containing clened message data suitable for sending over a transport
	 **/
	payload:{
		get: function(){
			return this.toJSON();
		}
	}

	,subject:{
		get: function(){
			joi.assert(this.options.subject, joi.string().required(),'Subject is required')
			return this.options.subject		
		}
	}

	/**
	 * @name from
	 * @memberof module:alice-core/lib/mail/message
	 * @property {Object} returns the email address the from field will be populated with. defaults to {@link module:alice-core/conf.mail.defaultemail}
	 **/
	,from: {
		get: function( ){
			var from = this.options.from || conf.get('mail:defaultemail');
			joi.assert(from, joi.alternatives([
				joi.string().email().required(),
				joi.object({
					name:joi.string().required()
					,address:joi.string().email().required()
				})
			]), 'from must be a valid email address')
			return from
		}
	}
	/**
	 * @name sender
	 * @memberof module:alice-core/lib/mail/message
	 * @property {Object} payload An object containing clened message data suitable for sending over a transport
	 **/
	,sender:{
		get: function(){
			return this.options.sender || conf.get('mail:defaultsender');
		}
	}
	/**
	 * @name recipients
	 * @memberof module:alice-core/lib/mail/message
	 * @property {Array} recipients a complete list of addresses that will recieve this message
	 **/
	, recipients:{
		get: function(){
			var to, cc, bcc;
			return [].concat( this.to, this.cc, this.bcc )
		}
	}

	/**
	 * @name id
	 * @memberof module:alice-core/lib/mail/message.headers
	 * @property {String} id The Id of the current message. If not specified, will be auto generated
	 **/
	,id: {
		get: function(){
			return ( this.options.id = this.options.id || id() )
		}
	}

	/**
	 * @name bcc
	 * @memberof module:alice-core/lib/mail/message.headers
	 * @property {Array} bcc array of emails this message will be blined coppied on
	 **/
	,bcc:{
		get:function(){
			var bcc = this.options.bcc;
			if( !this._bcc || !this._bcc.length ){
				this._bcc = Array.isArray( bcc ) ? bcc : isString( bcc )  ? bcc.split(',').map(trim) : []
			}
			return this._bcc;
		}

		,set: function( address ) {
			var response = addressValidator.validate( address );
			if( response.error ){
				throw( response.error );
			}
			address = toArray( address );
			this._bcc = address.length ? this.bcc.concat( address ) : address;
			return address;
		}
	}

	/**
	 * @name cc
	 * @memberof module:alice-core/lib/mail/message.headers
	 * @property {Array} cc array of emails this message will be carbon copied on
	 **/
	,cc:{
		get:function(){
			var cc = this.options.cc;
			if( !this._cc || !this._cc.length ){
				this._cc = isArray( cc ) ? cc : isString( cc )  ? cc.split(',').map(trim) : []
			}
			return this._cc;
		}
		,set: function( address ) {
			var response = addressValidator.validate( address );
			if( response.error ){
				throw( response.error )
			}
			address = toArray( address );
			this._cc = address.length ? this.cc.concat( address ) : address;
			return address;
		}
	}

	/**
	 * @name to
	 * @memberof module:alice-core/lib/mail/message.headers
	 * @property {Array} to array of emails this message will be sent to directly
	 **/
	,to:{
		get: function(){
			var to = this.options.to;
			if( !this._to || !this._to.length ){
				this._to = isArray( to ) ? to : isString( to )  ? to.split(',').map(trim) : []
			}

			return this._to;
		}
		,set: function( address ) {
			var response = addressValidator.validate( address );
			if( response.error ){
				throw( response.error );
			}
			address = toArray( address );
			this._to = address.length ? this.to.concat( address ) : address;
			return address;
		}
	}
	/**
	 * @name headers
	 * @memberof module:alice-core/lib/mail/message.headers
	 * @property {Object} headers Object containing headers after initial processing
	 **/	
	,headers:{
		get: function(){
			return this.processHeaders();
		}
	}
});

module.exports = EmailMessage;
