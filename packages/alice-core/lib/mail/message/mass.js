/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * An interface for transforming messages in mass
 * @module alice-core/lib/mail/message/bulk
 * @author Eric satterwhite
 * @since 0.0.1
 * @requires alice-stdlib/class
 * @requires moduleB
 * @requires moduleC
 */

var Class = require( 'alice-stdlib/class' )
  , MassMail
  ;

/**
 * Description
 * @class module:bulk.js.Thing
 * @param {TYPE} param
 * @example var x = new bulk.js.THING();
 */

MassMail = new Class({
	
	/**
	 * This does something
	 * @param {TYPE} name description
	 * @param {TYPE} name description
	 * @param {TYPE} name description
	 * @returns something
	 **/
	method: function(){

		/**
		 * @name bulk.js.Thing#event
		 * @event
		 * @param {TYPE} name description
		 **/	
		this.emit('event', arg1, arg2)
	}
});
