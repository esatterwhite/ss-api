/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Message Type that will auto convert mandrill defined headers before being sent
 * @module alice-core/lib/mail/message/mandrill
 * @author Eric Satterwhite
 * @since 0.2.0
 * @requires alice-stdlib/class
 * @requires alice-stdlib/class/parent
 * @requires alice-stdlib/class/options
 * @requires alice-core/lib/mail/message/base
 */

var conf         = require('alice-conf')
  , Class        = require( 'alice-stdlib/class' )
  , EmailMessage = require('./base')
  , mandrillPrefix
  , mandrillHeaders
  ;

mandrillPrefix = 'X-MC-';
mandrillHeaders = {
	Track                   : true
  , Autotext                : true
  , AutoHtml                : true
  , Template                : true
  , MergeVars               : true
  , GoogleAnalytics         : true
  , GoogleAnalyticsCampaign : true
  , Metadata                : true
  , URLStripQS              : true
  , PreserveRecipients      : true
  , InlineCSS               : true
  , TrackingDomain          : true
  , SigningDomain           : true
  , Subaccount              : true
  , ViewContentLink         : true
  , BccAddress              : true
  , Important               : true
  , IpPool                  : true
  , ReturnPathDomain        : true
  , SendAt                  : true
};


/**
 * @constructor
 * @alias module:alice-core/lib/mail/message/mandrill
 * @param {Object} options
 */
EmailMessage = new Class({
	inherits: EmailMessage

	/**
	 * Hook to apply some preprocessing to Email headers before they the message is sent
	 * @method module:alice-core/lib/mail/message/mandrill#processHeaders
	 * @return {Object} headers
	 **/
	, processHeaders: function processHeaders( ){
		var headers = { }
		  , current
		  ;

		for( var header in this.options.headers ){
			current = mandrillHeaders[ header ] ? ( mandrillPrefix + header ) : header
			headers[ current ] = this.options.headers[ header ];
		}

		return headers;
	}
});

module.exports = EmailMessage
