/*jshint laxcomma: true, smarttabs: true, node: true*/
'use strict';
/**
 * A terminal email transport that just writes the message to STDOUT
 * @module alice-core/lib/mail/transports/mandrill
 * @author Eric satterwhite
 * @since 0.2.0
 * @requires util
 * @requires alice-core/lib/mail/transports/smtp
 */

var util    = require( 'util' )
  , os      = require( 'os' )
  , mailer  = require( 'nodemailer' )
  , Class   = require( 'alice-stdlib/class' )
  , logger  = require( 'alice-log' )
  , conf    = require( 'alice-conf' )
  , SMTP    = require( './smtp' )
  , Email   = require('../message/mandrill')
  , Mandrill
  , mandrillHeaders
  , mandrillPrefix
  , username
  , apikey
  , mailconf
  ;

apikey         = conf.get('MANDRILL_APIKEY');
username       = conf.get('MANDRILL_USERNAME');
mailconf       = conf.get('mail:mandrill');

/**
 * @constructor
 * @alias module:alice-core/lib/mail/transports/mandrill
 * @extends module:alice-core/lib/mail/transports/smtp
 */
Mandrill = new Class({
	
	inherits:SMTP
	
	,options:{
		port: mailconf.port || 25
	  , host: mailconf.host || 'localhost'
	  , name: 'Mandrill ( ' + os.hostname() + ' )' 
	  , authMethod: 'LOGIN'
	}

	, constructor: function( options ){
		this.parent('constructor', options )
		this.options.auth = this.options.auth || { user:username, pass:apikey };
	}

	, message: function message( ){
		return Email;
	}
});

module.exports = Mandrill;
