alice-core
==========
The Core package provides foundational functions, helpers and base line implementations to be extended and used by the rest of the **Alice** platform

## Helpers


### Loading

A module that provides the base line for finding and loading various resources into the framework. 

#### Fixtures

This fixture loader, can find fixture file for a specific package, all packages, the host project, or of course all availible fixtures. In addition to locating files, it can also require & return fixtures data.

Fixture lookups can be performed with the `find` method
```js
var fixtures = require('alice-core').loading.fixtures;

// project fixtures
fixtures.find('alice'); 
{
	alice:['/path/to/project/fixtures/data.json']
}

// project fixtures, and fixtures from alice-core 
fixtures.find('alice', 'core'); 
{
	alice:['/path/to/project/fixtures/data.json']
  , 'alice-core':[ '/path/to/project/packages/alice-core/fixtures/data.json' ] 
}
```

Fixture loading is done in a similar fashion with the `load` function
```js
// project fixtures
fixtures.find('alice'); 
{
	alice:[{ ... }, { ... }]
}

// project fixtures, and fixtures from alice-core 
fixtures.find('alice', 'core'); 
{
	alice:[{ ... }, { ... } ]
  , 'alice-core':[ { ... }, { ... }, { ... } ] 
}
```

## Commands

### Data Loading

The core package provides a couple management commands to dump data from your models to a fixture file as well import a fixture file to populate a data collection

#### import

import a fixture to populate a DB collection

#### export

export a DB collection through a model to a fixture. By default `export` will only print out the fixture. If you would like it in a file, pass the `output` flag to the command or pipe the output to a file of your choosing