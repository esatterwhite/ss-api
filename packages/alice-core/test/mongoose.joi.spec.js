var joi = require('joi')
var assert = require('assert')
var mongoose = require('mongoose')
var Schema = mongoose.Schema
var toJoi = require('alice-core/lib/db/schema/toJoi')
describe('tojoi', function( ){

	describe('from_string', function(){
		var RequiredSchema = new Schema({
			name:{type:String, required:true}
		});

		var ValidationSchema = new Schema({
			name:{type:String, required:false, validate:/(foo|bar)/}
		});

		var EnumSchema = new Schema({
			name:{type:String, required:true}
			,list:{type:String, enum:['a','b','c']}
		})
		var R = mongoose.model('1', RequiredSchema )
		var V = mongoose.model('2', ValidationSchema )
		var E = mongoose.model('3', EnumSchema )

		it('should honor the required flag', function(){
			var r = new R();
			var validator = toJoi( RequiredSchema );

			isValid = validator.validate( r.toObject() );
			assert.equal( isValid.error.name, "ValidationError" );

			r.name = 'fake';
			isValid = validator.validate( r.toObject() );
			assert.equal( isValid.error, null );
			assert.equal( isValid.value.name, 'fake' );

		});
		it('should enforce enum values', function(){
			var e = new E({
				name:'fake'
			});
			var validator = toJoi( EnumSchema );
			isValid = validator.validate( e.toObject() );

			assert.equal( isValid.error, null )

			e.list = 'a';
			isValid = validator.validate( e.toObject() );
			assert.equal( isValid.error, null );

			e.list = 't';
			isValid = validator.validate( e.toObject() );
			assert.equal( isValid.error.name, 'ValidationError' );
		});
		it('should honor regex validators', function( ) {
			var v = new V();

			var validator = toJoi( ValidationSchema );

			isValid = validator.validate( v.toObject() );
			assert.equal( isValid.error, null );

			v.name = 'foo';
			isValid = validator.validate( v.toObject() );
			assert.equal( isValid.error, null );
			assert.equal( isValid.value.name, 'foo' );

			v.name = 'bar';
			isValid = validator.validate( v.toObject() );
			assert.equal( isValid.error, null );
			assert.equal( isValid.value.name, 'bar' );

			v.name = 'invalid';
			isValid = validator.validate( v.toObject() );
			assert.equal( isValid.error.name, "ValidationError" );
			assert.equal( isValid.error.details[0].path, 'name' );
		});
	});

	describe('from_number', function(){
		var NumberSchema = new Schema({
			value:{type:Number, min:0, default:0}
			,price:{type:Number, max:10}
		});
		var N = mongoose.model('4', NumberSchema )
		it('should honor min/max options', function(){
			var n = new N();
			var validator = toJoi( N.schema );

			isValid = validator.validate( n.toObject());

			assert.equal( isValid.error, null )
			assert.equal( isValid.value.value, 0 )

			n.value = -1;
			isValid = validator.validate( n.toObject() );
			assert.equal( isValid.error.name, 'ValidationError')
			assert.equal( isValid.error.details[0].path, 'value' )

			n.value = 1;
			n.price = 10;
			isValid = validator.validate( n.toObject() );
			assert.equal( isValid.error, null );
			assert.equal( isValid.value.value, 1 );
			assert.equal( isValid.value.price, 10 );

			n.price = 11;
			isValid = validator.validate( n.toObject() );
			// assert.equal( isValid.error.name = "ValidationError" );
			// assert.equal( isValid.value, null )

			isValid = validator.validate( {price:'11'} );
			assert.equal( isValid.error.name, "ValidationError")
		})
	});
	
	describe('nested objects', function(){
		var NestedSchema = new Schema({
			name:{type:String}
			,obj:{
				x:{type:Number, min:5, max:10, default:6}
				,y:{type:String, required:true}
				,z:{type:Number}
			}
		});

		var NN = mongoose.model('5', NestedSchema )
		it('should should validate nested documents', function(){
			var nest = new NN({
				name:'abc'
				,obj:{
					z:1,x:1
				}
			});

			var validator = toJoi( NN.schema );
			var isValid = validator.options({abortEarly:false}).validate( nest.toObject() )
			assert.equal( isValid.error.details.length, 2 )
			var errors = isValid.error.details	
										.map(function( detail ){
											return detail.path
										})
										.sort()

			assert.equal( errors[0], 'obj.x')
			assert.equal( errors[1], 'obj.y')
		})
	})
})
