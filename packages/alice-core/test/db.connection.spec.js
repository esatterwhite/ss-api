var assert      = require('assert')
  , path        = require('path')
  , os          = require('os')
  , util        = require('util')
  , Connection = require('../lib/db/connection');


describe('db', function(){


	describe('Connection', function(){
		beforeEach(function( done ){

			done();
		});

		afterEach(function( done ){
			done();
		});

		it( 'should accept a connection string', function( done ){
			var conn = new Connection({
				default:'list'
				,databases:{
					"list":'mongodb://localhost:27017/list'
				}
				,'onDb:connection': function(name, conn ){
					assert.equal( name, 'list' );
					done()
				}
			});
			assert.ok( true );
		});

		it( 'should accept a connection object', function( done ){
			var conn = new Connection({
				default:'fake'
				,databases:{
					"fake":{
						driver:'mongodb'
						,host:'localhost'
						,port:27017
					}
				}
				,'onFake:connected': function(){
					done()
				}
				,'onDb:connection': function(name, conn ){
					assert.equal( name, 'fake' );
				}
			});
			assert.ok( true );
		});


	})

})
