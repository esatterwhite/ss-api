var assert     = require('assert')
var Mail       = require('../mail')
var MailParser = require('mailparser').MailParser
var async      = require('async')
var simple     = require('simplesmtp')
var SMTP       = require("alice-core/lib/mail/transports/smtp")

describe('alice-core/mail', function(){
	describe('SMTP', function(){
		var server, transport, inbox
		beforeEach(function( done ){
			Mail('default').outbox = null;
			server = simple.createServer({
				ignoreTLS:true
				,debug:false
				,requireAuthentication:false
				,disableSTARTTLS:true
				,disableDNSValidation:true
			});
			transport = new SMTP({
				port:3333
				, debug:false
			})
			inbox = []
			server.listen( 3333, done )
		});

		afterEach(function( done ){
			server.removeAllListeners()
			server.end(console.log );
			inbox = null;
			done()
		})

		describe("#send", function(){
			it('it should send individual messages objects', function( done ){
				server.on('startData', function( conn ){
					conn.data =""
				})
				
				server.on('data', function( conn, chunk ){
					conn.data += chunk.toString('utf8');
				});

				server.on('dataReady', function( conn, callback ){
					inbox.push( conn.data )
					callback( null )
				})

				transport.send(
					{to:'a@mail.com', from:'b@mail.com', text:'hello world', 'subject':'test mail' }
					,function( err ){
						var msg = inbox[0];
						var parser = new MailParser()
						parser.on('end', function( mail ){
							assert.equal(mail.text,'hello world')
							assert.equal(mail.subject, 'test mail')
							done();
						})

						parser.write( msg );
						parser.end();
					}
				)
			});

			it('it should send individual full Message Instances', function( done ){
				server.on('startData', function( conn ){
					conn.data =""
				})
				
				server.on('data', function( conn, chunk ){
					conn.data += chunk.toString('utf8');
				});

				server.on('dataReady', function( conn, callback ){
					inbox.push( conn.data )
					callback( null )
				})

				transport.send(
					new Mail.Message( {to:'a@mail.com', from:'message@mail.com', text:'hello world', 'subject':'test Message' } )
					,function( err ){
						var msg = inbox[0]
						var parser = new MailParser()
						
						parser.on('end', function( mail ){
							assert.equal(mail.text,'hello world')
							assert.equal(mail.subject, 'test Message')
							assert.equal( mail.from[0].address, "message@mail.com")
							done();
						})

						parser.write( msg );
						parser.end();
					}
				)
			});

			it('it should send multiple message objects', function( done ){
				server.on('startData', function( conn ){
					conn.data ="";
				});
				
				server.on('data', function( conn, chunk ){
					conn.data += chunk.toString('utf8');
				});

				server.on('dataReady', function( conn, callback ){
					inbox.push( conn.data );
					callback( null );
				});

				transport.send(
					{to:'testone@mail.com', from:'senderone@mail.com', text:'I am number one', 'subject':'test1 mail' },
					{to:'testtwo@mail.com', from:'sendertwo@mail.com', text:'I am number two', 'subject':'test2 mail' }
					,function( err ){
						var msgone = inbox[0]
						var msgtwo = inbox[1]
						var parserone = new MailParser()
						var parsertwo = new MailParser()

						async.parallel([
							function( cb ){
								parserone.on('end', function( mail ){
									cb( null, mail  )
								});
								parserone.write( msgone );
								parserone.end();
							}
							,function(cb ){
								parsertwo.on('end', function( mail ){
									cb( null, mail )
								});
								parsertwo.write( msgtwo );
								parsertwo.end();
							}
						], function( err, results ){

							// these are run in parallel and asycronously.
							// order may be off
							var mail_1 = results[0].subject == 'test1 mail' ? results[0] : results[1];
							var mail_2 = results[1].subject == 'test2 mail' ? results[1] : results[0];

							assert.equal(mail_1.text,'I am number one');
							assert.equal(mail_1.subject, 'test1 mail');

							assert.equal(mail_2.text,'I am number two');
							assert.equal(mail_2.subject, 'test2 mail');
							done();
						});

					}
				)
			});

		});
	})
})
