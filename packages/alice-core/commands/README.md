## Commands


## Fixture handling

Fixtures represent a static data set that needs to make it way in to the application data store

### Import

You can use a fixture to load data into the application data store

### Export

You Export specific data from the data store into a fixture for easy portability and reloading