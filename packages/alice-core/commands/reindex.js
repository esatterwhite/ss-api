/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * @module alice-core/commands/reindex
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires path
 * @requires os
 * @requires child_process
 * @requires seeli
 * @requires elasticsearchclient
 */

 var cli           = require( 'seeli' )                           // npm seel instance for creating commands
   , os            = require( 'os' )                              // node os for windows checks. cause windows
   , util          = require( 'util' )                            // node util module for string formatting
   , debug         = require( 'debug' )('alice:commands:reindex') // internal debugging for command
   , async         = require( 'async' )                           // npm module async
   , ESClient      = require( 'elasticsearchclient' )             // client for elastic search
   , Reindex                                                      // command instance
   ;


/**
 * Starts up the API server in `harmony` mode with the env configured so you don't have to remember
 * @alias module:alice-core/commands/reindex
 * @name reindex
 * @property {Object} flags Command line flags
 * @property {Boolean} [flags.drop] drop the existing index if it exists
 * @property {Boolean} [flags.confirm] confirm that you really want to drop the existing index
 * @property {Number} [flags.batch=1000] The number of documents to index per iteration
 * @property {String} flags.host Host of the remote search instance to connect to
 * @property {Number|String} flags.port Port number of the remote search instance to connect to
 * @property {String} [flags.username] Username to authenticate with against the remote search instance
 * @property {String} [flags.password] password to authenticate with against the remote search instance
 * @example
alice reindex Tenant --drop --confirm --host=localhost --port=9200
 */
module.exports = Reindex = new cli.Command({
	description:'Command to manage Elastic Search Index'
	,usage:[
		cli.bold('Usage: ') + 'alice reindex --help',
		cli.bold('Usage: ') + 'alice reindex -i',
		cli.bold('Usage: ') + 'alice reindex <MODEL> --drop --confirm --host=localhost --port=9200'
	]

	,flags:{
		drop:{
			shorthand:'d'
			,type:Boolean
		}

		,confirm:{
			type:Boolean
			,description:'Are you sure you want to drop the current index'
			,when: function( data ){
				return !!data.drop;
			}
		}
		,batch:{
			type:Number
			,default:1000
			,description:'Number of document to index at a time'
		}
		,host:{
			'type':String
			,description:'Remote host of elastic search instance'
			,required:true
		}

		,port:{
			type:Number
			,description:'port number of remote elastic search instance'
			,required:true
		}

		,username:{
			type:String
			,description:'username used to authenticate against the remote search instance'
			,required:false
		}

		,password: {
			type:String
			,description:'password used to authenticate against the remote search instance'
			,required:false
			,when: function( data ){
				return !!data.username;
			}
		}
	}
	,run: function run( directive, data, done ){
		var sending   // Currnt number of document being indexed
		  , processed // Number of documents sucessfully indexed
		  , errors    // Count of errors encounted
		  , Model     // The Model we are reading data from
		  , db        // The name of the database to find models in
		  , models    // core library for loading models
		  , exit_id   // id for timer to exit process
		  , client    // elastic search client
		  ;

		sending   = 0;
		processed = 0;
		errors    = 0;
		db        = require('alice-core/lib/db').connection;
		models    = require('alice-core/lib/loading/models');
		exit_id   = null;

		client = new ESClient({
			host:data.host
			,port:data.port
			,auth: (data.username && data.password) ? {username:data.username, password:data.password} : null
		});

		// load data models
		models.load();


		debug('batch size %d', data.batch );
		debug('connecting to %s:%s', data.host, data.port );
		Model = db['default'].models[ directive ];
		
		debug('model %s ', Model.modelName);
		async.series([
			function( cb ){
				if( data.drop && data.confirm ){
					if( typeof Model.index === 'function' ){
						debug('Droping index', Model.index());
						var type = typeof Model.index === 'function' ?  Model.type() : Model.modelName.toLowerCase();
						return client.deleteByQuery(Model.index(), type ,{'query':{'match_all':{}}}, cb );
					}

					done( new Error(util.format('Model %s has no method named index', Model.modelName) ), null );
					return process.exit(1);
				}
				cb( null );
			}

		], function( error ){
			var stream // Model query stream
			  , cargo  // async cargo runner instance
			  ;

			cargo = async.cargo( function( tasks, callback ){
				var action = (data.drop && data.confirm)  ? 'index' : 'update'
				  , cmds = [] // bulk command list to send to Elastic search
				  , last = 0  // the last number of documents that was indexed
				  ;

				tasks.forEach(function(t){
					var cmd = {} // command object to send to elastic search
					  ;

					cmd[action]= {
						_index:Model.index()
						,_type:'tenant'
						,_id:t.id
					};
					cmds.push(cmd, t);
				});


				client.bulk( cmds, function( err, data ){
					if( err ){
						console.log( err );
					}else{
						var indexed = JSON.parse( data ).items.length;
						processed += indexed;
						debug('%s %s documents ', action, processed );
					}
					callback();
				});
				
				last = sending;
				sending += ( cmds.length / 2);
				debug('indxing batch %d - %d', last, sending  );
			}, data.batch );


			// start the stream
			stream = Model.find().stream();

			// start pumping documents
			stream.on('data', function( doc ){
				doc = doc.toObject({virtuals:true, getters:true});
				doc.indexed = new Date().toISOString();
				doc.model = Model.Modelname;
				cargo.push( doc );
			});


			stream.on('close', function(){
				debug('Model Stream exhausted');
				// when all of worker functions have  returned
				cargo.drain = function(){
					clearTimeout( exit_id );
					console.log('waiting for index to flush...');
					exit_id = setTimeout(function(){
						done(null, ['Index rebuilt', util.format('%s documents indexed', processed) ].join( os.EOL ) );
						process.exit(0);
					}, 10000);
				};
			});
		});
	}
});
