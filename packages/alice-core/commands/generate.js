/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * Loads configuration form different data stores before the server starts
 * @module alice-core/commands/generate
 * @author Eric Satterwhite
 * @since 0.0.0
 * @requires seeli
 * @requires path
 * @requires util
 * @requires yeoman-generator
 */

var cli       = require( 'seeli' )
  , path      = require('path')
  , util      = require('util')
  , yeoman    = require("yeoman-generator")
  , generator = require('../../../lib/generators/generator-alice')
  , env       
  ;

env = yeoman(null,{
	base:path.resolve(__dirname, '..', '..', '..', "lib" ,"generators", "generator-alice")
} , new generator.Adapter);

module.exports = new cli.Command({
	description:"Generates a new package for the Alice platform"
	,usage:[
		cli.bold("Usage: " ) + " alice generate --name=core --description='this package is great'"
		,cli.bold('Usage: ') + 'alice generate -i'
	]

	,flags:{
		'name':{
			type:String
			,required:true
			,description:util.format( "The name of your package ( %s )", cli.bold( 'payment' ) )
		}

		,'description':{
			type:String
			,required:true
			,description:"The general purpose of your  package"
		}

		, 'conf':{
			type:Boolean
		  , default:true
		  , description:"Generate a starter configuration module"
		}
		, 'lib':{
			type:Boolean
			, default:true
			, description:"Generate package libraries"
		}
		, 'commands':{
			type:Boolean
			, default:true
			, description:"Generate A starter Command"
		}
		, 'models':{
			type:Boolean
		  , default:true
		  , description:"Generate a starter models module"
		}
		, 'test':{
			type:Boolean
			, default:true
			,description:"Generate a test spec"
		}
		, 'resources':{
			type:Boolean
			,default: true
			, description:"Generate API resources directory"
		}
		, 'events':{
			type:Boolean
			, default:true
			,description:"Generate package events"
		}
		, 'scripts':{
			type:Boolean
			, default:true
			, description:"Generate scripts directory"
		}
	}
	,run: function(cmd, data, done  ){
		env.registerStub(generator,'generator:alice')
		
		var gen = env.create('generator:alice', data);
		gen.seeli_data = data;
		gen.run( done.bind( null, null, '' ) )
	}
})
