/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Generates documentation for the Platform
 * ###### Flags
 * - conf ( `string` ) path to a valid jsdoc conf file
 * - rm ( `string` ) path to documentation directory to remove before regenerating

 * @module alice-core/commands/docs
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires seeli
 * @requires path
 * @requires os
 * @requires child_process
 * @requires util
 * @requires debug
 */
var cli   = require( 'seeli' )
  , fs    = require('fs')
  , os    = require('os')
  , path  = require('path')
  , spawn = require( 'child_process' ).spawn
  , util  = require('util')
  , debug = require('debug')('alice:commands:docs')
  , rm    = require("../../../scripts/rimraf")
  ;
var top = path.resolve( __dirname, '..', '..', '..'  );

/**
 * auto generages API documentation for the code base using JSDoc
 * @instance
 * @name docs
 * @memberof module:alice-core~commands
 * @property {Object} flags Command line flags
 * @property {String} [flags.conf=doc/conf.json]
 * @property {String} [flags.rm=null]
 */
module.exports = new cli.Command({
	interactive: false
	,description:"Generates HTML docmentation from the Alice source code"
	,usage:[
		cli.bold("Usage: ") + 'alice docs',
		cli.bold("Usage: ") + 'alice docs --rm=./path/to/directory'
	]

	,flags:{

		conf:{
			type:String
			,required:false
			,descripts:'path to a valid jsdoc configuration json file'
			,default:path.resolve( top, 'doc', 'conf.json' )
		}

		,rm:{
			type:String
			,required:false
			,description:'A path to a directory to remove before generating docs'
		}

	}
	,run: function( cmd, options, done ){
		var proc
		  , execPath = process.execPath

		debug('execution path %s', execPath );
		debug('reading %s', options.conf );

		if( options.rm ){
			debug( 'removing %s', path.resolve( options.rm ) );
			rm.sync( path.resolve( options.rm ) );

		}
		proc = spawn(execPath, ['node_modules/.bin/jsdoc', '-c', options.conf ],{
				cwd: top
			}
		);

		proc.stdout.pipe( process.stdout );
		proc.stderr.pipe( process.stderr );

		proc.on('close', function( code ){
			done( null, os.EOL + "all done " + code );
		})
	}
})
