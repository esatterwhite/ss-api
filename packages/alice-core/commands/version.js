/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * displays version information about the current host installation
 * @module alice-core/commands/version
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires seeli
 * @requires semver
 * @requires cli-table
 * @requires os
 * @requires fs
 * @requires path
 * @requires util
 */

var cli    = require( 'seeli' )               // npm seeli module for creating commands
  , semver = require( 'semver' )              // npm semver module for parsing version information
  , Table  = require( 'cli-table' )           // npm cli-table module for pretty printing
  , os     = require('os')                    // node os module for X-platform line breaks
  , fs     = require('fs')                    // node fs module for reading files
  , path   = require('path')                  // node path module for resolving paths
  , util   = require('util')                  // node util module for string formatting
  , pkg    = require('../../../package.json') // alice package.json file
  ;

/**
 * Renders version information for installed packages
 * @instance
 * @name version
 * @memberof module:alice-core~commands
 * @property {Boolean} [flags.table=true] if true, pretty prints the version information in a table
*/
module.exports = new cli.Command(/* @lends module.THING.prototype */{
	description:'Displays version information of the current installation'
  , interactive: false
  , usage:[
	cli.bold('Usage: ')	+ 'alice version',
	cli.bold('Usage: ')	+ 'alice version --help',
	cli.bold('Usage: ')	+ 'alice version --no-color',
	cli.bold('Usage: ')	+ 'alice version --no-table',
  ]
  , flags:{
		table:{
			type:Boolean
		  , shorthand: 't'
		  , default:true
		  , description: 'outputs version information in unicode table'
		}
  }
  , run: function( cmd, data, done ){
		var apps
		  , alice
		  , basepath
		  , table
		  , that
		  , out;

		that = this 
		apps = []
		alice = semver.parse( pkg.version )
		basepath = path.join( __dirname, '..','..')
		table = new Table({
			head:['', 'Major', 'Minor', 'Patch', 'Prerelease']
		  , style:{head:['cyan', 'bold']}
		  , chars: { 'top': '═' , 'top-mid': '╤' , 'top-left': '╔' , 'top-right': '╗'
		  , 'bottom': '═' , 'bottom-mid': '╧' , 'bottom-left': '╚' , 'bottom-right': '╝'
		  , 'left': '║' , 'left-mid': '╟' , 'mid': '─' , 'mid-mid': '┼'
		  , 'right': '║' , 'right-mid': '╢' , 'middle': '│' }
		});


		apps.push( data.table ? { 'Alice': [ alice.major, alice.minor, alice.patch, alice.prerelease ]  } : util.format('%s@%s', pkg.name, pkg.version))

		// read everything in the packages directory
		fs.readdir( basepath  , function( err, files ){
			// for every file
			files.forEach( function( file ){
				var packagepath = path.join(basepath, file, 'package.json' )
				var app = {};
				var pack;
				var version;

				// if their is a package.json file
				// read it and add it to the list of apps
				if( fs.existsSync(  packagepath ) ){
					pack =  require( packagepath );
					try{
						if( !data.table ){
							apps.push( util.format('%s@%s', pack.name, pack.version) );
						} else {
							version = semver.parse( pack.version );
							app[ pack.name ] = [ 
								version.major
							  , version.minor
							  , version.patch
							  , version.prerelease
							];
							
							apps.push( app );
							
						}
						
					} catch( e ){
						return that.emit('error', e )
					}
				}
			});

			if( data.table ){
				// push everything into the table
				// and spit it out
				table.push.apply( table, apps );
				out = table.toString();
			} else{
				out = apps.join(os.EOL );
			}
			done( null,  out );
		})
	}

});
