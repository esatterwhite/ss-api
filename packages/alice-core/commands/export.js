/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Exports data associated to a Data model in a portable JSON format
 * @module module:alice-core/commands/export
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires seeli
 * @requires fs
 * @requires os
 * @requires path
 * @requires progress
 * @requires util
 * @requires debug
 */

var cli      = require( 'seeli' )                          // npm seeli module for creating commands
  , fs       = require( 'fs' )                             // node fs for reading files
  , os       = require( 'os' )                             // node os module for x platform line breaks
  , path     = require( 'path' )                           // node path module for resolving absolute paths
  , progress = require( 'progress' )                       // npm progress for displaying progress updates
  , util     = require( 'util' )                           // node util module for string formatting
  , debug    = require( 'debug' )('alice:commands:export') // npm debug for silent debug information

  ;



/**
 * Exports a data for a specific model to stdout or a file
 * @instance
 * @memberof module:alice-core~commands
 * @property {Object} flags Command line flags
 */

module.exports = new cli.Command({
	description:'Exports a database table for easy import. '
	,usage:[
		cli.bold('Usage: ') + 'alice export product --index=2 > products.json'
		,cli.bold('Usage: ') + 'alice export product --location=products.json'
		,cli.bold('Usage: ') + 'alice export product --location=products'
	]
	,flags: {
		indent:{
			type:Number
			,description: 'Indentation level of the output'
			,shorthand:'i'
		}
		,initial:{
			type: Boolean
			,description: 'make output file an initial data fixture'
			,default:false
		}
		,database:{
			type:String
			,description: 'The database to export Data from'
			,default:'default'
		}
		,output:{
			shorthand:'o'
			,description:"Path to place the output. If not specified, will use stdout"
			,type:String
		}
		,sort:{
			shorthand:'s'
			,type:String
			,description:'A valid field path to sort by'
		}
	}
	,run: function( model, data, done ){
		var Model
		  , query
		  , stream
		  , bar
		  , conf     = require('alice-conf')
		  , PACKAGE_PATH = conf.get('PACKAGE_PATH')
		  , FIXTURE_PATH = conf.get('FIXTURE_PATH')
		  , PROJECT_ROOT = conf.get('PROJECT_ROOT')
		  
		var models = require('alice-core/lib/loading/models')
		var db = require("alice-core/lib/db").connection
		models.load();
		conf = require("alice-conf")

		Model = db[data.database].models[ model ];

		Model.count(function( err, MAX ){
			var writes, hearder;

			writes = 0

			function onEnd(){
				stream.write('] }')
				debug( data.ouput )
				if( !data.output ){
					done( null, '' );
					process.exit( 0 );	
				}
				return !!data.output && stream.end();
			}

			if( data.output ){
				var alicecheck = /^alice/
				var bits = data.output.split('.')
				var file_ext = /\.([\w+])$/
				var app = bits.length == 1 ? "" : bits[0]
				app = alicecheck.test( app ) ? app : 'alice-' + app;

				var file = app ? bits[1] : bits[0]
				var location = path.join(  ( app ? PACKAGE_PATH : PROJECT_ROOT ) , app, FIXTURE_PATH, ( data.initial ? ( 'initial_' + file ) : file) )
				debug('output specified %s', data.output)
				debug('application selected  %s', app )
				location.replace(file_ext, '')
				location += '.json'
				debug("file location %s", location)
				stream = fs.createWriteStream(location,{
					'encoding':'utf8'
					,flags:'w'
				});
				bar = new progress('progress [:bar] :current/:total (:percent)', {
					total:MAX
					,width:600
					,callback:onEnd
				});

			} else {
				debug("no output location specified ")
				stream = process.stdout
				bar ={
					tick:function(){
						if( writes >= MAX ){
							onEnd()
						}
					}
				}
			}
			// file header
			hearder = [
				"{"
				,'"driver":"mongoose"'
				,util.format(',"exported":%d', ( + new Date() ) )
				,',"pk":"_id"'
				,util.format(',"model":"%s"', Model.modelName )
				,util.format(',"collection":"%s"', Model.collection.name)
				,',"data": [\n'
			].join( os.EOL )

			// write the header
			stream.write(hearder);

			if( !MAX ){
				onEnd();
			
			}
			// start the stream
			query = Model.find()

			if( data.sort ){
				var sortopt = {}
				sortopt[data.sort] = 1
				query = query.sort( sortopt );
			}

			query = query.stream()
			
			stream.on('finish', function(){
				// kill the process when done
				done( null, '' );
				process.exit( 0 );
			});

			query.on('data', function( doc ){
				writes++;
				stream.write('    ')
				doc = doc.toObject({vituals:true});
				doc.model = Model.modelName
				stream.write( JSON.stringify(doc, null, data.indent ? Math.abs( data.indent ) : 0 ) )
				stream.write( writes != MAX ? ',' :'' )
				stream.write(  os.EOL  )
				bar.tick()
			});
		})
	}
});
