/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Pre configured REPL for Alice. Auto loads schemas and env configuration
 * ###### Flags
 * - conf ( `boolean` ) auto loads the conf module into the main context

 * @module alice-core/commands/repl
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires seeli
 * @requires repl
 * @requires os
 * @requires fs
 * @requires path
 * @requires util
 * @requires alice-conf
 */

var cli           = require('seeli')
  , repl          = require('repl')
  , art           = require('ascii-art')
  , os            = require('os')
  , fs            = require('fs')
  , path          = require('path')
  , util          = require('util')
  , path          = require('path')
  , basepath      = path.resolve( __dirname, '..','..', '..' )


/**
 * instance of selli.Command that excutes a REPL shell with a pre configured context to work in
 * @instance
 * @name repl
 * @memberof module:alice-core~commands
 * @property {Object} flags Command line flags
 * @property {String} [flags.conf=true] auto load the {@link module:alice-conf|conf} module as `conf` and the entire config object as `env`
 * @property {String} [flags.rm=null]
*/
module.exports = new cli.Command(/* @lends module:alice-core/commands/repl.prototype */{
	description:'Starts up a Node.js Repl with current configuration and data models loaded '
	,usage:[
		util.format('%s %s', cli.bold( 'Usagee:'),  'alice repl' )
	  , util.format('%s %s', cli.bold( 'Usagee:'),  'alice repl --no-color' )
	  , util.format('%s %s', cli.bold( 'Usagee:'),  'alice repl --traceback' )
	]
	,flags:{
		conf:{
			type:Boolean
			,default: true
			,description: 'Loads config loader and exposes default setup'
		}
	}
	,run: function(cmd, data, done ){

		// force the process to the root directory
		process.chdir( basepath )

		// node vm will catch empty lines as errors 
		// if the domain is enabled. lame
		process.domain = null
		
		art.Figlet.fontPath = path.resolve(__dirname, '..', './font' ) + path.sep
		art.font('Alice 2.0', 'standard',function( r ){

			var schemapath
			  , models = require( 'alice-core/lib/loading/models')
			  , shell
			  , schemas

			shell      = repl.start('> ');

			var loaded = models.load()
			for( var app in  loaded){
				loaded[app].forEach(function( model ){
					if( !model.modelName ){
						console.log( cli.red("broken model in  %s ? " ),app,  model )
					} else{
						shell.context[ model.modelName ] = model 
					}
				})
			}

			if( data.conf ){
				try{
					console.log(
						util.format(cli.yellow("loading configuration"))
					)
					shell.context.conf = require('alice-conf')
					shell.context.env = shell.context.conf.get();
				} catch( e ) {
					process.stdout.write(
						cli.red('unable to load configuration')
						, e.message 
					);
				}
			}
			process.stdout.write( cli.magenta( r ) );
			done(null,'');
		});
	}
})
