/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * Starts the API server in `harmony` mode with NODE_PATH configured and passes argv through to the config module
 * @depricated
 * - use the web command instead
 * @module alice-core/commands/serve
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires seeli
 * @requires path
 * @requires os
 * @requires child_process
 * @requires module:mout/lang/clone
 */

 var cli           = require( 'seeli' )         // npm seel instance for creating commands
   , path          = require( 'path' )          // node path for resolving file locations
   , os            = require( 'os' )            // node os for windows checks. cause windows
   , clone         = require('mout/lang/clone') // mout clone function
   , child_process = require('child_process')   // node child_process for spawning an new pre-configured node process 
   , util          = require('util')
   , Serve
   ;


/**
 * Starts up the API server in `harmony` mode with the env configured so you don't have to remember
 * @instance
 * @name serve
 * @depricated
 * @memberof module:alice-core~commands
 * @property {Object} flags Command line flags
 * @property {!Number} [PORT=3001] The port to run the server on
 * @property {String[]} [logger] The types of log transports to start the server with - `stdout`, `file`
 */
module.exports = Serve = new cli.Command({
	description:'Starts the API server with the NODE_PATH resolved, passing and CLI flags you specify'
	,usage:[
		cli.bold('Usage: ') + 'alice serve',
		cli.bold('Usage: ') + 'alice serve --help',
		cli.bold('Usage: ') + 'alice serve -p 4400 -l stdout --log:stdout:colorize=1'
	]
	,flags:{
		PORT:{
			shorthand:'p'
			, type: Number
			, default:3001
			, description:'port to run the server on'
		}
		,logger:{
			type:[String, Array]
			, default:null
			, description:'The logger types to configure the server to use'

		}
	}

	,run: function run( cmd, data, done ){
		done( null
			, util.format(
				'%s: use %s instead'
				,cli.yellow( 'Depricated')
				,cli.bold( 'alice web' )
			) 
		)
	}
});
