/**
 * Created by ss-dev-01 on 6/10/2014.
 */
'use strict';

var mongoose = require('mongoose'),
    SpiritShopSchema = require('alice-core/lib/db/models').auditable,
    connection  = require('alice-core/lib/db').connection;

var promotionSchema = new SpiritShopSchema({
    name:{type:String, required:true},
    message:{type:String},
    discountType:{type:String, enum:['percent', 'fixed']},
    discountValue: {type:Number},
    linkUrl:{type:String},
    iconUrl:{type:String, required:true, default:'http://s7.spiritshop.com/defaultActivityIcon'},
    expiry:{type:Date},
    useType:{type:String, enum:['one-time', 'type', 'global']},
    placements:{type:String, enum:['top-nav', 'widget', 'mega-menu', 'side-bar']},
    tags:[String]


});

module.exports = connection.commerce.model('Discount', promotionSchema);
