'use strict';
var mongoose = require('mongoose'),
    Schema =require('alice-core/lib/db/models').auditable,
    ObjectId = require('mongoose').Schema.ObjectId,
    historySchema = new Schema({
        database:String,
        col:String,
        doc:ObjectId,
        operation:{type:String, enum:['create','update','delete']},
        author:String,
        auditDate:Date,
        audit:[{
            property:String,
            oldValue:String,
            newValue:String
        }]
    });


module.exports = mongoose.model('History', historySchema);
