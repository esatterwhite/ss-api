/*
 * Created by Ryan Fisch on 4/12/2014.
 */
'use strict';

var mongoose = require('mongoose')
    , AuditiableSchema = require('alice-core/lib/db/models').auditable
    , connection  = require('alice-core/lib/db').connection
    ;

/* Schema */
var addressSchema = new AuditiableSchema({
    addressType: {type: String, enum: ['home', 'work'], required: true},
    addressLine1: {type: String, required: true},
    addressLine2: String,
    city: {type: String, required: true},
    state: {type: String, lowercase: true, trim: true },
    postalCode: String
});

module.exports = connection.commerce.model('Address', addressSchema);
