/*jshint laxcomma: true, smarttabs: true, node: true*/
'use strict';
/**
 * events handlers for cart related events.
 * @module alice-cart/listeners
 * @author Eric Satterwhite
 * @since 0.2.0
 * @requires alice-conf
 * @requires alice-core
 * @requires alice-log
 */

var util       = require( 'util' )
  , joi        = require( 'joi' )
  , validators = require( 'alice-core/lib/db/schema/validators/defaults' )
  , mail       = require('alice-core').mail
  , logger     = require('alice-log')
  ;

// This should trigger an error email to be sent.
exports.get_error = {

	method:'GET'
  , path:'/check/mail'
  ,handler: function(request, reply){
  	reply( new Error("Mail Test"))
  }
};


exports.post_list = {
	method:'post'
	,path:'/core/email'
	,config:{
		tags:['api']
		,description:'Dumb proxy endpoint that Sends an email '
		,validate:{
			query: validators.query.clone()
			,payload:joi.object({
				to:joi.string().email().required()
				,from:joi.string().email().required()
				,subject:joi.string().required()
				,message:joi.string().required()
				,text:joi.string()
			}).rename('message', 'text',{alias:true})
		}
		,handler: function( request, reply ){
			logger.warning("Depricated. Email tasks are prefered")
			mail.send( request.payload, function(e){
				reply( e )
			});
		}
	}
}
