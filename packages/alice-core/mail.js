/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Module for creating and sending Email messages
 * @module alice-core/mail
 * @author Eric Satterwhite
 * @since 0.2.0
 */

module.exports = require('./lib/mail')
