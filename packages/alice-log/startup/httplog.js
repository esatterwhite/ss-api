/*jshint laxcomma: true, node: true, smarttabs: true*/
'use strict';
/**
 * Enables request logging though standard logger
 * @module alice-log/boot/httplog
 * @author Eric satterwhite
 * @since 0.1.0
 * @requires alice-stdlib/date
 * @requires url
 * @requires alice-log
 */

var logger = require( '../index' )
  , date         = require("alice-stdlib/date")
  , url          = require("url")


exports.register = function( server, options, next ){
	// sets some time stamps when the request was started
	server.ext('onRequest', function(request, reply) {
		request.startAt = process.hrtime()
		request.startTime = new Date();
		reply.continue();
	});

	server.ext('onPreResponse', function(req, reply) {
		var diff = process.hrtime(req.startAt);
		var ms = diff[0] * 1e3 + diff[1] * 1e-6;
		logger.http(
			'%s [%s] %s %s HTTP/%s %s ( %s ms )'
			, req.info.remoteAddress
			, date.format(new Date(), 'iso8601')
			, req.method.toUpperCase()
			, url.format(req.url)
			, req.raw.req.httpVersion
			, req.response.statusCode || req.response.output.statusCode
			, ms.toFixed(3)
		);
		reply.continue();
	})
	next();
}

exports.register.attributes ={
	name:'httplog'
	,version:require('../package.json').version
};
