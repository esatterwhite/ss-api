/*jshint node: true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * Configuration options for alice-web
 * @module alice-web/conf
 * @author Eric Satterwhite
 * @since 0.1.0
 */

/**
 * @property {String} [host=0.0.0.0] host address to listen on
 **/
exports.host = '0.0.0.0';
/**
 * @property {Number} [PORT=3001] Port for the HTTP server to isten on
 **/
exports.PORT = 3001;
/**
 * @property {Object} [server]
 * @property {Object} [server.router]
 * @property {Boolean} [server.router.stripTrailingSlash=true]
 **/
exports.server = {
	router:{
		stripTrailingSlash:true
	}
}

/**
 * @name module:alice-web/conf.hapi-swagger
 * @property {Object} [hapi-swagger] Configuration for the endpoint documentation
 * @property {String} [hapi-swagger.apiVersion=2.0] Current version of the api
 * @property {Number} [hapi-swagger.pathPrefixSize=2] Level of namespace generation
 * @property {String[]} [hapi-swagger.produces=['application/json','text/xml','text/javascript']] Defines the serialization formats the API UI will generate
 * @property {String[]} [hapi-swagger.consumes=['application/json','text/xml']] Defines the 
 **/
exports['hapi-swagger'] ={
	auth:false
	,apiVersion:'2.0'
	,pathPrefixSize:2
	,endpoint:'/schema'
	,produces:['application/json','text/xml','text/javascript']
	,consumes:['application/json','text/xml']
}
/**
 * @property {Object} [documentation] Configuration for the code documentation Plugin
 * @property {String} [documentation.endpoint=/code-docs] The endpoint to register JS documentation plugin under 
 **/
exports.documentation = {
  endpoint:'/code-docs'
}
