/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Api helpers functions and values
 * @module alice-web/lib/api
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires alice-web/lib/api/helpers
 */
exports.helpers = require('./helpers')
