/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Module containing helper functions for common api interactions
 * @module alice-web/lib/api/helpers
 * @author Eric satterwhite
 * @since 0.1.0
 * @requires alice-web/lib/api/helpers/qs
 */
exports.qs = require("./qs")
