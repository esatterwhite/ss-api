/*jshint laxcomma: true, smarttabs: true, node:true */
'use strict';
/**
 * A custom paginaotor class for Mongoose mquery objects
 * @module alice-web/lib/paginator
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires alice-stdlib/class
 * @requires tastypie/lib/paginator
 */

var url = require('url')
  , Class = require( 'alice-stdlib/class' )
  , Paginator = require('tastypie/lib/paginator')
  , clone = require('alice-stdlib/lang').clone
  , MongoosePaginator;
  ;

/**
 * @alias module:alice-web/lib/paginator
 * @constructor 
 * @extends tastypie/lib/paginator
 * @param {Object} [options] Instance configuration options
 */
MongoosePaginator = new Class({
	inherits:Paginator

	,options:{
		collectionName:'data'
	}
	,count: function count( ){
		return this.options.count
	}
	,uri: function uri( limit, offset ){
		var values = clone(this.options.req.query || {});

		values.limit = limit;
		values.offset = offset;
		return url.format({
			 pathname:this.options.req.path
			 ,query: values
		});
	}
	,slice: function slice( limit, offset ){
		return this.options.objects;
	}
})

module.exports = MongoosePaginator;
