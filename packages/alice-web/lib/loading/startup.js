/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * Module that locates startup / boot scripts for the web server pack instanc
 * @module alice-web/lib/loading/startup
 * @author Eric satterwhite
 * @since 0.1.0
 * @requires alice-stdlib/class
 * @requires alice-stdlib/class/options
 * @requires alice-stdlib/lib/loadinger/loader
 */

var  Class   = require( 'alice-stdlib/class' )
   , Options = require( 'alice-stdlib/class/options' )
   , Loader  = require('alice-core/lib/loading/loader')
   , path    = require('path')
   , BootLoader
   ;


/**
 * @alias module:alice-web/lib/loading/startup
 * @constructor
 * @extends module:alice-core/lib/loading/loader
 */
BootLoader = new Class(/* @lends module:alice-web/lib/loading/startup.prototype */{
    inherits:Loader
	,options:{
		searchpath:'startup'
		,filepattern:/\.js$/
	}	
	,toName: function( app, pth ){
	  return pth.replace(this.options.extensionpattern ,'').substr(pth.lastIndexOf( path.sep )+1)
	}
});

var loader = new BootLoader()

/**
 * Locates fixture files located in the project
 * @param {...String} Any number of applications to load scripts from
 * @return Object an object where keys are app names and its value is an array of full paths to all of the fixture files that were found
 */
exports.find = function find(){
	return loader.find.apply( loader, arguments )
};

/**
 * Returns An object containe all of the fixture objects listed by app name
 * @param {...String} Any number of applications to load scripts from
 * @return Object an object where keys are app names and its value is an array the fixture data to all of the fixture files that were found
 */
exports.load = function load(){
	return loader.load.apply( loader, arguments )
}
exports.Loader = Loader
