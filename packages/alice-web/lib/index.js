/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * Lib module for internal libraries and classes
 * @module alice-web/lib
 * @author Eric Satterwhite
 * @since 0.1.0
 */
