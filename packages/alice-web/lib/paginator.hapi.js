/*jshint laxcomma: true, smarttabs: true, node:true */
'use strict';
/**
 * A custom paginaotor class for data sets sent directly to hapi rather than
 * using an mquery object.
 * @module alice-web/lib/paginator
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires alice-stdlib/class
 * @requires tastypie/lib/paginator
 * @see module:alice-catalog/resources/v1/catalog
 */

var url       = require('url')
  , Class     = require( 'alice-stdlib/class' )
  , Paginator = require('tastypie/lib/paginator')
  , clone     = require('alice-stdlib/lang').clone
  , isNumber  = require('alice-stdlib/lang').isNumber
  , HapiPaginator;
  ;

/**
 * @alias module:alice-web/lib/paginator
 * @constructor 
 * @extends tastypie/lib/paginator
 * @param {Object} [options] Instance configuration options
 */
HapiPaginator = new Class({
	inherits:Paginator

	,options:{
		collectionName:'data'
	}

	,uri: function uri( limit, offset ){
		var values = clone(this.options.req.query || {});

		values.limit = limit;
		values.offset = offset;
		return url.format({
			 pathname:this.options.req.path
			 ,query: values
		});
	}

	, limit: function limit(){
		var lmt;
		lmt = this.options.req && isNumber( this.options.req.query.limit ) ? this.options.req.query.limit : this.options.limit ? this.options.limit : 25
		lmt = parseInt( lmt, 10);
		lmt = Math.min( lmt, this.options.max);
		if(!isNumber( lmt ) ){
			this.emit(
				'error'
				, new exceptions.BadRequest('limit must be a number')
			    , this.options.req
				, this.options.res	
			);
		}

		return lmt;
	}

})

module.exports = HapiPaginator;
