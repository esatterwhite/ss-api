/*jshint laxcomma: true, smarttabs: true, node: true*/
'use strict';
/**
 * Primary Web Server for the Alice platform
 ## alice-web

 Provides the pimart HTTP server for the platform API.

 #### As A Module

 ```js
 var web = require('alice-web')

 web.start(function(){
  console.log("server running!")
 })
 ```


 #### As a command

 starts the server on port 4000 with the stdout logger

 ```sh
 alice web -l stdout -p 4000
 ```

## Endpoint Definitions

#### Querystring format

Querystrings should use array subscript with numeric values to denote and array and array subscript notation with named indexes to denote an object.
```js
// ["bar", "baz"]
/param?0=bar&1=baz
```

```js
// {name:{foo:"bar", bar:"baz} }
/param?name[foo]=bar&name[bar]=baz
```

```js
// {name:[1,2,3]}
/param?name[0]=1&name[1]=2&name[2]=3
```

All query strings should be safe url encoded using `encodeURIComponent` prior to addition to full uri strings. Use `encodeURI` when 
attempting encode a full URI including query strings
 * @module alice-web
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires path
 * @requires glob
 * @requires debug
 * @requires hapi
 * @requires alice-log
 * @requires alice-conf
 * @tutorial alice-web
 */

// glob to find alice packages... 
// glob.sync('./{node_modules,packages}/**/boot/*.js').filter(minimatch.filter('./**/alice-*/**'))

var Hapi         = require( 'hapi' )
  , os           = require( 'os' )                             // alice standard config loader
  , conf         = require( 'alice-conf' )                     // alice standard config loader 
  , logger       = require( 'alice-log' )                      // alice stadard logging module
  , url          = require( 'alice-stdlib/url')
  , startup      = require( './lib/loading/startup' )          // start up script loader
  , Resources    = require( './lib/loading/resources' ).Loader // API resource loader
  , merge        = require( 'alice-stdlib/object').merge
  , Mail         = require('alice-core/mail')
  , resources    = new Resources()
  , resourceMapper                                             // used to generate version urls for resources configs
  , app
  ;

app = new Hapi.Server();
app.connection(
  merge({},{
        port:  conf.get('PORT')
      , host: conf.get('host')
      , labels:'alice'
    }, conf.get('server') )
)
resourceMapper = function( version, resource ){
  resource.path = url.join( '/api',  version, resource.path);
  return resource;
};
var starthapi = app.start;
/**
 * starts the configured http server.
 * @memberof module:alice-web
 * @static
 * @param {Function} callback A callback function to call when the server is running
 * @function start
 */
app.start = function( callback ){
  if( !app.started ){

    // load start up scripts
    logger.debug('web: loading start up scripts');
    startup
      .load()
      .flat()
      .forEach( function( plugin ){
        if( plugin.register ){
          var attributes = plugin.register.attributes
            , name       = attributes.name || attributes.pkg.name
            , version    = attributes.version || attributes.pkg.version
            ;
          logger.info('loading %s plugin', name )
          app.register( { register:plugin, options:( conf.get(name) || {} ) } ,function( err ){

            if (err){
              return console.log('error loading plugin %s', plugin.register.attributes.name );
            }
            logger.info('plugin: %s @ %s loaded', name, version);
          });
        }

        if( typeof plugin === 'function' ){
          logger.debug('executing start up script %s', plugin.name );
          plugin( app );
        }
      });



    logger.debug('web: loading v1 resources');
    app.route( 
      resources
        .reset()
        .setOptions({searchpath:'resources/v1'})
        .load()
        .flat()
        .map(resourceMapper.bind( null, 'v1' )) 
    );

    logger.debug('web: loading v2 resources');
    app.route( 
      resources
        .reset()
        .setOptions({searchpath:'resources/v2'})
        .load()
        .flat()
        .map(resourceMapper.bind( null, 'v2' )) 
    );

    logger.debug('web: starting server');
    app.started = true;
  }

  starthapi.apply( app, arguments );
};

// expose the server
module.exports = app;

// start the server if `$ node server.js`
if (require.main === module) {

  app.start(function(){
    // app.auth.default( 'alice' );
    logger.info('server running at: %s', app.info.uri );
    conf.get('NODE_ENV') != 'production' && logger.debug('RTFM: http://%s:%s/code-docs/',os.hostname(), conf.get('PORT') )
  });
}
