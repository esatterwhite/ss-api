## alice-web

Provides the pimart HTTP server for the platform API.

#### As A Module

```js
var web = require('alice-web')

web.start(function(){
	console.log("server running!")
})
```


#### As a command

starts the server on port 4000 with the stdout logger

```sh
alice web -l stdout -p 4000
```
