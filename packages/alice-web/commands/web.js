/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Default command for the alice-web package
 * @module alice-web/commands/alice-web
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires seeli
 * @requires util
 */

var cli = require( 'seeli' )
  , util = require( 'util' )
  , path          = require( 'path' )          // node path for resolving file locations
  , os            = require( 'os' )            // node os for windows checks. cause windows
  , child_process = require("child_process")   // node child_process for spawning an new pre-configured node process 
  ;

module.exports = new cli.Command({
	description:"Starts the HTTP API server with confgured options"
	,usage:[
		cli.bold('Usage: ') + 'alice alice-web --help'
	  , cli.bold('Usage: ') + 'alice alice-web --no-color'
	  , cli.bold('Usage: ') + 'alice alice-web -i'
	]

	,flags:{
		PORT:{
			shorthand:'p'
			, type: Number
			, default:3001
			, description:"port to run the server on"
		}
		,logger:{
			type:[String, Array]
			, default:null
			, description:'The logger types to configure the server to use'

		}
	}
	/**
	 * This does something
	 * @param {String|null} directive a directive passed in from the cli
	 * @param {Object} data the options collected from the cli input
	 * @param {Function} done the callback function that must be called when this command has finished
	 * @returns something
	 **/
	,run: function( cmd, data, done){

		var clone = require('mout/lang/clone')         // mout clone function
		  , args  = clone( process.argv )              // process arguments flags
		  , env   = clone( process.env )               // current host environment variables
		  , npath = ( env.NODE_PATH || '' )            // existing node path
		  				.split( path.delimiter ) 
		  , root                                       // root director of this project 
		  , pkgdir                                     // director for packages
		  , server                                     // server child process
		  ;

		args.splice(0,3)
		args.unshift( path.join( __dirname,'..', 'index.js' ) )
		args.unshift('--harmony')
	
		root          = path.resolve( __dirname, '..', '..', '..')
		pkgdir        = path.join( root, 'packages' )
		npath.push( pkgdir );
		env.NODE_PATH = npath.join( path.delimiter )

		server = child_process.spawn( process.execPath, args, {
			cwd: root
			,env:env
		});
		
		server.stdout.pipe( process.stdout )
		server.stderr.pipe( process.stderr )
	}
});
