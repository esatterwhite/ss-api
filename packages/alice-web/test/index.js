var assert      = require('assert')
  , path        = require('path')
  , os          = require('os')
  , server      = require('alice-web')
  , util        = require('util')
  , conf        = require('alice-conf')
  , crypto      = require('crypto')
  , url         = require('alice-stdlib/url')
  , token       = require('alice-auth/lib/token')
  , secret      = conf.get('jwtToken:secret')
  , hash
  , key
  ;

hash = crypto.createHash('sha1');
hash.update( crypto.randomBytes(1028) )
key = hash.digest('hex')

conf.set('affiliateApiKeys:'+ key, 'alicetest')

describe('web', function(){

	before(function( done ){
		server.start( done )
	});

	after(function( done ){
		server.stop( done )
	});

	describe('HTTP', function(){
		var authtoken
		it( 'should reject un authorized requests', function( done ){
			server.inject({
				url: '/' 
			},function( res ){
				assert.equal(res.statusCode, 401);
				done()
			})
		})

		it('should allow valid api keys to proceed', function( done ){
			server.inject({
				url: '/?apikey=' + key 
			},function( res ){
				assert.equal(res.statusCode, 200);
				done();
			});
		});

		it('should reject uknown api keys', function( done ){
			server.inject({
				url: '/?apikey=' + 'abacadaba'
			},function( res ){
				assert.equal(res.statusCode, 401);
				done();
			});
		});


		it('should allow valid Authorization headers to proceed',function( done ){
			var payload = token.tokenize( key )
			authtoken = token.encode( payload, secret )
			server.inject({
				url:'/'
				,headers:{
					Authorization:'Bearer ' + authtoken
				}
			}, function( res ){
				assert.equal( res.statusCode, 200 )
				done();
			})
		})
		
		it('should reject invalid Authorization headers to proceed',function( done ){
			server.inject({
				url:'/'
				,headers:{
					Authorization:'Bearer ' + 'aba.ca.daba'
				}
			}, function( res ){
				assert.equal( res.statusCode, 401 )
				done();
			})
		})
	});

})
