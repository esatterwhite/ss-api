/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';

var helpers = require('../lib/api/helpers')
var mongoose = require('mongoose')
var assert = require('assert')
describe('api helpers', function( ){
	var Test
	before( function(){
		var TestSchema = new mongoose.Schema({
		    firstname:{type:String}
		    ,lastname:{type:String}
		    ,age:{type:Number}
		 });
		Test = mongoose.model( 'Fake', TestSchema )
	});

	describe('qs', function(){
		describe('#buildfilters', function(){
			it('should construct mongo filters from a model and querystring', function(){
				var filters = helpers.qs.buildFilters(Test, 'firstname__startswith=Ma&lastname__startswith=bi')
				assert.ok( filters )
				assert.equal( filters.firstname.$regex.toString(), '/^Ma/' )
				assert.equal( filters.lastname.$regex.toString(), '/^bi/' )
			});

			it('should allow multiple filter types on the same field', function(){
				var filters = helpers.qs.buildFilters(Test, 'age__lt=100&age__gt=20')
				assert.equal( filters.age.$lt, 100 )
				assert.equal( filters.age.$gt, 20 )
			});

			it('should construct array lookups from ideltical filters', function(){
				var filters = helpers.qs.buildFilters(Test, 'firstname__in=Matt&firstname__in=Jeff')
				assert.ok(Array.isArray( filters.firstname.$in ))
			});

			it('should accept querystring objects', function(){
				var data = {firstname__in:['a','b'], age__lt:20, age__gt:100}
				var filters = helpers.qs.buildFilters(Test, data )
				assert.equal( filters.age.$lt, 20 )
				assert.equal( filters.age.$gt, 100 )
				assert.ok( Array.isArray( filters.firstname.$in ))
			});
		})
	})
})
