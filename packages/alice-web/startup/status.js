/*jshint laxcomma:true, smarttabs: true, node:true */
/**
 * Registers the root url to return some process information
 * @module alice-web/startup/status
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires alice-log
 **/

var logger =require('alice-log');
exports.register = function( server, options,  next ) {

  server.route({
    method:'GET'
    ,path:'/'
    ,config:{

      tags:['status', 'debug']
      ,description:"Returns process information about the current process"
    }
    ,handler:function(request,reply){
    	reply({
    		os:logger.exception.getOsInfo()
    		,process:logger.exception.getProcessInfo().memoryUsage
    	}).code(200)
    }
  });
};

exports.register.attributes={
  name:"status"
  ,version:'0.1.0'
};
