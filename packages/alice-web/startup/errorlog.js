/*jshint laxcomma: true, node: true, smarttabs: true*/
'use strict';
/**
 * Enables request logging though standard logger
 * @module alice-log/boot/httplog
 * @author Eric satterwhite
 * @since 0.1.0
 * @requires url
 * @requires alice-stdlib/date
 * @requires alice-log
 * @requires alice-core
 * @requires alice-conf
 * @requires http
 */
var util   = require( 'util' )             // Node util module for str formatting
  , http   = require('http')               // Node http module for status code names
  , logger = require( 'alice-log' )        // Internal logger module
  , Mail   = require( 'alice-core/mail' )  // intermal mail module for sending error messages
  , conf   = require( 'alice-conf' )       // internal conig loader module
  , url    = require( 'url' )

module.exports = function( server ){
	server.on('request-error', function(request, err) {
		logger.error( err.message, err.stack  )
		var status = err && err.output ? err.output.statusCode : 500

		// only care about 500 and 404( ? )
		switch( status ){
			case 500:
			case 404:
				Mail.send({
					to:conf.get( 'mail:admins' )
					,from:conf.get( 'mail:defaultemail' )
					,subject:util.format( '[Alice] %s %s', http.STATUS_CODES[ status ], request.path ) 
					,text:util.format( 
						'Type: %s\nMessage: %s\nTraceback:\n %sRequest:\n\n  Method: %s\n\n url: %s\n\n Headers: %s\n\n  Parameters:%s\n\n queryt:%s\n\n Body: %s\n\nENV:\n%s\n\nCONFIG:%s'
						, err.name
						, err.message
						, err.stack
						, request.method
						, url.format( request.url )
						, JSON.stringify( request.headers, null, 2 )
						, JSON.stringify( request.params, null, 2 )
						, JSON.stringify( request.query, null, 2 )
						, JSON.stringify( request.payload, null, 2 )
						, JSON.stringify({
							os:logger.exception.getOsInfo()
							,process:logger.exception.getProcessInfo().memoryUsage
						}, null, 2)
						, JSON.stringify(conf.get(), null, 2) 
					)
				});
				break;		
		}
	})
}
