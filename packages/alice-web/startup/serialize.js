/*jshint laxcomma: true, smarttabs: true, node: true*/
'use strict';
/**
 * Allows for multiple data form format serialization and deserialization
 * @module alice-web/startup/serialize
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires tastypie/lib/serializer
 * @requires tastypie/lib/mime
 */

var serializer = require('tastypie/lib/serializer')
  , mime       = require('tastypie/lib/mime')
  , isObject   = require('alice-stdlib/lang').isObject
  , Serializer = new serializer({
  	content_types:{
  		'application/xml':null
  	}
  })
  ;

function determineFormat( request, reply, types ){
  var fmt = request.query && request.query.format
    , ct  = Serializer.convertFormat( fmt )
    ;

  if( fmt && !ct ){
	return reply(new Boom.unsupportedMimeType('Unsupported serialization format: ' + fmt ));
  } else if( fmt && ct ){
	return ct;
  }
  // default type lookups
  return mime.determine( request, types );
}

exports.register = function( plugin, options, next ){
  plugin.ext('onPostAuth', function( request, reply ){

  	// if it is a format hapi deals with, it has already parsed it
  	if( !request.payload || isObject( request.payload ) ){
  		return reply.continue();
  	}

  	// if it is a string, lets try to deserialize it
	Serializer.deserialize( request.payload, request.headers['content-type'], function( err, data ){
	  if( data ){
		request.payload = data;
	  }
	  reply( err );
	});
  });

  plugin.ext('onPreResponse', function(request, reply){
	var response = request.response
	  , query    = request.query
	  , format   = determineFormat( request, reply, Serializer.types )
	  ;
	if( query.hasOwnProperty('callback') && !( query.hasOwnProperty('format') ) ){
	  return reply.continue();
	}

	if(!format){
	  return reply.continue();
	}
	
	if( response.isBoom ){
	  return reply.continue();
	}
	
	Serializer.serialize( response.source, format, request.query, function(err, content ){
	  reply(content)
		.type(format);
	});
  });
  next();
};

exports.register.attributes = {
	name:'serialize'
	,version:'0.0.1'
};
