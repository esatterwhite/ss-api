/*jshint laxcomma: true, smarttabs: true, node: true*/
'use strict';
/**
 * includes middleware to auto generate & serve documentation while not in production
 * @module alice-web/startup/documentation
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires alice-conf
 * @requires fs
 * @requires path
 * @requires child_process
 * @requires alice-core/commands/docs
 */

var conf          = require( 'alice-conf' )
  , fs            = require( 'fs' )
  , path          = require( 'path' )
  , PROJECT_ROOT  = conf.get('PROJECT_ROOT')
  , DocCommand    = require('alice-core/commands/docs')
  , logger        = require('alice-log')
  , DOCS_ROOT     = path.join( PROJECT_ROOT, 'documentation' )
  , Boom          = require('boom')
  ;

/**
 * @param {Server} plugin hapi server instance
 * @param {Object} options runtime configuration options
 * @param {Function} next Callback to be called when registration is complete
 **/
exports.register = function( plugin, options, next ){
	var buildDocs // invokes the docs command
	  , envCheck  // check if in Production mode
	  ;

	  logger.info("loading documentation plugin")
	  buildDocs = function buildDocs( next, exists ){

	  	if( !exists ){
	  		DocCommand.run(null,next);
	  	} else{
	  		next();
	  	}
	  };

	  envCheck = function envCheck( req, res, next ){
	  	
	  	next();
	  };

	plugin.route({
		method:'GET'
		,path:  (options.endpoint || '/docs') + '/{param*}' 
		,config:{
			tags:["documentation"]
			,description:"Displays full code documentation for the application while not in production mode"
			,auth:false
			,handler:{
				directory:{
					path:DOCS_ROOT
				}
			}
			,pre:[{
				method:function( request, reply ){
					if( conf.get('NODE_ENV') === 'production' ){
					  return reply(new Boom.notFound() )
					}
					fs.exists(DOCS_ROOT, function( exists ){
						if(!exists){

							return DocCommand.run(null,function(){
								reply('done')
							});
						}
						reply('done')
					})
				}
				,assign:'docs'
			}]
		}
	});
  logger.info("documentation plugin loaded")

	next();
};

exports.register.attributes = {
	name:'documentation'
	,version:'0.1.0'
}
