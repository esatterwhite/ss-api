var assert      = require('assert')
  , path        = require('path')
  , os          = require('os')
  , util        = require('util')
  , server      = require('alice-web')
  , conf        = require('alice-conf')
  , crypto      = require('crypto')
  , date        = require('alice-stdlib/date')
  , url         = require('alice-stdlib/url')
  , token       = require('alice-auth/lib/token')
  , debug       = require('debug')('alice:cart:test:http')
  , async       = require('async')
  , stripe      = require( 'stripe' )( conf.get( 'stripe:apiKey' ) )
  , secret      = conf.get('jwtToken:secret')
  , Product     = require('alice-catalog/models/product')
  , Tenant      = require('alice-catalog/models/tenant')
  , Promotion   = require('alice-catalog/models/promotion')
  , Cart        = require('../models/cart')
  , Order       = require('../models/order')
  , hash
  , key
  ;




hash = crypto.createHash('sha1');
hash.update( crypto.randomBytes(1028) )
key  = hash.digest('hex')

describe('HTTP - cart', function(){
	var authtoken;
	var p, tenant ,singlePromo, generalPromo

	before(function( done ){
		var payload = token.tokenize( key )
		authtoken = token.encode( payload, secret )
		singlePromo = new Promotion({
			volatile:true
			,description:"Test Single"
			,rate: 15
			,code:'TEST-101'
		})
		generalPromo = new Promotion({
			volatile:false
			,description:"Test General"
			,rate: 100
			,code:'TEST-102'
		})

		p = new Product({
			"name": (+ new Date).toString(16),
			"brand": "Jerzees",
			"shortDescription": "Hooded Lightweight Sweatshirt for Women, 50% cotton/ 50% polyester, Brand: Jerzees",
			"fullDescription": "This lightweight hooded sweatshirt for women is perfect for any season! The plush pullover has a classic kangaroo front pocket and is pre-shrunk and laundered. The fabric is made from 50% cotton and 50% polyester and has a fabric weight of 8.0 oz.\r\n*Item runs big, is recommended to order a size down*",
			"swatch": "48db4d5850884b179afef41606309831",
			"recordstatus": "active",
			"gender": "f",
			"search": {
			    "tags": [
			        "hoodie"
			    ]
			},
			"approvedForPublish": true,
			"economics": {
			    "productCost": 1600,
			    "productMSRP": 1600,
			    "price": 2000,
			},
			"categories": [],
			"lifecycle": {
			    "availableOn": "2014-01-01T06:00:00.000Z"
			},
			"manufacturer": {
			    "name": "Alice Test Runner",
			    "internalId": "406"
			},
			"backImage": {
			    "imageRef": "1c93995aea764ec38f8ac1e793fe1e45",
			    "vignettes": []
			},
			"frontImage": {
			    "imageRef": "7d3877766ef649e0b0cde5521d7d1ee6",
			    "vignettes": []
			},
			"colors": [
			    {
			        "name": "Purple",
			        "hexColor": "#6E2A81",
			        "manufacturerId": "447",
			        "_id": "54343c6fcfc17bbf779b65c6"
			    },
			    {
			        "name": "Red",
			        "hexColor": "#D41C28",
			        "manufacturerId": "196",
			        "_id": "54343c6fcfc17bbf779b65c5"
			    },
			    {
			        "name": "Heather Gray",
			        "hexColor": "#CBCBCB",
			        "manufacturerId": "231",
			        "_id": "54343c6fcfc17bbf779b65c4"
			    },
			    {
			        "name": "White",
			        "hexColor": "#FFFFFF",
			        "manufacturerId": "1",
			        "_id": "54343c6fcfc17bbf779b65c3"
			    },
			    {
			        "name": "Black",
			        "hexColor": "#000000",
			        "manufacturerId": "2",
			        "_id": "54343c6fcfc17bbf779b65c2"
			    }
			]
		});

		async.parallel([
			function( callback ){
				Tenant.findOne(function(ee, t){
					tenant = t
					callback( ee )
				})
			}
			,generalPromo.save.bind( generalPromo )
			,singlePromo.save.bind( singlePromo )
			,p.save.bind( p )
		],function(){
			server.start( done )
		})
	});

	after(function( done ){
		async.parallel([
			p.remove.bind( p )
			,generalPromo.remove.bind( generalPromo )
			,singlePromo.remove.bind( singlePromo )
		], function(){
			server.stop( done )
		})

	});
	describe('v1', function(){
		describe('finalization', function( ){
			it('should return an error for invalid products',function(done){
				server.inject({
					url:'/api/v1/cart/action/finalize'
					,method:'POST'
					,headers:{
						Authorization:'Bearer ' + authtoken
					}
					,payload:JSON.stringify({
						"shipping":{
							"firstName":"Alie"
							,"email":"noreply@mail.com"
							,"lastName":"Test"
							,"address1":"123 Main St"
							,"address2":"Apt 101"
							,"state":"NY"
							,"city":"Brooklyn"
							,"postalCode":"60613"
						}
						,"billing":{
							"firstName":"Alie"
							,"email":"noreply@mail.com"
							,"lastName":"Test"
							,"address1":"123 Main St"
							,"address2":"Apt 101"
							,"state":"NY"
							,"city":"Brooklyn"
							,"postalCode":"60613"
						}
						,"subtotal":p.economics.price
						,"cartItems":[{
							"tenant":{
								id: tenant.id
								,name: tenant.name
							}
							,"unitPrice": p.economics.price
							,"product":{
								id:new Product()._id
								,name:p.name
							}
							,"decal":{
								"id":"544fec95469d4092375c4e04"
							}
							,"decalColors":{
								"clrp":"FFD700"
								,"clrs":"ffffff"
								,"clrt":"000000"
								,"clrw":"ffffff"
								,"clrb":"929694"
							}
							,"personalization":[]
							,"size":"2"
							,"productHexColor":"000000"
							,"quantity":2
						}]
					})
				}, function( response ){
					assert.equal(response.statusCode,400)
					done()
				})
			});

			it('shoul add the cost of personalization for back printing', function( done ){
				server.inject({
					url:'/api/v1/cart/action/finalize'
					,method:'POST'
					,headers:{
						Authorization:'Bearer ' + authtoken
					}
					,payload:JSON.stringify({
						"shipping":{
							"firstName":"Alie"
							,"email":"eric@spiritshop.com"
							,"lastName":"Test"
							,"address1":"123 Main St"
							,"address2":"Apt 101"
							,"state":"TN" // should be no shipping cost here
							,"city":"West Kakalaki"
							,"postalCode":"44939"
						}
						,"billing":{
							"firstName":"Alie"
							,"email":"eric@spiritshop.com"
							,"lastName":"Test"
							,"address1":"123 Main St"
							,"address2":"Apt 101"
							,"state":"NY"
							,"city":"Brooklyn"
							,"postalCode":"60613"
						}
						,"subtotal":p.economics.price * 2
						,"cartItems":[{
							"tenant":{
								id: tenant.id
								,name: tenant.name
							}
							,"unitPrice": p.economics.price
							,"printUrls": {
							    "front":'http://itdoesntnamtter.com/front.png' ,
							    "back": 'http://itdoesntnamtter.com/back.png'
							}
							,"product":{
								id:p.id
								,name:p.name
							}
							,"decal":{
								"id":"544fec95469d4092375c4e04"
							}
							,"decalColors":{
								"clrp":"FFD700"
								,"clrs":"ffffff"
								,"clrt":"000000"
								,"clrw":"ffffff"
								,"clrb":"929694"
							}
							,"personalization":[]
							,"size":"2"
							,"productHexColor":"000000"
							,"quantity":2
						}]
					})
				}, function( response ){
					var estimatedcost = ( p.economics.price + p.economics.personalization  ) * 2
					assert.equal( estimatedcost, response.result.subtotal, "Back printing should add a peronalization cost per item" )
					done()
				})
			})
			it('should accept a post request',function( done ){
				server.inject({
					url:'/api/v1/cart/action/finalize'
					,method:'POST'
					,headers:{
						Authorization:'Bearer ' + authtoken
					}
					,payload:JSON.stringify({
						"shipping":{
							"firstName":"Alie"
							,"lastName":"Test"
							,"address1":"123 Main St"
							,"address2":"Apt 101"
							,"state":"NY"
							,"city":"Brooklyn"
							,"email":"noreply@mail.com"
							,"postalCode":"60613"
						}
						,"billing":{
							"firstName":"Alie"
							,"lastName":"Test"
							,"address1":"123 Main St"
							,"address2":"Apt 101"
							,"state":"NY"
							,"city":"Brooklyn"
							,"email":"noreply@mail.com"
							,"postalCode":"60613"
						}
						,"subtotal":p.economics.price
						,"cartItems":[{
							"tenant":{
								id: tenant.id
								,name: tenant.name
							}
							,"unitPrice": p.economics.price
							,"product":{
								id:p.id
								,name:p.name
							}
							,"decal":{
								"id":"544fec95469d4092375c4e04"
							}
							,"decalColors":{
								"clrp":"FFD700"
								,"clrs":"ffffff"
								,"clrt":"000000"
								,"clrw":"ffffff"
								,"clrb":"929694"
							}
							,"personalization":[]
							,"size":"2"
							,"productHexColor":"000000"
							,"quantity":2
						}]
					})
				}, function(response){
					var details = response.result;
					assert.equal( response.statusCode, 201)
					assert.equal( details.subtotal , ( p.economics.price * 2 ))
					async.waterfall([
						function(callback){
							var now = new Date()
							stripe.tokens.create({
								card:{
									number: '4242424242424242'
									,exp_month: date.format( now, '%m')
									,exp_year: '' + ( parseInt(date.format( now, '%y'),10) + 1 )
									,cvc: '123'
								}
							}, callback )
						}
						,function( token, callback ){

							server.inject({
								url:util.format( '/api/v1/cart/%s/action/checkout', details.cart_id)
								,method:'POST'
								,headers:{
									Authorization:'Bearer ' + authtoken
								}
								,payload:{
									card:token.id
									,total:details.total
								}
							}, function( r){
								callback( null, r )
							})
						}
						,function( resp, callback ){

							debug('looking up cart %s', details.cart_id )
							Cart.findById(details.cart_id, function( err, c ){
								assert.equal( err, null )
								assert.ok( c )
								callback(null, c )
							});
						}

						,function( cart, callback ){

							debug('looking up order for cart %s', cart._id )
							Order.findOne({cart:cart._id}, function( err, o ){
								assert.ok( o )
								callback(null, [cart, o])
							})
						}

					], function(err, results ){

						var cart = results[0]
						var order = results[1]
						debug("cart_id %s", cart._id)
						debug("order_id %s", order._id)
						assert.ok( cart )
						assert.ok( order )
						assert.equal( order.orderStatus,'queued' )
						async.parallel([
							function( callback ){
								cart.remove( callback )
							}
							,function( callback ){
								order.remove( callback )
							}
						], function( err ){
							done();
						})
					})
				})
			});

			it('calculate promotion code discounts',function( done ){
				server.inject({
					url:'/api/v1/cart/action/finalize'
					,method:'POST'
					,headers:{
						Authorization:'Bearer ' + authtoken
					}
					,payload:JSON.stringify({
						"shipping":{
							"firstName":"Alice"
							,"lastName":"Test"
							,"address1":"123 Main St"
							,"address2":"Apt 101"
							,"state":"NY"
							,"city":"Brooklyn"
							,"email":"noreply@mail.com"
							,"postalCode":"60613"
						}
						,"billing":{
							"firstName":"Alice"
							,"lastName":"Test"
							,"address1":"123 Main St"
							,"address2":"Apt 101"
							,"state":"NY"
							,"city":"Brooklyn"
							,"email":"noreply@mail.com"
							,"postalCode":"60613"
						}
						,"appliedPromotionalCode":generalPromo.code
						,"subtotal":p.economics.price
						,"cartItems":[{
							"tenant":{
								id: tenant.id
								,name: tenant.name
							}
							,"unitPrice": p.economics.price
							,"product":{
								id:p.id
								,name:p.name
							}
							,"decal":{
								"id":"544fec95469d4092375c4e04"
							}
							,"decalColors":{
								"clrp":"FFD700"
								,"clrs":"ffffff"
								,"clrt":"000000"
								,"clrw":"ffffff"
								,"clrb":"929694"
							}
							,"personalization":[]
							,"size":"2"
							,"productHexColor":"000000"
							,"quantity":2
						}]
					})
				}, function(response){
					var details = response.result;
					assert.equal( response.statusCode, 201)
					assert.equal( details.subtotal , ( p.economics.price * 2 ))
					// FIXME: need way to validate this
					discount = details.discounts.reduce(function(a,b){
						return ( a.amount || 0 ) + ( b.amount || 0)
					},0);


					assert.equal( details.total ,  ( details.subtotal + details.shippingCost + details.tax ) - discount )
					async.waterfall([
						function(callback){
							var now = new Date()
							stripe.tokens.create({
								card:{
									number: '4242424242424242'
									,exp_month: date.format( now, '%m')
									,exp_year: '' + ( parseInt(date.format( now, '%y'),10) + 1 )
									,cvc: '123'
								}
							}, callback )
						}
						,function( token, callback ){

							server.inject({
								url:util.format( '/api/v1/cart/%s/action/checkout', details.cart_id)
								,method:'POST'
								,headers:{
									Authorization:'Bearer ' + authtoken
								}
								,payload:{
									card:token.id
									,total:details.total
								}
							}, function( r){
								callback( null, r )
							})
						}
						,function( resp, callback ){

							debug('looking up cart %s', details.cart_id )
							Cart.findById(details.cart_id, function( err, c ){
								assert.equal( err, null )
								assert.ok( c )
								callback(null, c )
							});
						}

						,function( cart, callback ){

							debug('looking up order for cart %s', cart._id )
							Order.findOne({cart:cart._id}, function( err, o ){
								assert.ok( o )
								callback(null, [cart, o])
							})
						}

					], function(err, results ){

						var cart = results[0]
						var order = results[1]
						debug("cart_id %s", cart._id)
						debug("order_id %s", order._id)
						assert.ok( cart )
						assert.ok( order )
						assert.equal( order.orderStatus,'queued' )
						async.parallel([
							function( callback ){
								cart.remove( callback )
							}
							,function( callback ){
								order.remove( callback )
							}
						], function( err ){
							done();
						})
					})
				})
			})
		})
	})

})
