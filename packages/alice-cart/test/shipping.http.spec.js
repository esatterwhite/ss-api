var assert = require('assert')
  , server = require('alice-web')
  , Shipping = require('../models/shipRates')
  , conf        = require('alice-conf')
  , crypto      = require('crypto')
  , date        = require('alice-stdlib/date')
  , url         = require('alice-stdlib/url')
  , secret      = conf.get('jwtToken:secret')
  , token       = require('alice-auth/lib/token')
  , hash
  , key

hash = crypto.createHash('sha1');
hash.update( crypto.randomBytes(1028) );
key  = hash.digest('hex');

describe('shipping', function(){
	var payload = token.tokenize( key )
	var authtoken = token.encode( payload, secret )
	describe('resources', function(){
		describe('v1', function(){
			before( function( done ){
				server.start( done )
			});

			after(function( done ){
				server.stop( done )
			});

			describe('shipping', function(){
				it('should exist', function( done ){
					server.inject({
						url:'/api/v1/shipping'
						,method:'get'
						,headers:{
							Authorization:'Bearer ' + authtoken
						}
					}, function( response ){
						assert.equal(response.statusCode, 200)
						done()
					})
				})

				it('should allow filtering by field names', function( done ){
					server.inject({
						url:'/api/v1/shipping?orderMinimum__gt=10000'
						,method:'get'
						,headers:{
							Authorization:'Bearer ' + authtoken
						}
					}, function( response ){
						assert.equal(response.statusCode, 200);
						response.result.data.forEach( function( rate ){
							assert.ok( rate.orderMinimum > 10000 )
						})
						done()
					})	
				})

				it('should allow for multile filters', function( done ){
					server.inject({
						url:'/api/v1/shipping?rate__gt=1000&rate__lt=2000'
						,method:'get'
						,headers:{
							Authorization:'Bearer ' + authtoken
						}
					}, function( response ){
						assert.equal(response.statusCode, 200);
						assert.ok(response.result.data.length < response.result.meta.count )
						
						response.result.data.forEach( function( rate ){
							assert.ok( rate.rate > 1000 )
							assert.ok( rate.rate < 2000 )
						})
						done()
					})	

				})
			})
		})
	})
})
