/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';

var http         = require('http')
  , joi          = require( 'joi' )
  , Boom         = require( 'boom' )
  , url          = require('url')
  , util         = require('util')
  , Paginator    = require('alice-web/lib/paginator')
  , forEach      = require('alice-stdlib/collection').forEach
  , toJoi        = require('alice-core/lib/db/schema/toJoi')
  , validators   = require('alice-core/lib/db/schema/validators/defaults')
  , SalesTaxRate = require('alice-cart/models/salesTaxRates')
  , ShipRate     = require('alice-cart/models/shipRates')
  , helpers      = require('alice-web/lib/api/helpers')
  , async        = require('async')
  , queryset     = SalesTaxRate.find().limit(25).toConstructor()
  , listquery
  ;

// querystring validators for get_list
listquery = validators
				.query
				.clone()
				.unknown(); // this allows for filters to pass through

/**
 * ##### GET /api/v1/taxes
 * Endpoint for retreiving Tax information
 * @see module:alice-web/lib/api/helpers/qs
 * @property {String} method=get
 * @property {Object} querystring
 * @property {Number} querystring.limit=25 The maximum number of results to return
 * @property {Number} querystring.offset=0 The starting position to return results from
 * @property {String} querystring.format=json The data format to return `json`, `jsonp`, `xml`
 * @property {String} querystring.callback=callback The name of a callback function to use for jsonp responses
 * @property {String} querystring.apikey The apikey to authenticate with
 */
exports.get_list = {
	method:'get'
  , path:'/shipAndTaxes'
  , config:{
	  description:"Listing endpoint for available shipping and tax rates"
	  , tags:[ 'api' ]
	  , validate:{}
	  , handler: function get_ship_and_tax( request, reply ){
	  		var query
	  		  , filterValues
	  		  ;
      async.parallel(
        {
          shipping: function (callback) {
              ShipRate.find().exec( callback );
          },
          taxes: function (callback) {
              SalesTaxRate.find().exec( callback );
          }
        }, function (err, results) {
          if (err) {
		  			return reply( new Boom.badRequest( err ));
          }
          reply(results);
        }
		)
	}
}
};
