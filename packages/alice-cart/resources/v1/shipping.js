/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * API Endpoints for shipping rates.
 * ### Filtering Data
 * The listing endpoint can be filtered by field values using a querystring syntax where field names can also contain
 * a filter separated by a double underscore ( `__` ) followed by the {@link module:alice-web/lib/api/helpers/qs.terms|filter type}.
 *```
 * <FIELD>__<FILTER>=<VALUE> 
 *```
 * Mutliple filters on the same field will be `add`ed together.
 * For example:
 *```
 * field__gt=1000&field__lt=2000
 *```
 * translates to `documents` where `field` is `greater than` 1000, **and** `field` is `less than` 2000
 * @module alice-cart/resources/v1/shipping
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires http
 * @requires url
 * @requires joi
 * @requires boom
 * @requires util
 * @requires alice-web/lib/paginator
 * @requires alice-stdlib/collection
 * @requires alice-core/lib/db/schema/toJoi
 * @requires alice-core/lib/db/schema/validators/defaults
 * @requires alice-cart/models/shipRates
 * @requires alice-web/lib/api/helpers
 */

var http         = require('http')
  , joi          = require( 'joi' )
  , Boom         = require( 'boom' )
  , url          = require('url')
  , util         = require('util')
  , Paginator    = require('alice-web/lib/paginator')
  , forEach      = require('alice-stdlib/collection').forEach
  , toJoi        = require('alice-core/lib/db/schema/toJoi')
  , validators   = require('alice-core/lib/db/schema/validators/defaults')
  , ShipRate     = require('alice-cart/models/shipRates')
  , helpers      = require('alice-web/lib/api/helpers')
  , queryset     = ShipRate.find().limit(25).toConstructor()
  , listquery 
  ;

// querystring validators for get_list
listquery = validators
				.query
				.clone()
				.unknown(); // this allows for filters to pass through

/**
 * ##### GET /api/v1/shipping
 * Endpoint for retreiving shipping information
 * @see module:alice-web/lib/api/helpers/qs
 * @property {String} method=get
 * @property {Object} querystring 
 * @property {Number} querystring.limit=25 The maximum number of results to return
 * @property {Number} querystring.offset=0 The starting position to return results from
 * @property {String} querystring.format=json The data format to return `json`, `jsonp`, `xml` 
 * @property {String} querystring.callback=callback The name of a callback function to use for jsonp responses
 * @property {String} querystring.apikey The apikey to authenticate with
 * @property {Object} filters A set of field filters to narrow the results by
 * @property {String} filters.type The type of shipping to retrieve -`standard`, `premium`, `nextday`
 * @property {Number} filters.orderMinimum The minimum amount of the order total in cents
 * @property {Number} filters.orderMaximum The maximum amount of the order total in cents
 * @property {String} filters.rateType The type of shiping rate - `flat` or `percentage`
 * @property {Number} filters.rate The mount of the shipping rate itself
 * @example
curl -H "Accept: application/json" http://localhost:3001/api/v1/shipping?orderMaximum__gte=10000
{
    "meta": {
        "count": 11,
        "limit": 25,
        "next": null,
        "offset": 0,
        "previous": null
    },
    "data": [
        {
            "__v": 0,
            "_id": "53d6487ce4b0215297bfdf4a",
            "orderMaximum": 99999999,
            "orderMinimum": 16000,
            "rate": 2099,
            "rateType": "flat",
            "type": "premium"
        },
        {
            "__v": 0,
            "_id": "53d648d8e4b0215297bfdf4e",
            "orderMaximum": 99999999,
            "orderMinimum": 16000,
            "rate": 6599,
            "rateType": "flat",
            "type": "nextday"
        }
    ]
}
 */
exports.get_list = {
	method:'get'
  , path:'/shipping'
  , config:{
	  description:"Listing endpoint for available shipping rates"
	  , tags:[ 'api' ]
	  , validate:{
	  	query: listquery
	  }
	  , handler: function get_shipping( request, reply ){
	  		var query
	  		  , filterValues
	  		  ;

			query = new queryset();
		  	query.model.count( function( cerror, count ){
		  		if( cerror ){
		  			return reply( new Boom.wrap( cerror ) );
		  		}

		  		// shift the query by the paging params
		  		query
		  			.skip( request.query.offset || 0 )
		  			.limit( request.query.limit );

		  		// generate db query based on filters
		  		try {
			  		filterValues = helpers.qs.buildFilters( query.model, request.query );
		  		} catch( e ){
		  			return reply( new Boom.badRequest( e.message ));
		  		}

		  		// apply any sorting params
		  		helpers.qs.applySorting( query, request.query );

		  		// append the query
		  		query.where( filterValues );

		  		// execute & return
		  		query.lean().exec( function( err, results ){
					var paginator = new Paginator({
						req:request
						,res:reply
						,objects:results
						,count:count
						,limit:request.query.limit
						,offset:request.query.offset
					});
					reply( paginator.page() );
				});
		  	});
		}
	}
};

/**
 * ##### GET /api/v1/shipping/{shipping_id}
 * @property {String} method=get
 * @property {Object} querystring Default query string keys
 * @property {Number} querystring.limit=25 The maximum number of results to return
 * @property {Number} querystring.offset=0 The starting position to return results from
 * @property {String} querystring.format=json The data format to return `json`, `jsonp`, `xml` 
 * @property {String} querystring.callback=callback The name of a callback function to use for jsonp responses
 * @property {String} querystring.apikey The apikey to authenticate with
 * @property {String} shipping_id A valid Mongo ID of a shipping rate to fetch
 * @example
 curl -H "Accept: application/json" http://localhost:3001/api/v1/shipping/53d6487ce4b0215297bfdf4a
 {
     "__v": 0,
     "_id": "53d6487ce4b0215297bfdf4a",
     "orderMaximum": 99999999,
     "orderMinimum": 16000,
     "rate": 2099,
     "rateType": "flat",
     "type": "premium"
 }
 */
exports.get_detail = {
	method:'get'
	,path:'/shipping/{shipping_id}'
	,config:{
		tags:['api']
		,validate:{
			query: listquery
			,params:{
				shipping_id:joi.string().alphanum().length(24).description('valid mongo ID to lookup')
			}
		}
		,handler: function( request, reply ){
			new queryset()
				.findOne({_id:request.params.shipping_id})
				.lean()
				.exec( function( err, rate ){
					if(!rate){
						return reply( new Boom.notFound(util.format('Shipping rate with id %s not found', request.params.shipping_id) ) )
					}
					reply( err || rate )
				})
		}
	}
};
