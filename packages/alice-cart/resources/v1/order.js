/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * V1 Api resources for order entities
 * @module alice-cart/resources/v1/order
 * @author Eric Satterwhite
 * @since 0.2.1
 * @requires boom
 * @requires async
 * @requires util
 * @requires joi
 * @requires alice-core/lib/db/schema/validators
 * @requires alice-cart/models/order
 */

var util        = require( 'util' )
  , boom        = require( 'boom' )
  , joi         = require( 'joi' )
  , validators  = require( 'alice-core/lib/db/schema/validators' ).defaults
  , Order       = require( '../../models/order' )
  , Cart        = require( '../../models/cart' )
  ;


exports.get_by_cart = {
	method:'GET'
	,path:'/order/{order_id}'
	,config:{
		tags:['api']
		,description:'Finds an order by confirmation id. which is a cart id for the time being'
		,validate:{
			params:{
				order_id:joi.string().alphanum().required()
			}
			,query: validators.query.clone()
		}
		,pre:[
			{
				 assign:'order'
				, method:function( request, reply ){
					var id = request.params.order_id

					Order.findById(id, function( err, obj ){
						if( err ){
							return reply( new boom.badRequest( err.message ) )
						}

						reply( obj || new boom.notFound(util.format("No order with confirmation code %s", id) ))
					})
					
				}
			}
		]
		,handler: function( request, reply ){
			var cart_id // id of the cart associated to the order
			  , payload // response payload
			  ;

			cart_id = request.pre.order.cart
			payload = new boom.notFound(util.format("Unable to determine status of order %s. Contact Spiritshop for further details", cart_id) )

			// FIXME: Fullfillment is operating on the cart instace
			// rather the updating the order. So set the order status
			// to the order of the cart
			Cart.findById( cart_id, function(err, cart ){
				if( err ){
					return reply( new boom.badRequest( err.message ) )
				}

				if( cart ){
					payload = request.pre.order.toObject();
					payload.orderStatus = cart.monitoring.lifeCycle.status
				}


				reply( payload )
				
			})
		}
	}
}
