/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * Provides the API for manageing a cart
 * @module alice-cart/resources/v1/cart
 * @author Eric Satterwhite
 * @author Ryan Fisch
 * @since 1.9.0
 * requires async
 * @requires alice-cart/lib/cartmanager
 */

var async       = require('async')
  , util        = require('util')
  , joi         = require('joi')
  , Exception   = require( 'alice-stdlib/exception' )
  , Cart        = require('alice-cart/models/cart')
  , validators  = require('alice-core/lib/db/schema/validators').defaults
  , toJoi       = require('alice-core/lib/db/schema/toJoi')
  , CartManager = require('../../lib/cartmanager')
  , Boom        = require('boom')
  , Order       = require( '../../models/order' )
  , logger      = require( 'alice-log' )                        // alice standard logger
  ;

// FIXME: The reaspon you have to create a new cart manager every request
// is because the cart manager stores state. Thats what a database is for

/**
 * Defines the listing endpoint for cars
 * @param {Object} query Acceptable query string parameters
 */
exports.get_list = {
	method:'GET'
	,path:'/cart'
	,config:{
		tags:['api']
		,description:'DEPRICATED. this was never actually used / implemented'
		,validate:{
			query: validators.query.clone().keys({
				userId:joi.string().length(24).alphanum().optional().description("Mongo Id of a user")
				,cartToken:joi.string().length(24).alphanum().optional().description("Mongo ID of an existing cart")
			})
		}
		,handler:function get(request, reply) {
			reply(new Boom.notImplemented("Cart is not a discoverable resource"))
		}

	}
}

exports.get_detail = {
	method:'GET'
	,path:'/cart/{id}'
	,config:{
		description:'Get an existing Cart'
		,tags:['api']
		,validate:{
			params:{
				id:joi.string().length(24).alphanum().description("Mongo ID of the cart to update")
			}
		}
		,handler:function get_detail(request, reply) {
      Cart.findById(request.params.id, function (err, cart) {
        if (cart) {
          reply({"data": cart});
        } else {
          reply( {"data": false});
        }
      });
		}
}
}


/**
 * Description
 * @method <NAME>
 * @param {TYPE} <NAME> <Description>
 * @param {TYPE} <NAME> <Description>
 */
exports.put_detail ={
	method:'PUT'
	,path:'/cart/{id}'
	,config:{
		description:'Update an existing Cart'
		,tags:['api']
		,validate:{
			params:{
				id:joi.string().length(24).alphanum().description("Mongo ID of the cart to update")
			}
			,query:validators.query.clone()
			,payload:toJoi( Cart.schema )
		}
		,handler:function put_detail(request, reply) {
			reply( new Boom.notImplemented() )
		}
	}
};



/**
 * @param params ([0]=> cart, [1]=> userid, {optional: [2]=> non-default cc token , [3] => manual one-time use cc)
 * @param callback
 * @return {ApiResponse}
 */
exports.finalize = {
	method:'POST'
	,path:'/cart/action/finalize'
	,config:{
		description:'Validates a new cart for payment and porder fulfillment'
		,tags:['api']
		,response:{
			schema:joi.object({
				cart_id:joi.string().length(24).alphanum().required()
			}).unknown()
		}
		,validate:{
			query:validators.query.clone()
			,payload:toJoi( Cart.schema )
						.keys({
							subtotal:joi.number().optional().integer().description("Not really needed. subtotal is calculated from product price")
						})
						.rename('cartItems','items')
						.rename('shipping','delivery')
						.rename('subTotal', 'subtotal')
		}
		,handler:function finalize(request, reply) {
			//var cart = params.cart;
			var cartMgr = new CartManager(request.payload);
			async.waterfall([
				//finalize
				function( cb ){
					cartMgr.finalize( cb );
				}

				// validate
				,function( payment, cb ){
					cartMgr.validate( cb )
				}

				// save
				,function( payment, cb ){
					cartMgr.save( cb )
				}
			], function(err, payment){
				var resp = reply( err ? new Boom.badRequest( err.message, err.errors ) : payment  )
				resp.code && resp.code(201)
			})
		}
	}
}

/**
 * @param params ([0]=> cart, [1]=> userid, {optional: [2]=> non-default cc token , [3] => manual one-time use cc)
 * @param callback
 * @return {ApiResponse}
 */
exports.cart_checkout = {
	method:'POST'
	,path:'/cart/{cart_id}/action/checkout'
	,config:{
		tags:['api']
		,description:'Processes payment and sets a previously validated cart to be fulfilled'
		,validate:{
			query:validators.query.clone()
			,params:joi.object({
				cart_id:joi.string().length(24).alphanum()
			}).rename('id','cart_id')
			,payload:joi.object({
				card:joi.alternatives(joi.string(), joi.object()).required().description("Stripe charge Token or Object with Credict card info")
				,total:joi.number().optional()
				,id:joi.string().optional()
				,shipping:joi.object().optional().unknown()
				,billing:joi.object().optional().unknown()
				,payment:joi.object().optional().unknown()
				,subtotal:joi.number().optional().integer()
				,delivery:joi.object().optional()
				,cartItems:joi.array().optional()
				,items:joi.array().optional()

			})
			.rename('cartItems','items')
			.rename('shipping','delivery')

		}
		,handler:function checkout(request, reply) {
			var cartMgr = new CartManager();

			cartMgr.load( request.params.cart_id, function(err, cart ){
				if( err ){
					logger.error( err )
					return reply( new Boom.wrap( err ) );
				}
				cartMgr.checkout(request.payload.card, request.payload.total, function( err, charge ){
					reply( err ? new Boom.badRequest(err) : charge )
				});
			});
		}
	}
}

exports.cart_getBroken = {
  method: 'GET'
  , path: '/cart/admin/broken'
  , handler: function (request, reply) {
    Cart.find({'monitoring.lifeCycle.status': 'fulfill_error'}, function (err, carts) {
      if (err) {
	        logger.error( err )
					return reply( new Boom.wrap( err ) );
      }
      reply ( carts )
    } );
  }
};
exports.cart_getCreated = {
  method: 'GET'
  , path: '/cart/admin/suspicious'
  , handler: function (request, reply) {
    Cart.find({'$or': [{'monitoring.lifeCycle.status': 'fulfill_created', "createdOn": {"$gte": new Date("2015-02-04T00:00:00.000Z")}}, {'monitoring.lifeCycle.status': 'fulfill_received', 'monitoring.externalOrderId': {'$exists': false}}]}, function (err, carts) {
      if (err) {
	        logger.error( err )
					return reply( new Boom.wrap( err ) );
      }
      reply ( carts )
    } );
  }
};

exports.cart_getByChargeId = {
  method: 'GET'
  , path: '/cart/admin/getByCharge/{charge_id}'
  , handler: function (request, reply) {
    async.waterfall([
      function (callback) {
        var chargeId = request.params.charge_id;
        Order.find({'paymentConfirmationCode': chargeId}, function (err, order) {
          if (err || order.length != 1) {
            return  reply (new Boom.notFound("No order by that charge or Error"));
          } else {
            callback(null, order[0]);
          }
        });
      },
      function (order, callback) {
        Cart.find({'assignedOrderId': order._id }, function (err, cart) {
          if (err || cart.length != 1) {
             return reply (new Boom.notFound ("No cart found with that order or error"));
          } else {
             callback(null, cart[0]);
          }
        });
      }
    ], function (err, result) {
      if (err) {

      } else {
         return reply(result);
      }

    });
  }
};

exports.cart_getBySpreadshirtId = {
  method: 'GET'
  , path: '/cart/admin/spreadshirt/{spreadshirt_id}'
  , handler: function (request, reply) {
    Cart.find({'monitoring.externalOrderId': request.params.spreadshirt_id}, function (err, cart) {
      if (err || cart.length != 1) {
					return reply( new Boom.notFound( "Not found or DB error" ) );
      }  else {
        reply ( cart[0] )
      }
    } );
  }
};

 /**
  * DEPRICATED. Here for legacy codeship check url.
  */
 exports.checkoutguest= {
	method:'GET',
	path:'/cart/getcart',
	handler:function( request, reply ){
		reply(new Cart())
	}
 }
