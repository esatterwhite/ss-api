/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Primary events module for alice-cart
 * @module alice-cart/events
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires events
 */

var events = require( 'events' )
  ;

module.exports = new events.EventEmitter();
module.exports.setMaxListeners( 50 );
