/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * Generates a one of token and charge to stipe and prints the output
 * @module alice-core/commands/stripe_charge
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires seeli
 * @requires stripe
 * @requires module:alice-conf
 * @requires module:alice-stdlib/date
 */

var cli    = require( 'seeli' )
  , conf   = require( '../../alice-conf' )
  , stripe = require( 'stripe' )( conf.get('stripe:apiKey'))
  , date   = require('alice-stdlib/date')
  , number = require('alice-stdlib/number')
  , now    = new Date() // used to generate default date values
  ;


/**
* Generates an ad-hoc chage to stripe
* @instance
* @name stripe_charge
* @memberof module:alice-core~commands
* @property {Object} flags Command line flags
* @property {String} flags.amount amount to charge the card
* @property {Boolean} [flags.token=false] if true will return a valid card token for the supplied infomration
* @property {String} [flags.currency=usd] curreny type to pay in
* @property {String} [flags.card=4242424242424242] credit card number to charg
* @property {String} [flags.card_month=09] the two digit exp month on the cart
* @property {String} [flags.card_year=12] the two digit exp year on the card 
* @property {String} [flags.cvs=123] the three digit security code on the card
*/
module.exports = new cli.Command({
	description:'Generates an one off token and places an ad-hoc charge to stripe'
	,usage:[
		cli.bold('Usage: ') + 'alice stripe_charge -i'
		,cli.bold('Usage: ') +' -c 1111111111111111 -m 10 -y 15 -a 30'
	]
	,flags:{
		card:{
			type:Number
			,default:'4242424242424242'
			,description:'Credit Card number to charge'
			,shorthand:'c'
			,choices:[
				'4242424242424242',
				'4012888888881881',
				'4000056655665556',
				'5555555555554444',
				'5200828282828210',
				'5105105105105100',
				'378282246310005',
				'371449635398431',
				'6011111111111117',
				'6011000990139424',
				'30569309025904',
				'38520000023237',
				'3530111333300000',
				'3566002020360505',
				'4000000000000010',
				'4000000000000028',
				'4000000000000036',
				'4000000000000044',
				'4000000000000101',
				'4000000000000341',
				'4000000000000002',
				'4000000000000127',
				'4000000000000069',
				'4000000000000119'
			]
		}
		,'card_month':{
			description:'Month of card expirtation (mm)'
			,default: date.format( now, '%m')
			,type:String
			,shorhand:'m'
			, choices:[ '01','02','03','04','05','06','07','08','09','10','11','12' ]
			, validate: function( value ){
				var _value = parseInt( value, 10 )
				var valid = value.length == 2 && !isNaN( _value ) && number.between( _value, 1, 12 )
				return valid ? true : 'enter a valid month code between 01 - 12'
			}
		}

		,'card_year':{
			description:'Year of card expirtation (yy)'
			,default: date.format( now, '%y')
			,type:String
			,shorhand:'y'
			,validate: function( value ){
				var _value = parseInt( value, 10  )
				return value.length == 2 && !isNaN( _value )
			}
		}
		,cvc:{
			description:'Credit Card security code'
			,type:Number
			,default:123
			,shorthand:'v'
		}
		, token: {
			type: Boolean
			,description: 'Only generate a card token'
			,default: false
			,shorthand: 't'
		}
		,amount:{
			type:Number
			,description: 'Dollar( $ ) amount to charge to the card'
			,default: 10
			,shorthand:'a'
		}
		,currency:{
			type:String
			,description: '3-letter ISO currency code'
			,default:'usd'
		}
	}

	,run: function run( cmd, data, done ){

		// generate a one off token
		stripe.tokens.create({
			card:{
				number: data.card
				,exp_month: data.card_month
				,exp_year: data.card_year
				,cvc: data.cvc
			}
		}, function( err, token ){
			// token && console.log('Card Token: %s', token.id )
			if( data.token ){
				return done( err, token && token.id )
			}
			// send the charge
			stripe.charges.create({
				card: token.id
				,currency: data.currency
				,amount:(data.amount * 100 )
				,description: 'Test transaction ' + (+new Date())
			}, function( e, resp ){
				done(e, resp ? JSON.stringify( resp , null, 2 ) : '' )
			})
		});
		return '';
	}
});
