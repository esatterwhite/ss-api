/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Handles Order and payment mangement for the alice platform
 * @module alice-cart
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires alice-cart/events 
 * @requires alice-cart/models/cart 
 * @requires alice-cart/models/item
 * @requires alice-cart/models/order
 * @requires alice-cart/models/paymentdetails
 * @requires alice-cart/models/salesTaxRates
 * @requires alice-cart/models/shipRates
 * @requires alice-cart/lib/cartmanager
 * @requires alice-cart/lib/exceptions
 * @requires alice-cart/lib/paymentprocessor
 */


module.exports = require('./events');



/**
 * @property {Object} models Contains all of the defined model classes for the cart package
 * @property {Model} models.cart The internal Shopping Cart model
 * @property {Model} models.item Abstraction for holding products inside of a cart
 * @property {Model} models.order The primary Order model used for product fulfillment
 * @property {Model} models.paymentdetails Reusable schema for collecting payment information
 * @property {Model} models.salesTaxRates Model representing sales tax rates for specific states
 * @property {Model} models.shipRates A model representing the types and rates for various shipping methods
 **/
module.exports.models = {
	cart           : require('./models/cart')
  , item           : require('./models/item')
  , order          : require('./models/order')
  , paymentdetails : require('./models/paymentdetails')
  , salesTaxRates  : require('./models/salesTaxRates')
  , shipRates      : require('./models/shipRates')
}

/**
 * @see module:alice-cart/lib/cartmanager
 * @property {Class} Manager The Primary {@link module:alice-cart/lib/cartmanager|Cart Manager} class
 **/
module.exports.Manager = require('./lib/cartmanager')

/**
 * @see module:alice-cart/lib/exceptions
 * @property {Object} exceptions Defined exception classes used the cart package
 **/
module.exports.exceptions = require('./lib/exceptions')

/**
 * @see module:alice-cart/lib/paymentprocessor
 * @property {Class} Payment The {@link module:alice-cart/lib/paymentprocessor|base class} responsible for dealing with internal payments
 **/
module.exports.Payment = require('./lib/paymentprocessor')
