/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * events handlers for cart related events.
 * @module alice-cart/listeners
 * @author Eric Satterwhite
 * @since 0.2.0
 * @requires debug
 * @requires alice-conf
 * @requires alice-core
 * @requires alice-core/lib/loading/templates
 * @requires alice-core/events
 */

var debug     = require( 'debug' )('alice:cart:listeners')
  , conf      = require('alice-conf')
  , events    = require('./events')
  , util      = require('util')
  , logger    = require('alice-log')
  , templates = require('alice-core/lib/loading/templates')
  , stripe    = require('stripe')(conf.get('stripe:apiKey') )
  , mail      = require('alice-core').mail
  ;



debug('adding hander for order_created')
events.on('order_created',function( order, cart ){
	var to =  order.billing.email || order.shipping.email || null

	if( to ){
		logger.notice('sending confirmation order for %s', order._id.toString())
		mail.send({
			to:to
			,from:{name:'Spirit Shop',address:'support@spiritshop.com'}
			,replyTo:'support@spiritshop.com'
			,sender:"Spirit Shop"
			,subject:"Your SpiritShop.com Order has been Placed"
			,template:'confirmation.eml'
			,data:{order:order.toObject(), cart:cart}
		})
	}
})
