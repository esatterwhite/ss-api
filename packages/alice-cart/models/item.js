/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Represents a single item in a cart
 * @module alice-cart/models/item
 * @author Ryan Fisch
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires mongoose
 * @requires alice-catalog/models/product
 * @requires alice-catalog/models/vignette
 * @requires alice-core/lib/db
 * @requires alice-catalog/models/tenant
 */
var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectId
    , Product = require('alice-catalog/models/product')
    , Decal = require('alice-catalog/models/vignette')
    , connection = require('alice-core/lib/db').connection
    , Tenant = require('alice-catalog/models/tenant')
    ;
var cartItemSchema = new Schema({
    tenant: {
        id: {type: ObjectId, ref: "Tenant"},
        name: {type: String},
        mascot: {type: String}
    },
    product: {
        id: {type: ObjectId, ref: "Product"},
        name: {type: String},
        brand: {type: String},
        description:{type:String},
        manufacturer: {
          internalId: {type: Number},
          name: {type: String}
      },
      color: {
      manufacturerId: { type: String},
      hex:  {type: String}
      },
      size: {
        manufacturerId: {type: String},
        name: {type: String}
    }
    },
    decal: {
       id: {type: ObjectId, ref: "Decal"},
       activity:{type:String}
    },
    printUrls: {
        front: {type: String},
        back: {type: String}
    },
    decalColors: {
        clrp: {type: String},
        clrs: {type: String},
        clrt: {type: String},
        clrw: {type: String},
        clrb: {type: String}
    },
    unitPrice:{type:Number},
    size: {type: Number},
    quantity: {type: Number},
    personalization: [
        {
            name:{type:String, enum:['txtpnm', 'txtpnbr', 'date1', 'date2']},
            value:{type:String}
        }
    ],
    images:[
    {
      view:{type:String, enum:['front', 'back', 'sleeve']},
      size:{type:String, enum:['thumb', 'preview']},
      url:{type:String}
    }]

},{collection:'cart_item'});
module.exports = connection.commerce.model('CartItem', cartItemSchema);
module.exports.Schema = cartItemSchema;
