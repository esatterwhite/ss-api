/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * Provides the Payment detail Schema used by the Cart Schema
 * @module alice-cart/models/paymentdetails
 * @author Eric Satterwhite
 * @author Ryan Fisch
 * @since 1.9.0
 * @requires mongoose
 * @requires alice-stdlib/number
 * @requires alice-stdlib/lang
 * @requires alice-core/lib/db
 */

var mongoose        = require('mongoose') // Mongo ODM module
  , Schema          = mongoose.Schema // Document schema Class
  , number          = require('alice-stdlib/number') // Standard Number module
  , isInteger       = require("alice-stdlib/lang").isInteger // Standard lib function to determine integer types
  , connection      = require('alice-core/lib/db').connection // Internal DB Connection
  ;


/**
 * Be cause this is mostly an embeded document in an array, we can't blindly try to convert
 * becuase Instanciation calls setters, AND SchemaArray.push calls setters. which would result in
 * ( value * 100 ) * 100
 * @private
 */
var toCents = function toCents( value ){
    // if it is an integer, or a string that looks like an integer
    // parseFloat will return an integer.
    // this is here to catch cents passed as strings - "450" ~ $4.50
    value = parseFloat( value );
    return isInteger( value ) ? value : number.setCents( value );
};

/**
 * schema used to collect payment information about a particular cart
 * primarily used as an embeded document
 * @constructor
 * @alias module:alice-cart/models/paymentdetails
 * @extends mongoose.Model
 * @param {Object} fields
 * @param {Number} fields.checkSum Not used (? )
 * @param {Number} [fields.shippingCost=0]  Cost of shipping for associated cart.
 * @param {Number} [fields.tax=0] The amount of tax to apply to final charge, in cents.
 * @param {Number} fields.taxRate The amount of tax as defined by the state, as a percentage ( 0.0625 )
 * @param {Array}  fields.discounts Unused( ? )
 * @param {Number} [fields.total=0] Total amount to charge in cents
 * @param {Number} [fields.subtotal=0] If given a double precision number, will assume dollars and convert it to cents
 */
var paymentDetailsSchema = new Schema(/* @lends module:models/paymentdetails.Detail.prototype */{

    /**
     * @depricated
     * @memberof module:alice-cart/models/paymentdetails
     * @property {String} checkSum
     */
    checkSum: {
        type: String
    }
    /**
     * @name shippingCost
     * @instance
     * @memberof module:alice-cart/models/paymentdetails
     * @property {Number} shippingCost Cost of shipping for associated cart
     */
    ,shippingCost: {
        type: Number
        , default:0
    }

    /**
     * If given a double precision number, will assume dollars and convert it to cents
     * @name tax
     * @instance
     * @memberof module:alice-cart/models/paymentdetails
     * @property {Number} tax The amount of tax to apply to final charge, in cents.
     */
    ,tax: {
        type: Number
        , default:0
    }

    /**
     * The amount of tax as defined by the state, as a percentage ( 0.0625 )
     * @name taxRate
     * @instance
     * @memberof module:alice-cart/models/paymentdetails
     * @property {Number} taxRate
     */
    ,taxRate: { type: Number}

    /**
     * @name discounts
     * @instance
     * @memberof module:alice-cart/models/paymentdetails
     * @property {Object[]} discounts Unused( ? )
     */
    ,discounts: [{
        description: {
            type: String
        }
        ,amount: {
            type: Number
            , default:0
        }
    }]

    /**
     * @name total
     * @instance
     * @memberof module:alice-cart/models/paymentdetails
     * @property {Number} total Total amount to charge in cents
     */
    ,total: {
        type: Number
        , default:0
    }

    /**
     * @name subtotal
     * @instance
     * @memberof module:alice-cart/models/paymentdetails
     * @property {Number} subtotal If given a double precision number, will assume dollars and convert it to cents
     */
    ,subtotal: {
        type: Number
        , default:0
    }
},{collection:'cart_paymentdetail'});


module.exports = connection.commerce.model('PaymentDetail', paymentDetailsSchema);
module.exports.Schema = paymentDetailsSchema
