/*jshint laxcomma: true, smarttabs: true, node:true */
'use strict';
/**
 * Model for transfering product information into payment processing and fulfillment
 * @module alice-cart/models/cart
 * @author Ryan Fisch
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires mongoose
 * @requires alice-cart/models/item
 * @requires alice-cart/models/order
 * @requires alice-cart/models/paymentdetails
 * @requires alice-core/lib/constants
 * @requires alice-core/lib/db
 * @requires alice-stdlib/number
 */

var mongoose       = require('mongoose')
  , logger         = require('alice-log')
  , number         = require('alice-stdlib/number')
  , StatesEnum     = require('alice-core/lib/constants').states
  , connection     = require('alice-core/lib/db').connection
  , get            = require('alice-stdlib/object').get
  , CartItem       = require('./item')
  , Order          = require('./order')
  , PaymentDetails = require('./paymentdetails')
  , pkgevents      = require( '../events')
  , Schema         = mongoose.Schema
  , ObjectId       = Schema.ObjectId
  , Cart
  ;

/**
 * Represents a data base schema for an individual shopping cart which is transferable to an order
 * @constructor
 * @alias module:alice-cart/models/cart
 * @param {Object} fields
 * @param {Date} [fields.createdOn=Date.now()] The Date the cart was created
 * @param {Date} [fields.lastUpdatedOn=Date.now()] The date the cart was last modified
 * @param {Number} [fields.subTotal=0] The subtotal on the cart. Calculated from the intems
 * @param {Object[]} [fields.paymentDetails] An array of {@link module:alice-cart/models/paymentdetails|payment detail} information.
 * @param {Object[]} [fields.items] An array of {@link module:alice-core/models/item|Cart Items} to be added to the final order
 * @param {?String} fields.assignedOrderId An Object id referencing the {@link module:alice-cart/models/order|Order} which was generated from this cart
 * @param {String} fields.appliedPromotionalCode
 * @param {Object} fields.delivery
 * @param {String} fields.delivery.firstName First name of the person to receive delivery
 * @param {String} fields.delivery.lastName last name of the person to receive delivery
 * @param {String} fields.delivery.address1 First line of the address of the person to receive delivery
 * @param {String} fields.delivery.address2 Second line of the address of the person to receive delivery
 * @param {String} fields.delivery.city City of the person to receive delivery
 * @param {String} fields.delivery.state State of the person to receive delivery
 * @param {String} fields.delivery.postalCode Postal Code of the person to receive delivery
 * @param {String} [fields.delivery.shippingType=shipping] The shipping type to be used for delivery
 * @param {Object} fields.monitoring
 */
Cart = new Schema({
   /**
    * @name createdOn
    * @instance
    * @memberof module:alice-cart/models/cart
    * @property {Date} createdOn The date at which the cart was first created
    **/
    createdOn              : { type: Date,   default : Date.now() }
   /**
    * @name lastUpdatedOn
    * @instance
    * @memberof module:alice-cart/models/cart
    * @property {Date} lastUpdatedOn The Date at which the cart was last updated
    **/
  , lastUpdatedOn          : { type: Date,   default : Date.now() }
   /**
    * @name subTotal
    * @instance
    * @memberof module:alice-cart/models/cart
    * @property {Number} subTotal The initial subtotal for all items in the cart
    **/
  , subTotal               : { type: Number, default : 0 }
  , payment: {type:Object, required:false}
   /**
    * @name paymentDetails
    * @see module:alice-cart/models/paymentdetails
    * @instance
    * @memberof module:alice-cart/models/cart
    * @property {Array} paymentDetails An array Payment information reguarding the cart
    **/
  , paymentDetails         : [ PaymentDetails.schema ]
   /**
    * @name items
    * @see module:alice-cart/models/item
    * @instance
    * @memberof module:alice-cart/models/cart
    * @property {Array} items An array of items to be included in the final order
    **/
  , items                  : [ CartItem.schema ]
   /**
    * @name assignedOrderId
    * @instance
    * @see module:alice-cart/models/order
    * @memberof module:alice-cart/models/cart
    * @property {ObjectId} assignedOrderId An Object id referencing the Order which was generated from this cart
    **/
  , assignedOrderId        : { type: ObjectId, ref: 'Order', default:null }
   /**
    * @name appliedPromotionalCode
    * @instance
    * @memberof module:alice-cart/models/cart
    * @property {String} appliedPromotionalCode The promotion code that was used during the checkout process
    **/
  , appliedPromotionalCode : { type:String } //potential candidate for validator once we have promo sequence identified

   /**
    * @name delivery
    * @instance
    * @memberof module:alice-cart/models/cart
    * @property {Object} delivery Final delivery information for the cart
    * @property {String} delivery.firstName First name of the person to receive delivery
    * @property {String} delivery.lastName last name of the person to receive delivery
    * @property {String} delivery.address1 First line of the address of the person to receive delivery
    * @property {String} delivery.address2 Second line of the address of the person to receive delivery
    * @property {String} delivery.city City of the person to receive delivery
    * @property {String} delivery.state State of the person to receive delivery
    * @property {String} delivery.postalCode Postal Code of the person to receive delivery
    * @property {String} delivery.shippingType The shipping type to be used for delivery
    **/
  , delivery: {
        firstName    : {type: String, required: true, default:'' }
      , lastName     : {type: String, required: true, default:'' }
      , address1     : {type: String, required: true, default:'' }
      , address2     : {type: String, required: false, default:'' }
      , companyName  : {type: String, required: false, default:'' }
      , city         : {type: String, required: true, default:'' }
      , state        : {type: String, required: true, default:'', enum:StatesEnum }
      , postalCode   : {type: String, required: true, default:'' }
      , shippingType : {type: String, enum: ['standard', 'premium', 'nextday'],  default: 'standard'}
      , email        : {type:String, required:false }
    }
  , billing: {
        firstName    : {type: String, required: true, default:'' }
      , lastName     : {type: String, required: true, default:'' }
      , address1     : {type: String, required: true, default:'' }
      , address2     : {type: String, required: false, default:'' }
      , companyName  : {type: String, required: false, default:'' }
      , city         : {type: String, required: true, default:'' }
      , state        : {type: String, required: true, default:'', enum:StatesEnum }
      , postalCode   : {type: String, required: true, default:'' }
      , email        : {type:String, required:false }
      , shippingType : {type: String, enum: ['standard', 'premium', 'nextday'],  default: 'standard'}
    }
  /**
   * @name monitoring
   * @instance
   * @memberof module:alice-cart/models/cart
   * @property {Object} monitoring Information about the carts' lifecycle
   * @property {String} monitoring.activity.status Currently unused indicator of the Cart's age
   * @property {String} monitoring.lifeCycle.status Indicator of the Cart's status (new, paid, ordered....)
   * @property {String} monitoring.lifeCycle.message Possible error messages from External Vendor
   * @property {String} monitoring.externalOrderId Order ID assigned from External Vendor
   **/
  , monitoring: {
        activity: {
            status : {type: String, enum: ['active', 'cooling', 'cold'], default:'active'}
        }

      , lifeCycle: {
            status : {type: String, enum: ['new', 'shopping', 'paid', 'ordered', 'abandoned', 'fulfill_created', 'fulfill_received', 'fulfill_error'], default: 'new'}
          , message : { type:String }

        }
      , externalOrderId: { type: String }
    }
},{collection:'cart_shoppingcart'});

/**
 * Method to determine if new items may be added to the cart
 * @instance
 * @deprecated since version 0.1.0
 * @function mutable
 * @memberof module:alice-cart/models/cart
 * @return {Boolean} mutable
 **/
Cart.methods.mutable = function( ){
    return ['new','shopping'].indexOf( this.monitoring.lifeCycle.status ) >= 0
};

Cart.pre('save', function (next) {

    if( this.isDirectModified('monitoring.lifeCycle.status') ){
        var status = get( this,'monitoring.lifeCycle.status' )
        if( status == 'paid' ){
            var shippingtotal = 0
              , data = this.toObject()
              , that = this
              , tax = 0
              , total = 0
              , order
              ;

            data.paymentDetails.forEach( function( pd ){
                shippingtotal += pd.shippingCost
                tax           += pd.tax;
                total         += pd.total
            });

            /**
             * Emitted when a cart instance transitions to paid for the first time
             * @name module:alice-cart#cart_paid
             * @event
             * @param {Cart} cart the cart instance that was paid
             **/
            pkgevents.emit('cart_paid', this )
            logger.notice('cart %s has transitioned to paid', this._id.toString() )
            logger.warning("Generating new Order")

            order = new Order({
                customer      : data.delivery
              , shipping      : data.delivery
              , manufacturers : [{ name: 'spreadshirt'}]
              , subTotal      : data.subTotal
              , shippingTotal : shippingtotal
              , cart: this._id
              , paymentConfirmationCode: data.payment ? data.payment.id : null
              , tax           : tax
              , total         : data.subTotal + shippingtotal + tax
              , orderItems    : data.items
              , billing       : data.billing
            });


            return order.save( function(err, o ){
                if( err ){
                   return logger.error( err.message, logger.exception.getAllInfo( err ) );
                }
                /**
                 * Emitted when an {@link module:alice-cart/models/Order|order} instance is generated from a cart being paid for the first time
                 * @name module:alice-cart#order_created
                 * @event
                 * @param {Order} order an order that was generated from a cart instance
                 **/
                pkgevents.emit('order_created', o, data )
                that.assignedOrderId = o._id;
                next();
            })
        }
        next()
    } else {
        next()
    }
});
/*
cartSchema.statics.transformUserCart = function (userCart) {
  var newItems = [];
  for (var i = 0; i < userCart.items.length;  i++) {
     newItems.push(CartItem.transformUserCartItem(userCart.items[i]));
  }
  userCart.items = newItems;
  return userCart;
};
*/
function createCartChecksum(cartItems) {
    //implemlent off of cart items.
}

module.exports = connection.commerce.model('Cart', Cart);
