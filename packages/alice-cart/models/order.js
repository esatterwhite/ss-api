/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Represents a completed cart that has been processed and read for fulfillment
 * @module alice-cart/models/order
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires mongoose
 * @requires alice-cart/models/item
 * @requires alice-core/lib/constants
 * @requires alice-core/lib/db
 */
var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectId
    , CartItem = require('./item')
    , constants = require('alice-core/lib/constants')
    , AffiliatesCodes = constants.affiliateCodes
    , StatesEnum = constants.states
    , connection = require('alice-core/lib/db').connection;

var orderSchema = new Schema({
    customer: {
        firstName: {type: String},
        middleInitial: {type: String},
        lastName: {type: String}
    },
    pk:{
        type: Number
    },
    cart:{type:ObjectId, unique:true, index:true},
    shipping: {
        firstName: {type: String, required: true},
        lastName:{type: String, required: true},
        address1: {type: String, required: true},
        address2: {type: String, required: false},
        city: {type: String, required: true},
        state: {type: String, required: true, enum: StatesEnum},
        postalCode: {type: String, required: true},
        email        : {type:String, required:false }

    },
    billing: {
        firstName: {type: String, required: true},
        lastName:{type: String, required: true},
        address1: {type: String, required: true},
        address2: {type: String, required: false},
        city: {type: String, required: true},
        state: {type: String, required: true, enum: StatesEnum},
        postalCode: {type: String, required: true},
        email        : {type:String, required:false }
    },
    paymentGateway:{type:String, enum:['stripe'], default:'stripe'},
    paymentConfirmationCode: {type: String},
    manufacturers: [
        {
            name: {type: String, enum: ['era', 'spreadshirt'], default:'spreadshirt'}
        }
    ],
    orderStatus: {type: String, enum: ['queued', 'transferring', 'transferred', 'shipped', 'cancelled'], default:'queued'},
    updated: {type: Date, default: Date.now() },
    subTotal: {type: Number, default:0},
    shippingTotal: {type: Number, default:0},
    tax: {type: Number, default:0},
    discounts: {type: Number, default:0},
    total: {type: Number, default:0},
    orderItems: [CartItem.schema],
    fulfillmentNotes: [
        {
            source: {type: String},
            type:{type:String, enum:['info', 'error', 'warning']},
            postedOn: {type: String},
            note: {type: String}
        }
    ],
    appliedPromotionalCode:{type:String}, //potential candidate for validator once we have promo sequence identified
    affiliate:{type:String, enum:AffiliatesCodes}
},{collection:'catalog_order'});

function getDollars(num) {
    return (num / 100).toFixed(2);
}

function setCents(num) {
    return num * 100;
}

orderSchema.pre('save', function( next ){
    // auto generate a new sequenct ID
    this.pk = this.pk || ( process.hrtime()[1] );
    next();
})
module.exports = connection.commerce.model('Order', orderSchema);


