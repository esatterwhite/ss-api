/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Represents tax rates for individual states
 * @module alice-cart/models/salesTaxRates
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires mongoose
 * @requires alice-core/lib/db
 */

var connection = require('alice-core/lib/db').connection;
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var salesTaxRateSchema = new Schema({
    state: {type: String},
    rate: {type: Number, default:0},
},{collection:'cart_salestaxrate'});

module.exports = connection.commerce.model('SalesTax', salesTaxRateSchema);
