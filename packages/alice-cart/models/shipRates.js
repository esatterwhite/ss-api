/*jshint laxcomma: true, smarttabs: true, node:true */
'use strict';
/**
 * represents fee schedules for variause shipping types and carriers
 * @module alice-cart/models/shipRates
 * @author Eric Satterwhite 
 * @author Ryan Fisch
 * @since 0.1.0
 * @requires mongoose
 * @requires alice-core/lib/db
 */

var connection = require('alice-core/lib/db').connection
  , mongoose   = require('mongoose')
  , Schema     = mongoose.Schema
  , Rate
  ;

/**
 * @constructor
 * @alias module:alice-cart/models/shipRates
 * @param {Object} fields
 * @param {String} fields.type Type of shipping method can be one of `standard`, `premium`, `nextday`
 * @param {String} fields.description Description of estimated delivery time
 * @param {Number} [fields.orderMinimum=0] The minimum allowable amount required for the method
 * @param {Number} [fields.orderMaximum=0] The maximum allowable amount required for the shipping method
 * @param {String} [fields.type=flat] The type of rate to apply to cart totals. Can be one of `flat`, or `percentage`
 * @param {Number} [fields.rate=0] The rate to apply to an order total
 */
Rate = new Schema({
	/**
	 * @name method
	 * @instance
	 * @memberof module:alice-cart/models/shipRates
	 * @property {String} method defined shipping method
	 **/
    method         : {type: String, enum:['standard', 'premium', 'nextday'], required:true},
    /**
     * @name orderMinimum
     * @instance
     * @memberof module:alice-cart/models/shipRates
     * @property {Number} orderMinimum mimimum allowable amount on the shipping type
     **/
    orderMinimum : {type: Number, default:0},
    /**
     * @name orderMaximum
     * @instance
     * @memberof module:alice-cart/models/shipRates
     * @property {Number} orderMaximum the maximum allowable amount on the shipping type
     **/
    orderMaximum : {type: Number, default:0},
    /**
     * @name type
     * @instance
     * @memberof module:alice-cart/models/shipRates
     * @property {String} type type of rate to apply to totals
     **/
    type     : {type: String, enum:['flat', 'percentage'], default:'flat'},
    /**
     * @name rate
     * @instance
     * @memberof module:alice-cart/models/shipRates
     * @property {Number} rate The final type rate to apply. If `type` is a percentage, this should be a value between `0` and `100`
     **/
    rate         : {type: Number, default:0 },
    /**
     * @name description
     * @instance
     * @memberof module:alice-cart/models/shipRates
     * @property {String} description A description of the expected delivery times
     **/
    description  : {type: String, required:true}
},{collection:'cart_shippingrate'});

Rate.methods.calculate = function( cost ){
	return this.type == 'flat' ?  this.rate : Manth.round(( cost * ( this.rate / 100 ) ) )
}
module.exports = connection.commerce.model('ShippingRate', Rate);
