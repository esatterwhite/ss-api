/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * Provides payment and transaction functionality
 * @module alice-cart/lib/paymentprocessor
 * @author Eric Satterwhite
 * @author Ryan Fisch
 * @since 1.9.0
 * @requires util
 * @requires events
 * @requires stripe
 * @requires alice-conf
 * @requires alice-log
 * @requires alice-stdlib/class
 * @requires alice-stdlib/class/options
 * @requires alice-stdlib/number
 */

var conf         = require('alice-conf')
  , logger       = require('alice-log')
  , Class        = require('alice-stdlib/class')
  , Options      = require('alice-stdlib/class/options')
  , EventEmitter = require('events').EventEmitter
  , stripe       = require('stripe')(conf.get('stripe:apiKey') )
  ;
logger.debug("Using stripe key: " + conf.get('stripe:apiKey'));
stripe.setApiVersion(conf.get('stripe:apiVersion') );
/**
 * Provides an interface to the stripe interface
 * @alias module:alice-cart/lib/paymentprocessor
 * @constructor
 */
var PaymentService = new Class({
    inherits:EventEmitter
    ,mixin:[ Options ]
    ,options:{
        card:{
            number:'4242424242424242'
            ,month:new Date()
            ,year: new Date()
            ,cvc: 123
        }
    }
    ,constructor: function( options ){
        this.setOptions( options );
    }

    /**
     * Creates a new stripe customer
     * @method module:alice-cart/lib/paymentprocessor#createCustomer
     * @param {Object} data its the data...
     **/
    , createCustomer: function createCustomer(data) {
        var self = this;
        self.emit('start', arguments);
        stripe.customers.create(data, function (err, customer) {
            if (err) self.emit('error', err);
            else self.emit('done', customer);
        });
    }

    /**
     * Retreives a Stripe Customer
     * @method module:alice-cart/lib/paymentprocessor#getCustomer
     * @param {String} id The customer id
     **/
    , getCustomer: function getCustomer(customerId) {
        var self = this;
        self.emit('start', arguments);
        stripe.customers.retrieve(customerId, function (err, customer) {
            if (err) self.emit('error', err);
            else self.emit('done', customer);
        });
    }

    /**
     * deletes a stripe customer
     * @method module:alice-cart/lib/paymentprocessor#deleteCustomer
     * @param {String} id The stipe customer id
     **/
    , deleteCustomer: function deleteCustomer(customerId) {
        var self = this;
        self.emit('start', arguments);
        stripe.customers.del(
            customerId,
            function (err, confirmation) {
                if (err) self.emit('error', err);
                else self.emit('done', confirmation);
            }
        );
    }

    /**
     * Updates a customer with the supplied data
     * @method module:alice-cart/lib/paymentprocessor#updateCustomer
     * @param {String} id stripe customer id
     * @param {Object} data Its some data
     **/
    , updateCustomer: function updateCustomer(customerId, data) {
        var self = this;
        self.emit('start', arguments);
        stripe.customers.update(customerId, data, function (err, customer) {
            if (err) self.emit('error', err);
            else self.emit('done', customer);
        });
    }

    /**
     * Creates a new credit card with stripe
     * @method module:alice-cart/lib/paymentprocessor#createCard
     * @param {String} id The stripe Customer id
     * @param {Object} data Some data
     **/
    , createCard: function createCard(customerId, data) {
        var self = this;
        self.emit('start', arguments);
        stripe.customers.createCard(
            customerId,
            data,
            function (err, card) {
                if (err) self.emit('error', err);
                else self.emit('done', card);
            }
        );
    }

    /**
     * Fetches credit card information from stripe
     * @method module:alice-cart/lib/paymentprocessor#getCard
     * @param {String} id Customer id
     * @param {String} card Stripe credit card id
     **/
    , getCard: function getCard(customerId, cardId) {
        var self = this;
        self.emit('start', arguments);
        stripe.customers.retrieveCard(
            customerId,
            cardId,
            function (err, card) {
                if (err) self.emit('error', err);
                else self.emit('done', card);
            }
        );
    }

    /**
     * Lists all of the credit cards for a customer
     * @method module:alice-cart/lib/paymentprocessor#listCards
     * @param {String} id The customer id
     * @param {Object} data Some data...
     **/
    , listCards: function listCards(customerId, data) {
        var self = this;
        self.emit('start', arguments);
        stripe.customers.listCards(customerId, function (err, cards) {
            if (err) self.emit('error', err);
            else self.emit('done', cards);
        });
    }

    /**
     * Deletes an active credit card from strip backend
     * @method module:alice-cart/lib/paymentprocessor#deleteCard
     * @param {String} customerid stripe id for specific customer
     * @param {String} cardid stripe id for a specific card associated to the aforementioned customer
     **/
    , deleteCard: function deleteCard(customerId, cardId) {
        var self = this;
        self.emit('start', arguments);
        stripe.customers.deleteCard(
            customerId,
            cardId,
            function (err, confirmation) {
                if (err) self.emit('error', err);
                else self.emit('done', confirmation);
            }
        );
    }

    /**
     * Creates a new payment for a stripe customer
     * @method module:alice-cart/lib/paymentprocessor#makePayment
     * @param {Object} data Some data...
     **/
    , makePayment: function makePayment(data, callback) {
        var self = this;
        self.emit('start', arguments);
        logger.debug('starting payment %s for card: %s',data.amount, data.card );
        stripe.charges.create({
            card:data.card
          , amount: data.amount
          , currency: 'usd'
        }, function (err, charge) {
            if (err){
                logger.error(err.message, err );
            } else {
                self.emit('done', charge);
            }
            return callback( err, charge );
        });
    }
});

module.exports = PaymentService;
