/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * Loads configuration form different data stores before the server starts
 * @module alice-cart/lib/cartmanager
 * @author Eric Satterwhite
 * @author Ryan Fisch
 * @since 1.9.0
 * @requires util
 * @requires events
 * @requires mongoose
 * @requires stripe
 * @requires async
 * @requires util
 * @requires underscore
 * @requires alice-log
 * @requires alice-stdlib/class
 * @requires alice-stdlib/exception
 * @requires alice-catalog/models/cart
 * @requires alice-catalog/models/shipTaxRates
 * @requires alice-catalog/models/shipRates
 * @requires alice-catalog/models/paymentdetails
 * @requires alice-catalog/models/product
 */
var EventEmitter     = require( 'events' ).EventEmitter              // Node built-in EventEmitter
	, mongoose         = require( 'mongoose' )                         // npm mongoose module
	, async            = require( 'async' )                            // npm async modle
	, util             = require( 'util' )                             // node util used for formating strings
	, logger           = require( 'alice-log' )                        // alice standard logger
	, number           = require( 'alice-stdlib/number' )
	, Class            = require( 'alice-stdlib/class' )               // Alice standard Class implementation
	, Exception        = require( 'alice-stdlib/exception' )           // Alice Base Exception Class for creating custom errors
	, Cart             = require( 'alice-cart/models/cart' )           // Mongoose Cart Model
	, SalesTaxRate     = require( 'alice-cart/models/salesTaxRates' )  // mongoose SalesTaxRate model
	, ShipRate         = require( 'alice-cart/models/shipRates' )      // mongoose ShipRate model
	, PaymentDetails   = require( 'alice-cart/models/paymentdetails' ) // mongoose PaymentDetails model
	, Promotion        = require( 'alice-catalog/models/promotion')
	, productSchema    = require( 'alice-catalog/models/product' )  // mongoose productSchema model
	, PaymentProcessor = require( './paymentprocessor' )               // internal PaymentProcessor class for handling stripe interactions
	, exceptions       = require( './exceptions' )                     // Cart exceptions classes
	, ObjectId         = mongoose.Types.ObjectId                       // mongoose ObjectId
	, CartManager
	, CartException
	;

// NOTE: I'm not really seeing the usefulness of this. All this does is create a Cart and do stuff with it's data
// The Cart data model seems like a much better place for this stuff.

// TODO: Push logic down to the Data Model itself
/**
 * This is basically a wrapper around a Cart Model that add tax calc methods
 * @constructor
 * @alias module:alice-cart/lib/cartmanager
 * @extends EventEmitter
 */
module.exports = CartManager = new Class({
	inherits: EventEmitter
	,constructor: function( cart ){
		this._cart = new Cart(cart);
	}

	,load: function load( id, callback ){
		var that = this;
		var err = new exceptions.CartNotAvailable({
			message: util.format( "Cart with id: %s was not found", id )
		});

		Cart
		.findById(id, function(e, cart ){
			if(!cart){
				e = err;
			}

			if( cart && !cart.mutable() ){
				var e = new exceptions.CartNotPayable({
					message:"Cart Has already been paid, or no longer payable"
				});
			}
			this._cart = cart;
			callback( e, e ? null: this._cart );
		}.bind( this ));
	}

	, checkout: function checkout( token, total, callback ){
		var that = this;
		var payment = new PaymentProcessor({
			card:token,
			amount: total
		});

		payment.makePayment({
			card:token
			,amount:this._cart.paymentDetails[0].total
		}, function( e , resp ){
			if(!e){
				var orderid = this._cart.assignedOrderId;

				async.series([
					function( cb ){
						var lookup = that._cart.appliedPromotionalCode

						if( !lookup ){
							return cb( null, null )
						}
						Promotion.get( lookup, function( err, promo ){
							try{
								logger.info("applying promo %s to cart %s", promo.code, that._cart._id )
								promo.apply( total )
								cb(null, null )
							} catch( e ){
								return cb(e, null )
							}
						})
					}
					,function( cb ){
						that._cart.monitoring.lifeCycle.status = 'paid';
						that._cart.payment = resp;
						that._cart.save(cb);
					}
				],function( err, results ){
					resp.order_id = orderid
					callback( err, resp );
				})
			} else {
				 callback (e, e);
			}
		}.bind( this ));
	}
	/**
	 * Attempts to verify the price of the cart be re-calculating the price of the cart.
	 * @method module:alice-cart/lib/cartmanager#verify
	 * @param  {Function} callback function executed when the cart is verified or generates an error.
	 **/
	,verify: function verify( cb ){
		// FIXME : This verify function is expanded from OLD code when I got here.
		// It basically verifies that The subtotal of the products in the cart from the front end
		// matches what We add up from the products in the DataBase.
		// Which is silly. Who cares what the Front end says. What is in DB is gospel.
		// Give Me IDs & quantities of products, And I tell you what the price is.
		// Or it's to ensure that we're not showing the user one price and charging them another.

		var that
			, idSearchArray
			, i
			;

		that          = this;
		idSearchArray = [];
		for (i = 0; i < this._cart.items.length; i++) {
			idSearchArray.push(
				new ObjectId( '' + this._cart.items[i].product.id)
			);
		}

		logger.info('looking up products for verification' );
		productSchema.find(
			{'_id': {$in: idSearchArray}}
			, function (err, products) {
				var productMap   // Mapping of product PKs to their respective products
					, cartSubtotal // initial subtotal of the cart
					, item         // loop variable storing product items
					, price        // price of a product
					, carterror    // error generated from cart validation
					, i            // loop counter variable
					;

				productMap = {};
				cartSubtotal = 0;

				// I don't see the necessity for a loop helper. its just a for loop.
				products.forEach(function (product) {
						productMap[product._id] = product;
				});

				for (i = 0; i < that._cart.items.length; i++) {
					item = that._cart.items[i];
					if (!productMap.hasOwnProperty(item.product.id)) {
						carterror = new exceptions.InvalidCartItem({
								message: 'Cart could not be verified. Invalid Product ' + item.product.id
						});
						logger.error('invalide product found: %s' , item.product.id, logger.exception.getTrace( carterror ) );
						return cb && cb( carterror, null );
					}

					var prod = productMap[item.product.id];
					price = prod.economics.price;
					price += item.printUrls.back ? prod.economics.personalization : 0;
					cartSubtotal += ( price * item.quantity );
				}

				that._cart.subTotal = cartSubtotal;
				that.emit('CartVerified', cartSubtotal);
				async.parallel({
					// tax
					tax:function( callback ){
							that.taxRate(
									that._cart.delivery.state
								, callback
							);
					}

					// shipping
					,shipping:function( callback ){
							that.shippingRate(
									 cartSubtotal
								, that._cart.delivery.shippingType
								, callback
							);
					}
					, promotion: function( callback ){
						var lookup = that._cart.appliedPromotionalCode

						if( !lookup ){
							return callback( null, null )
						}
						Promotion.get( lookup, callback  )
					}
				}, function( err, results ){
					var error = err  // error, if their is one
						, pd           // payment details for the cart
						, subtotal     // calculated subtotal
						, shippingCost // associated shipping cost
						, total        // final cart total
						, tax          // the calculated amount of tax to apply to charge
						, taxRate      // the assoicated tax rate for the state
						, promo
						, price
						;

					if( error ){
						logger.error(error.message);
						return cb && cb( error, null );

					}
					subtotal     = that._cart.subTotal;
					shippingCost = results.shipping;

					try {
						promo        = results.promotion && results.promotion.calculate( subtotal )
					} catch ( e ){
						return cb && cb( e, null );
					}
					price        = promo ? promo.cost : subtotal
					total        = price + results.shipping;
					tax          = Math.round( price * results.tax );
					taxRate      = results.tax;
					total        = total +  tax;

					pd          =  new PaymentDetails({
						subtotal      : subtotal
						,shippingCost : shippingCost
						,tax          : tax
						,taxRate      : taxRate
						,total        : total
						,discounts    : promo ? [{description:promo.description, amount: promo.discount }] : undefined
					});

					logger.debug("serialized payment data", JSON.stringify(pd, null, 3));

					logger.debug("cart subtotoal %s - paymentdetail subtotal %s",that._cart.subTotal, pd.subtotal);
					// payment details are in cents
					if( pd.subtotal != that._cart.subTotal ){
						error = new exceptions.InvalidCartTotal({
							message: util.format( 'Cart Total does not match expected total - expected %s, got %s', that._cart.subTotal, pd.subtotal )
						});
						cb && cb( error, null );
						return that.emit('error', error);
					}

					that._cart.paymentDetails = [ pd ]
					that._cart.monitoring.lifeCycle.status = 'shopping';

					cb && cb( error, pd );
					return that.emit( 'finalize', that._cart );
				});
			}
		);
	}

	/**
	 * Compiles a final charge price for the car Calls the verify method
	 * @method module:alice-cart/lib/cartmanager#finalize
	 */
	,finalize: function finalize( cb ){
		// was finalizeCart
		var that = this;
		if (!this._cart) {
				return that.emit(
						'error'
						, new exceptions.CartNotAvailable({
								message:'no cart available'
						})
				);
		}
		logger.info('finilizing cart %s', this._cart._id.toString() );
		this.verify( cb );
	}

	/**
	 * attemts to save the configured cart instance back to the database
	 * @method module:alice-cart/lib/cartmanager#save
	 * @param  {Function} callback executed after data has been saved to the database. the first parameter will be an error if their is one
	 */
	, save: function save( callback ){
		var error =null;
		// cart passed validation.
		// do save
		this.emit('presave', this._cart );
		logger.notice('attempting to save cart %s', this._cart._id.toString() );
		this._cart.save( function(save_err){
			var _pd;
			if( save_err ){
				error = save_err;
				logger.error('there was a problem saving your cart', error);
			} else{
				logger.info("Cart %s saved successfully", this._cart._id.toString() );
				_pd = this._cart.paymentDetails[0].toObject({getters:true});
				_pd.cart_id = this._cart._id.toString();
			}
			this.emit('save', this._cart);
			this.emit('done', _pd );
			return callback && callback( error, _pd );
		}.bind( this ));
	}

	/**
	 * validates the the internal price configured on the cart adds up to the price of the summed products.
	 * @method module:alice-cart/lib/cartmanager#validate
	 * @param  {Function} callback executed whith validation infomation about the cart
	 */
	, validate: function validate( callback ){
		var that = this;
		this._cart.validate(function( cerror ){
			var _pd = null;
			var error = null;
			// didn't validate
			if( cerror ){

				error = new exceptions.InvalidCartItem({
						message: util.format('Unable to save cart: %s', cerror.message)
				});

				logger.error('problem saving cart', cerror );
				error.errors = cerror.errors
				return callback && callback( error );
			}

			return callback && callback( error, that._cart );
		});
	}

	/**
	 * Reset the internal cart
	 * @method module:alice-cart/lib/cartmanager#clear
	 */
	,clear: function clear( ){
		if (!this._cart) {
				return this.emit(
						'error'
						, new exceptions.CartNotAvailable({
								message:'no cart available'
						})
				);
		}

		this._cart.paymentDetails = {};
		this.emit('clear', this._cart);
		return this;
	}

	/**
	 * looks up the tax rate for a specific state
	 * @chainable
	 * @method module:alice-cart/lib/cartmanager#taxRate
	 * @param {String} abbr two letter state abbreviation
	 * @param {Function} callback Called when tax data is retreived.
	 * @return {CartManager} the current manager instance
	 */
	,taxRate: function taxRate( state, callback ){
		// was getTaxRate
		var query = SalesTaxRate.find({state: state});
		logger.info('looing up tax rate for %s', state );
		query.exec(function (err, taxRate) {
			var error =  null;
			if (err) {
				error = new exceptions.TaxRateNotFound({
					message: 'Unable to retrieve tax rate: ' + err.message
				});
			} else {
				if (taxRate.length == 1) {
					this.emit('TaxRateAvailable', taxRate[0].rate);
				} else if (taxRate.length > 1) {
					error = new exceptions.MultipleTaxRatesFound({
							message: util.format( 'more than one tax rate found for %s', state )
					});
					return callback && callback(error, null);
				} else {
					return callback && callback(null, 0);
				}
			}
			callback && callback( err, taxRate ? taxRate[0].rate : 0 );
		}.bind(this));
		return this;
	}

	/**
	 * Calculates a shipping rate for a given price and type. Standards is all their is right now
	 * @method module:alice-cart/lib/cartmanager#shippingRate
	 * @param {Number} total The sum total of the base price of items in the cart
	 * @param {String} type The type of shipping to apply to the cart
	 */
	,shippingRate: function shippingRate( orderTotal, shippingType, callback ){
		// was getShippingRate
		var that = this;
		this.orderTotal = orderTotal;
		var query = ShipRate.find({method: shippingType})
			.where('orderMinimum').lte(orderTotal)
			.where('orderMaximum').gte(orderTotal)
			.limit(1);

		logger.info('looing up shipping rate for %s @ %s', orderTotal, shippingType );
		query.exec(function (err, shipRate) {
			var error = null;
			if (err) {
				error = exceptions.ShippingRateNotFound({
						message:'Unable to retrieve shipping rate: ' + err.message
				});
			} else {
				error = shipRate && shipRate.length ? error : new exceptions.ShippingRateNotFound({
					message:util.format( 'Unable to retrieve %s shipping rate in range of %s', shippingType, orderTotal )
				});
			}
			callback && callback( error, shipRate.length && shipRate[0].calculate( orderTotal ) );
		});
	}
});
module.exports = CartManager;
