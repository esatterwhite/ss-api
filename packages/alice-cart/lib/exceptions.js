/*jshint laxcomma: true, smarttabs: true, node: true*/
'use strict';
/**
 * Base exception classes for cart operations
 * @module alice-cart/lib/exceptions
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires alice-stdlib/class
 * @requires alice-stdlib/exception
 */
var Class = require( 'alice-stdlib/class' )
	, Exception = require( 'alice-stdlib/exception' )
	, CartException
	, constants
	;

/**
 * Exception Type and code constants
 * @namespace module:alice-cart/lib/exceptions.constants
 **/
constants = {

	/**
	 * Constant value represention a cart exception
	 * @memberof module:alice-cart/lib/exceptions.constants
	 * @const {Object} CART_EXCEPTION
	 * @property {String} TYPE=cart_exception
	 * @property {String} CODE=5000
	 **/
	 CART_EXCEPTION    :  { TYPE:'cart_exception', CODE:5000 }

	/**
	 * Constant value represention a invalid cartitem
	 * @name INVALID_CARTITEM
	 * @memberof module:alice-cart/lib/exceptions.constants
	 * @const {String} INVALID_CARTITEM
	 * @property {String} TYPE=invalid_cartitem
	 * @property {String} CODE=5001  
	 **/
 , INVALID_CARTITEM  :  {TYPE:'invalid_cartitem', CODE:5001}
 
	/**
	 * Constant value represention a invalid cartitem
	 * @name INVALID_STATE
	 * @memberof module:alice-cart/lib/exceptions.constants
	 * @const {String} INVALID_STATE
	 * @property {String} TYPE=invalid_state
	 * @property {String} CODE=5002
	 **/
 , INVALID_STATE     :  {TYPE:'invalid_state', CODE:5002}

	/**
	* Constant value represention a multiple taxrates
	* @name MULTIPLE_TAXRATES
	* @memberof module:alice-cart/lib/exceptions.constants
	* @const {String} MULTIPLE_TAXRATES
	* @property {String} TYPE=multiple_taxrates
	* @property {String} CODE=5003  
	**/
 , MULTIPLE_TAXRATES :  {TYPE:'multiple_taxrates', CODE:5003}

	 /**
	 * Constant value represention An invalid cart total
	 * @name INVALID_CARTTOTAL
	 * @memberof module:alice-cart/lib/exceptions.constants
	 * @const {String} INVALID_CARTTOTAL
	 * @property {String} TYPE=invalid_carttoal
	 * @property {String} CODE=5004
	 **/
 , INVALID_CARTTOTAL :  {TYPE:"invalid_carttoal", CODE:5004}

	 /**
	 * Constant value represention An unavailable cart
	 * @name CART_NOTAVAILABLE
	 * @memberof module:alice-cart/lib/exceptions.constants
	 * @const {String} CART_NOTAVAILABLE
	 * @property {String} TYPE=cart_notavailable
	 * @property {String} CODE=5005
	 **/
 , CART_NOTAVAILABLE :  {TYPE:'cart_notavailable', CODE:5005}


	 /**
	 * Exception values for missing tax rates
	 * @name TAXRATE_NOTFOUND
	 * @memberof module:alice-cart/lib/exceptions.constants
	 * @const {String} TAXRATE_NOTFOUND
	 * @property {String} TYPE=taxrate_notfound
	 * @property {String} CODE=5006
	 **/
 , TAXRATE_NOTFOUND  :  {TYPE:'taxrate_notfound', CODE:5006}

	 /**
	 * Exception values for missing shipping types
	 * @name SHIPPING_NOTFOUND
	 * @memberof module:alice-cart/lib/exceptions.constants
	 * @const {String} SHIPPING_NOTFOUND
	 * @property {String} TYPE=shipping_notfound
	 * @property {String} CODE=5007
	 **/
 , SHIPPING_NOTFOUND :  {TYPE:'shipping_notfound', CODE:5007}
 /**
 * Exception values to be used when a specific cart is not payable
 * @name CART_NOTPAYABLE
 * @memberof module:alice-cart/lib/exceptions.constants
 * @const {String} CART_NOTPAYABLE
 * @property {String} TYPE=cart_notpayable
 * @property {String} CODE=5008
 **/
 , CART_NOTPAYABLE:  {TYPE:'cart_notpayable', CODE:5008}
};

/**
 * Base Exception implemention for shopping cart related errors
 * @class module:alice-cart/lib/exceptions.CartException
 * @extends module:alice-stdlib/exception
 * @param {Object} [options]
 * @param {Object} [options.code=5000]
 * @param {Object} [options.type=cart_exception]
 * @param {Object} [options.message=Their was a problem processing your cart instance]
 * @param {Object} [options.name=CartException]
 */
exports.CartException = CartException = new Class(/* @lends module .THING.prototype */{
	inherits: Exception

	,options:{
		code: constants.CART_EXCEPTION.CODE
		, type: constants.CART_EXCEPTION.TYPE
		, name:'CartException'
		, message:'Their was a problem processing your cart instance'
	}
	, constructor: function( options ){
		this.parent('constructor', options )
	}
	,toJSON: function toJSON(){
		return {
			name:this.name
			,message: this.message
			,code: this.code
			,stack: this.stack
			,type: this.type
			,status: this.options.status
		};
	}
});

	Object.defineProperties(CartException.prototype,{
		status: {
			get: function( ){
				return this.options.status;
			}
		}
	})

/**
 * Exception type to be used in the case a cart has already been processed
 * @class module:alice-cart/lib/exceptions.CartNotAvailable
 * @extends module:alice-stdlib/exception
 * @param {Object} [options]
 * @param {Object} [options.code=5005]
 * @param {Object} [options.type=cart_notavailable]
 * @param {Object} [options.message=Cart Has already been paid, or no longer payable]
 * @param {Object} [options.name=CartNotAvailable]
 */
exports.CartNotAvailable = new Class({
	inherits: CartException
	,options:{
		code:constants.CART_NOTAVAILABLE.CODE
		,type:constants.CART_NOTAVAILABLE.TYPE
		,message: 'Cart object not found'
		,name:"CartNotAvailable"
	}
});

/**
 * Exception type to be used in the case a cart has already been paid
 * @class module:alice-cart/lib/exceptions.CartNotPayable
 * @extends module:alice-stdlib/exception
 * @param {Object} [options]
 * @param {Object} [options.code=5008]
 * @param {Object} [options.type=cart_notpayable]
 * @param {Object} [options.message=Cart Has already been paid, or no longer payable]
 * @param {Object} [options.name=CartNotPayable]
 */
exports.CartNotPayable = new Class({
	inherits:CartException
	,options:{
		type :constants.CART_NOTPAYABLE.TYPE
		,code:constants.CART_NOTPAYABLE.CODE
		,name:"CartNotPayable"
		,message:"Cart Has already been paid, or no longer payable"
	}
});

/**
 * Exception type to be used when a cart contins an invalid or unavailable product 
 * @class module:alice-cart/lib/exceptions.InvalidCartItem
 * @extends module:alice-stdlib/exception
 * @param {Object} [options]
 * @param {Object} [options.code=5001]
 * @param {Object} [options.type=invalid_cartitem]
 * @param {Object} [options.message=Cart could not be verified]
 * @param {Object} [options.name=InvalidCartItem]
 */
exports.InvalidCartItem = new Class({
	inherits:CartException
	,options:{
		name:'InvalidCartItem'
		, message: 'Cart could not be verified'
		, code: constants.INVALID_CARTITEM.CODE
		, type: constants.INVALID_CARTITEM.TYPE
	}
});

/**
 * Exception type to be used in the case the subtotal does not match the calculated subtotal
 * ** DEPRICATED - cart totals are calculated from product items and user input is irreverent
 * @deprecated
 * @class module:alice-cart/lib/exceptions.InvalidCartTotal
 * @extends module:alice-stdlib/exception
 * @param {Object} [options]
 * @param {Object} [options.code=500]
 * @param {Object} [options.type=]
 * @param {Object} [options.message]
 * @param {Object} [options.status]
 */
exports.InvalidCartTotal = new Class({
	inherits: CartException
	,options:{
		name:'InvalidCartTotal'
		, message: 'Cart Total does not match expected total'
		, type:constants.INVALID_CARTTOTAL.TYPE
		, code:constants.INVALID_CARTTOTAL.CODE
	}
});

/**
 * Exception Type to be used when a specific tax rate can not be found
 * @class module:alice-cart/lib/exceptions.TaxRateNotFound
 * @extends module:alice-stdlib/exception
 * @param {Object} [options]
 * @param {Object} [options.code=5006]
 * @param {Object} [options.type=taxrate_notfound]
 * @param {Object} [options.message=Invalid Tax lookup parameters]
 * @param {Object} [options.name=TaxRateNotFound]
 */
exports.TaxRateNotFound = new Class({
	inherits: CartException
	,options:{
		message: 'Invalid Tax lookup parameters'
		, name: 'TaxRateNotFound'
		, code: constants.INVALID_STATE.CODE
		, type: constants.INVALID_STATE.TYPE
	}
});

/**
 * Exception type to be used in the case more than one tax rate was found using lookup parameters
 * @class module:alice-cart/lib/exceptions.MultipleTaxRatesFound
 * @extends module:alice-stdlib/exception
 * @param {Object} [options]
 * @param {Object} [options.code=5003]
 * @param {Object} [options.type=multiple_taxrates]
 * @param {Object} [options.message=More than one tax rate found]
 * @param {Object} [options.name=MultleTaxRatesFound]
 */
exports.MultipleTaxRatesFound = new Class({
	inherits: CartException
	,options:{
		name:'MultleTaxRatesFound'
		, message: 'More than one tax rate found'
		, code: constants.MULTIPLE_TAXRATES.CODE
		, type: constants.MULTIPLE_TAXRATES.TYPE
	}
});

/**
 * Description
 * @class module:alice-cart/lib/exceptions.ShippingRateNotFound
 * @extends module:alice-stdlib/exception
 * @param {Object} [options]
 * @param {Object} [options.code=5007]
 * @param {Object} [options.type=shipping_notfound]
 * @param {Object} [options.message=Unable to retrieve shipping rate]
 * @param {Object} [options.name=ShippingRateNotfound]
 */
exports.ShippingRateNotFound = new Class({
	inherits: CartException
	,options:{
		code: constants.SHIPPING_NOTFOUND.CODE
		, type: constants.SHIPPING_NOTFOUND.TYPE
		, name: 'ShippingRateNotfound'
		, message:'Unable to retrieve shipping rate'
	}
})

exports.constants = constants;
