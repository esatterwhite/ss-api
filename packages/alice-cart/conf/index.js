/*jshint laxcomma: true, smarttabs: true, node:true */
'use strict';
/**
 * Configuration options for alice-cart
 * @module alice-cart/conf
 * @author Eric satterwhite
 * @since 0.1.0
 */

/**
 * @property {Object} [stripe] Config options for stripe API integration
 * @property {String} [stripe.apiKey=sk_test_ALpLT1ER813TvmNeEiqNMXJ1] Stripe dev key
 * @property {String} [stripe.apiVersion=2014-03-28] The api stripe api version to interact with
 **/
exports.stripe = {
    apiKey: 'sk_test_ALpLT1ER813TvmNeEiqNMXJ1',
    apiVersion: '2014-03-28'
}
