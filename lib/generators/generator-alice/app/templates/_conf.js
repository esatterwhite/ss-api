/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Configuration options for alice-<%= name %>
 * @module module:alice-<%= name %>/conf
 * @author 
 * @since 0.1.0
 */

exports.<% name.toUpperCase() %>_FOO = 1

exports.<% name.toUpperCase() %>_THINGS = {
	BAR:{
		BAZ:2
	}
}
