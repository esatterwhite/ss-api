/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * <%= description %>
 * @module alice-<%= name %>
 * @author 
 * @since 0.1.0
 <% if ( events ) { %>* @requires alice-<%= name %>/events <% } %>
 <% if ( commands ) { %>* @requires alice-<%= name %>/commands <% } %>
 <% if ( models ) { %>* @requires alice-<%= name %>/models <% } %>
 <% if ( lib ) { %>* @requires alice-<%= name %>/lib <% } %>
 */

<% if ( events ) { %>
module.exports = require('./events');
<% } else if( lib ){%>
module.exports = require('./lib');
<% } else {%>
module.exports = {};
<% } %>

<% if ( models ) { %>
// models
module.exports.models = require('./models')
<% } %>
