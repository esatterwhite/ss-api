/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Default command for the alice-<%= name %> package
 * @module alice-<%= name %>/commands
 * @author 
 * @since 0.0.1
 * @requires alice-<%= name %>/commands/<%= name %>
 */

exports.<%= name %> = require('./<%= name %>')
