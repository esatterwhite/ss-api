/*jshint laxcomma: true, node: true, smarttabs: true*/
'use strict';
/**
 * DESCRIPTION
 * @module alice-<%= name %>/resources.js
 * @author 
 * @since 0.0.1
 * @requires http
 * @requires joi
 * @requires url
 * @requires alice-web/lib/paginator
 * @requires alice-stdlib/collection
 * @requires alice-core/lib/db/schema/toJoi
 * @requires alice-core/lib/db/schema/validators/defaults
 */

var http         = require('http')
  , joi          = require( 'joi' )
  , url          = require('url')
  , Paginator    = require('alice-web/lib/paginator')
  , forEach      = require('alice-stdlib/collection').forEach
  , toJoi        = require('alice-core/lib/db/schema/toJoi')
  , defaultQuery = require('alice-core/lib/db/schema/validators/defaults/query')
  , Model        = require('../models/index')
  , queryset     = Model.find().limit(25).toConstructor()
  ;

var CODES =[];

forEach( http.STATUS_CODES, function(v,k){
	CODES.push({code:k,message:v})
});

var responseValidator = joi.object({
	 data:joi.array()
	 			.includes(joi.object())
	 			.required()
	 			.description('list of tenant instances')

	,meta:joi.object({
		next:joi.any().optional()
		,previous:joi.any().optional()
		,count:joi.number().required()
		,limit:joi.number().required()
		,offset:joi.number().required()
	}).description('metadata')
});

exports.get_list = {
	method:"GET"
	,path:'/<%= name %>'
	,config:{
		jsonp:'callback'
		,description:"Look up tenants matching specific criteria"
		,tags:['api']
		,plugins:{
			'hapi-swagger':{
				responseMessages:CODES
			}
		}
		,response:{
			schema: responseValidator.clone()
		}
		,handler: function tenant_get( request, reply ){
			var query = new queryset();
			query.model.count(function( err, count ){

				if( err ){
					return reply(new request.hapi.boom.wrap( err ) );
				}

				query
				.skip( request.query.offset || 0 )
				.limit( request.query.limit )
				.exec(function( err, results ){
					var paginator = new Paginator({
						req:request
						,res:reply
						,objects:results
						,count: count
						,limit:request.query.limit
						,offset:request.query.offset
					});
					
					reply( paginator.page() );
				});

			})
		}
		,validate:{
			query: defaultQuery.clone()
		}
	}
};

exports.get_detail = {
	method:'GET'
	,path:'/<%= name %>/{<%= name %>_id}'
	,config:{
		description:"Find a specific <%= name %> by id"
		,tags:['api']
		,plugins:{
			'hapi-swagger':{
				responseMessages:CODES
			}
		}
		,response:{
			schema: joi.object().unknown()
		}
		,validate:{
			query: defaultQuery.clone()
			,params:{
				<%= name %>_id:joi.string().length(24).alphanum().required().description("Mongo Object ID String")
			}
		}
		,handler: function tenant_by_id( request, reply ){
			var query = new queryset();

			query.findOne({_id:request.params.tenant_id}, function( err, t ){
				reply( err && new request.hapi.boom.wrap( err ) || t );
			});
		}
	}
};


exports.post_list = {
	method:'POST'
	,path:'/<%= name %>'
	,config:{
		description:"Creates a new <%= name %> instance"
		,tags:['api']
		,validate:{
			query: defaultQuery.clone()
			,payload:toJoi( Model.schema )
		}
		,handler: function post_list( request, reply ){
			// NOTE - This doesn't save. Just validates payload
			// creates a new instances and returns it.
			return reply(new Model(request.payload).toJSON())
		}
	}
}

