'use strict';
var util = require('util');
var path = require('path');
var debug = require('debug')('alice:generator')
var yeoman = require('yeoman-generator');
var yosay = require('yosay');

var AliceGenerator = yeoman.generators.Base.extend({
  initializing: function () {
    this.pkg = require('../package.json');
    this.sourceRoot(path.resolve(__dirname , 'templates' ) )
    this.destinationRoot( path.resolve( __dirname, "..", "..", "..", "..", "packages") )
  },

  prompting: function () {

    // Have Yeoman greet the user.
    this.log(yosay(
      'Welcome to the striking Alice generator!'
    ));

    var prompts = [{
      type: 'input',
      name: 'package',
      message: 'Name of package',
    }];

  },

  writing: {
    app: function () {
      var pkglocation = util.format('alice-%s', this.seeli_data.name )
      this.dest.mkdir( pkglocation );
      
      ['conf','lib', 'test', 'commands', 'models', 'events', 'scripts', 'resources']
      .filter(function( dir ){
          return this.seeli_data[ dir ]
      }.bind( this ))
      .forEach(function( dir ){
          var directory = pkglocation + '/' + dir
          debug('create directory %s ', directory)
          this.dest.mkdir( directory );

          debug('generating %s README', dir )
          this.template('_README.md', directory + '/' + 'README.md', this.seeli_data);

          if( dir != 'commands' && dir != 'scripts'){
            debug('generating %s module', dir )
            this.template( util.format( '_%s.js', dir ) , directory + "/" + "index.js", this.seeli_data)
          }

      }.bind( this ));

      this.dest.mkdir('fixtures');
      this.src.copy('gitkeep', pkglocation + "/fixtures/" + ".gitkeep" );

    },

    projectfiles: function () {
      var pkglocation = util.format('alice-%s', this.seeli_data.name )

      this.src.copy('jshintrc', pkglocation + '/' + '.jshintrc');

      debug('generating package.json ')
      this.template('_package.json', pkglocation + '/' + 'package.json', this.seeli_data );

      debug('generating main README ')
      this.template('_README.md', pkglocation + '/' + 'README.md', this.seeli_data );

      debug('generating package entry point ')
      this.template('_pkg.js', pkglocation + '/' + 'index.js', this.seeli_data );

      debug('generating gitkeep' )
      this.src.copy('gitkeep', pkglocation + "/" + ".gitkeep" )

      if( this.seeli_data.commands ){
        this.template('_commands.js', pkglocation +"/commands/" + this.seeli_data.name +'.js' , this.seeli_data )
      }
    }
  },

  end: function () {
    // this.installDependencies();
  }
});

AliceGenerator.Adapter = require('./adapter')
module.exports = AliceGenerator;
