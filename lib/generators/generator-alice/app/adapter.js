/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * Loads configuration form different data stores before the server starts
 * @module adapter.js
 * @author 
 * @since 0.0.0
 * @requires moduleA
 * @requires moduleB
 * @requires moduleC
 */

/**
 * Description
 * @class <MODULEPATH>
 * @extends <CLASS>
 * @mixes <CLASS>
 * @example var x = new memory.THING({});
 */

var DataAdapter = module.exports =  function DataAdapter()/* @lends module.THING.prototype */{};

DataAdapter.prototype.prompt = function( questions, callback ) {
	callback( questions )

};

DataAdapter.prototype.diff = function(actual, expected) {
	return "diff not implemented"
};

DataAdapter.prototype.log = function(){

}

DataAdapter.prototype.log.create = function(){};