'use strict';

var PromoBasedSchema = function (sub) {

    sub.promotions =[ {
      message:{type:String},
      //associatedDiscount:{type:}, //make this a reference type to the Disocunt Object collection
      placement:{type:String, enum:['mega-menu', 'top-nav', 'side-bar', 'widget' ]},
      iconUrl:{type:String},
      linkUrl:{type:String}
    }];
    return sub;
};

module.exports = PromoBasedSchema;