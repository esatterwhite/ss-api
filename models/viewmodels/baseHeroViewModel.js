/**
 * Created by ss-dev-01 on 5/13/2014.
 */
/**
 * Created by ss-dev-01 on 5/13/2014.
 */
'use strict';

var HeroBasedSchema = function (sub) {
    sub.hero = {
        imageUrl: {type: String},
        header: {type: String}
    };

    return sub;
};

module.exports = HeroBasedSchema;