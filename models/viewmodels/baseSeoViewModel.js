/**
 * Created by ss-dev-01 on 5/13/2014.
 */
'use strict';
var SeoBasedSchema = function (sub) {

    sub.meta = {
        h1:{type:String},
        title: {type: String},
        description: {type: String}
    };
    return sub;
};

module.exports = SeoBasedSchema;