/*
 * Created by Ryan Fisch on 5/12/2014.
 */
'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    HeroSchema = require('./baseHeroViewModel'),
    SeoSchema = require('./baseSeoViewModel'),
    PromoSchema = require('./basePromoViewModel');


var schema = Schema({
    "tenantType": {type: String},
    "widgets":
    [{"row":[
        {"size": {type: String},
         "content":[
             {
                 "media": {type: String, enum: ['video', 'promo', 'info', 'state', 'image']},
                 "size": {type: String},
                 "src": {type: String},
                 "h1": {type: String},
                 "h2": {type: String},
                 "text": {type: String},
                 "url": { type: String },
                 "href": { type: String }
             }
         ]
        }
    ]

     }]});




     module.exports = mongoose.model('TenantTypeViewModel', schema);
