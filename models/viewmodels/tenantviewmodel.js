/**
 * Created by ss-dev-01 on 6/2/2014.
 */
/*
 * Created by Ryan Fisch on 5/12/2014.
 */
'use strict';

var mongoose = require('mongoose'),
    HeroSchema = require('./baseHeroViewModel'),
    SeoSchema = require('./baseSeoViewModel'),
    Schema = mongoose.Schema;

var subType = HeroSchema(SeoSchema({
    h2:{type:String}
    , subNavigationPromo: {
        "promo": {type: String},
        "link": {type: String}
    },
    widgets: [
        [
            {size: {type: String},
             content: [
                 {
                     "media": {type: String, enum: ['video', 'promo', 'info', 'state', 'image']},
                     "size": {type: String},
                     "type": {type: String},
                     "src": {type: String},
                     "h1": {type: String},
                     "h2": {type: String},
                     "text": {type: String},
                     "url": {type: String},
                     "href": { type: String }
                 }
             ]
            }
        ]
    ]
}));

var schema = new Schema(subType);

module.exports = mongoose.model('TenantTypeViewModel',schema);
