#!/usr/bin/env node
/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * Command line Interface for alice. This module is here to load other management
 * commands. And that is about it.
 * @module alice.js
 * @author Eric Satterwhite
 * @since 1.9.0
 * @requires seeli
 * @requires fs
 * @requires child_process
 * @requires debug
 * @requires mout/lang/clone
 */

 var cli = require( 'seeli' )
   , child_process = require( 'child_process' )
   , fs            = require('fs')                          // fs module
   , path          = require('path')                          // fs module
   , debug         = require('debug')( 'bin:alice')
   , clone         = require('mout/lang/clone')
   , packagepath   = path.normalize( path.resolve(__dirname,'..','packages') )
   , jsregex       = /\.js$/
   , files
   ;

debug('current dir', __dirname);
debug('package path: %s', packagepath);

// check to see that we can load core packages
try{
	require.resolve('alice-log')
} catch( e ){
	console.log("Unable to resolve core packages")
	console.log("Ensure the packages directory is set on the NODE_PATH env variable")
	console.log(" - export NODE_PATH=$NODE_PATH:$HOME/<path>/<to>/spiritshop-api/packages")
}

fs
	.readdirSync( packagepath )
	.forEach( function( file ){


		var searchpath = path.join( packagepath, file, 'commands' )
		
		if( fs.existsSync( searchpath ) && fs.statSync( searchpath ).isDirectory() ){
			fs.readdirSync(searchpath).forEach( function(module){
				if( jsregex.test( module ) ){
					var requirepath = path.join( searchpath, module )
					var cmd = require(requirepath)
					var name = ((cmd.options.name ? cmd.options.name : module)).replace(jsregex,'').toLowerCase().replace('-', '_');
					try{
						debug('loading %s', requirepath)
						debug('registering %s command', module )
						cli.use( name, cmd)
						
					} catch( e ){
						debug('unable to register module %s', module )
					}
				}
			} );
		}
	});
cli.run()

