# Alice 
The Ecomerce Platform which powers Spiritship.com.

## Primary Packages

* {@link module:alice-stdlib|Standard Lib}

* {@link module:alice-log|Log}

* {@link module:alice-web|Web}

* {@link module:alice-cache|Cache}

* {@link module:alice-core|Core}

* {@link module:alice-conf|Conf}

* {@link module:alice-catalog|Catalog}

* {@link module:alice-cart|Cart}


## Installation

installing, both the application dependancies and Command Line Tool can be done through npm.

```sh
git clone git@bitbucket.org:rgable/spiritshop-api.git
cd spiritshop-api
npm install && npm link
```
### Docker

The Alice platform relies on numerous eternal services to function. While shared dev instances exist, it is recommended to run local instances. [Docker](https://docs.docker.com) is the recommeded means for running local services.

## Running A Server

To Run a server you will need to include the application `packages directory` to your NODE_PATH env variable.

```sh
export NODE_PATH=$NODE_PATH:$PWD/packages
```

### Configuration

Configuration of your server can be done in a number of ways. Primarily with `Environment variables`, `Configuration files`, and `Command Line Arguments`. Default configuration is defined as a JSON document. Can easily override values and nested values using Command line arguments. For example, if you wanted to enable terminal color output and pretty printing of log messages to your terminal you would do something like this;

```sh
node server --logger=stdout --log:stdout:colorize=1 --log:stdout:prettyPrint=1
```

## CLI ( `alice` )

`alice` is the management command runner. Use this to discover and execute commands to mange your application environment. Executing alice with no arguments will list all available commands. By convention, all packages found in the `packages` directory can create a sub folder in there named `commands` where each module can export an instance of a command. The command will be registered by the name of the module's file.

```sh
alice
alice help version
alice version --help
alice version --no-color
alice sync --fake
alice serve -i 2
```

## Building A database

While a shared mongo instance is used by default, it is recommended that you use a local database when doing any active development. An initial database can be built from fixtures using the import command:

### MongoDB

Install and run a mongodb image with docker.

```
docker pull dockerfile/mongodb
docker run -d -p 27017:27017
```

### Configure

To always use a local database, create a file named **alice.json** in the root of the project and define a **commerce** entry in a **databases** object. This will tell *Alice* to always use your local database for commerce related operations.

```js
{
	"databases":{
		"commerce":{
			"host":"localhost"
			,"port":27017
			,"user":null
			,"password":null
		}
	}	
}
```

### Fixture Loading

To load all initial data fixtures, pass the `initial` flag with out specifing a specific fixture.

```sh
DEBUG=alice:* alice import --initial
```

If you wish to reload a specific fixture you may pass the name of the fixture, sans **initial_**. Meaning, if there is a an fixture named `initial_tenants.json`, you may selectively run it with

```sh
DEBUG=alice:* alice import tenants --initial
```

**WARNING** The initial flag will drop the existing collection and re-create it

## Building A Search Index
Alice uses hosted version [Elastic Search](http://elasticsearch.org), [Searchly](http://searchly.com) in a some places. It is recommeded to use a local instance for developoment

### Setup Elastic search
Install an elastic search image and set up a persistent data directory

```
docker pull dockerfile/elasticsearch
mkdir -p $HOME/dev/data/plugins
```

### Configuration

create a a config file for elastic search

```
touch $HOME/dev/data/elasticsearch.yml
```

Edit your config file to include the head plugin

```yml
path:
  plugins: <path>/<to>/data/plugins
```

### Plugins

Load the Head admin plugin and start a server. You may install any other plugins for your search instance in a similar manner

```sh
docker run -v $HOME/dev/data:/data dockerfile/elasticsearch /elasticsearch/bin/plugin -i mobz/elasticsearch-head
docker run -d -p 9200:9200 -p 9300:9300 -v $HOME/dev//data:/data dockerfile/elasticsearch /elasticsearch/bin/elasticsearch -Des.config=$HOME/dev/data/elasticsearch.yml
```

## Exceptions

The Alice Stdlib contains an `exception` works around problems from subclassing the native JavaScript `Error` object

Error Code | Usage 
-----------:|------
1000 - 1999 | System level and unhandled errors
2000 - 2999 | Reserved for the **alice-stdlib** package
3000 - 3999 | Reserved for the **alice-conf** package
4000 - 4999 | Reserved for the **alice-core** package
5000 - 5999 | Reserved for the **cart** package
6000 - 6999 | Reserved for the **catalog** package

## Tests

Tests can be run via `npm` using the `test` command;

```sh
npm test
```

Test found in the `test` directory of the top level application as well as `test` directories found in sub packages found in the `packages` directory

## Docker Images

You can run the server in a docker container that resembles Heroku's cedar stack which is what runs in production. You will need the base node container before you can build Alice.

#### Build the base image

```sh
git clone https://esatterwhite@bitbucket.org/esatterwhite/dockerfile-node.git
cd dockerfile-node
docker build --rm=true -t spiritshop/node .
```
#### Build Alice
```sh
cd /<path>/<to>/<spiritshop-api>
docker build --rm=true -t spiritshop/alice .
docker run -t -i -p 3001:3001 spiritshop/alice
```

## Package Scripts

Alice ships with a number of **npm** script to help with som common tasks. 

Script | Usage | Description
:-----|:-------|:----------
scripts/test.js | npm test | executes mocha test runner recurrsively
scripts/install.js | npm install | auto installs all sub package dependencies
scripts/clean | npm run clean | removes all node_modules except the npm module
scripts/slugify-tenants | npm run sligifytenants | A script to back fill tenant slug fields


## Managment Commands

to get a comprehensive list of commands just invoke the alice help system

```bash
alice --help
```

You can also ask for help on any of the available commands


```bash
alice  <command> --help
alice  help <command>
```


Command | Usage | Package | Description
:------:|:-----:|:-------:|:-----------
help | alice `<command>` --help | alice | help...
export | alice export <model> --output=products.json  | alice-core | Exports a database model for easy import. 
import | alice import <file> | alice-core | imports a valid data dump into it's respective data base collection
job | alice jobs -j fulfillment | alice-jobs | Starts a new job runner instance
generate | alice generate -i | alice-core | Generates a new package for the Alice platform
repl | alice repl | alice-stdlib | Starts up a Node.js Repl with current configuration and data models loaded 
web | alice web -l stdout -l file | alice-stdlib | Starts the API server with the NODE_PATH resolved, passing and CLI flags you specify
charge | alice stripe_charge --token | alice-stdlib | Generates an one off token and places an ad-hoc charge to stripe
version | alice version --no-table | alice-stdlib | Displays version information of the current installation
docs | alice docs --rm=documentation | alice-core | Generates API documentaion for the Platform project
reindex | alice reindex Tenant --drop | alice-core | Rebuilds, or updates an index in an elastic search instance

