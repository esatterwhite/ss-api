### Best Practices

* K.I.S.S. - Endpoints are not the application, the are the interface to the application. As much application specific logic as possible should be kept out of endpoin1t definitions. Move logic into libraries, modules and helpers that can be reused. Endpoint logic should be reserved for things like **error handling**, **paging**, **redirection**, **serialization**, etc.
* Do not worry about custom serializtion. Return Simple Objects.
* Start with simple CRUD operations first. Exand when necessary.
* Use endpoint validation for simple data validation. Define complex business logic validation down to The Model definition or in separate libs
* Return Correct HTTP Status Codes. Don't wait for the server to crash and return a 500. 

### Serialization | Deserialization

All endpoint support multiply data formats including `JSON`, `JSONP` and `XML` by default. The is handled by the [tastypie](https://github.com/esatterwhite/node-tastypie) package and the {@link module:alice-web/startup/serialize|serialization start up script} in the `web` package. This process is transporatent. Return & expect simple JSON objects. While Additional formats are easily supported, These are the formats currently  supported

##### Content Types

| Format | Content Type | Query Value |
|:------:|:------------:|:-----------:|
| XML | text/xml | xml |
| JSON | application/json | json |
| JSONP | text/javascript | jsonp |


In the case of `JSONP`, responses will be wrapped in a callback function, the name of which can be specified using the `callback` query parameter. It will default to `callback` if not specified.

Formats can be specified via the HTTP `Accept`, and `Content-Type` Headas as well as the `format`query string parameter.

The following two request are the same:
```sh
curl -H "Content-Type: application/json" -H "Accept: text/xml" http://localhost:3001/api/v2/tenant"
```
```sh
curl http://localhost:3001/api/v2/tenant?format=xml"
```


### Locations
Endpoint definitions should be placed in a directory named `resources`. If it is a versionable endpoint, they should live inside of a sub directory of the name of the version (**v1**, **v2**, etc) *collector* modules such as a `index.js` **MUST** be omitted as this would cause resource path collision

```
├── resources
    ├── README.md
    ├── v1
    │   └── fake.js
    └── v2
        └── fake.js

```

### Configuration

Endpoints are route configurations for the [Hapijs](http://hapijs.com) web framework. Valid configuration **MUST** be an object with, at minimum, a **method*, **path** and **handler** value where:

* **Method** - `String` Denoting the HTTP method it will accept ( **GET**, **PUT**, **POST**, etc)
* **Path** - `String` A sepcific route path. 
  * **Route** paths **must** omit prefix values, eg. */api/v1*. This information is build automatically be they primary server module at start up time. The name of the version folder dictates how the url is constructed. 
* **Handler** - `Function` A Handler function that **MUST** have the signature of ( `request`, `reply` ).
* **Tags** - `Array` And array of metadata tags.
  * You mae use a special tag of `api` to include the enpoint into the API exporer UI
* **Config** - `Object` if Config is used, The config object should contain the handler.
  * **validate** - `Object` An object defining **query**, **params** and **payload** validation rules. These should be an object with [Joi](https://github.com/hapijs/joi) or an full Joi Object

`reply` is a function which is used to return either an error or data back to the requesting client.

It is common convetion for enpoint modules to define an enpoint as a single property on the `exports` object whose name give some indication of what it's intention for. *NOTE*, the names a not actually used by the server for any logical purpose, This is merely a convention for the reader.

General **crud** endpoints are usually named:

| METHOD | Path | Name | Purpose |
|--------|------|------|---------|
| GET | / | **get_list** | return a list of resource data entities |
| GET | /{id} | **get_detail** | return a sepcific resource entity by id |
| POST | / | **post_list** | Create a new entity for a given resource type |
| PUT | /{id} | **put_detail** | Replace / update an existing resource entity matching `id` |
| DELETE | /{id} |  **delete_list** | Delete a specific resource entity by matching `id` |

```js
// api/v1/fake.js
exports.get_detail = {
	method:'GET'
	,path:'/fake/{fakeId}'
	,config:{
		description:"Look up a fake by id"
		,tags:['api']
		, validate:{
			params:{
				fakeId:joi.string().length(24).alphanum().description("ObjectID of the fake to look up")
			}
		}
		,handler: function( request, reply ){
			var Boom = request.hapi.boom
			   , id =  request.params.fakeId
			
			Fake.findByID( id, function(err, fake){

				if(err){
					return reply(new Boom.notFound('Fake matching id' + id + 'not found') );
				}

				return reply( fake.toObject({ virtuals:true }) );
			})
		}
	}
}

```

### Paging

Pagination for listing endpoints should use the {@link module:alice-web/lib/paginator|Paginator} Class which is a subclass of the paginator found in the [tastypie](https://github.com/esatterwhite/node-tastypie) module. When creating Basic Crud & listing points, it is considered Best Practice to pre-define the Default Query with sane set of defaults, including size( ~25 is considered a save default) And generate a response payload with the paginator class

Values used for resource paging should be included in query string paramers using these variables:

* limit - The number of results to return ( using mongoose.Model#limit )
* offset - The index of the item to start the data page at ( using mongoose.Model#skip )


```
// api/v2/foobar
var queryset = FooBar.find({}).limit( 25 ).toConstructor();
  , Paginator = require('alice-web/lib/paginator')

exports.get_list = {
	method:"GET"
	,path:'/foobar'
	,config:{
		jsonp:'callback'
		,description:"Look up tenants matching specific criteria"
		,tags:['api']
		,handler: function tenant_get( request, reply ){
			var query = new queryset();
			query.model.count(function( err, count ){

				if( err ){
					return reply(request.hapi.boom.wrat( err ) )
				}

				query
				.skip( request.query.offset || 0 )
				.limit( request.query.limit )
				.exec(function( err, results ){
					var paginator = new Paginator({
						req:request
						,res:reply
						,objects:results
						,count: count
						,limit:request.query.limit
						,offset:request.query.offset
					});
					
					reply( paginator.page() )
				});

			})
		}
	}
};
```

### Validation

Validation is done via the [Joi](https://github.com/hapijs/joi) Object schema validation libaray automatically at the server level. 

#### Querystring Parameters

`Alice` has a {@link module:alice-core/lib/db/schema/validators/defaults/query|prebuilt validator} for query string param commanly used query params, including paging.

```
// api/v2/foobar
var queryset = FooBar.find({}).limit( 25 ).toConstructor();
  , Paginator = require('alice-web/lib/paginator')
  , QueryValidator = require('alice-core/lib/db/schema/validators/defaults/query')

exports.get_list = {
	method:"GET"
	,path:'/foobar'
	,config:{
		jsonp:'callback'
		,description:"Look up tenants matching specific criteria"
		,tags:['api']
		,validate:{
			query: QueryValidator
						.clone()
						.keys({  /* Add additional validations for your endpoint */})
		}
		,handler: function tenant_get( request, reply ){
			var query = new queryset();
			query.model.count(function( err, count ){

				if( err ){
					return reply(request.hapi.boom.wrat( err ) )
				}

				query
				.skip( request.query.offset || 0 )
				.limit( request.query.limit )
				.exec(function( err, results ){
					var paginator = new Paginator({
						req:request
						,res:reply
						,objects:results
						,count: count
						,limit:request.query.limit
						,offset:request.query.offset
					});
					
					reply( paginator.page() )
				});

			})
		}
	}
};
```

#### Request Payload

Using thing `payload` key of the `validate` config options, you can define validation rules for incoming payload from `PUT` and `POST` requests. For basic model validation, `Alice` has a {@link module:alice-core/lib/db/schema/toJoi|helper} to auto generate a Joi validation object from an existing Schema

```js
// api/v2/fizzbuzz.js

var toJoi = require('alice-core/lib/db/schema/toJoi')
  , QueryValidator = require('alice-core/lib/db/schema/validators/defaults/query')
  , FizzBuzz // Some Model

exports.post_list = {
	method:'POST'
	,path:'/tenant'
	,config:{
		description:"Creates a new tenant instance"
		,tags:['api']
		,validate:{
			query: QueryValidator.clone()
			,payload:toJoi( FizzBuzz.schema )
		}
		,handler: function post_list( request, reply ){
			// if the request got this far, it has already been validated

			new FizzBuzz( request.payload,function(err, fz ){
				return reply(fz.toObject({virtuals:true})).created(); // POSTs should return a 201
			});
		}
	}
}
```
#### URI Paramters

URI Parameters are defined the same as query string and payload validation rules. The defined values **MUST** match the definded parameters in the `path` value.

```js
exports.get_detail = {
	method:'Get'
	,path:'/fizzbuzz/{fizz_id}'
	,config:{
		description:"Creates a new tenant instance"
		,tags:['api']
		,validate:{
			params:{
				fizz_id:joi.number().required() // validates the fizz buzz parameter
			}
		}
		,handler: function post_list( request, reply ){
			// if the request got this far, it has already been validated

			new FizzBuzz( request.payload,function(err, fz ){
				return reply(fz.toObject({virtuals:true})).created(); // POSTs should return a 201
			});
		}
	}
}

```


### Querystring format
Query string parsing is auto matically handled internally by the web server. However, it is important to understand valid query string format and encoding practices to ensure data is passed correctly.

Querystrings should use array subscript with numeric values to denote and array and array subscript notation with named indexes to denote an object.

```js
// ["bar", "baz"]
/param?0=bar&1=baz
```

```js
// {name:{foo:"bar", bar:"baz} }
/param?name[foo]=bar&name[bar]=baz
```

```js
// {name:[1,2,3]}
/param?name[0]=1&name[1]=2&name[2]=3
```

All query strings should be safe url encoded using `encodeURIComponent` prior to addition to full uri strings. Use `encodeURI` when attempting encode a full URI including query strings
