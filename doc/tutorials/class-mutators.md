Mutators are what give the Class system it's flexibilty and as a result it's power. A Mutator can best be thought of as a plugin. However, in the situation of Mutators, they are executed during the creation of the class definition when you call `new Class()`. Mutator functions are executed in the context of the Class definition allowing the plugin to modify ( mutate ) the class as it is being defined. While, you may not know it, you have seen two Mutators already in **Implements** and **Extend**

Mutators are implemented on {@link module:alice-stdlib/class|Class} via that {@module:accessor|accessor} module allowing you to define your own Mutators anytime you like. Whatever you name your Mutator will become a special key on the class that will perform whatever action you like

### Example - Track Class Instances
Lets take a look at a simple example of creating a new Class Mutator. I would like to be able to keep track of all of the instances of a specific Class so I can perform bulk operations on them. For memory limitation reasons I don't want functionality enabled all of the time, so I'll need to be able to turn it on only when needed.

```js
var Class = require('alice-stdlib/class')
Class.defineMutator("Track", function( allowtrack ){
	if( !allowtrack ){
		// nothing to do
		return;
	}

	// the original constructor function
	var old_init = this.prototype.constructor;
	var cls = this;

	// overload the constructor function
	cls.prototype.constructor = function( ){
		// stash references to the instance in an array
		// create it if it doesn't exist
		( cls.instances = cls.instances || [] ).push( this );

		// call the original constructor
		old_init.apply( this, arguments )
	}
});
```
That's it! When I define a new {@link module:alice-stdlib/class|Class} I can set a property **Track** to true and it will automatically save all of the instances of that {module:alice-stdlib/class|Class}

```js
var Class = require('alice-stdlib/class')
var MyClass = new Class({
	constructor: function(){
		this.data = {}
	}
	,Track:true
});

var x = new MyClass();
var y = new MyClass()

MyClass.instances // [ x, y ]
```

### Example - Integrate Class w/ jQuery
jQuery is very popular, functional, domain specific library with a lot of plugins behind it. One of the primary drawbacks is that it does not support any kind of OOP, Class like infrastructure. However, With the power of Mutators we can create a Class that create a jQuery plugin that creates classes!

```js
// mutators.js
define(["class"],function( Class ){

	Class.defineMutators({
		jQuery: function( name ){
			var that = this; // quick reference to the class instance

			// if there is no jQuery... Nothing todo
			if(!jQuery){
				return;
			}

			// the jQuery plugin function
			jQuery.fn[name] = function( arg ){

				// convert arguments to a *real* array
				var args = Array.prototype.slice.call( arguments, 1 );

				// if a string is passed, its a method call on an existing instance
				if( typeof arg == 'string'){
					var instance = jQuery( this ).data(name);

					// if there a class defined  - call the method with the arguments
					if(instance){
						instance[arg].apply( instance, args);

					}
				// Other wise we need to create a store an instance
				// somewhere in a jQuery...
				} else {
					// store a new instance of this Class under the passed in name
					jQuery( this ).data(
						name
						, new that(
							this.selector // selector of the current jQuery object
							, jQuery.extend( that.prototype.options, arg )
						) // end class constructor
					) // end .data()
				}
			}
		}
	})
})
```

We taking advantage of the fact jQuery stores its plugins in a namespace called **fn** and use that to tie our logic into the internals of the jQuery library. Essentially, our mutator takes a `string`, arugument that it will use as the id of our class which will store in instance of that class on a DOM element using jQuery's css selector syntax. Using the same selector will return the instance of the class so you can work with it. So the name sepcified on the jQuery Mutator will be the name of the jQuery plugin. Calling the plugin with an `Object` will instanciate a new instance of the A {@link module:alice-stdlib/class|Class}. Calling it again, with a string will call a method with a matching name

In other words, you define a new {@link module:alice-stdlib/class|Class} as usual, using the jQuery Mutator key and you can use both libraries interchanably. For this example, we'll call it `aliceQuery`
It looks something like this:

```js
// require the mutators module defined above
// alicequery.js
var Class = require('alice-stdlib/class')
var mutator = require('./mutators')
var aliceQuery;

// Still just a regular Class
aliceQuery = new Class({
	mixin:[Options]
	,options:{
		crazy:true
		,mind:"calm"
	}
	,jQuery:"aliceQuery" // This is the magic
	,constructor: function( selector, options ){
		// The first argument to a jquery plugin is always a CSS selector
		// do soemthing with selector..


		// configure the class
		this.setOptions( options )
	}

	,sayWhat: function( ){
		if( this.options.crazy ){
			alert( "mind = " + this.options.mind )
		}
	}
});

module.exports = aliceQuery;
```

#### Usage: Class Style

```js
var Class = require('alice-stdlib/class')
var mutator = require('./aliceQuery')

// class creation
var instance = new aliceQuery(null, {}); // OOP style

// call sayWhat method
instance.sayWhat() // alerts mind = calm
```

#### Usage: jQuery Style

```js
// class creation
$("#someElement").aliceQuery({mind:"blown"}) // jQuery style

// call sayWhat method
$("#someElement").aliceQuery("sayWhat") // alerts: mind = blown
```
