It is common for web applications to need to execute a series of tasks before the server starts to ensure
the environment is suitable to run correctly. The {@link module:alice-web|web} package does this through *startup scripts*.
Alice will recursively look for start up scripts in a directory called `startup`. Start up scripts can be simple functions, 
or they can be a Hapi.js plugin. Plugins will be automatically registered with the server instance before it starts. 
sSimple functions will simply be called once.

### Plugins

To create a plugin, you must define a module that has a function called `register`, and you must define an `attributes` object on the register function with at least a `name` and `version` property. Alternatively attributes can define a `pkg` property. The function will be passed the `server`, plugin specific `options`, as a `callback` function which must be called when your plug in is finished

```js
exports.register = function register( server, options, next){
	server.route({
		method:'GET'
		,path:'/api/fake'
		,handler: function( request, reply ){
			reply('fake!').code(200)
		}
	});

	next();
};

exports.register.attributes = {
	name:'fake'
	,version:'0.0.1'
};

```
### Functions 

Start up functions are executed once before the server starts.

```js
var logger = require('alice-log');
module.exports = function logger( ){
	process.startup = (+ new Date())

	logger.debug('server start time %s', process.startup );
};
```
