Commands are an easy way to expose internal functionality from the alice framework via a command line interface ( CLI ). The alice platform comes
with a cli tool, `alice`, that auto-discovers commands in the code base. A command is a `module` which lives in a directory named *commands* in an alice packages. The `module` must export an instance of a [Seeli](https://github.com/esatterwhite/node-seeli) command, or an instance of a subclass of a Seeli command. Seeli is installed along with the project.

### Create a module

Start by creating a *commands* directory with a js module for your command. The name of the module is what the name of the command will be called.

```bash
cd packages/alice-starter
mkdir commands
touch hello.js

*
|
|-- packages
|   `--alice-starter
|      `--commands
|         `--hello.js
```

With this simple set up you technically have a new command called *hello* which can be invoked with `alice hello`. Our module is empty and this will of course throw an error. We can fix this by exporting a comman instance from our module. All we need to do is define a `run` method on a `Command` instance. The `run` method accepts three arguments, `directive`, `data` and a `done` callback function. The only thing a command *must* do is call the done function when it has finished doing what ever it does. The `done` function is a standard node.js callback which takes an error as the first argument if there is one. If you would like to print anything to the terminal, just pass it to the done function as the second argument.

```js
var cli = require( 'seeli' );

module.exports = new cli.Command({
	run: function( directive, data, done ){
		done( null, 'ok' )
	}
});
```

We now have a command that will just print *ok* out to the terminal.

```sh
alice hello
>>> ok
```

### Directives

In general every command accepts an invocation like such

```sh
alice <command> [directive] [flags]
```
The directive is an optional single string that can be used for what ever you want. Lets extend out `hello` command to accept person's name as a directive and say hello to that person.

```js
var cli = require( 'seeli' );
var util = require( 'util' );

module.exports = new cli.Command({
	run: function( directive, data, done ){
		done( null, util.format('Hello, %s', directive) )
	}
});
```

```sh
alice hello Bill
>>> Hello, Bill
```

### Flags & Data

At some point you are going to want to accept and collect user input of some kind. Commands accomplish this through `flags`. `flags` are an object where the key is the name of the flag and the value is the configuration options for that flag. All user input collected from flags is passed to your command as the second argument to the run function, `data`. Lets extend our hello command to accept a single flag, `yell` which is a boolean. If yell is true, we'll print out put put in all caps.  

```js
var cli = require( 'seeli' );
var util = require( 'util' );

module.exports = new cli.Command({
	flags:{
		yell:{
			type:Boolean
			,default:false
			,description:"Pass this flag to yell!"	
		}
	},
	run: function( directive, data, done ){
		var out = util.format('Hello, %s', directive);
		done( null, data.yell ? out.toUpperCase() : out )
	}
});
```

One neat think about boolen flags, is that they don't require a value. The presence of the flag is `true`, and the absence of the flag is `false`. You can also force a `false` value by prefixing a boolean flag with `no-`

```bash
alice hello bill --yell
>>> HELLO, BILL

alice hello bill --no-yell
>>> Hello, bill
```
 
Each command instance has two boolean flags by default `color` and `interactive`. The color flag is used to strip any ANSI color formatting, and the interactive flag is used to trigger the interactive mode for your command. 


### Help

One of the really great things about [Seeli](https://github.com/esatterwhite/node-seeli) commands is that, the help generation is built in, you just need to fill in A description and usage examples for your command. As you might imaging, a command accepts a `usage` and `description` property.


```js
var cli = require( 'seeli' );
var util = require( 'util' );

module.exports = new cli.Command({
	description:'A command to say hello to anyone in the world'
	,usage:'alice hello bill'
	,flags:{
		yell:{
			type:Boolean
			,default:false
			,description:"Pass this flag to yell!"	
		}
	},
	run: function( directive, data, done ){
		var out = util.format('Hello, %s', directive);
		done( null, data.yell ? out.toUpperCase() : out )
	}
});
```


**note** - usage can be a single `string`, or an `array` if you have multiple usage examples.

This will automatically be tied into seeli's help output. 

```bash
alice hello --help

>>>
Usage:alice hello --help
Usage:alice hello bill
Options:
  -i, --interactive, --no-interactive <boolean> [false] Use the interactive propmts
     --color, --no-color <boolean> [true] Enable ANSI color in output
     --yell, --no-yell <boolean> [false] Pass this flag to yell!
```

#### Interactive

Every command has an interactive mode, which prompts the user for input for each of the flags. All of the answers as collected into a single object and passed to the `run` method as the data object.

##### Interactive Flags

There are a number of flags specifically for interactive mode that gives you more fine grain control over how input is handled

###### skip

If this option is true, the flag will be omitted from the prompts

###### when

This options takes a function which is passed all answers up to the current point in time. The when function should return `true` to indicate that this question should be asked, and `false` if the answer should be skipped.

###### filter

This option takes a function which will be passed all input up to the current point in time. The value returned from this function will be added back into the data before it is passed to the `run` function

###### validate

This options takes a function that will be passed user input for this flag and should return true if the value passes validation. Othwise and string to be used as the error message should be returned

#### Colors & Formatting

In many situations, it is beneficial to colorize output before printing it out. Making error text red, or making important text bold for example. **Seeli** exports all of the methods from the popular [chalk](https://www.npmjs.com/package/chalk) module, so you can apply a variety of different colorizations to your output.

```js
var cli = require( 'seeli' );
var util = require( 'util' );

module.exports = new cli.Command({
	description:'A command to say hello to anyone in the world'
	,usage:[
		cli.bold( 'Usage:' ) + 'alice hello --help'
	  , cli.bold( 'Usage:' ) + 'alice hello Bill'
	  , cli.bold( 'Usage:' ) + 'alice hello Bill --yell'
	  , cli.bold( 'Usage:' ) + 'alice hello Bill --no-yell'
	]
	,flags:{
		yell:{
			type:Boolean
			,default:false
			,description:"Pass this flag to yell!"	
		}
	},
	run: function( directive, data, done ){
		if( data.yell ){
			out = 'HELLO ' + cli.red( directive.toUpperCase() )
		} else{
			out = 'Hello ' + directive 
		}

		done( null, out )
	}
});
```

### Validation

Command validation checks that any flag marked as required, gets passed by the end user. If not supplied, the command will emit an error and exit out of the process. 

```js
var cli = require( 'seeli' );
var util = require( 'util' );

module.exports = new cli.Command({
	description:'A command to say hello to anyone in the world'
	,usage:[
		cli.bold( 'Usage:' ) + 'alice hello --help'
	  , cli.bold( 'Usage:' ) + 'alice hello Bill'
	  , cli.bold( 'Usage:' ) + 'alice hello Bill --yell'
	  , cli.bold( 'Usage:' ) + 'alice hello Bill --no-yell'
	]
	,flags:{
		yell:{
			type:Boolean
			,default:false
			,description:"Pass this flag to yell!"	
		}
		,test:{
			type:String
			,required:true
			,description:'this is a required flag'
		}
	},
	run: function( directive, data, done ){
		if( data.yell ){
			out = 'HELLO ' + cli.red( directive.toUpperCase() )
		} else{
			out = 'Hello ' + directive 
		}

		done( null, out )
	}
});
```
By not specifing a test flag, The command will stop processing before the `run` method is called, out

```
alice hello Bill
>>> Error: test is required
```

### Shorthands & Abbreviations

##### Shorthands

All command flags can be given a short hand assignment if that is more your style. For example, if `--yell` was too much typing for your tastes, we could assign a shorthand of `y` just as easily.

```js
var cli = require( 'seeli' );
var util = require( 'util' );

module.exports = new cli.Command({
	description:'A command to say hello to anyone in the world'
	,usage:[
		cli.bold( 'Usage:' ) + 'alice hello --help'
	  , cli.bold( 'Usage:' ) + 'alice hello Bill'
	  , cli.bold( 'Usage:' ) + 'alice hello Bill --yell'
	  , cli.bold( 'Usage:' ) + 'alice hello Bill -y'
	  , cli.bold( 'Usage:' ) + 'alice hello Bill --no-yell'
	]
	,flags:{
		yell:{
			type:Boolean
			,default:false
			,shorthand:'y'
			,description:"Pass this flag to yell!"	
		}
		,test:{
			type:String
			,required:true
			,description:'this is a required flag'
		}
	},
	run: function( directive, data, done ){
		if( data.yell ){
			out = 'HELLO ' + cli.red( directive.toUpperCase() )
		} else{
			out = 'Hello ' + directive 
		}

		done( null, out )
	}
});
```

```bash
alice hello Bill -y
>>> HELLO BILL
```

##### Abbreviations

All commands are automatically aliased with `alice` as their abbreviated counter parts. A single alias is created for every letter and the previous set of letters in the name of commnad. So all of the following invocations are the same.

```bash
alice h Bill -y
alice he Bill -y
alice hel Bill -y
alice hell Bill -y
alice hello Bill -y
```

The thing to keep in mind here is that the first command to be registered wins and subsequent command of a similar name will only register abbreviations that have not been previously registered. In the case that someone else has created a `helloworld.js` command, our abbreviations would end up to be 

abbreviation | command
-------------|---------
h | hello.js
he | hello.js
hel | hello.js
hell | hello.js
hello | hello.js
hellow | helloworld.js
hellowo | helloworld.js
helloworl | helloworld.js
helloworld | helloworld.js

### Extending Commands
If you need to alter the default behaviors or even extend the functionality of the Base Command, cli gives you a simple inheritance model to do that. Using the `cli.Class` factory, you can create a new type of command which inherits from the Base command.

```js
var cli = require( 'seeli' );
var ExtendedCommand;

ExtendedCommand = new cli.Class({
	inherits:cli.Command
	,format: function( input ){
		return cli.red( input.toUpperCase() )
	}
})

var hello = new ExtendedCommand({
	run: function( directive, data, done ){
		done( null, "Hello " + this.format( directive ) );
	}
})
```

### Using Commands Programmatically 

While, it is highly recommended that complex logic be kept in modules separate from the command for easy re-use, on occasion, you'll find there are times where you want to replicate the logic in a command in the life cycle of you application.  One of the major benefits of using modules as commands is that you can always require it and use it like any other module. This also make **testing** commands rather easy

To execute a command, simply call the `run` function on the command. You pass the `run` method a directive( or null ) and a callback function. You can also set flags on the command by setting the `args` array options where each item in the array is a flag & value, or a short hand followed by its value as a separate argument

```js
var hello = require('alice-starter/commands/hello')

hello.run("Bill", function( err, out ){
	process.stdout.write( out )
})
// Hello Bill

hello.setOptions({
	args:['--yell']
});

hello.run('Sam', function( err, out ){
	process.stdout.write( out )
})
// HELLO SAM

hello.reset();

hello.setOptions({
	args:['-y']
});
hello.run('Sam', function( err, out ){
	process.stdout.write( out )
})
// HELLO SAM
hello.reset();


hello.setOptions({
	args:['Billy', '-y']
});
hello.run(null, function( err, out ){
	process.stdout.write( out )
})
// HELLO BILLY
```

#### Events

Instances of the seeli Command or Commands the inherit from it as also instances of the `EventEmitter` class. By default any flag that has its `event` option set to `true` will emit an event with the value of of the flag before the run function is executed.

```js
var cli = require( 'seeli' );
var util = require( 'util' );

module.exports = new cli.Command({
	description:'A command to say hello to anyone in the world'
	,usage:[
		cli.bold( 'Usage:' ) + 'alice hello --help'
	  , cli.bold( 'Usage:' ) + 'alice hello Bill'
	  , cli.bold( 'Usage:' ) + 'alice hello Bill --yell'
	  , cli.bold( 'Usage:' ) + 'alice hello Bill -y'
	  , cli.bold( 'Usage:' ) + 'alice hello Bill --no-yell'
	]
	,flags:{
		yell:{
			type:Boolean
			,default:false
			,shorthand:'y'
			,description:"Pass this flag to yell!"	
		}
		,test:{
			type:String
			,required:true
			,event:true
			,description:'this is a required flag'
		}
	},
	run: function( directive, data, done ){
		if( data.yell ){
			out = 'HELLO ' + cli.red( directive.toUpperCase() )
		} else{
			out = 'Hello ' + directive 
		}

		done( null, out )
	}
});
```

'''js
var hello = require('alice-starter/commands/hello')
hello.setOptions({
	args:['--test=yes']
})

hello.on('test', function( value ){
	console.log( value )
}) // yes

hello.run("Bill", function( err, out ){
	process.stdout.write( out )
})

// Hello Bill
'''
