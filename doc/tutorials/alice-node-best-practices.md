# Best Practices

Primary best practices for the Alice Platform codebase. Adopted from [RisingStack](https://github.com/RisingStack/node-style-guide)

## Become A package Developer

Node.js, the platform and ecosystem is modular. The biggest step for new Node.js developers is to think in terms of module. Not classes, interfaces services, applications, eggs or whatever else your alternate language may prefer. Gone are the days of monolithic applications. Node applications, are really collections of packages that, when composed, yeild complex behavior. It is important to stop trying to build an application, and focus more on building packages. 

### Anatomy of a Package

```
.
|-- package-one
|   |-- moduleone
|   |   |-- submodule1.js
|   |   |-- submodule2.js
|   |   +-- index.js
|   |-- moduletwo
|   |   |-- submodule1.js
|   |   |-- submodule2.js
|   |   +-- index.js
|   |-- modulethree.js
|   |-- package.json
|   +-- index.js
|-- package-two
```

Both **directories** and **files** should be A single, **lower cased** word - If you think two words are warrented, make a **directory** with word one, and a **file** of word two.

```
// Bad
.
|
+-- SeriousCache.js
```


```
// good
.
|-- serious
    |-- cache.js
    +-- index.js
```

While you may name **files** and **directories** what you whish, specific names are reserved for package developers to house certain pieces of logic and functionality  

A package should follow the following conventions

#### Directories

These directories are reserved for a special purpose within the application and are expected follow these guidelines. If only a single file is expected, the directory should contain a single module named **index**, or the directory can be omitted in exchange for a module of the same name. Any of the folders can be omitted if they are not needed

Directory | Purpose 
---------:|--------
**conf**  | Application / package specific configuration defaults
**lib**   | contains modules for package specific functionality
**test**  | location for package specific tests and test data
**commands** | houses management commands for the command line interface

#### Modules ( Files )

File    | Purpose 
-------:|--------
**index.js**  | The entry point for the package. This will be loaded when someone executes `require('your-package')`
**README.md**  | Package specific documentation and examples of usage
**package.json**  | The npm package definition. You should define a **name**, **version**, and **dependencies** at the least
**resources.js** | API Resources that define endpoints for a given content type with in the system
**signals.js** | predefined event emitters used to provide hooks into application behavior
**.gitkeep**  | will force git to keep the folder when changing branches
**.gitignore**  | Will force git to ignore specific files or patterns of files
**.npmignore**  | similar to gitignore, but tells npm which files and directories to ignore when published or packed

#### Package
A **package** is a **directory** with a `package.json` file. A package, is comprised of modules, each with a narrow focus that together solve a *pattern*. By building a package that solves a pattern, and not a problem, the package can be used by other projects and applications to solve specific problems, you may not have thought about.

#### module
a **module** is a **file** with a `.js`, `.json`, or `.node` extension. Modules should adhere to the Unix philosophy, Do one thing and do one thing very well. Modules can contain `methods`, **private** `functions` `classes` or anything other construct that helps solve a pattern. Modules can, and should export more than one thing. 

#### submodule
A **module** can also be a **directory** with, at the very least, an **index.js** also referred to as a `submodule` A module can also be a directory with one or more **modules** to break up logic and keep file size down a more easily digestible by humans. 

#### entrypoint ( `index.js` )
Every directory in a node.js package should have an entrypoint, typically `index.js`. This allows the `require` function to load `packages`, `modules` and `submodules`.

in the example package structure above, a developer would be able to do the following:

```js
var packages = require('package')
var moduleone = require('package/moduleone')
var moduletwo = require('package/moduletwo')
var modulethree = require('package/modulethree')
```

The `index.js` module is typically used as a `collector` module or `bootstrap` module in which modules within the same directory or required and exported under properties with the same name as the module's file

## Dealing With Errors
Node.js has two primary ways of handling errors, [domains](http://nodejs.org/api/domain.html) and callbacks

#### Account for Domains
Domains can be especially helpful in keeping unexpectedly thrown errors from crashing a process. Developers who use domains will be listening for an `error` event. It's best practice to emit and error event with a single `error` rather than throwing the error. 

#### Account for Callbacks

It is common practice with in the Node core libraries to accept callbacks for functions and methods that do some kind of asynchronous work and executing the callback when it has finished. All callback should accept 2 parameters, and `error`, if there is one, and the `result` of the original function call. if their is no error, a `null` should be passed. Not *false*, `undefined` or `0`. Callbacks should also be an `optional` parameter, and only executed when passed. The result of the callback should also be `returned`



```js
// callback with error
function async( param, callback ){
	// do something asyc...
	var e = new Error();
	e.name = "AsyncError";
	e.message = "Something Async didn't finish"

	return callback && callback( err, null );
}
```


```js
// callback with no error
function async( param, callback ){
	// do something asyc...
	var e = new Error();
	e.name = "AsyncError";
	e.message = "Something Async didn't finish"

	return callback && callback( null, data );
}
```

#### Relaying Errors 

Because javascript allows any object ( string, object literals, arrays ) to be thrown as errors, and the error object is limited in its introspect-ability, it is considered best practice to always define the `messagee` and `name` property.

```js
var e = new Error();
e.name = 'MyError';
e.message = 'You did something wrong';
``` 

To account for both optional callbacks and error domains, errors should first be sent with an `error event` followed by a callback executed with the error passed as the first argument

```js
function async( param, callback ){
	// do something asyc...
	var e = new Error();
	e.name = "AsyncError";
	e.message = "Something Async didn't finish"

	this.emit('error', e);
	return callback && callback( err, null )
}
```


#### Custom Exceptions
Because the javascript Error object is rather difficult to deal with, The **Alice** platform provides a way to easily create custom error classes that maintain their name and current stack context. If you are in control of the error being produced, always create a custom Error class for the situation and use that over the default error.

```js
// bad

try{
  // crazy thing
} catch( e ){
  throw e;
}
```

```js
// good
var Exception = require('alice-stdlib/exception')
try{
  // crazy thing
} catch( e ){
  var CrazyException = new Exception({
    name:"CrazyException"
    message: e.message || "That crazy thing went bad"
  });

  throw CrazyException;
  
}
```

## Logging
Logging should be used to trace what an application is doing, and general application state when it is doing it. When things go wrong in production, developers should be able to use logs as an audit trail to determine what was happening and why something went wrong.

Don't use `console.log` For internal debugging information use the [debug](https://www.npmjs.org/package/debug) module to write names-paced debugging information. 

```js
// bad
console.log('this is bad')
```

```js
// good
var debug = require('debug')('namespace:module')
var message = 'because I can tell where it can from';
debug('this is better %s', message)
```

Use the [alice-log](https://bitbucket.org/rgable/spiritshop-api/src/5d4a13f6efa7856c9170f032ce4a6f4ff8447f01/packages/alice-log/?at=master) module to log behavior, complex logic, in areas of potential errors. Logs should be verbose enough to make sense to a developer other than the one who wrote it.

```js
// bad
var logger = require('alice-log')
function complex( x, y, z ){
  logger.info( x, y, z )
  return x + y + z;
}
```

```js
// good
var logger = require('alice-log')
function complex( x, y, z ){
  var result = x + y + z;
  logger.info('complex execution result = %s', result, {x:x, y:y, z:z})
  
}

```

Use appropriate logging levels to accurately portray what is happening

```js
var logger = require('alice-log')

logger.info('something bad just happend')
logger.error('an error has ocured', new Error())
logger.crit('exiting the process');

```

```js
var logger = require('alice-log')

logger.warning('something bad just happend')
var e = new Error();
e.message = 'some process did a bad thing'
logger.error('an error has occurred: %s', e.message, logger.exception.getAllInfo( e ) )
logger.crit('exiting the process');
process.abort();
```
## Configuration

Where possible internal application behavior should be driven by configuration, and excluded from code. This helps to minimize the number and frequency of deployment for environmental changes. Configuration management should handled by the [alice-conf](https://bitbucket.org/rgable/spiritshop-api/src/5d4a13f6efa7856c9170f032ce4a6f4ff8447f01/packages/alice-conf/?at=master) module

Configuration definitions should be:

* Defined as simple, or nested objects that can be serialized to and from JSON
* Defined as `lower` cased keys, are intended to be reserved exclusively environment variables, where `upper` case is preferred.
* Defined as finde rained as possible allowing implementation decide how to deal with the values


## Variables

* Delcare all variables at the top of the scope in which they are contained.
```js
// good
function test(){
  var a, b;
  if( a ){
    b = 1;
  }
  return b;
}

// bad

function test(){
  var a;

  if( a ){
    var b = 1
  }

  return b;
}
```

* Use a single var keyword

```js
// good
var one
  , two
  , three
  ;

// bad
var one;
var two;
var three;
```

* assignments should be kept separate from declarations ( aside from requires ), immediately follow the declaration block

```js
// good 
var one
  , two
  , cast
  ;

one  = 1;
two  = 2;
cast = someFn()

// bad

var cast = someFn()
  , one  = 1
  , two  = 2
  , three = one + two
  ;
```

* Don't use single letter or ambiguous variable names

```js
// good
var index
  , resultArray
  , pkgName

// bad
var a
  , i
  , data
```

* Document All variables.  

```js
// good
var index       // loop variable for indexing result array
  , resultArray // array to collect results for return value
  , pkgName     // name of the package to inspect
  ; 

// bad
var a
  , i
  , data
```

## Require Statements

* All require statements should made at the top of the file before any other code. And in to following order
    * Core Node Modules
    * NPM dependency modules
    * Internal Packages 
    * Relative path modules
    * Optional dependencies

```js
var fs    = require( 'fs' )
  , os    = require( 'os' )
  , async = require( 'async' )
  , pkg   = require( 'internal-package' )
  , Base  = require( './Base' )
  , heapdump
  ;

try{
  heapdump = require( 'heapdump' );
} catch( e ){
  // I don't care.
}
```

* Always exclude file extensions

```js
// bad 
var mod = require("../mod.js")

// good
var mod = require( '../mod' )
``` 
* Module requires are also variable assignments, and should follow the variable best practices 
## Functions

- Function expressions:

```js
// anonymous function expression
var anonymous = function() {
  return true;
};

// named function expression
var named = function named() {
  return true;
};

// immediately-invoked function expression (IIFE)
(function() {
  console.log('Welcome to the Internet. Please follow me.');
})();
```

- Never declare a function in a non-function block (if, while, etc). Assign the function to a variable instead.

```js
// bad
if (currentUser) {
  function test() {
    console.log('Nope.');
  }
}

// good
var test;
if (currentUser) {
  test = function test() {
    console.log('Yup.');
  };
}
```

- Never name a parameter `arguments`, this will take precedence over the `arguments` object that is given to every function scope.

```js
// bad
function nope(name, options, arguments) {
  // ...stuff...
}

// good
function yup(name, options, args) {
  // ...stuff...
}
```

- Always name your functions, it makes debugging and tracing significantly easier

```js
// bad
var fn = function(){
	...
}
// good
var fn = function fn(){
	
}

//bad
var Class = {
	method: function(){
		...
	}
}

//good
var Class = {
	method: function method(){
		...
	}
}
```
## Accessors

  - Accessors for public properties are not required
  - If you need to keep data priviate, or which to condense complex logic, use [Object Properties](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperty)

```js
    // bad
    
    Object.defineProperties(exports,{
    	enumerable: false
    	,writable: true
    	,configurable: false
    	,hidden:{
    		get: function getHidden(){
    			// complex logic
    			return result;
    		}
    		,set: function setHidden( value ){
    			// complex logic
    			privatevar = result;
    			return this;
    		}
    	}
    })

    exports.hidden // calls get
    exports.hidden = 12; // calls set
```
  - If you do make accessor functions use getVal() and setVal('hello')

  - If the property is a boolean, use isVal() or hasVal()

```js
    // bad
    if (!dragon.age()) {
      return false;
    }

    // good
    if (!dragon.hasAge()) {
      return false;
    }
```

  - It's okay to create get() and set() functions, but be consistent.

```js
    function Jedi(options) {
      options || (options = {});
      var lightsaber = options.lightsaber || 'blue';
      this.set('lightsaber', lightsaber);
    }

    Jedi.prototype.set = function(key, val) {
      this[key] = val;
    };

    Jedi.prototype.get = function(key) {
      return this[key];
    };
```

## Code Blocks

* Always use use braces. 
* Always place braces on their own line

```js
// bad

if( true ) return 1;
if( true ){ return 2; }

// good
if( true ){
  return 1;
}
```

## Errors

* Use Try / Catch in synchronous code.
* In Async Code, emit an `error` event or invoke a callback passing the error as the first arguments

```js
//bad
function readPackageJson (callback) {
  fs.readFile('package.json', function (err, file) {
    if (err) {
      throw err;
    }
    ...
  });
}
//good
function readPackageJson (callback) {
  fs.readFile('package.json', function (err, file) {
    if (err) {
      return  callback(err);
    }
    ...
  });
}
```


* Avoid using explicit Try/catch in heavily used functions, as they force the entire function to be de-optimized by V8. Use a helper method

```js
//bad
function expensive( data, cb ){
  try{
    for( var x = 0; x < data.length; x++ ){
      // really expensive work
    }
  } catch(e){
    cb( e )
  }
}

//good
function attempt( fn, data, cb ){
  try{
    return fn(data, cb);
  } catch( e ){
    return null
  }
}

function expensive( data, cb ){
    for( var x = 0; x < data.length; x++ ){
      // really expensive work
    }
}

attempt( expensive, data, cb)
```

## Comments

* Use JSDoc Comment blocks to document `modules`, `classes`, `properties`, `functions`, `errors`, and `events`
* Use `//` single inline comments to document logic and behavior

* Use `// FIXME:` comments to annotate known problems which need to be fixed. A JIRA issue ID should be included if available

```js

// bad 
// this doesn't work
function broke( ){
  throw new Error();
}

// good
// FIXME: [WHIT-XXX] This doesn't work
function broke(){
  throw new Error()
}
```

* Use `// TODO:` to annotate code which is yet to be implemented

```js

// bad 

function empty( ){

}

// good
// TODO: [WHIT-XXX] This needs to be done
function empty(){

}
```
