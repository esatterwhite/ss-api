## Introduction
{@link module:alice-stdlib/class|Class} provides the foundation for classical object orientated development patterns. With in the context of Alice, {@link module:alice-stdlib/class|Class} provides *inheritance*, *multiple inheritance* via *mixins*, and *plugins*, known as `Mutators`.  All of these things can be mix and match in any fashion giving you a very robust Object model.

### Usage
Using Class is exceptionally simple. When you require the {@link module:alice-stdlib/class|class} module, it returns the Class object itself which you can use like any other JavaScript object. You define a class by assigning a class instance to a variable and passing it's constructor a single object which will server as your class prototype.

#### Example - Basic Class
Lets create a module called car.js which defines a simple car class that has a driver and keeps track of the distance it has traveled
```js
// car.js
var Class = require('alice-stdlib/class');
var Car = new Class({
      distance: 0
    , drive: function( ){
        this.distance++;
        return this.distance
    }
    , reverse: function( ){
        this.distance--;
        return this.distance;
    }
    , honk: function( ){
        alert( "beep" )
    }
});

// returning the class definition
module.exports = Car;
```

Simple enough, Now we can create a new instance of a Car and start using it.
```js
var pinto = new Car();

pinto.drive( ) // 1
pinto.drive( ) // 2
pinto.reverse( ) // 1
pinto.honk( ) // beep
```

### Constructor Functions
Our basic car is just that, basic. It moves! Let say we want to do a little setup work by allowing the end user to specify the name of the driver of their specific car instances. For that, we are going to need a constructor function to accept some parameters. The a `Class` definition looks for a method call `constructor` that will be used as the constructor function for each class instance.

#### Example - Implement a constructor
```js
// car.js
var Class = require('alice-stdlib/class');
var Car = new Class({
     constructor: function( driver ){
        // set the driver
        this.driver = driver;

        // reset the distance to 0
        this.distance = 0
    }
    , drive: function( ){
        this.distance++;
        return this.distance
    }
    , reverse: function( ){
        this.distance--;
        return this.distance;
    }
    , honk: function( ){
        alert( "beep" )
    }
});

// returning the class definition
return Car;
```

```js
var pinto = new Car("Bobby");

pinto.drive( ) // 1
pinto.drive( ) // 2
pinto.reverse( ) // 1
pinto.honk( ) // beep

pinto.driver // Bobby;
```

Class provides the foundation for building javascript applications in a more traditional Object orientated fashion. In this example we have created a simple Car that has a handful of method for us to expand on.
