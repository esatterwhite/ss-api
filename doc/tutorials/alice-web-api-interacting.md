### Sorting

`v2` endpoint support multi field and multi direction and sorting using the `orderby` query string params, where th value is the name of the field. to reverse the direction of the sort, prepend a (`-`) to the field name.

```
{@lang bash}
# order by count in an ascending fashion 
curl localhost:3001/api/v1/fake?orderby=count
```

### Fetching Collections of Resources
Listing Endpoints will return singular uniform objects. We can use the shipping endpoint as a reference

```
curl http://localhost:3001/api/v1/shipping
```

will return data that looks like:

```
{@lang javascript}
{
	"meta": {
		"count": 11,
		"limit": 25,
		"offset": 0,
		"previous": null,
		"next": "/api/v1/shipping?apikey=c4125761-7f82-49b4-8436-3bde15dabb7e&fetch=activities&offset=3&limit=3"
	},
	"data": [{
		"_id": "53d647f9e4b0215297bfdf45",
		"method": "standard",
		"description": "Delivery within 4-8 business days",
		"__v": 0,
		"rate": 0,
		"type": "flat",
		"orderMaximum": 99999999,
		"orderMinimum": 10000
	}, {
		"_id": "53d64779e4b0215297bfdf43",
		"method": "standard",
		"description": "Delivery within 4-8 business days",
		"__v": 0,
		"rate": 450,
		"type": "flat",
		"orderMaximum": 4999,
		"orderMinimum": 0
	}, {
		"_id": "53d647b5e4b0215297bfdf44",
		"method": "standard",
		"description": "Delivery within 4-8 business days",
		"__v": 0,
		"rate": 700,
		"type": "flat",
		"orderMaximum": 9999,
		"orderMinimum": 5000
	}]
}

```

There are something to pay attention to here:

* By Default results will be paginated with a page size of `25` by default.
* The meta object will contain the `previous` and `next` properties. If populated, these are the URIs to their respective pages
* You will get a lits of the resource objects under the `data` key
* Setting a `limit` query string parameter will change the page size.
* Setting a `limit` of 0 will skip paging all together

```
{@lang bash}
# order by count in an ascending fashion and name in a descending fashion
curl localhost:3001/api/v1/fake?orderby=count&orderby=-name
```

### Filtering 

`v2` Endpoint and select `v1` endpoint expose an expressive filtering interface which allows consumers of the API to select the specific subsets of data information. Filters are used by passing query string values to the endpoint. And values That do not match pre defined values will be treated as field filters. `Filter Types` are appended to the name of the field with a double underscore (`__`). 

For example if you want to restrict the data set to only return items where the count field is greater than five, you would use the `gt` filer


```
{@lang bash}
curl localhost:3001/api/v1/fake?count__gt=5
``` 

It is possible to specifiy any number of fields and filter types to further restrict the data that is returned. Multple filter types are `added` together

```
{@lang bash}
# files items where count is greater than 5, less than 50 and the name starts with Mak
curl localhost:3001/api/v1/fake?count__gt=5&count__lt=50&name__startswith=Mak
```

If the same field / filter type is specified more than once, the values are combined in to an array before proecessing. This is typically used with array filters, `in`, `nin`,  `all`


You can filter by `nested fields` with the same `__` syntax. The last `__` is always considered a filter. For example, if we were getting the following Data back
```
// Fake resource Data
{
	"meta":{}
	"data":[{
		"nested":{
			"name":'Foo',
			"value":1
		}
	}]
}
```

We would be able to filter for resources where nested value is 1 with the following urls

```
{@lang bash}
curl http://localhost:3001/api/v1/fake?nested__value=1
curl http://localhost:3001/api/v1/fake?nested__value__exact=1
curl http://localhost:3001/api/v1/fake?nested__value__gte=0
```

##### Things to note:

* query string parameters can not be overloaded, meaning, if they are defined on an endpoint for specific use age in the endpoint prior to filtering, they will be excluded from filtering purposes. 
* Mulitple text filters on the same field is no recommeded. filtering for nested name fields with `__startswith=F` & `__endswith=o` will yeild poor results. it is better to use the `contains` filter.
* if a filter type is not provided, it will default to `exact`
```
{@lang bash}
# find all items were the name is foo, or the name is bar
curl localhost:3001/api/v1/fake?name__in=foo&name__in=bar
```
#### Filter Types

Filter | Since | Usage | Value Types | Example
-------|-------|-------|-------------|--------
exact       | v1 | Exact Match                     | `String`, `Number` | `field1__exact=1`, `field2=foo`
iexact      | v1 | Case Insensitive Exact Match    | `String`           | `field1__iexact=FOO`, `field2=foo`
gt 			| v2 | Greater Than                    | `Number`           | `field__gt=1`
gte         | v2 | Greater Than or Equal           | `Number`           | `field__gte=3`
lt          | v2 | Less Than                       | `Number`           | `field__lt=5`
lte         | v2 | Less Than or Equal To           | `Number`           | `field__lte=1`
ne          | v2 | Not Equal To                    | `Number`           | `field__ne=4`
in          | v2 | In                              | `Array`            | `field__in=foo&field__in=bar`
nin         | v2 | Not In                          | `Array`            | `field__nin=foo&field__nin=bar`
size        | v2 | Length Match                    | `String`, `Array`  | `field__size=10`
match       | v2 | ...                             | `Number`           | `field__match=`
all         | v2 | Deep Match                      | `Array`            | `field__all=a&field__all=b&field__all=c`
regex       | v2 | Regex Match                     | `String`           | `field__regex=^([\w]*)`
contains    | v2 | Partial Match                   | `String`           | `field__contains=ell`
icontains   | v2 | Case insensitive Partial Match  | `String`           | `field__icontains=AKE`
startswith  | v2 | Case Sensitive Leading match    | `String`           | `field__startswith=Hel`
istartswith | v2 | Case Insensitive Leading match  | `String`           | `field__istartswith=the`
endswith    | v2 | Case Sensitive Trailing match   | `String`           | `field__endswith=ing`
iendswith   | v2 | Case Insensitive Trailing match | `String`           | `field__iendswith=ArKiNg`

### Serialization

All endpoint in the **Alice** frame work will accept and return multiple data formats. JSON is the default format, however, alternate formats can be specified using the ``Accept` header to get a specific format returned. The `Content-Type` head will denote the data format you are trying to send

##### xml
Set the `Accept` head to `text/xml` to get data returned as **XML**

```
{@lang bash}
curl -H "Accept: text/xml" localhost:3001/api/v1/fake 
```
```
{@lang xml}
<?xml version="1.0" encoding="UTF-8"?>
	<response />
		<meta type="object" / > 
		...
```
JSONP can be returned by setting the `Accept` header to text/javascript.
```
{@lang bash}
curl -H "Accept: text/javascript" localhost:3001/api/v1/fake 
```
```
callback({meta:{},data:{}})
```
When requesting JSONP, a callback parameter can be specified to change the name of the callback function in the response
```
{@lang bash}
curl -H "Accept: text/javascript" localhost:3001/api/v1/fake?callback=foobaf
```
```
foobar({meta:{},data:{}})
```
