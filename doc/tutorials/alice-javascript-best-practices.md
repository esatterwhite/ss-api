These are syntax and style conventions extracted from the Alice core code. This page has not been written by an Alice developer and is solely intended to provide a basis for module developers to assimilate their coding style to the framework’s.

## Documentation

The Alice project code documentation follows the [JSDoc](http://usejsdoc.org) style of code documentation

### Module

A module file should reserve the first 3 lines of the file for JSHint configuration docstrings and strict declaration

```js
/*jshint node:true, laxcomma: true, smarttabs: true*/
/*global define*/
'use strict';
```
Following he configuration header, each module must declare a module header which denotes, at minimum, module `authors`, module `requires`, module `name`, module `description`, and the `semver` of when the module was introduced

```js
/**
 * Descripion here
 * @module module:alice-stdlib/exception
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires fs
 * @requires os
 */
```

### Classes

A class should be proceeded by a document header which denotes class `name`, `inheritance`, class `mixins`, class `parameters`,  and `events` the class may emit, and any `errors` the class may throw. Class doc headers may optionally include one or more example usage tags

```js
/**
 * A Description of the calls
 * @class module:PACKAGE/MODULE.CLASS
 * @extends Error
 * @mixes EventEmitter
 * @fires foo
 * @throws bar
 * @param {Object} options Config options for the error instance
 * @example var Exception = require("alice-stdlib/exception");
 */
```
### Variables
Variable should be documents for their intended usage with in their defined scope.

## Whitespace

Alice uses tabs for indentation. Trailing white space is discouraged as well as indentation for empty lines.

### Commas

Commas typically have no leading space, and a singe trailing space. When a line break is introduced in a list or object, the comma should start the line and have a trailing space

```js
var a = [ 1, 2, 3,4 ];
var b = { key: 'value', foo: 'bar'}
```
```js
var a = [
	  1
	, 2
	, 3
	, 4
];

var obj ={
	  'key1':'value1'
	, 'key2':'value2'
	, 'key3':'value3'
};
```
## Comments

The commenting style of the framework preserves the Natural Docs syntax. Since Natural Docs is no longer being used by the Alice project, the commenting style is up to the plugin developer.


## Alignment

In the pursuit of clean and readable code, modules should adhere to a simple alignment conventions 

### Variables
When more then one variable is defined, they will be defined in a single statement with a single newline after each varable and each line must begin with a comma. Each comma shall be aligned so that the first letter of each variable is aligned with the previous. 

```js
var one
  , two
  , three
```
All Variables should be defined at the very top of a module or function with a single line break between each variable as to allow for variable documentation. A hanging statement terminator is allowed. One and **only** one variable declaration statement should be used per function context, or block when in `harmony` mode. Each variable doc comment should be aligned with the first slash of the previous and should allot enough space so there is a single space from the longest variable name

```js
var one        // variable one docs
  , two        // variable two docs
  , three      // variable three docs
  , reallylong // variable reallylong docs
  ;
```
#### Assignments
## Statements

### Brackets

They do not take their own lines when beginning a statement.
In function definitions, they accompany the named parameters without a space.

```js
function $A(iterable){
	...
}
```

The compound statements that involve only a simple statement when executed must include brackets to minimize confusion. The `case` statement is an exception

```js
if (typeof a1 == 'string'){
	return add( this, a1, a2, a3 );
}
```
```js
case 1: return 'element';
```

```js
} else {
	return ctx.getElementsByTagName(tag);
}
```
### Statement terminator ;

### Simple Statements.

It’s **always** included.

```js
var unlinked;
```
### Compound Statements.

It’s not included after the closing bracket, with the exception of the global object literal definition.

```js
var Alice = {
   'version': '1.2.0'
 , 'build': ''
};
```

### Function Expressions

It’s **always** included after the closing bracket.

```js
var foo = function(){
	...
};
```
### Function Declarations

It’s **always** included after the closing bracket.

```js
function foo(){
	...
};
```
## Separation

### Spaces

As previously pointed out, brackets accompany function parameters without a space, which accompany the function keyword without a space.
Operators are always separated with single spaces within the expression. The negation operator is an exception.
Object literal expressions do not include extra spaces at the beginning or end when used as parameter.

```js
this.headers.extend({'Accept': 'application/json', 'X-Request': 'JSON'});
```

All reserved keywords are followed by spaces, with the exception of the function definition.
Function calls or definitions shall have a single space after the opening paren, and a single space before the closing when parameters are present.
no space is required if there are no parementers 

```js
object.implement = function( a1, a2, a3 ){
	if (typeof a1 == 'string'){
			return add( this, a1, a2, a3 );
	}
	for (var p in a1){
			 add( this, p, a1[p], a2 );
	}
	return foo();
};
```

### Linebreaks

Statements within a bracketed block are separated with a single line break. Optionally, for the sake of readability, it’s observed that extra line breaks are used to separate logical pieces of code within the same compound statement.

Classes initialization, function definitions, generic objects and global function calls are separated from each other with two line breaks (an empty line between them).

Blocks of code that belong to the same component are separated with three line breaks (two empty lines), followed by comments describing the component.

Each component commented description and license is succeeded with two line breaks (an empty line).
Classes definition include empty lines after the opening bracket and before the closing bracket.

```js
Base.Clone = new Class({

	inherits: Base,
				...
	success: function(text){ }

}); 
```
### Indentation

A single tab is used to indent code within a bracketed compound statement, unless it has been shortened. This also applies to subsequent, inner statements.

### Quotation

The use of the single quotation mark is preferred.

In Object literal expressions, quotation is generally omitted for the key part.

```js
sender.send({data: this, url: url || sender.options.url});
```