The web package provides the HTTP layer for the platform API. The package exports a fully configured `hapi` web server.
If the package is invoked from the primary node process, it will automatically start. If required as a module, you will need to call the {@link module:alice-web.start|start} method

#### As A Module

```js
var web = require('alice-web')

web.start(function(){
	console.log("server running!")
});
```

#### As a command

starts the server on port 4000 with the stdout logger

```sh
alice web -l stdout -p 4000
```
