- [Basics](#markdown-header-basics)
- [Commit Messages](#markdown-header-commit-messages)
- [Tags](#markdown-header-tagging)
- [Branching](#markdown-header-branching)
- [Merging](#markdown-header-merging)
- [Hotfixing](#markdown-header-hotfixing)

## Basics

1. Make a branch for that. Branches are cheap, can be discarded, moved, split, and can point to any point in time in the project history. If you are doing work on a new issue **make a branch**. If you are doing some experimental work, **make a branch**

2. **Commit early** - Commit often. Remember, Committing and pushing are two very different things. Don't run the risk of losing work by accident.

3. ***Small Commits** - The smaller the commit, the easier it is to find where bugs are introduced. In general, you should be able to describe the entirety a commit in a single sentence.

4. **Form and Function** - Where possible, you should keep commits the change formatting, structure ( linting ) separate from functional changes to code. The fact that no functional changes were made should be noted in the commit message

5. **Push your branch** - Branches are cheap, work is not. Pushing a branch also allows multiple people work on the same set of functionality in isolation from the integration branch with minimal conficts.

6. **Commit Messages** Be descriptive. 

## Commit Messages
A Well structured Commit message should consist of 3 primary lines

1. Short Message
2. Blank Line
3. Long Message

### Short line
The first part of the short line should be a valid JIRA ticket id that relates to the work in the commit, or the explict word `NOISSUE` if there is no ticket associated to the work. The remainder of the short line should give a quick description of the commit. The length restriction is so that short logs, GUI tools and other things the generate GIT commit trees can display the entire message with out truncating it.

The short line should follow the following format

```
[JIRA ID | NOISSUE] description of the work in the commit <= 80 characters
```


### Blank line
Blank line to provide separation

### Long description
The long description should be a detailed message of the commit, can be any length and span as many characters as needed ( within reason ).

#### Examples

###### With Issue
```
[WHIT-XXX] Finalize work on queuing feather

* fix rate limiting
* update logging
* adding some doc

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce semper nec felis interdum maximus. 
Suspendisse fermentum ornare velit pellentesque posuere. Praesent porttitor tempor viverra. 
Vivamus dui lectus, tincidunt vel metus eget, iaculis vehicula velit. Maecenas ac neque vitae nibh tincidunt viverra. 
Nam aliquam sapien sem, a consequat arcu venenatis nec. Nulla lacus risus, euismod eget quam eu, rutrum varius nisi. Pellentesque faucibus tincidunt ex sed lobortis.
```

###### No Issue
```
[NOISSUE] Finalize work on queuing feather

* fix rate limiting
* update logging
* adding some doc

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce semper nec felis interdum maximus. 
Suspendisse fermentum ornare velit pellentesque posuere. Praesent porttitor tempor viverra. 
Vivamus dui lectus, tincidunt vel metus eget, iaculis vehicula velit. Maecenas ac neque vitae nibh tincidunt viverra. 
Nam aliquam sapien sem, a consequat arcu venenatis nec. Nulla lacus risus, euismod eget quam eu, rutrum varius nisi. Pellentesque faucibus tincidunt ex sed lobortis.
```

## Tagging

In general tags should be used to mark releases and should be named the with same [semver](http://semver.org/spec/v2.0.0.html) format as the project it marks, excluding a prerelease:

* `major`.`minor`.`patch[-prerelease.<count>]`
* [v]`major`.`minor`.`patch[-prerelease.<count>]`

```
0.1.9
v1.1.9
v1.2.0-alpha.1
```
#### semver

* major - introduces backward incompatible changes or changes that alter how a system fundamentally operates
* minor - typically associated with new features and functionality
* patch - 

Tags should be made after the release branch has been fully tested and passed QA. Tags should be signed with gpg by the package maintainer. **Every** push to production should yield a new version & tag.

* `git tag -sf <version>`

## Branching
In general a branch should be made for work relating to JIRA issue IDs. Branches should be generated for Story level tasks. Epics become to general and sub-task generate to much noise and merging. 

#### Master
The `master` branch is reserved as the integration & main line branch where all divergent branches shall be merged back into before releases. The master branch should be considered safe, working code.

## Merging
It is considered best practice to not use the `pull` command, but rather use `fetch` and `merge` or `rebase` as needed between branched. Branches need to stay up to date with the `master` ( or integration ) branch. Before merging a branch back into the integration branch, you should rebase your branch on top of the integration branch and apply a fast-forward merge back into the integration branch

```
git check WHIT-XXX
git rebase origin/master
git checkout master
git merge origin/master
git merge WHIT-XXX --ff-only
```

## Hotfixing

When fixes need to be applied to a previous release and deployed, it is important that those fixes also be applied to the integration branch as well as any other active branches. To make this as easy as possible, a few simple steps should be followed:

* Branch from the release that needs the fix
```
git checkout hotfix/WHIT-XXX production_branch
```
* Commit work to hotfix branch
* checkout and update master
```
git checkout master
git fetch origin
git merge origin/master
```
* apply a non fast-forward merge into `master`
```
git merge --no-ff hotfix/WHIT-XXX
>> ac58de9ff515705ca94606e1e31bffebefc16541
```

* checkout the production release
* merge the second parent of the previous merge commit into the production branch
* increment patch version with tag
```
git checkout production_branch
git merge ac58de9ff^2 
git tag -sf 2.0.1
git push origin production_branch
git push origin --tags
```