# Build Great Software

Hackers together together code. Programmers implement features. Engineers build great software. However, great software doesn't happen on accident. As an engineers we strive to improve out craft, learn from our mistakes, obsess over perfection and treat our code as art. Their as seven primary factors that will ultimately yield great software.


## Working

Working software is software that meets business functional expectations, meets peer functionality expectations, operates at scale, under stress and under unexpected use cases. 

##### Testable / Tested

To ensure the working nature of your software, crafted in a way that allows it to be tested by, both , a peer as well as in an automated environment. All working software should be shipped with valid test that we built with the intention of **disproving** its working nature. Tests should be easily reproducible and run under stressful circumstances. 

## Documented

Undocumented code creates knowledge silos, raises the barrier of entry to new engineers, increases future development time and maintenance cost. All great software should ship with in-line API documentation, with working examples and public API documentation where applicable.

## Usable

Customer satisfaction is only part of the equation. Usable software should be usable, not only by end users, but other engineers as well. While building your software, always keep in mind how someone else may want to use what you are building, and what could be done to make it easier for them to use it. The same can be said for the non-technical end user.

## On Time

Don't be late. Even the best software will be passed over if you miss the original dead line. Plot out time for testing, documentation and padding for unexpected hiccups in development before you begin writing code.

## Supportable

When unexpected behavior arises, being able to determine the nature of the behavior is critical to correcting problems in a timely fashion. Restarting it is not a viable support option. If their are dedicated support staff, the software should make their job easier, it should allow them to better support customers and manage the related systems. 

##### Introspect-able 

Applications should allow for live introspection where possible. If not viable, extensive postmortem through the most appropriate means - Logging, error reporting, event tracking, data analysis, etc.   

## Innovative

Don't be comfortable doing what you know. Push your own boundaries and look for new and better practices. Tinker, build, explore, and experiment. Share what you learn. Innovation is a collaborative, evolutionary process. Innovative software is a result of being able to analyze your problem space, recognize what is wrong with current solutions and being able to produce working solutions that have yet to be implemented by others. 

## Marketable

Marketable software is software that solves a specific class of problems. More importantly, allows other people to solve a specific class of problems. Marketable software is software that, not only you, but other people ( customers, companies and development teams ) want to use because it provides them the best avenue to do so. Software that is well documented at multiple levels, easy to follow, use and extend in an easy and flexible way will yield marketable software
