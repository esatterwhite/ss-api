A {@link module:alice-catalog/models/decal|Decal} is analogous to an Image Template that has been loaded into adobe's [Scene 7](http://en.wikipedia.org/wiki/Scene7). In short you can think of Scene 7( S7 ) as an *API* to dynamically turn off and on layers of an adobe illustrator or PSD file, perform variable replacement and apply filters and transformations to resulting images dynamically. The process referred to as **Decal Inclusion** is deterministic process which decides which layers to turn on and what the values to be used in variable replacements. It is very much a similar process text or HTML Templating. 

Each of the image permutations is then applied to each of the available products that spirit shop offers, for each {@link module:alice-catalog/models/tenant|School} in the system. This is how a product catalog is dynamically generated for each school

1. A blob a data, which is determined by the {@link module:alice-catalog/models/decal|Decal} instance, is generated programmatically
2. Data is passed to **S7** via URL
3. **S7** applies Data to an Image Template
4. **S7** returns a unique image

**S7** has 2 primary services for images.

* Serve - returns a static hosted image with filters and manipulations applied to it ( [Example](http://crc.scene7.com/is-docs-4.0/) )
* Render - Applies data and nested [FXG filters](https://marketing.adobe.com/resources/help/en_US/s7/is_ir_api/is_api/http_ref/r_request_nesting_and_embedding.html) to an image template and returns a generated image

A single decal can generate multiple images. Within the confines of Spiritshop, the permutations of a decal tend to be a base image with multiple activity names or/or artwork graphics( which also live in **S7**. For example:

```bash
|-- Decal ss0001
|   |-- Activity Football
|   |   |-- artwork art_act_football_001.png
|   |   |   `-- Renders - Image 1
|   |   |-- artwork art_act_football_002.png
|   |   |   `-- Renders - Image 2
|   |   -- artwork art_act_football_003.png
|   |   |   `-- Renders - Image 3
|   |-- Activity Baseball
|   |   |-- artwork art_act_baseball_001.png
|   |   |   `-- Renders - Image 4
|   |   |-- artwork art_act_baseball_002.png
|   |   |   `-- Renders - Image 5
|   |   |-- artwork art_act_baseball_003.png
|   |   |   `-- Renders - Image 6
```

For Decal 0001, given 2 different activities, which each have 3 available images, we would be able to render 6 different images. While their are other variables that can be manipulated, the activity information is what yields additional permutations and is the most important to understand

### Activity Variable Rules

 Text Inclusion | Art Inclusion | Action | Notes 
:--------------:|:-------------:|--------|-------
 &#10004; | &#10006; | Render a Temlpate & Display Decal once per Activity Text Category specified in data set | <ul><li>The rules Under the Included Text Variables section Apply.</li><li>The Decal's Activity Category Text  is an explicit UI filter</li></ul>
&#10006; | &#10004; | Render a Temlpate & Display Decal Once Per Matching Activity Category in Activity Collection | <ul><li>The rules Under the Included Text Variables  section Apply.</li><li>The Decal's Activity Category Text  is an explicit UI filter.</li></ul>
&#10006; | &#10006; | Render a Temlpate & Display Decal once per Activity Text Category | <ul><li>The rules Under the Included Text Variables section Apply.</li><li>The Decal's Activity Category Text  is an explicit UI filter.</li></ul>
&#10004; | &#10004; | Render a Temlpate & Display Decal once per Activity Text & Art Pairing | <ul><li>The rules Under the Included Text Variables section Apply.</li><li>The Decal's Activity Category Text  is an explicit UI filter.</li></ul>

### Text Length Variable Rules
Certain Decal Templates have designs that do not look good when the text variables are too long. As a result, part of the determination process involves excluding those Decals from the catalog if the text is too long. A decal may be {@link module:alice-catalog/models/decal.lengthLimits|annotated} with size restrictions denoting which lengths are allowed. At the time of writing, Text size is compared against the name of a school, and the name of a schools mascot. 

There are two determinations that text length inclusion rules apply to.

1. If string of a certain length can fit in the alloted space on the decal template
2. which text layer in the template to show

If a bit of text is determined too big for the alloted space on the decal template, processing of that decal stops, and the decal is **excluded** entirely from the catalog.

### Text Variable Inclusion

Each Decal is aware of its own text variable inclusion options. This information is held in the {@link module:alice-catalog/models/decal.personalization|personalization} property. While the internal representation of the addition keys are in some cases cryptic. They are stored in the catalog {@link module:alice-catalog/lib/constants|constants} module to reduce confusion and minimize human error when programming against these values

Constant | Value | Meaning 
---------|-------|--------
ART_MASCOT | arttm | Addition key for the artwork of the schools mascot
ART_ACTIVITY | artad | Addition key for the artwork for an activity
TEXT_TENANT_NAME | txtnm | The name of the school
TEXT_TENANT_ACRONYM | txtacro | The short code of the school ( MHS )
TEXT_TENANT_TYPE | txttyp | The internal type of the school
TEXT_LOGO_NAME | txttl | The name of the schools mascot ( Warriors, Bears, Falcons, etc )
TEXT_TENANT_CITY | txtcty | The resident city of the school
TEXT_TENANT_STATE | txtst | The resident state of the school
TEXT_TENANT_COUNTRY | txtctry | The resident country of the school
TEXT_ACTIVITY_NAME | txtanm | The textual name of an activity
TEXT_DATE_ONE | txtdate1 | A text place holder for the current Year
TEXT_DATE_TWO | txtdate2 | A text place holder for the current Year

#### Date options

In most situations all text inclusion rules can be represented as simple boolean values. The two exceptions are the two date options which must display either a two ( `2` ) or four ( `4` ) digit year value - **%y** and **%Y** for `2` and `4` digits respectively. These fields actually hold a {@link module:alice-stdlib/date.format|date format} string so as the value of these fields can be changed with out code changes.

##### Decal Inclusion Matrix

   | Tenant Name | Tenant Acronym | Tenant Mascot | Tenant Type | Tenant City | Tenant State | Tenant Country | Activity | Date1 | Date2
--:|:-----------:|:--------------:|:-----------:|:-----------:|:-----------:|:------------:|:--------------:|:--------:|:-------:|:----:
ss0001  | &#10004; | &#10006; | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10105; |   
ss0002  | &#10004; | &#10006; | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10103; |   
ss0003  | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; |   
ss0004  | &#10004; | &#10006; | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; |   
ss0005  | &#10004; | &#10006; | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10004; | &#10006; | &#10006; |   
ss0006  | &#10004; | &#10006; | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10004; | &#10006; | &#10006; |   
ss_0007 | &#10004; | &#10006; | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10103; | &#10006; |   
ss_0008 | &#10004; | &#10006; | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10103; | &#10006; |   
ss_0009 | &#10004; | &#10006; | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10103; | &#10006; |   
ss_0010 | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10004; | &#10105; | &#10006; |   
ss_0011 | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10004; | &#10105; | &#10105; |   
ss_0012 | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10103; | &#10006; |   
ss_0013 | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10004; | &#10006; | &#10006; |   
ss_0014 | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10004; | &#10006; | &#10006; |   
ss_0016 | &#10006; | &#10006; | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; |   
ss_0017 | &#10006; | &#10006; | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; |   
ss_0018 | &#10004; | &#10006; | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; |   
ss_0019 | &#10006; | &#10006; | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; |   
ss_0020 | &#10004; | &#10006; | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; |   
ss_0021 | &#10006; | &#10006; | &#10004; | &#10006; | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; |   
ss_0023 | &#10004; | &#10006; | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10004; | &#10105; | &#10105; |   
ss_0024 | &#10004; | &#10006; | &#10006; | &#10004; | &#10006; | &#10006; | &#10006; | &#10004; | &#10103; | &#10006; |   
ss_0025 | &#10004; | &#10006; | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; |   
ss_0026 | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10103; | &#10103; |   
ss_0028 | &#10006; | &#10006; | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10105; | &#10006; |   
ss_0029 | &#10004; | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10004; | &#10006; | &#10006; |   
ss_0030 | &#10004; | &#10006; | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10105; | &#10006; |   
ss_0031 | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10103; | &#10006; |   
ss_0032 | &#10004; | &#10006; | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10004; | &#10006; | &#10006; |   
ss_0033 | &#10004; | &#10006; | &#10006; | &#10004; | &#10004; | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; |   
ss_0034 | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10004; | &#10105; | &#10006; |   
ss_0035 | &#10004; | &#10006; | &#10006; | &#10004; | &#10006; | &#10006; | &#10006; | &#10004; | &#10006; | &#10006; |   
   | *Tenant Name* | *Tenant Acronym* | *Tenant Mascot* | *Tenant Type* | *Tenant City* | *Tenant State* | *Tenant Country* | *Activity* | *Date1* | *Date2*
ss_0036 | &#10004; | &#10006; | &#10006; | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10103; | &#10103; |   
ss_0037 | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10004; | &#10105; | &#10006; |   
ss_0038 | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10004; | &#10105; | &#10006; |   
ss_0039 | &#10004; | &#10006; | &#10006; | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; |   
ss_0040 | &#10004; | &#10006; | &#10004; | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; |   
ss_0041 | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10004; | &#10105; | &#10006; |   
ss_0042 | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10004; | &#10105; | &#10006; |   
ss_0043 | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10105; | &#10006; |   
ss_0044 | &#10004; | &#10006; | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; |   
ss_0045 | &#10006; | &#10006; | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; |   
ss_0046 | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; |   
ss_0047 | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10004; | &#10105; | &#10006; |   
ss_0048 | &#10004; | &#10006; | &#10006; | &#10004; | &#10006; | &#10006; | &#10006; | &#10004; | &#10006; | &#10006; |   
ss_0049 | &#10004; | &#10006; | &#10006; | &#10006; | &#10004; | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; |   
ss_0051 | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10105; | &#10006; |   
ss_0052 | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; |   
ss_0053 | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10105; | &#10006; |   
ss_0054 | &#10004; | &#10006; | &#10004; | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; |   
ss_0055 | &#10004; | &#10006; | &#10004; | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; |   
ss_0056 | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10105; | &#10006; |   
ss_0057 | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; |   
ss_0059 | &#10004; | &#10006; | &#10006; | &#10004; | &#10006; | &#10006; | &#10006; | &#10004; | &#10006; | &#10006; |   
ss_0060 | &#10004; | &#10006; | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10004; | &#10105; | &#10006; |   
ss_0061 | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10004; | &#10006; | &#10006; |   
ss_0062 | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10004; | &#10006; | &#10006; |   
ss_0063 | &#10004; | &#10006; | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10105; | &#10006; |   
ss_0064 | &#10004; | &#10006; | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; |   
ss_0065 | &#10004; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10103; | &#10006; |   
ss_back_0001 | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; |   
ss_back_0002 | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; |   
ss_back_0003 | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; | &#10006; |   

### Inclusion Logic

The logic for decal inclusion is split between two primary modules. The {@link module:alice-catalog/lib/interpolate|interpolate} and the {@link module:alice-catalog/lib/item|Catalog Item} module. Interpolate is responsible for *generating a **catalog item** for each of the permutations of a decal image template for a particular school*. The Catalog Item is responsible for generating valid URLs for **S7** and combine these urls with product images appropriately.


#### Image Layering

The spirit shop platform really only offers a handful of products, but allows users to purchase many permutations of a particular prodct & decal combination. We accomplish this by laying a series of images on top of each other to display each of the permutation

* A generated Decal Image render from a S7 template
```sh
------- Decal Image ---------
```

* A generated [Decal](https://marketing.adobe.com/resources/help/en_US/s7/is_ir_api/ir_api/http_protocol/r_ir_material_attributes.html) Image render from a S7 template on top of a an image of a cloth/material swatch of a specific color ( default to a grey peice of material ) 
```sh
------- Decal Image ---------
------- Swatch Image --------
```

* A generated Decal Image render from a S7 template on top of a full production image of varing colors
```sh
------- Decal Image ---------
------- Product Image -------
```
