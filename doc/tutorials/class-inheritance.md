
While the example is simple, however, we will probably want to be able to configure more things for our class instance. For example, the number of wheels it has, the color of the car, if it is convertible or not. As you might expect, managing all of the possible configurations and supplying some amount of sane defaults will be come pretty messy. This is where mixins come into play. A `Class` has a special Keyword `Implements` which takes either a single Class, or an array of as many `Classes` as you want and mixes them into your Class.


**Alice** ships with a number of mixins that meet common OOP patterns in javascript. They include native `Events` module in node, {@link module:alice-stdlib/class/options|Options}, {@link module:class.Storage|Storage} {@link module:class.Chain|Chain}. When you require the *class* module, all of the related class mixins are included as properties on the *Class* object convenience For our situation, we will take advantage of the *Options* mixin class. Options provides simple configuration handling by storing configuration in an object called `options` on the class instance. You can predefine any default you wish in that object as well as update and retrieve values from it during native `theinstance` module in node

{@link module:alice-stdlib/class/options|Options} has a single method on it, {@link module:class.Options#setOptions|setOptions} that does the work of setting and overriding values stored in the *options* object. It will also create that *options* object if it doesn't not exist already. So we can change our constructor function to take a single object that represents the configuration for our car and call *setOptions*.

#### Example - Using Mixins: Options
```js
// car.js
var Class = require('alice-stdlibe/class')
var Options = require('alice-stdlibe/class/options')

var Car = new Class({
     mixin:[ Options ]
     ,options:{
        color:"red"
        ,driver:null
        ,unit:"mi"
        ,windowtint:false
        ,convertable:false
        ,spoiler:false
        ,wheels:4
        ,efficiency: 0.985
        ,gas:{
            type:"regular"
            ,gallons:20
        }
     }

     ,constructor: function( options ){
        // reset the distance to 0
        this.distance = 0

        // set the driver
        this.setOptions( options );

        if( !this.options.driver ){
            var err = new Error()
            err.name = "ImproperlyConfigured"
            err.message = "You must specify the name of your driver"

            throw err;
        }
    }

    , drive: function( ){
        this.distance++;
        return this.distance
    }

    , reverse: function( ){
        this.distance--;
        return this.distance;
    }

    , honk: function( ){
        alert( "beep" )
    }

    , getDriver: function(){
        // getter for the driver
        return this.options.driver;
    }

});

module.exports = Car;
```

So in the constructor function of our car class we've off loaded all of the work or managing configuration and default, etc to native `thethe` module in node{@link module:alice-stdlib/class/options|Options} mixin. We've also added some extra options for expanding on our car class later - like window tinting, spoiler support. The essentials. We make use of that by passing in configuration of the class when it is instantiated.

```js
var eclipse = new Car({
    color:"red"
    ,driver:Armondo
    ,unit:"mi"
    ,windowtint:true
    ,convertable:true
    ,spoiler:true
    ,wheels:4
    ,efficiency: 0.870
});

eclipse.drive() // 1;
```


Another common pattern in javascript is the ability of notifying other pieces of code that something of interest is about to, or has just happened. *Alice's* Class system support the notion of {@link module:class.Events|Events}. Events allow would allow users to listen to instances of our car class. For eample, it might be helpful to know when the car was started, turned off, or getting low on fuel. To add even handling behavior, we add the {@link module:class.Events|Events} mixin to our car class

#### Example - Using Mixins: Events

```js
 // car.js
 var events = require('events')
 var Class = require('alice-stdlib/class')
 var Options = require('alice-stdlib/class/options')
 var Car = new Class({
      mixin:[ Options, events.EventEmitter ]
      ,options:{
         color:"red"
         ,driver:null
         ,unit:"mi"
         ,windowtint:false
         ,convertable:false
         ,spoiler:false
         ,wheels:4
         ,fuel{
              type:"regular"
             ,efficiency: 0.985
             ,gallons:20
         }
      }

      constructor: function( options ){
         // reset the distance to 0
         this.distance = 0
         this.state = "off"

         this.setOptions( options );

         if( !this.options.driver ){
             var err = new Error()
             err.name = "ImproperlyConfigured"
             err.message = "You must specify the name of your driver"

             throw err;
         }
     }

     , drive: function( ){
         this.distance++;
         this.state = "driving"
         return this.distance;
     }
     , brake: function(){
        this.state = "idle"
     }
     , reverse: function( ){
         this.distance--;
         return this.distance;
     }

     , honk: function( ){
         alert( "beep" )
     }

     , getDriver: function(){
         // getter for the driver
         return this.options.driver;
     }

    , get: function( key ){
        return this.options[key]
    }

    , set: function( key, value ){
        var old_value
            ,event

        old_value = this.options[ key ];
        if( old_value !== value ){
            this.options[key] = value
            event = key + "change";
            this.fireEvent( event, value, old_value );
        }
    }
    , start: function(){
        this.$fuel_timer = setInterval( this.checkFuel, 300 );
        this.state = "idle";
        this.fireEvent('start', [ this.state, this.options.gallons, this ] );
    }

    , stop: function(){
        clearInterval( this.$fuel_timer );
        this.state = "off";
        this.fireEvent('stop', [ this.state, this.options.gallons, this ] );
    }

    , consumeFuel: function(){
        var gallons
            ,efficiency
            ,modifier
            ,wheels
            ,gas_to_lose;

        gallons    = this.options.fuel.gallons;
        efficiency = this.options.fuel.efficiency
        wheels     = this.options.wheels

        switch( this.state ){
            case "idle":
                modifier = 0.5;
                break;
            case "driving":
                modifier = 1;
                break;
            default:
                modifier = 0;
        }

        gas_to_lose = ( efficiency * modifier  ) * ( wheels / 2 )
        gallons = gallons - gas_to_lose

        if( gallons > 0 ){
            gallons = 0;
        }

        this.setOptions({
            gas:{
                gallons: gas_to_lose
            }
        });
    }

    , checkFuel: function(){
        var fuel_event
            ,gallons;

        fuel_event = null;
        gallons = this.options.fuel.gallons;
        if( gallons == 0 ){
            fuel_event = "empty"
        } else if( gallons <= 2 ){
            fuel_event = "lowfuel"
        }

        this.fireEvent(fuel_event, [ gallons, this ])
    }
 });

module.exports =  Car;
 ```

Our little Class has grown up! We've added a few things here. namely a **start**, **stop**, **brake**, **checkFuel**, and **consumeFuel**  method. When the car starts it will slowly start consuming fuel while it is idle. While it is driving it will consume a larger amount of fuel. When the car is low on gas it will start firing events to let the anyone knowing it is almost out of gas. We can start listening for events with the `on` method we inherited from the `EventEmitter` class.

```js
var Car = require('./car')
var eclipse = new Car({
    color:"red"
    ,driver:Armondo
    ,unit:"mi"
    ,windowtint:true
    ,convertable:true
    ,spoiler:true
    ,wheels:4
    ,fuel:{
      efficiency: 0.870
    }
});

eclipse.on("start", function( state, gallons, car_instance ){
  alert("we have " + gallons + " of gas")
});

eclipse.on('lowfuel', function( gallons, car_instance ){
    alert "Oh noes! only " + gallons + " of gas left!"
    car_instance.brake();
});

eclipse.drive();
```

#### Tip: Events & Options
The native `Events` module in node and {@link module:alice-stdlib/class/options|Options} Mixins are designed to work in tandem in that you can specify event handlers as options. An option key that starts with the word **on** followed by an uppercase letter will be considered to be a event handler for an event of the lowercased name of the key **sans** the on

```js
  var eclipse = new Car({
      color:"red"
      ,driver:Armondo
      ,onLowFuel: function( gallons, car_instance ){
        alert "Oh noes! only " + gallons + " of gas left!"
        car_instance.brake();
      }
  });

  eclipse.drive();
```

### Class Inheritance
So far we have looks at a psuedo-inheritance model with **Mixins**. However, there is much to be desired with the mixin model. Mainly that there is not way to subclass. Luckily the **Alice** Class system supports another special keyword in **inherits** which takes a single class definition to use as it's super class. 

##### Parent Methods
You can use the {@link module:alice-stdlib/class/parent|Parent} mixin to allow you to override method and have the ability to call the superclass method of the same name with a special function, {@link module:alice-stdlib/class/parent#parent|parent}. Parent calls the method of the same name it was called from accepting the same arguments as the original function.

#### Example - Class Inheritance: inherits
```js
// angrycar.js
var Class = require('alice-stdlib/class')
var Parent = require('alice-stdlib/class/parent')
var Car = require('./car')
var AngryCar = new Class({
    inherits: Car
    , mixin:Parent
    , constructor: function( options ){
      this.parent('constructor', options );
    }

    , honk: function( ){
      alert("MOVE!");
    }

    , drive: function( ){
        this.distance += 2;
        this.state = "driving";
        return this.distance;
    }
});

module.exports = AngryCar;
```

Notice here that we don't have to declare all of the methods over again, only the ones that we to add functionality to or prevent certain behaviors from happening. Also, calling the parent function is completely optional. additionally, we don't need to redefine our options, or mixin Events and Options. Its already done! Now we can use our Angry Car just like a regular car

```js
Alice([ "angrycar" ], function( Angrycar ){
  var eclipse = new AngrycarCar({
      color:"red"
      ,driver:Armondo
      ,unit:"mi"
      ,windowtint:true
      ,convertable:true
      ,spoiler:true
      ,wheels:4
      ,fuel:{
        efficiency: 0.870
      }
  });

  eclipse.on("start", function( state, gallons, car_instance ){
    alert("we have " + gallons + " of gas")
  });

  eclipse.on('lowfuel', function( gallons, car_instance ){
      alert "Oh noes! only " + gallons + " of gas left!"
      car_instance.brake();
  });

  eclipse.drive();
  eclipse.brake();
  eclipse.honk();
  eclipse.drive();
  eclipse.brake();
  eclipse.honk();
  eclipse.drive();

});
```
