FROM spiritshop/node

VOLUME ["/etc/alice"]

ADD . /opt/alice/
WORKDIR /opt/alice

ENV NODE_PATH $NODE_PATH:/opt/alice/packages

# make sure to install it really hard
RUN rm -rf node_modules
RUN npm install && node ./scripts/install && npm link

EXPOSE 3001
CMD alice web -l stdout --conf=/etc/alice
