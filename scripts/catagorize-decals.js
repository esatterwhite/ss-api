#!/usr/bin/env node
var Decal = require('alice-catalog/models/decal')
  , Activity = require('alice-catalog/models/activity')
  , unique   = require('alice-stdlib/array').unique
  , constants = require('alice-catalog/lib/constants')
  , activityArt  // The s7id of the art file associated for the given activity
  , activityName // The category name of the specified activity
  ;

var mapper = function( a ){
	return a.category
}
Activity
	.find()
	.select('_id category')
	.lean()
	.exec(function( err, activities){
		var all = unique( activities.map( mapper ) );
		var stream = Decal.find().populate('activities').stream();
		stream.on('data', function( decal ){
			var that = this;
			var acts =  decal.activities.length ? unique(decal.activities.map( mapper ) ): all;
			that.pause()
			decal.categories = acts
			decal.save(function(){
				that.resume();
			})
		})
	});
