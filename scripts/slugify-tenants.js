/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * one off script to slugify the name of the school and index it
 * @module scripts/slugify-tenants
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires models/tenant
 */

var Tenant = require('alice-catalog/models/tenant')
  , stream = Tenant.find().stream();
  ;


stream.on('data',function( tenant ){
	var that = this;
	that.pause();
	tenant.validate(function(serror){
		if(serror){
			console.log('%s, did not pass validation', tenant.longName)
			console.log(serror.message )
			return that.resume();
		}
		tenant.save(function(err, t, count){
			if( t ){
				console.log('%s slugified', t.longName )
			}
			if(err){
				console.log('error on %s: %s', tenant.longName, err.message)
			}
			that.resume();
		});
	})
})
stream.on('error',function(e){
	console.log('error: %s', e.message)
	process.exit( 1 )
})
stream.on('close',function(){
	console.log('all records updated')
	process.exit(0)
})
